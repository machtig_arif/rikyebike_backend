<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_controller extends CI_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		  
		 $this->load->model('Admin_modal');
		 $this->load->helper('captcha');
	}

	public function index()
	{
     $user_id=$this->session->userdata('user_id');
      $restaurant_data=$this->Admin_modal->select_restaurant_id($user_id);      
      $restaurant_id=$restaurant_data[0]['restaurant_id'];
        $data['restaurant_details']=$this->Admin_modal->restaurant_details($restaurant_id);
        $data['package']=$this->Admin_modal->restaurant_package($restaurant_id);
		$data['coupon_count']=$this->Admin_modal->coupon_record_counts($where='',$restaurant_id);
		$data['event_count']=$this->Admin_modal->event_record_count($where='',$restaurant_id);
		$this->load->view('user/index',$data);
	}


public function registration(){
  $submit=$this->input->post('submit');     
      if (isset($submit)){
             $captcha=$_POST['g-recaptcha-response'];
    if($captcha){ 
    $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LdVnKoUAAAAAK_1pr4eOv1qCwOTIWSCwFsTQvzV&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
    if($response['success'] == true)
    {       
      		$user_email=$this->Admin_modal->email_validation($this->input->post('email'));
      		if($user_email>0)
      			{
      				 $this->session->set_flashdata('msg','This email is already registered.'); 
      			}else{
      				//echo $this->input->post('email');die;
      		function getRandomString($length = 45) {
			    $characters = '0123456789abcdef';
			    $string = '';
			    for ($i = 0; $i < $length; $i++) {
			        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
			    }
			    return $string;
			}
			$session_id=getRandomString();
	        $date=date('Y-m-d');
	        // $bar_type= $this->input->post('bar_type');
	        // $music_type= $this->input->post('music_type');
	        $Eatery_type= $this->input->post('Eatery_type');
	        $amenities= $this->input->post('amenities');
         	$food_type= $this->input->post('food_type');
          	$average_crowd= $this->input->post('average_crowd');
            $file_count=count($_FILES['files']['name']);

        $use_data = array('first_name' => $this->input->post('fname'),'last_name' => $this->input->post('lname'),'user_email' => $this->input->post('email'),'user_password' => md5($this->input->post('password')),'user_role' => 'Restaurant','user_reg_date' =>$date ,'remember_token' => $session_id,'status'=>'1', );
        if($this->Admin_modal->add_user($use_data))
        {
        	 $restaurant_id = $this->db->insert_id();
        }

        $add_restaurant = array('title' => $this->input->post('business_name'),'street' => $this->input->post('street'),'city' => $this->input->post('city'),'state' => $this->input->post('state'),'zipcode' => $this->input->post('zipcode'),'status' =>'0', 'website' => $this->input->post('website'),'phone' => $this->input->post('phone'),'create_date' =>$date,'user_id'=>$restaurant_id,'subscription_type'=>$this->input->post('subscription_type'),'open_time' => $this->input->post('otime'),'close_time' => $this->input->post('ctime'),'lat' => $this->input->post('latitude'),'log' => $this->input->post('longitude'),'restaurant_id'=>$restaurant_id, );
      

         $this->Admin_modal->add_restaurant($add_restaurant);
        
    	 
       // for($i=0;$i<count($bar_type);$i++)
       // {
      
       //  $this->Admin_modal->add_restaurant_bar($restaurant_id,$bar_type[$i]);

       //  }

       //  for($i=0;$i<count($music_type);$i++)
       // {
      
       //  $this->Admin_modal->add_restaurant_music($restaurant_id,$music_type[$i]);

       //  }

        for($i=0;$i<count($Eatery_type);$i++)
       {
      
        $this->Admin_modal->add_restaurant_music_genre($restaurant_id,$Eatery_type[$i]);

        }

        for($i=0;$i<count($amenities);$i++)
       {
      
        $this->Admin_modal->add_restaurant_amenities($restaurant_id,$amenities[$i]);

        }

        for($i=0;$i<count($food_type);$i++)
       {
      
        $this->Admin_modal->add_restaurant_food($restaurant_id,$food_type[$i]);

        }
        for($i=0;$i<count($average_crowd);$i++)
       {
      
        $this->Admin_modal->add_restaurant_crowd($restaurant_id,$average_crowd[$i]);

        }

       for($i=0;$i<$file_count;$i++)
       {
         
        $file_name=$_FILES['files']['name'][$i];
        $file_tmp_name=$_FILES['files']['tmp_name'][$i];
        if(move_uploaded_file($file_tmp_name, 'assets/images/'.$file_name))
        {
          $this->Admin_modal->add_restaurant_image($restaurant_id,$file_name);
          
        }
        
       }
        $this->session->set_flashdata('corrct','Restaurant successfully added please logIn'); 
       echo "<script>window.location.href='".base_url('')."';</script>";
     
       }

      }else{
      	 $this->session->set_flashdata('msg','Not a valid captcha'); 
      }
    }else{
    
      $this->session->set_flashdata('msg','Please check the the captcha form.');   
    }
    }
  
      
      
          

     //        	$values = array(
					// 	'word' => '',
					// 	'word_length' => 8,
					// 	'img_path' => './images/',
					// 	'img_url' => base_url() .'images/',
					// 	'font_path' => base_url() . 'system/fonts/texb.ttf',
					// 	'img_width' => '150',
					// 	'img_height' => 50,
					// 	'expiration' => 3600
					// 	);
					// 	$data = create_captcha($values);
				 // $_SESSION['captchaWord'] = $data['word'];

				  	$data['bar']=$this->Admin_modal->select_bar();
      				$data['state']=$this->Admin_modal->select_state();
       				$data['music']=$this->Admin_modal->select_music();
        			$data['music_genre']=$this->Admin_modal->select_music_genre();
        			$data['amenities']=$this->Admin_modal->select_amenities();
         			$data['food']=$this->Admin_modal->select_food();
         			$data['crowd']=$this->Admin_modal->select_crowd();

		$this->load->view('user/registration',$data);
	}

	

public function captcha_refresh(){
$values = array(
'word' => '',
'word_length' => 8,
'img_path' => './images/',
'img_url' => base_url() .'images/',
'font_path' => base_url() . 'system/fonts/texb.ttf',
'img_width' => '150',
'img_height' => 50,
'expiration' => 3600
);
$data = create_captcha($values);
$_SESSION['captchaWord'] = $data['word'];
echo $data['image'];

}


 public function total_events($offset=0)
    {


     $event_id=$this->input->post('event_id');     
      $submit=$this->input->post('submit');
      $user_id=$this->session->userdata('user_id');
      $restaurant_data=$this->Admin_modal->select_restaurant_id($user_id);      
      $restaurant_id=$restaurant_data[0]['restaurant_id'];
      if (empty($event_id)) {

      if (isset($submit)) {

              $tmp_name=$_FILES['event_imsge']['tmp_name'];
              $file_name=$_FILES['event_imsge']['name'];
              if(move_uploaded_file($tmp_name, 'assets/images/'.$file_name))
             {
      	$event_date=date("Y-m-d", strtotime($this->input->post('event_date')));
      $add_event = array('event_name' =>$this->input->post('event_name') ,'event_date' =>$event_date ,'event_time' =>$this->input->post('event_time') ,'cover_charge' =>$this->input->post('cover_charge') , 'average_crowd' =>$this->input->post('average_crowd') ,'music_genre' =>$this->input->post('music_genre') ,'create_date' =>date('Y-m-d'),'event_close_time' =>$this->input->post('event_e_time') ,'status'=>'1', 'user_id'=>$restaurant_id,'event_imsge'=>$file_name,'event_terms' =>$this->input->post('terms') ,'event_description' =>$this->input->post('description') , 'lat' => $this->input->post('latitude'),'log' => $this->input->post('longitude'),'address' => $this->input->post('address'),'venue' => $this->input->post('venue'),'phone' => $this->input->post('phone'),);
      if($this->Admin_modal->add_events($add_event))
      {
       
      	$this->session->set_flashdata('msgs','Event added successfully'); 
        echo "<script>window.location.href='".base_url('total_events')."';</script>";
      //	redirect(base_url('total_events'));
      }
    }
    	
      }

    	
    }else{

        $tmp_name=$_FILES['event_imsge']['tmp_name'];
              $file_name=$_FILES['event_imsge']['name'];
             
           if(empty($file_name)){            
    		$event_date=date("Y-m-d", strtotime($this->input->post('event_date')));
      		$update_event = array('event_name' =>$this->input->post('event_name') ,'event_date' =>$event_date ,'event_time' =>$this->input->post('event_time') ,'cover_charge' =>$this->input->post('cover_charge') , 'average_crowd' =>$this->input->post('average_crowd') ,'music_genre' =>$this->input->post('music_genre') ,'event_close_time' =>$this->input->post('event_e_time') ,'status' => $this->input->post('status'),'event_terms' =>$this->input->post('terms') ,'event_description' =>$this->input->post('description') ,'lat' => $this->input->post('latitude'),'log' => $this->input->post('longitude'),'address' => $this->input->post('address'),'venue' => $this->input->post('venue'),'phone' => $this->input->post('phone'),);
           }else{

              $event_date=date("Y-m-d", strtotime($this->input->post('event_date')));
          $update_event = array('event_name' =>$this->input->post('event_name') ,'event_date' =>$event_date ,'event_time' =>$this->input->post('event_time') ,'cover_charge' =>$this->input->post('cover_charge') , 'average_crowd' =>$this->input->post('average_crowd') ,'music_genre' =>$this->input->post('music_genre') ,'event_close_time' =>$this->input->post('event_e_time') ,'status' => $this->input->post('status'),'event_imsge'=>$file_name,'event_terms' =>$this->input->post('terms') ,'event_description' =>$this->input->post('description') ,'lat' => $this->input->post('latitude'),'log' => $this->input->post('longitude'),'address' => $this->input->post('address'),'venue' => $this->input->post('venue'),'phone' => $this->input->post('phone'),);
           move_uploaded_file($tmp_name, 'assets/images/'.$file_name);

           }

      		
        if($this->Admin_modal->update_event($update_event,$event_id))
      {
      	$this->session->set_flashdata('msgs','Event updated successfully'); 
        echo "<script>window.location.href='".base_url('total_events')."';</script>";
      	//redirect(base_url('total_events'));
      }

    }
   

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->event_record_count($Searchkey);
   
  $config['base_url'] = base_url()."total_events";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
  

  $query = $this->Admin_modal->select_event($Searchkey,$config['per_page'],$this->uri->segment(2),$restaurant_id);
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
 
   $data['result'] =  $query;
  
   $data['average_crowd']=$this->Admin_modal->average_crowd();
   $data['music_genre']=$this->Admin_modal->music_genre();

      $this->load->view('user/total_events',$data);

    }

    

     public function event_delete()
    {
      $event_id=$this->input->post('event_id');
      if($this->Admin_modal->event_delete($event_id))
      {
        echo 'success';
        $this->session->set_flashdata('add_state','Event deleted successfully'); 
      }
    }

     public function deal_delete()
    {
      $deal_id=$this->input->post('deal_id');
      if($this->Admin_modal->deal_delete($deal_id))
      {
        echo 'success';
        $this->session->set_flashdata('msg','Deal deleted successfully'); 
      }
    }


    public function coupon()
    {
    	$user_id=$this->session->userdata('user_id');
     
      $restaurant_data=$this->Admin_modal->select_restaurant_id($user_id);      
      $restaurant_id1=$restaurant_data[0]['restaurant_id'];
      $coupon_id=$this->input->post('coupon_id');   
      $submit=$this->input->post('submit');


     if (empty($coupon_id)){
       
        $date=date("Y-m-d");
      if (isset($submit)) {
         $tmp_name=$_FILES['coupon_img']['tmp_name'];
              $file_name=$_FILES['coupon_img']['name'];    

        $data = array('coupon_title' =>$this->input->post('coupon_title') ,'coupon_description' =>$this->input->post('coupon_description') ,'status' =>$this->input->post('status'),'create_date'=>$date,'coupon_terms' =>$this->input->post('coupon_terms') ,'start_date' =>$this->input->post('coupon_start_date') ,'end_date' =>$this->input->post('coupon_end_date') ,'start_time' =>$this->input->post('coupon_start_time') ,'end_time' =>$this->input->post('coupon_end_time') ,'coupon_img' =>$file_name , 'created_by'=>$restaurant_id1, );
         move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
        if($this->Admin_modal->add_coupon($data))
        {
           $this->session->set_flashdata('msg','Coupon successfully added'); 
           echo "<script>window.location.href='".base_url('coupon')."';</script>";
          // redirect(base_url('coupon'));
        }
      }
    }else{
       $tmp_name=$_FILES['coupon_img']['tmp_name'];
              $file_name=$_FILES['coupon_img']['name'];    
              if (empty($file_name)) {               
     $data = array('coupon_title' =>$this->input->post('coupon_title') ,'coupon_description' =>$this->input->post('coupon_description') ,'status' =>$this->input->post('status'),'coupon_terms' =>$this->input->post('coupon_terms') ,'create_date'=>$date ,'start_date' =>$this->input->post('coupon_start_date') ,'end_date' =>$this->input->post('coupon_end_date') ,'start_time' =>$this->input->post('coupon_start_time') ,'end_time' =>$this->input->post('coupon_end_time') ,);
              }else{
                 $data = array('coupon_title' =>$this->input->post('coupon_title') ,'coupon_description' =>$this->input->post('coupon_description') ,'status' =>$this->input->post('status'),'coupon_terms' =>$this->input->post('coupon_terms') ,'create_date'=>$date,'start_date' =>$this->input->post('coupon_start_date') ,'end_date' =>$this->input->post('coupon_end_date') ,'start_time' =>$this->input->post('coupon_start_time') ,'end_time' =>$this->input->post('coupon_end_time') ,'coupon_img' =>$file_name ,);
                  move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
              }
        if($this->Admin_modal->update_coupon($data,$coupon_id))
        {
           $this->session->set_flashdata('msg','Coupon successfully updated'); 
           echo "<script>window.location.href='".base_url('coupon')."';</script>";
          // redirect(base_url('coupon'));
        }
    }
   

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->coupon_record_counts($Searchkey,$user_id);
   
  $config['base_url'] = base_url()."coupon";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_coupons($Searchkey,$config['per_page'],$this->uri->segment(2),$restaurant_id1);
  //echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
 
   $data['result'] =  $query;

    	$this->load->view('user/coupon',$data);
    }


    public function photo_gallery()
    {
    	 $user_id=$this->session->userdata('user_id');
    	 $restaurant_data=$this->Admin_modal->restaurant_data($user_id);
       $restaurant_id =$restaurant_data[0]['restaurant_id'];
    	 $submit=$this->input->post('submit');
    	 if (isset($submit)) {
    	 	
    	 $file_count=count($_FILES['files']['name']);
    	 
    	for($i=0;$i<$file_count;$i++)
       {
         
        $file_name=$_FILES['files']['name'][$i];
        $file_tmp_name=$_FILES['files']['tmp_name'][$i];
        if(move_uploaded_file($file_tmp_name, 'assets/images/'.$file_name))
        {
          $this->Admin_modal->add_restaurant_image($restaurant_id,$file_name);
          
        }
        
       }
             $this->session->set_flashdata('msg','Image uploaded successfully');
             echo "<script>window.location.href='".base_url('photo_gallery')."';</script>";
       //redirect(base_url('photo_gallery'));
   }
       $data['result']=$this->Admin_modal->select_restaurant_images($restaurant_id);

    	$this->load->view('user/photo_gallery',$data);
    }



    public function profile()
    {
    	 $submit=$this->input->post('submit');
    	 $user_id=$this->session->userdata('user_id');
    	 $restaurant_data=$this->Admin_modal->restaurant_data($user_id);
       $restaurant_id =$restaurant_data[0]['restaurant_id'];
      if (isset($submit)) {
      		
	      
	        // $bar_type= $this->input->post('bar_type');
	        // $music_type= $this->input->post('music_type');
	        $music_genre= $this->input->post('music_genre');
	        $amenities= $this->input->post('amenities');
         	$food_type= $this->input->post('food_type');
          	$average_crowd= $this->input->post('average_crowd');
           

           $file_name=$_FILES['files']['name'];
        if(!empty($file_name))
        {
        	$file_tmp_name=$_FILES['files']['tmp_name'];
        if(move_uploaded_file($file_tmp_name, 'assets/images/'.$file_name))
        {
           $use_data = array('first_name' => $this->input->post('fname'),'last_name' => $this->input->post('lname'),'user_email' => $this->input->post('email') ,'user_avatar'=>$file_name, );
          
        }
        }else{

        $use_data = array('first_name' => $this->input->post('fname'),'last_name' => $this->input->post('lname'),'user_email' => $this->input->post('email') , );
    }
        $this->Admin_modal->update_user($use_data,$user_id);
      

        $add_restaurant = array('title' => $this->input->post('business_name'),'street' => $this->input->post('street'),'city' => $this->input->post('city'),'state' => $this->input->post('state'),'zipcode' => $this->input->post('zipcode'), 'website' => $this->input->post('website'),'phone' => $this->input->post('phone'),'open_time' => $this->input->post('otime'),'close_time' => $this->input->post('ctime'),'lat' => $this->input->post('latitude'),'log' => $this->input->post('longitude'),);
        if( $this->Admin_modal->update_restaurant($restaurant_id,$add_restaurant)){     

      // $this->Admin_modal->delete_restaurant_bar($restaurant_id);
      // $this->Admin_modal->delete_restaurant_music($restaurant_id);
      $this->Admin_modal->delete_restaurant_music_genre($restaurant_id);
      $this->Admin_modal->delete_restaurant_amenities($restaurant_id);
      $this->Admin_modal->delete_restaurant_food($restaurant_id);
      $this->Admin_modal->delete_restaurant_crowd($restaurant_id);
    	 }
       // for($i=0;$i<count($bar_type);$i++)
       // {
      
       //  $this->Admin_modal->add_restaurant_bar($restaurant_id,$bar_type[$i]);

       //  }

       //  for($i=0;$i<count($music_type);$i++)
       // {
      
       //  $this->Admin_modal->add_restaurant_music($restaurant_id,$music_type[$i]);

       //  }

        for($i=0;$i<count($music_genre);$i++)
       {
      
        $this->Admin_modal->add_restaurant_music_genre($restaurant_id,$music_genre[$i]);

        }

        for($i=0;$i<count($amenities);$i++)
       {
      
        $this->Admin_modal->add_restaurant_amenities($restaurant_id,$amenities[$i]);

        }

        for($i=0;$i<count($food_type);$i++)
       {
      
        $this->Admin_modal->add_restaurant_food($restaurant_id,$food_type[$i]);

        }
        for($i=0;$i<count($average_crowd);$i++)
       {
      
        $this->Admin_modal->add_restaurant_crowd($restaurant_id,$average_crowd[$i]);

        }

       
      
    

      $this->session->set_flashdata('msg','Restaurant successfully updated'); 
     
  }
      
      
        
           $data['restaurant_info']=$this->Admin_modal->restaurant_all_info($restaurant_id);
          // $data['select_restaurant_bar']=$this->Admin_modal->select_restaurant_bar($restaurant_id);
           $data['select_restaurant_food']=$this->Admin_modal->select_restaurant_food($restaurant_id);
          // $data['select_restaurant_music']=$this->Admin_modal->select_restaurant_music($restaurant_id);
           $data['select_restaurant_music_genre']=$this->Admin_modal->select_restaurant_music_genre($restaurant_id);
           $data['select_restaurant_amenities']=$this->Admin_modal->select_restaurant_amenities($restaurant_id);
           $data['select_restaurant_crowd']=$this->Admin_modal->select_restaurant_crowd($restaurant_id);
           $data['select_restaurant_images']=$this->Admin_modal->select_restaurant_images($user_id);
            
            	
				  //	$data['bar']=$this->Admin_modal->select_bar();
      				$data['state']=$this->Admin_modal->select_state();
       			//	$data['music']=$this->Admin_modal->select_music();
        			$data['music_genre']=$this->Admin_modal->select_music_genre();
        			$data['amenities']=$this->Admin_modal->select_amenities();
         			$data['food']=$this->Admin_modal->select_food();
         			$data['crowd']=$this->Admin_modal->select_crowd();
    				$this->load->view('user/profile',$data);
   		 }


        public function change_password()
        {
   		 	$user_id=$this->session->userdata('user_id');

   		 	$submit=$this->input->post('submit');
            if (isset($submit)){
                $old_pass=md5($this->input->post('old_pass'));
      
                $new_pass=$this->input->post('new_pass');
                $con_pass=$this->input->post('con_pass');
                if(!empty($new_pass) && !empty($old_pass)){
                    $data=$this->Admin_modal->select_admin_password($user_id,$old_pass);
                    if (!empty($data)){
                        if($new_pass== $con_pass)
                        {
                            $new_pass1=md5($new_pass);
                            if($this->Admin_modal->change_password($new_pass1,$user_id))
                            {
                                $this->session->set_flashdata('msg','Password changed successfully');
                            }
                            else{
                                $this->session->set_flashdata('error','Some error occured, please try again');
                            }
                        }else{
                            $this->session->set_flashdata('error','New password and confirm password should be same');
                        }
                    }else
                    {
                        $this->session->set_flashdata('error','Old Password is Incorrect');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','Please fill all fields');
                }
            }

 			$data['result']=$this->Admin_modal->admin_profile($user_id);    
   		 	$this->load->view('user/change_password',$data);
   		 }


       function test()
       {
        
          $this->load->view('user/test');
       }


        public function user_feedback($offset=0)
    {
       
   

      $Searchkey=$this->input->get('status');

        $user_id=$this->session->userdata('user_id');
     
    $config['total_rows'] = $this->Admin_modal->user_feedback_count($Searchkey,$user_id);
    //print_r($config['total_rows']);
   //echo $this->db->last_query(); die;
  $config['base_url'] = base_url()."user_feedback";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_user_feedback($Searchkey,$config['per_page'],$this->uri->segment(2),$user_id);
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
 
   $data['result'] =  $query;
  //print_r($data['result']);
   
      $this->load->view('user/user_feedback',$data);

    }


     public function feedback_delete()
    {
      $id=$this->input->post('id');
      if($this->Admin_modal->feedback_deletes($id))
      {
        echo 'success';
        $this->session->set_flashdata('add_state','Event deleted successfully'); 
      }
    }

    public function registration_setting()
  {
    $user_id=$this->session->userdata('user_id');
    $banner_submit=$this->input->post('banner_submit');
    $t_c_submit=$this->input->post('t_c_submit');
    $about_submit=$this->input->post('about_submit');
    if (isset($banner_submit)) {

    $banner_id=$this->input->post('banner_id');
     $file_name=$_FILES['banner']['name'];
        $file_tmp_name=$_FILES['banner']['tmp_name'];
        if(move_uploaded_file($file_tmp_name, 'assets/images/'.$file_name))
        {
          $user_id=$this->session->userdata('user_id');
          $title='banner';
          $data2 = array(
           'data' => $file_name,
           'title' => $title,
           'user_id'=>$user_id
          );        

          $result=$this->Admin_modal->get_t_c($user_id,$title); 
         if ($result>0) {
          $this->Admin_modal->update_banner_r($user_id,$data2,$title);
           $this->session->set_flashdata('banner_update','Banner successfully updated');
          }else{

            if($this->Admin_modal->isert_data($data2))
          {
           $this->session->set_flashdata('t_c_update','Banner successfully added');
          } 

          }
        }
    }
    $data['result']=$this->Admin_modal->registration_setting($user_id);  


     $this->load->view('user/registration_setting',$data);
  }

  public function t_c_admin()
   {
       
          $user_id=$this->session->userdata('user_id');
          $title='t_c';
          $data2 = array(
           'data' => $this->input->post('data'),
           'title' => $title,
           'user_id'=>$user_id
          );        

          $result=$this->Admin_modal->get_t_c($user_id,$title); 
         if ($result>0) {
           
          if( $this->Admin_modal->update_t_c_r($user_id,$data2,$title))
          {
           $this->session->set_flashdata('t_c_update','Terms And Condition successfully updated');
          }       
         }else{
          if($this->Admin_modal->isert_data($data2))
          {
           $this->session->set_flashdata('t_c_update','Terms And Condition successfully added');
          } 

         }
        
    
   }

   public function about_admin()
   {
         $user_id=$this->session->userdata('user_id');
          $title='about';
          $data2 = array(
           'data' => $this->input->post('data'),
           'title' => $title,
           'user_id'=>$user_id
          );        

          $result=$this->Admin_modal->get_t_c($user_id,$title); 
          if ($result>0) {
         
          if( $this->Admin_modal->update_about_r($user_id,$data2,$title))
          {
           $this->session->set_flashdata('about_update','About us successfully updated');
          }       
        }else{
          if($this->Admin_modal->isert_data($data2))
          {
           $this->session->set_flashdata('t_c_update','About us successfully added');
          } 
        }
        
    
   }

    public function p_p_admin()
   {
         $user_id=$this->session->userdata('user_id');
          $title='p_p';
          $data2 = array(
           'data' => $this->input->post('data'),
           'title' => $title,
           'user_id'=>$user_id
          );        

          $result=$this->Admin_modal->get_t_c($user_id,$title); 
          if ($result>0) {
          if($this->Admin_modal->update_p_p_r($user_id,$data2,$title))
          {
           $this->session->set_flashdata('about_update','PRIVACY And POLICY successfully updated');
          } 
          }else{
            if($this->Admin_modal->isert_data($data2))
          {
           $this->session->set_flashdata('t_c_update','PRIVACY And POLICY successfully added');
          } 
          }      
        
    
   }


   public function deals()
    {
      $user_id=$this->session->userdata('user_id');
     
      $restaurant_data=$this->Admin_modal->select_restaurant_id($user_id);      
      $restaurant_id1=$restaurant_data[0]['restaurant_id'];
      $deal_id=$this->input->post('deal_id');   
      $submit=$this->input->post('submit');


     if (empty($deal_id)){
       
       
      if (isset($submit)) {
         $tmp_name=$_FILES['deal_img']['tmp_name'];
              $file_name=$_FILES['deal_img']['name'];    
$start_date=date("Y-m-d", strtotime($this->input->post('deal_start_date')));
        $end_date=date("Y-m-d", strtotime($this->input->post('deal_end_date')));
        $data = array('deal_name' =>$this->input->post('deal_name') ,'deal_title' =>$this->input->post('deal_title') ,'start_date' =>$start_date,'end_date' =>$end_date,'deal_start_time' => $this->input->post('deal_start_time'),'deal_end_time' => $this->input->post('deal_end_time'),'deal_description' =>$this->input->post('deal_description') ,'t_c' =>$this->input->post('deal_terms') ,'img'=>$file_name,'restaurant_id'=>$user_id);
         move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
        if($this->Admin_modal->add_deal($data))
        {
           $this->session->set_flashdata('msg','Deal successfully added'); 
           echo "<script>window.location.href='".base_url('deals')."';</script>";
          // redirect(base_url('coupon'));
        }
      }
    }else{
      $start_date=date("Y-m-d", strtotime($this->input->post('deal_start_date')));
        $end_date=date("Y-m-d", strtotime($this->input->post('deal_end_date')));
       $tmp_name=$_FILES['deal_img']['tmp_name'];
              $file_name=$_FILES['deal_img']['name'];   
              if (empty($file_name)) {               
     $data = array('deal_name' =>$this->input->post('deal_name') ,'deal_title' =>$this->input->post('deal_title') ,'start_date' =>$start_date,'end_date' =>$end_date,'deal_start_time' => $this->input->post('deal_start_time'),'deal_end_time' => $this->input->post('deal_end_time'),'deal_description' =>$this->input->post('deal_description') ,'t_c' =>$this->input->post('deal_terms'),'status'=>$this->input->post('status'));
              }else{
                 $data = array('deal_name' =>$this->input->post('deal_name') ,'deal_title' =>$this->input->post('deal_title') ,'start_date' =>$start_date,'end_date' =>$end_date,'deal_start_time' => $this->input->post('deal_start_time'),'deal_end_time' => $this->input->post('deal_end_time'),'deal_description' =>$this->input->post('deal_description') ,'t_c' =>$this->input->post('deal_terms') ,'img'=>$file_name,'status'=>$this->input->post('status'));
                  move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
              }
        if($this->Admin_modal->update_deal($data,$deal_id))
        {
           $this->session->set_flashdata('msg','Deal successfully updated'); 
           echo "<script>window.location.href='".base_url('deals')."';</script>";
          // redirect(base_url('coupon'));
        }
    }
   

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->deal_record_counts($Searchkey,$user_id);
   
  $config['base_url'] = base_url()."coupon";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_deal($Searchkey,$config['per_page'],$this->uri->segment(2),$restaurant_id1);
  //echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
 
   $data['result'] =  $query;

      $this->load->view('user/deals',$data);
    }

    public function deal_promote(){


            $user_id=$this->session->userdata('user_id');
     
      $restaurant_data=$this->Admin_modal->select_restaurant_id($user_id);      
      $restaurant_id1=$restaurant_data[0]['restaurant_id'];
      $deal_id=$this->input->post('deal_id');   
      $submit=$this->input->post('submit');


     if (empty($deal_id)){
       
       
      if (isset($submit)) {
         $tmp_name=$_FILES['deal_img']['tmp_name'];
              $file_name=$_FILES['deal_img']['name'];    
$start_date=date("Y-m-d", strtotime($this->input->post('deal_end_date')));
        $end_date=date("Y-m-d", strtotime($this->input->post('deal_start_date')));
        $data = array('deal_name' =>$this->input->post('deal_name') ,'deal_title' =>$this->input->post('deal_title') ,'start_date' =>$start_date,'end_date' =>$end_date,'deal_description' =>$this->input->post('deal_description') ,'t_c' =>$this->input->post('deal_terms') ,'img'=>$file_name,'restaurant_id'=>$user_id);
         move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
        if($this->Admin_modal->add_deal($data))
        {
           $this->session->set_flashdata('msg','Deal successfully added'); 
           echo "<script>window.location.href='".base_url('deal_promote')."';</script>";
          // redirect(base_url('coupon'));
        }
      }
    }else{
      $start_date=date("Y-m-d", strtotime($this->input->post('deal_start_date')));
        $end_date=date("Y-m-d", strtotime($this->input->post('deal_end_date')));         
                          
     $data = array('start_date' =>$start_date,'end_date' =>$end_date ,'deal_id'=>$deal_id,'restaurant_id'=>$user_id);
             
        if($this->Admin_modal->deal_promote_insert($data))
        {
           $this->session->set_flashdata('msg','Request successfully submitted'); 
           echo "<script>window.location.href='".base_url('deal_promote')."';</script>";
          // redirect(base_url('coupon'));
        }
    }
   

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->deal_record_promote_counts($Searchkey,$user_id);
   
  $config['base_url'] = base_url()."deal_promote";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_deal_promote($Searchkey,$config['per_page'],$this->uri->segment(2),$restaurant_id1);
 //print_r($query);
  //echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
 
   $data['result'] =  $query;
      $this->load->view('user/deal_promote',$data);
    }


  public function banner_promote(){
            $user_id=$this->session->userdata('user_id');        
        $submit=$this->input->post('submit');
        $promote_id=$this->input->post('promote_id');

      if (empty($promote_id)) {
        
      if (isset($submit)) {
         $tmp_name=$_FILES['banner_img']['tmp_name'];
              $file_name=$_FILES['banner_img']['name'];    
// $start_date=date("Y-m-d", strtotime($this->input->post('banner_start_date')));
//         $end_date=date("Y-m-d", strtotime($this->input->post('banner_end_date')));
        // $data = array('banner_title' =>$this->input->post('banner_title') ,'start_date' =>$start_date,'end_date' =>$end_date ,'banner_description' =>$this->input->post('banner_description') ,'banner_img'=>$file_name,'restaurant_id'=>$user_id);
             $data = array('title' =>$this->input->post('banner_title') ,'description' =>$this->input->post('banner_description') ,'banner_img'=>$file_name,'restaurant_id'=>$user_id); 
         move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
        if($this->Admin_modal->add_banner_promote($data))
        {
           $this->session->set_flashdata('msg','Banner successfully added'); 
           echo "<script>window.location.href='".base_url('banner_promote')."';</script>";
          
        }
      }   
      }else{

         $start_date=date("Y-m-d", strtotime($this->input->post('banner_start_date')));
        $end_date=date("Y-m-d", strtotime($this->input->post('banner_end_date')));          
                          
     $data = array('start_date' =>$start_date,'end_date' =>$end_date ,'banner_id'=>$promote_id,'restaurant_id'=>$user_id);
             
        if($this->Admin_modal->deal_promote_insert($data))
        {
           $this->session->set_flashdata('msg','Request successfully submitted'); 
           echo "<script>window.location.href='".base_url('banner_promote')."';</script>";
          // redirect(base_url('coupon'));
        }

      }
   

     $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->banner_record_promote_counts($Searchkey,$user_id);
   //echo $this->db->last_query();
  $config['base_url'] = base_url()."banner_promote";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_banner_promote($Searchkey,$config['per_page'],$this->uri->segment(2),$user_id);
 // print_r($query);
 //  echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
 
   $data['result'] =  $query;
      $this->load->view('user/banner_promote',$data);
    }


    public function subscription($id='')
    {
        // $Searchkey=$this->input->get('status');
        $user_id=$this->session->userdata('user_id');
        if(!empty($id)){
        		$subscription = $this->Admin_modal->get_subscription($id);
        		$amount = $subscription['0']['amount'];
            if (empty($amount)) {
            	$this->session->set_flashdata('msg','Please select valid package'); 
               echo "<script>window.location.href='".base_url('subscription')."';</script>";
            }
            $data['restaurant_id'] = $user_id;
            $data['package_id'] = $id;
            $data['amount'] = $amount;
            $data['paid'] = 1;
            $subscribed = $this->Admin_modal->add_subscription($data);
            if($subscribed){
                $this->session->set_flashdata('msg','You have subscribed successfully'); 
                echo "<script>window.location.href='".base_url('subscription')."';</script>";
            }
            else
            {
                $this->session->set_flashdata('msg','Some error occurred, please try again!'); 
                echo "<script>window.location.href='".base_url('subscription')."';</script>";
            }

        }
        $Searchkey=1;
		$config['total_rows'] = $this->Admin_modal->subscription_plan_record_count($Searchkey);
		$config['base_url'] = base_url()."subscription_plan";
		$config['per_page'] = 10;
		$config['uri_segment'] = '2';
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = false;
		$config['last_link'] = false;
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$query = $this->Admin_modal->select_subscription_plan($Searchkey,$config['per_page'],$this->uri->segment(2));
		$data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
		$data['result'] = null;
		$data['result'] =  $query;
        $this->load->view('user/subscription',$data);
    }

    public function clicks($value='')
    {
      $this->load->view('user/clicks');
    }

    public function viewdata($value='')
    {
      $this->load->view('user/viewdata');
    }
    public function deals_redeemed($value='')
    {
      $user_id=$this->session->userdata('user_id');
      $restaurant_data=$this->Admin_modal->select_restaurant_id($user_id);      
      $restaurant_id1=$restaurant_data[0]['restaurant_id'];
      $config['total_rows'] = $this->Admin_modal->deal_record_counts('',$user_id);
      $config['base_url'] = base_url()."deals_redeemed";
      $config['per_page'] = 10;
      $config['uri_segment'] = '2';
      $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['first_link'] = false;
      $config['last_link'] = false;
      $config['first_tag_open'] = '<li>';
      $config['first_tag_close'] = '</li>';
      $config['prev_link'] = '&laquo';
      $config['prev_tag_open'] = '<li class="prev">';
      $config['prev_tag_close'] = '</li>';
      $config['next_link'] = '&raquo';
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';
      $config['last_tag_open'] = '<li>';
      $config['last_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a href="#">';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $this->pagination->initialize($config);
      $query = $this->Admin_modal->select_deal('',$config['per_page'],$this->uri->segment(2),$restaurant_id1);
      $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
      $data['result'] = null;
      $data['result'] =  $query;
      // $this->load->view('user/deals',$data);
      $this->load->view('user/deals_redeemed',$data);
    }

    public function about_us()
    {
    	$data['setting'] = $this->Admin_modal->setting();
    	//print_r($data['setting']);
    	$this->load->view('user/about_us',$data);
    }

      public function t_c()
    {
    	$data['setting'] = $this->Admin_modal->setting();
    	//print_r($data['setting']);
    	$this->load->view('user/t_c',$data);
    }


  public function privacy_policy()
    {
    	$data['setting'] = $this->Admin_modal->setting();
    	//print_r($data['setting']);
    	$this->load->view('user/policy',$data);
    }


  public function contact_us()
    {
    	if($_POST['Submit']){
    		$ins['name'] = $this->input->post('name');
    		$ins['email'] = $this->input->post('email');
    		$ins['message'] = $this->input->post('message');
    		$ins['user_id'] = $_SESSION['user_id'];
    		$insert=$this->Admin_modal->contact_us($ins); 

    		//=====mail to restaurant email id
	       	$restaurant_info=$this->Admin_modal->admin_profile('1');
	       	$email = $restaurant_info['0']['user_email'];
	      
			$message = $ins['message'];
			$subject = "Query from ".$ins['name'];
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from('no-reply@yesitlabs.xyz'); // change it to yours
			$this->email->to($email);//msz  where and in which email to send?
			$this->email->subject($subject);
			$this->email->message($message);
			$this->email->set_mailtype("html");
			$this->email->send();
			$this->session->set_flashdata('msg','Mail Sent'); 
            echo "<script>window.location.href='".base_url('contact_us')."';</script>";


        	//======end
    	}
      $data['setting'] = $this->Admin_modal->setting();
    	$data['result']=$this->Admin_modal->admin_profile($_SESSION['user_id']); 
    	//print_r($data['setting']);
    	$this->load->view('user/contact_us',$data);
    }



	
}