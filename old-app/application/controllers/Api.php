<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

//include Rest Controller library 
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
class Api extends REST_Controller {

    public function __construct() { 
        parent::__construct();
        
        //load user model
        $this->load->model('Admin_modal','admin');
        $this->load->model('Api_model','apim');
        $this->load->library('twilio');
    }

    /***************************************
    **** Developer   : kalyan singh ********
    **** Date        : 07/31/2019 ***********
    **** API version : 00.00 ****************
    ****************************************/


    /*****************************
        Purpose   : Get detail of a state or list of all state 
        method    : GET
        Parameter : {id}
        URL       : http://fanspace.yesitlabs.xyz/api/state
        Response  : JSON
    */ 
    public function state_get($id = 0) {
        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned
        $cond['status'] = 1;
        if(!empty($id)){
            $cond['state_id'] = $id;
        }
        $state = $this->apim->getRows('state_id,state_name','tbl_states',$cond);
        //check if the user data exists
        if(!empty($state)){
            //set the response and exit
            $this->response([
                'status' => TRUE,
                'data' => $state,
            ], REST_Controller::HTTP_OK);
        }else{
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No state were found.'
            ], REST_Controller::HTTP_OK);
        }
    }


    /*****************************
        Purpose   : Get detail of a city or list of all city in a state 
        method    : GET
        Parameter : {stateId/cityId*}
        URL       : http://fanspace.yesitlabs.xyz/api/city
        Response  : JSON
    */ 
    public function city_get($id = 0) {
        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned
        $cond['status'] = 1;

        if(!empty($this->input->get('stateId')) || !empty($this->input->get('cityId'))){
            if(!empty($this->input->get('stateId'))){
                $cond['state_name'] = $this->input->get('stateId');
            }
            if(!empty($this->input->get('cityId'))){
                $cond['id'] = $this->input->get('cityId');
            }

            $city = $this->apim->getRows('id,city_name,state_name as state_id','tbl_city',$cond);
            //check if the user data exists
            if(!empty($city)){
                //set the response and exit
                $this->response([
                    'status' => TRUE,
                    'data' => $city,
                ], REST_Controller::HTTP_OK);
            }else{
                //set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No avatar were found.'
                ], REST_Controller::HTTP_OK);
            }
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Please send at least one parameter.'
            ], REST_Controller::HTTP_OK);
        }
    }


        /*****************************
        Purpose   : check email/mobile is exist or not. 
        method    : POST [form-data]
        Parameter : {email*}
        URL       : http://fanspace.yesitlabs.xyz/api/email_avail
        Response  : JSON
    */

    public function email_avail_post() {
        $userData = array();
       $userData['user_email'] = $this->post('email');
       $userData['status'] = '1';
        // print_r($userData);
        if(!empty($userData['user_email'])){
            //insert user data
            $rows = $this->apim->getRowNum('tbl_user',$userData);
            
            //check if the user data inserted
            if($rows===1){
                //set the response and exit
                $this->response([
                    'status' => TRUE,
                    'available' => 'no',
                    'message' => 'This Email Id is already registered.'
                ], REST_Controller::HTTP_OK);
            }
            else if($rows===0){
                 $this->response([
                    'status' => TRUE,
                    'available' => 'yes',
                    'message' => 'Email checked successfully.'
                ], REST_Controller::HTTP_OK);
            }

            else if($rows>=1){
                 $this->response([
                    'status' => TRUE,
                    'available' => 'no',
                    'message' => 'Multiple Account Found.'
                ], REST_Controller::HTTP_OK);
            }

            else{
                //set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Some problems occurred, please try again.'
                ], REST_Controller::HTTP_OK);
            }
        }else{
            //set the response and exit
            $this->response([
                    'status' => FALSE,
                    'message' => 'Provide email address for checking availablity.'
                ], REST_Controller::HTTP_OK);
        }
    }

        /*****************************
        Purpose   : To get information about the app
        method    : GET
        Parameter : { t_c/about/p_p/contact_detail* }
        URL       : http://fanspace.yesitlabs.xyz/api/app_info
        Response  : JSON
        */

        public function app_info_GET ($value='')
        {
            if(!empty($value)){

                if($value == 't_c'){
                    $data['id'] = '2';
                }
                if ($value == 'about') {
                    $data['id'] = '3';
                }
                if ($value == 'p_p') {
                    $data['id'] = '4';
                }
                if ($value == 'contact_detail') {
                    $data['id'] = '5';
                }
                
                $rec = $this->apim->getRow($value,'tbl_banner',$data);
                if ($rec) {
                    //set the response and exit
                    $this->response([
                        'status' => TRUE,
                        'data' => $rec,
                    ], REST_Controller::HTTP_OK);
                }
                else{
                     //set the response and exit
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Something went wrong, Please try again!'
                    ], REST_Controller::HTTP_OK);
                }

            }
            else{
                //set the response and exit
                $this->response([
                        'status' => FALSE,
                        'message' => 'Provide at least one parameter.'
                    ], REST_Controller::HTTP_OK);
            }
        }


        /*****************************
        Purpose   : Send otp to verify mobile number/email. 
        method    : POST [form-data]
        Parameter : {value*,type*}
        URL       : http://fanspace.yesitlabs.xyz/api/verify
        Response  : JSON
        */

        public function verify_post() {
        $value = $this->post('value');
        $type = $this->post('type');
        if (!empty($value) && !empty($type)) {
            $code = mt_rand(10000, 99999);
            if ($type=='mobile') {
                $msg = "Your otp for Fanspace app is $code";
                $res = $this->twilio->sms('+12512548900',$value,$msg);
                $xml = simplexml_load_string($res['result']);
                $json = json_encode($xml);
                $result = json_decode($json,TRUE);
                if(!empty($result['RestException']['Status'])){
                    //set the response and exit
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Some problems occurred, please try again.'
                    ], REST_Controller::HTTP_OK);
                }

                if (!empty($result['SMSMessage']['Status'])) {
                    //set the response and exit
                    $this->response([
                        'status' => TRUE,
                        'otp' => "$code",
                        'message' => 'Message send successfully.'
                    ], REST_Controller::HTTP_OK);
                }
                else{
                    //set the response and exit
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Some problems occurred, please try again.'
                    ], REST_Controller::HTTP_OK);
                }  
            }

            if ($type == 'email') {
                $message = 'Your Verification OTP Is :-'.$code;
                $this->load->library('email');
                $this->email->set_newline("\r\n");
                $this->email->from('no-reply@yesitlabs.xyz'); // change it to yours
                $this->email->to($value);// change it to yours
                $this->email->subject('Confirmation Mail');
                $this->email->message($message);
                $send_status=$this->email->send();
                if ($send_status) {
                     //set the response and exit
                    $this->response([
                        'status' => TRUE,
                        'otp' => "$code",
                        'message' => 'Mail send successfully.'
                    ], REST_Controller::HTTP_OK);
                }
                else
                {
                     //set the response and exit
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Some problems occurred1, please try again.'
                    ], REST_Controller::HTTP_OK);
                }
            }
        }
        else{
            //set the response and exit
             $this->response([
                    'status' => FALSE,
                    'message' => 'Provide mobile/email for verification.'
                ], REST_Controller::HTTP_OK);
        }
    }

    /*****************************
        Purpose   : Get detail of a eateries or list of all eateries 
        method    : GET
        Parameter : {id}
        URL       : http://fanspace.yesitlabs.xyz/api/eateries_type
        Response  : JSON
    */ 
    public function eateries_type_get($id = 0) {
        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned
        $cond['status'] = 1;
        if(!empty($id)){
            $cond['music_genre_id'] = $id;
        }
        $state = $this->apim->getRows('music_genre_id as eateries_id,music_genre_name as eateries_name','tbl_music_genre',$cond);
        //check if the user data exists
        if(!empty($state)){
            //set the response and exit
            $this->response([
                'status' => TRUE,
                'data' => $state,
            ], REST_Controller::HTTP_OK);
        }else{
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No state were found.'
            ], REST_Controller::HTTP_OK);
        }
    }

    /*****************************
        Purpose   : Change current password with old password
        method    : POST [form-data]
        Parameter : { user_id*, oldpassword*, newpassword* }
        URL       : http://fanspace.yesitlabs.xyz/Api/changepassword/
        Response  : JSON
    */
    public function changepassword_POST() {
        $user_id = $this->input->post('user_id');
        $oldpassword = $this->input->post('oldpassword');
        $newpassword = $this->input->post('newpassword');
        if ($this->input->post()) {
            if(!empty($user_id) && !empty($oldpassword) && !empty($newpassword)){
                $data['user_id'] = $user_id;
                $cond['user_id'] = $user_id;
                $data['user_password'] = md5($oldpassword);
                $result = $this->apim->getRowNum('tbl_user',$data);
                
                if ($result == 1) {
                    $pwd = array("user_password" => md5($this->input->post('newpassword')));
                    $res = $this->apim->update('tbl_user',$pwd, $cond);
                    // echo $this->db->last_query();
                    if($res > 0){
                        $this->response([
                            'status' => true,
                            'message' => 'Password change successfully !',
                            'data' => ''
                        ], REST_Controller::HTTP_OK); 
                        exit();
                    } else {
                        $this->response([
                            'status' => false,
                            'message' => 'sorry, there is some internal issue, please try again !',
                            'data' => ''
                        ], REST_Controller::HTTP_OK); 
                        exit();
                    }
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Sorry, invalid old password !',
                        'data' => ''
                    ], REST_Controller::HTTP_OK); 
                    exit();
                }
                
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Please fill all the fields !',
                    'data' => ''
                ], REST_Controller::HTTP_OK); 
                exit();
                
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP 
        }
    } 

    /*****************************
        Purpose   : Get detail of a user or list of all users or 
                    Get detail of a restaurat or list of all restaurat
        method    : GET
        Parameter : {id}
        URL       : http://fanspace.yesitlabs.xyz/api/user/User            or
                    http://fanspace.yesitlabs.xyz/api/user/Restaurant
        Response  : JSON
    */ 
    public function user_get($type, $id) {
        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned
        $cond['tbl_user.status'] = 1;
        if(!empty($id)){
            $cond['tbl_user.user_id'] = $id;
        }
        if ($type == 'User') {
            $cond['user_role'] = 'User';
            $user = $this->apim->getRowsJoin('*, tbl_user.user_id as user_id','tbl_user','tbl_user_address','tbl_user.user_id = tbl_user_address.user_id',$cond);
        }

        if ($type == 'Restaurant') {
            $cond['user_role'] = 'Restaurant';
            $user = $this->apim->getRowsJoin('*,tbl_user.user_id as restaurant_id','tbl_user','tbl_restaurant','tbl_user.user_id = tbl_restaurant.restaurant_id',$cond);
        }
        // echo $this->db->last_query();
        //check if the user data exists
        if(!empty($user)){
            //set the response and exit
            $this->response([
                'status' => TRUE,
                'data' => $user,
            ], REST_Controller::HTTP_OK);
        }else{
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No user/restaurant were found.'
            ], REST_Controller::HTTP_OK);
        }
    }

    /*****************************
        Purpose   : get eateries in a given range of a user
        method    : POST
        Parameter : {userId*,latitude*,longitude*,min_range,max_range,unit} default range is 0 to 100m(all will calculate in meter);unit must be in meter or feet by default it is in meter
        URL       : http://fanspace.yesitlabs.xyz/api/inRangeEateries
        Response  : JSON
    */

    public function inRangeEateries_post()
    {
        $user_id =  $this->post('userId');
        $latitude = $this->post('latitude');
        $longitude = $this->post('longitude');
        $min_range = $this->post('min_range');
        $max_range = $this->post('max_range');
        $unit = $this->post('unit');
        if(empty($min_range)){
            $min_range = '0';
        }

        if(empty($max_range)){
            $max_range = '25';
        }
        if($unit != 'meter'){
            $unit = 'miles';
        }

        if(!empty($user_id) && !empty($latitude) && !empty($longitude)){
            if($unit=='meter'){
            $earth_radius = '6371000';
            }
            else{
                $earth_radius = '3958.755866';
            }
            $date = date('Y-m-d H:i:s', time());
            $query = "SELECT tbl_restaurant.restaurant_id as id, tbl_user.status as status ,first_name,last_name,user_email, user_avatar,user_role,title,street,city,state,zipcode, lat, log, phone, open_time, close_time, ( $earth_radius * acos ( cos ( radians($latitude) ) * cos( radians( lat ) ) * cos( radians( log ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians( lat ) ) ) ) AS distance FROM tbl_restaurant INNER JOIN tbl_user on tbl_restaurant.restaurant_id = tbl_user.user_id HAVING distance>= $min_range AND distance < $max_range AND status = '1'";

            $data  = $this->apim->getRowsSql($query);

            if(!empty($data)){
                // set the response and exit
                $this->response([
                    'status' => true,
                    'data'  => $data,
                    'message' => 'Fetched Successfully'
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'No Data Found',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
        }else {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Provide complete billboard information to create',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }

    /*****************************
        Purpose   : Bookmark my places
        method    : POST [form-data]
        Parameter : { userId*, placeId* }
        URL       : http://fanspace.yesitlabs.xyz/Api/set_bookmark
        Response  : JSON
    */

    public function set_bookmark_POST()
    {
        if(!empty($this->input->post('userId')) && !empty($this->input->post('placeId'))){
            $data['user_id'] = $this->input->post('userId');
            $data['restaurant_id'] = $this->input->post('placeId');
            $exist = $this->apim->getRowNum('tbl_bookmark',$data);
            if (!$exist) {
                $insert = $this->apim->insert('tbl_bookmark',$data);
                if($insert){
                    //set the response and exit
                    $this->response([
                        'status' => true,
                        'message' => 'Bookmarked Successfully',
                    ], REST_Controller::HTTP_OK); 
                    exit();
                }
                else
                {
                    //set the response and exit
                    $this->response([
                        'status' => false,
                        'message' => 'Something goes wrong',
                    ], REST_Controller::HTTP_OK); 
                    exit();
                }
            }
            else
            {
                //set the response and exit
                    $this->response([
                        'status' => true,
                        'message' => 'Already bookmarked',
                    ], REST_Controller::HTTP_OK); 
                    exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }


    /*****************************
        Purpose   : Bookmark my places
        method    : POST [form-data]
        Parameter : { userId*, placeId* }
        URL       : http://fanspace.yesitlabs.xyz/Api/get_bookmark
        Response  : JSON
    */

    public function get_bookmark_POST()
    {
        if(!empty($this->input->post('userId')) || !empty($this->input->post('placeId'))){
            if (!empty($this->input->post('userId'))) {
                $data['user_id'] = $this->input->post('userId');
            }
            if (!empty($this->input->post('placeId'))) {
                $data['restaurant_id'] = $this->input->post('placeId');
            }
            $res = $this->apim->getRows('*','tbl_bookmark',$data);
            if($res){
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data' => $res,
                    'message' => 'Bookmarked Successfully',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'No data found',
                ], REST_Controller::HTTP_OK); 
                exit();
            }   
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }


    /*****************************
        Purpose   : remove my bookmarked places
        method    : POST [form-data]
        Parameter : { userId*, placeId* }
        URL       : http://fanspace.yesitlabs.xyz/Api/remove_bookmark
        Response  : JSON
    */

    public function remove_bookmark_POST()
    {
        if(!empty($this->input->post('userId')) && !empty($this->input->post('placeId'))){
            $data['user_id'] = $this->input->post('userId');
            $data['restaurant_id'] = $this->input->post('placeId');
            $delete = $this->apim->delete('tbl_bookmark',$data);
            if($delete){
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'Bookmark removed Successfully',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'Some goes wrong, please try again!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }   
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }



    /*****************************
        Purpose   : Sign up
        method    : POST [form-data]
        Parameter : { name*, email/mobile*, address*, latitude*, longitude*, city*, state*, zip_code*, password*, remember_token*, fcm_token* }
        URL       : http://fanspace.yesitlabs.xyz/Api/signUp
        Response  : JSON
    */
    public function signUp_post() {
        $data1['first_name'] = $this->input->post('name');
        $data1['user_email'] = $this->input->post('email');
        $data1['mobile'] = $this->input->post('mobile');
        $data['street'] = $this->input->post('address');
        $data['lat'] = $this->input->post('latitude');
        $data['lng'] = $this->input->post('longitude');
        $data['city'] = $this->input->post('city');
        $data['state'] = $this->input->post('state');
        $data['zipcode'] = $this->input->post('zip_code');
        $data1['remember_token'] = $this->input->post('remember_token');
        $data1['fcm_token'] = $this->input->post('fcm_token');
        $data1['user_password'] = md5($this->input->post('password'));
        

        if ( !empty($data1['first_name']) 
			&& ( !empty($data1['user_email']) || !empty($data1['mobile'])) 
			&& !empty($data['street']) 
			&& !empty($data['lat']) 
			&& !empty($data['lng']) 
			&& !empty($data['city']) 
			&& !empty($data['state']) 
			&& !empty($data['zipcode']) 
			&& !empty($data1['user_password']) 
			&& !empty($data1['remember_token'])
			&& !empty($data1['fcm_token']) 
			) {

            $data1['user_role'] = "User";
            $data1['status'] = 1;
            $data1['user_reg_date'] = date('Y-m-d H:i:s');
            $data['user_id'] = $this->apim->insert('tbl_user',$data1);
            // echo $this->db->last_query();
            $insert_id = $this->apim->insert('tbl_user_address',$data);

            if ($data['user_id'] && $insert_id) {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data'  => $data['user_id'],
                    'message' => 'User Created Successfully',
                ], REST_Controller::HTTP_OK); 
                exit();
            } else {
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Some goes wrong, please try again!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            
        } else {
			echo "hello else";
		die;
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }

    /*****************************
        Purpose   : login
        method    : POST [form-data]
        Parameter : { value*, password*, type* }
        URL       : http://fanspace.yesitlabs.xyz/Api/login
        Response  : JSON
    */


    public function login_post()
    {
        $type = $this->input->post('type');
        $value = $this->input->post('value');
        $password = md5($this->input->post('password'));
        if (!empty($type) && !empty($value) && !empty($password)) {
            if ($type == 'mobile') {
                $cond['mobile'] = $value;
            }
            elseif ($type == 'email') {
                $cond['user_email']  = $value;
            }
            else{
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'invalid value type parameter',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            $cond['user_password'] = $password;
            $data  = $this->apim->getRowJoin('first_name, user_email, mobile, street as address,lat,lng,city,state,zipcode,tbl_user.user_id as userId','tbl_user','tbl_user_address','tbl_user.user_id = tbl_user_address.user_id',$cond);
            if ($data) {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data' => $data,
                    'message' => 'valid credentials',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'wrong credentials',
            ], REST_Controller::HTTP_OK); 
            exit();
            }
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }

    /*****************************
        Purpose   : Change forget password
        method    : POST [form-data]
        Parameter : { value*, newpassword*, type* }
        URL       : http://fanspace.yesitlabs.xyz/Api/forgetpassword/
        Response  : JSON
    */
    public function forgetpassword_POST() {
        $value = $this->input->post('value');
        $type = $this->input->post('type');
        $newpassword = $this->input->post('newpassword');
        if(!empty($value) && !empty($type) && !empty($newpassword)){
            
            if ($type == 'mobile') {
                $cond['mobile'] = $value;
            }
            else if ($type == 'email') {
                $cond['user_email'] = $value;
            }
            else{
                $this->response([
                    'status' => false,
                    'message' => 'Inappropriate Type',
                    'data' => ''
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            $exist = $this->apim->getRowNum('tbl_user', $cond);
            if ($exist) {
                $pwd = array("user_password" => md5($this->input->post('newpassword')));
                $res = $this->apim->update('tbl_user',$pwd, $cond);
                // echo $this->db->last_query();
                if($res > 0){
                    $this->response([
                        'status' => true,
                        'message' => 'Password change successfully !',
                    ], REST_Controller::HTTP_OK); 
                    exit();
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'sorry, there is some internal issue, please try again !',
                        'data' => ''
                    ], REST_Controller::HTTP_OK); 
                    exit();
                }
            }
            else
            {
                $this->response([
                    'status' => false,
                    'message' => $type. " not exist",
                    'data' => ''
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            
            
        } else {
            $this->response([
                'status' => false,
                'message' => 'Please fill all the fields !',
                'data' => ''
            ], REST_Controller::HTTP_OK); 
            exit();
            
        }
    }

    /*****************************
        Purpose   : get list of deals
        method    : POST [form-data]
        Parameter : {  }
        URL       : http://fanspace.yesitlabs.xyz/Api/deals_list/
        Response  : JSON
    */

	public function deals_list_post() {
		$user_id =  $this->post('userId');
		$latitude = $this->post('latitude');
		$longitude = $this->post('longitude');
		$min_range = $this->post('min_range');
		$max_range = $this->post('max_range');
		//$unit = $this->post('unit');
		$date_deal =  $this->post('date_deal');
         
        if(empty($min_range)) {
            $min_range = '0';
        }

        if(empty($max_range)) {
            $max_range = '25';
        }
        if($unit != 'meter') {
            $unit = 'miles';
        }

        if( !empty($user_id) && !empty($latitude) && !empty($longitude)&& !empty($date_deal) ) {
            if($unit == 'meter') {
            	$earth_radius = '6371000';
            } else {
                $earth_radius = '3958.755866';
            }
            $date = date('Y-m-d H:i:s', time());

            $query = "SELECT tbl_deals.id as id, tbl_restaurant.restaurant_id as restaurant_id, tbl_restaurant.status as status , lat, log, deal_title, deal_description, start_date, end_date, img, open_time, close_time, deal_name, ( $earth_radius * acos ( cos ( radians($latitude) ) * cos( radians( lat ) ) * cos( radians( log ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians( lat ) ) ) ) AS distance FROM tbl_restaurant INNER JOIN tbl_deals on tbl_restaurant.restaurant_id = tbl_deals.restaurant_id HAVING distance >= $min_range AND distance < $max_range AND status = '1' AND start_date <= '$date_deal' AND end_date >= '$date_deal'";

            echo $query;
            $data  = $this->apim->getRowsSql($query);
			print_r($data);die;
            foreach ($data as $row) {
                $datanew['user_id'] =  $user_id;
                $datanew['dealid'] = $row['id'];

                $data_fav = $this->apim->getRows('*','tbl_favourite_deal',$datanew);

                if(!empty($data_fav)) {
					foreach ($data_fav as $value) {   
						$row['favourite_status'] = '1';
					}
                
				} else{
					$row['favourite_status'] = '0';
				}
            	$new[] = $row;
        	}


            if(!empty($new)){
                // set the response and exit
                $this->response([
                    'status' => true,
                    'data'  => $new,
                    'message' => 'Deals Lists'
                ], REST_Controller::HTTP_OK); 
                exit();
            } else {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'No Data Found',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
        } else {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Provide complete information to create',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }

    // public function deals_list_post($value='')
    // {
    //     $where['tbl_promote.status'] = 1;
    //     $where['deal_id !='] = null;
    //     $data = $this->apim->getRowsJoin('tbl_user.first_name,tbl_user.last_name,tbl_user.user_email,tbl_user.user_email,tbl_promote.deal_id,tbl_promote.banner_id,tbl_promote.restaurant_id,tbl_promote.start_date,tbl_promote.end_date,tbl_promote.status,tbl_promote.id','tbl_promote','tbl_user','tbl_user.user_id=tbl_promote.restaurant_id',$where);
    //      //set the response and exit
    //     $this->response([
    //         'status' => true,
    //         'data' => $data,
    //         'message' => '',
    //     ], REST_Controller::HTTP_OK); 
    //     exit();

    // }

    /*****************************
        Purpose   : follow a user
        method    : POST [form-data]
        Parameter : { userId*, followId* }
        URL       : http://fanspace.yesitlabs.xyz/Api/set_follow
        Response  : JSON
    */

    public function set_follow_POST()
    {
        if(!empty($this->input->post('userId')) && !empty($this->input->post('followId'))){
            $data['user_id'] = $this->input->post('userId');
            $data['follow_id'] = $this->input->post('followId');
            $exist = $this->apim->getRowNum('tbl_user_follow',$data);
            if (!$exist) {
                $insert = $this->apim->insert('tbl_user_follow',$data);
                if($insert){
                    //set the response and exit
                    $this->response([
                        'status' => true,
                        'message' => 'Followed',
                    ], REST_Controller::HTTP_OK); 
                    exit();
                }
                else
                {
                    //set the response and exit
                    $this->response([
                        'status' => false,
                        'message' => 'Something goes wrong',
                    ], REST_Controller::HTTP_OK); 
                    exit();
                }
            }
            else
            {
                //set the response and exit
                    $this->response([
                        'status' => true,
                        'message' => 'Already following',
                    ], REST_Controller::HTTP_OK); 
                    exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }


    /*****************************
        Purpose   : following users list
        method    : POST [form-data]
        Parameter : { userId/followId* }
        URL       : http://fanspace.yesitlabs.xyz/Api/get_follow
        Response  : JSON
    */

    public function get_follow_POST()
    {
        if(!empty($this->input->post('userId')) || !empty($this->input->post('followId'))){
            if (!empty($this->input->post('userId'))) 
            {
                 $data['user_id'] = $this->input->post('userId');
                 $res_ponse = $this->apim->getRows('follow_id','tbl_user_follow',$data);

                $retunuploadpath = base_url().'assets/images/';
                 //print_r($res_ponse);
                 for ($i=0; $i < sizeof($res_ponse) ; $i++) 
                 {
                   
        $follow_id['user_id'] = $res_ponse[$i]['follow_id'];
        $follower_details[$i]['followers_id'] = $res_ponse[$i]['follow_id'];
        $follower_details[$i]['name'] = $this->apim->getRows('first_name,last_name,user_avatar','tbl_user',$follow_id);
        $follower_details[$i]['reviews'] = $this->apim->getRowNum('tbl_week_rating',$follow_id);
        $follower_details[$i]['followers'] = $this->apim->getRowNum('tbl_user_follow',$follow_id);
                 
        if(!empty($follower_details[$i]['name'][0]['user_avatar']))
        {
    $follower_details[$i]['name'][0]['user_avatar'] = $retunuploadpath.$follower_details[$i]['name'][0]['user_avatar'];
         }           //echo $this->db->last_query();
                 }

                 $res = $follower_details;

            } 

            
            if (!empty($this->input->post('followId'))) 
            {
                 $data['follow_id'] = $this->input->post('followId');
                 $res = $this->apim->getRows('*','tbl_user_follow',$data);
            }
           
            

            if($res){
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data' => $res,
                    'message' => 'followed list',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'No data found',
                ], REST_Controller::HTTP_OK); 
                exit();
            }   
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }


    /*****************************
        Purpose   : remove my followed user
        method    : POST [form-data]
        Parameter : { userId*, followId* }
        URL       : http://fanspace.yesitlabs.xyz/Api/remove_follow
        Response  : JSON
    */

    public function remove_follow_POST()
    {
        if(!empty($this->input->post('userId')) && !empty($this->input->post('followId'))){
            $data['user_id'] = $this->input->post('userId');
            $data['follow_id'] = $this->input->post('followId');
            $delete = $this->apim->delete('tbl_user_follow',$data);
            if($delete){
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'unfollowed Successfully',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'Some goes wrong, please try again!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }   
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }



/*****************************
        Purpose   : Get detail of a Event or list of all Event 
        method    : GET
        Parameter : {id}
        URL       : http://fanspace.yesitlabs.xyz/api/event
        Response  : JSON
    */ 
    public function event_get($id = 0) {
        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned
        $cond['status'] = 1;
        if(!empty($id)){
            $cond['event_id'] = $id;
        }
        $event = $this->apim->getRows('event_id,event_name','tbl_event',$cond);
        //check if the user data exists
        if(!empty($event)){
            //set the response and exit
            $this->response([
                'status' => TRUE,
                'data' => $event,
            ], REST_Controller::HTTP_OK);
        }else{
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No Event were found.'
            ], REST_Controller::HTTP_OK);
        }
    }

    /*****************************
        Purpose   : Update User Profile
        method    : POST [form-data]
        Parameter : { name*, email/mobile*, address*, latitude*, longitude*, city*, state*, zip_code*,remember_token*, fcm_token* }
        URL       : http://fanspace.yesitlabs.xyz/api/updateUser
        Response  : JSON
    */

    public function updateUser_post()
    {
        
        $data1['first_name'] = $this->input->post('name');
        $data1['user_email'] = $this->input->post('email');
        $data1['mobile'] = $this->input->post('mobile');
        $data['street'] = $this->input->post('address');
        $data['lat'] = $this->input->post('latitude');
        $data['lng'] = $this->input->post('longitude');
        $data['city'] = $this->input->post('city');
        $data['state'] = $this->input->post('state');
        $data['zipcode'] = $this->input->post('zip_code');
        $data1['remember_token'] = $this->input->post('remember_token');
        $data1['fcm_token'] = $this->input->post('fcm_token');
        $data1['company'] = $this->input->post('company');
        $user_id =$this->input->post('userid');
         $cond['user_id'] = $user_id;
         $tmp_name=$_FILES['photo']['tmp_name'];
              $file_name=$_FILES['photo']['name'];       
              if(!empty($file_name))
              {
             if(move_uploaded_file($tmp_name, 'assets/images/'.$file_name))
             {
            $data3 = array('user_avatar' => $file_name); 
            $updateuser3 = $this->apim->update('tbl_user',$data3,$cond);        
             }
         }else{
             
         }
        

        if (!empty($data1['first_name']) && (!empty($data1['user_email']) || !empty($data1['mobile'])) && !empty($data['street']) && !empty($data['lat']) && !empty($data['lng']) && !empty($data['city']) && !empty($data['state']) && !empty($data['zipcode']) && !empty($user_id) && !empty($data1['remember_token']) && !empty($data1['fcm_token'])) {

            //$data1['user_role'] = "User";
            $data1['status'] = 1;
            //$cond['user_id'] = $user_id;
            $data1['modified_date'] = date('Y-m-d H:i:s');
            $updateuser = $this->apim->update('tbl_user',$data1,$cond);
            // echo $this->db->last_query();
            $result = $this->apim->update('tbl_user_address',$data,$cond);
           // $this->db->where($cond);
            //$result = $this->db->update('tbl_user_address',$data);

            if ($result) {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data'  => $user_id,
                    'message' => 'User Updated Successfully',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Some goes wrong, please try again!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }

     /*****************************
        Purpose   : Get detail of a FAQ or list of all FAQ 
        method    : GET
        Parameter : {id}
        URL       : http://fanspace.yesitlabs.xyz/api/faqlist
        Response  : JSON
    */ 
    public function faqlist_get($id = 0) {
        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned
        $cond['faq_status'] = 1;
        if(!empty($id)){
            $cond['faq_id'] = $id;
        }
        $state = $this->apim->getRows('faq_id,faq_question,faq_ans','tbl_faq',$cond);
        //check if the user data exists
        if(!empty($state)){
            //set the response and exit
            $this->response([
                'status' => TRUE,
                'data' => $state,
            ], REST_Controller::HTTP_OK);
        }else{
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No FAQ were found.'
            ], REST_Controller::HTTP_OK);
        }
    }

    /*****************************
        Purpose   : Suport
        method    : POST [form-data]
        Parameter : { user_id*, name*, email*, message*}
        URL       : http://fanspace.yesitlabs.xyz/Api/support
        Response  : JSON
    */

    public function support_post()
    {
        $data1['user_id'] = $this->input->post('user_id');
        $data1['name'] = $this->input->post('name');
        $data1['email'] = $this->input->post('email');
        $data1['message'] = $this->input->post('message');
       
        

        if (!empty($data1['user_id']) && (!empty($data1['name']) || !empty($data1['email'])) && !empty($data1['message'])) {

            $data1['status'] = 1;
            $data1['created'] = date('Y-m-d H:i:s');
            $insert_id = $this->apim->insert('tbl_contact_us',$data1);

            if ($insert_id) {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data'  => $data['user_id'],
                    'message' => 'User Created suport Successfully',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Some goes wrong, please try again!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }



    /*****************************
        Purpose   : Social Login
        method    : POST [form-data]
        Parameter : { social_id*, fcm_token*, remember_token*,lat*,lng*,first_name,last_name,user_email }
        URL       : http://fanspace.yesitlabs.xyz/Api/socialLogin
        Response  : JSON
    */


    public function socialLogin_post()
    {
       
        $data1['social_id'] = $this->input->post('social_id');
        $data1['first_name'] = $this->input->post('first_name');
        $data1['last_name'] = $this->input->post('last_name');
        $data1['user_email'] = $this->input->post('user_email');
        $data1['fcm_token'] = $this->input->post('fcm_token');
        $data1['remember_token'] = $this->input->post('remember_token');
        $data['lat'] = $this->input->post('lat');
        $data['lng'] = $this->input->post('lng');
         $data2['social_id'] = $this->input->post('social_id');
        

        if ((!empty($data1['social_id']) || !empty($data1['fcm_token']))  && !empty($data['lat']) && !empty($data['lng']) && !empty($data1['remember_token'])) {

             $data1['user_role'] = "User";
             $data1['social_type'] = 1;
            $data1['status'] = 1;
            $data1['user_reg_date'] = date('Y-m-d H:i:s');
            $data1['last_login'] = date('Y-m-d H:i:s');
            $exist = $this->apim->getRowNum('tbl_user',$data2);
            if ($exist) {
                $cond['social_id'] = $this->input->post('social_id');
             $dataget  = $this->apim->getRowJoin('first_name, user_email, mobile, street as address,lat,lng,city,state,zipcode,tbl_user.user_id as userId','tbl_user','tbl_user_address','tbl_user.user_id = tbl_user_address.user_id',$cond);

                   $this->response([
                    'status' => true,
                    'data'  => $dataget,
                    'message' => 'Login Successfully',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            $data['user_id'] = $this->apim->insert('tbl_user',$data1);
            // echo $this->db->last_query();
            $insert_id = $this->apim->insert('tbl_user_address',$data);

            if ($data['user_id'] && $insert_id) {
                 $cond['social_id'] = $this->input->post('social_id');
             $dataget  = $this->apim->getRowJoin('first_name, user_email, mobile, street as address,lat,lng,city,state,zipcode,tbl_user.user_id as userId','tbl_user','tbl_user_address','tbl_user.user_id = tbl_user_address.user_id',$cond);
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data'  => $dataget,
                    'message' => 'Login Successfully',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Some goes wrong, please try again!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }


    /*****************************
        Purpose   : Set Going and Not Going Status
        method    : POST [form-data]
        Parameter : { user_id*, restaurant_id*, event_id*, going*}
        URL       : http://fanspace.yesitlabs.xyz/Api/going
        Response  : JSON
    */

    public function going_post()
    {
        $data1['user_id'] = $this->input->post('user_id');
        $data1['restaurant_id'] = $this->input->post('restaurant_id');
        $data1['going'] = $this->input->post('going');
        $data1['event_id'] = $this->input->post('event_id');
       
        $data['event_id'] = $this->input->post('event_id');
         $data['user_id'] = $this->input->post('user_id');
         $data['restaurant_id'] = $this->input->post('restaurant_id');

        if (!empty($data1['user_id']) && (!empty($data1['restaurant_id']) || !empty($data1['event_id']))) {
            $data1['rate_date'] = date('Y-m-d H:i:s');
            $exist = $this->apim->getRowNum('tbl_week_rating',$data);
            if (!$exist) {
            $insert_id = $this->apim->insert('tbl_week_rating',$data1);

            if ($insert_id) {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data'  => $data['user_id'],
                    'message' => 'Going Status Successfully',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Some goes wrong, please try again!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
             }
            else
            {
                //set the response and exit
                    $this->response([
                        'status' => true,
                        'message' => 'Already Status Updated',
                    ], REST_Controller::HTTP_OK); 
                    exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }

    /*****************************
        Purpose   : Set Review
        method    : POST [form-data]
        Parameter : { userId*, restaurant_id*, star*, message*}
        URL       : http://fanspace.yesitlabs.xyz/Api/setReview
        Response  : JSON
    */

    public function setReview_post()
    {
        $data1['user_id'] = $this->input->post('userId');
        $data1['restaurant_id'] = $this->input->post('restaurant_id');
        $data1['star'] = $this->input->post('star');
        $data1['msg'] = $this->input->post('message');
       
        $data['user_id'] = $this->input->post('userId');
        $data['restaurant_id'] = $this->input->post('restaurant_id');
        

        if (!empty($data1['user_id']) && (!empty($data1['restaurant_id']) || !empty($data1['star']))) {
            
            $data1['rating_date'] = date('Y-m-d H:i:s');

            $exist = $this->apim->getRowNum('tbl_restaurant_star',$data);
            if (!$exist) {
            $insert_id = $this->apim->insert('tbl_restaurant_star',$data1);

            if ($insert_id) {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data'  => $data['user_id'],
                    'message' => 'Review Submitted Successfully',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Something goes wrong, please try again!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
             }
            else
            {
                //set the response and exit
                    $this->response([
                        'status' => true,
                        'message' => 'Review Already Submitted',
                    ], REST_Controller::HTTP_OK); 
                    exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }

    /*****************************
        Purpose   : User Detail By ID
        method    : POST [form-data]
        Parameter : { userId* }
        URL       : http://fanspace.yesitlabs.xyz/Api/get_userDetail
        Response  : JSON
    */

    public function get_userDetail_POST()
    {
        if(!empty($this->input->post('userId'))){
            if (!empty($this->input->post('userId'))) {
                 $cond = $this->input->post('userId');
            }
            


            //$res = $this->apim->getRows('*','tbl_user',$data);
            $res = $this->apim->getRowJoin_new($cond);

            if($res){
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data' => $res,
                    'message' => 'User Detail',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'No data found',
                ], REST_Controller::HTTP_OK); 
                exit();
            }   
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }

     /*****************************
        Purpose   : Banner List
        method    : POST [form-data]
        Parameter : {}
        URL       : http://fanspace.yesitlabs.xyz/Api/get_banner
        Response  : JSON
    */

    public function get_banner_get()
    {
            $data['status'] = "1";
            // $data['lat'] = $this->input->post("lat");
            // $data['log'] = $this->input->post("long");
           
            $path = base_url().'assets/images/';




            $res = $this->apim->getRows('banner_img','tbl_banner',$data);

          for ($i=0; $i <sizeof($res) ; $i++) 
          { 
              
              $match[$i]['banner_img'] = $path.$res[$i]['banner_img'];
            
          }
                
            if($match){
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data' => $match,
                    'message' => 'Banner List',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'No data found',
                ], REST_Controller::HTTP_OK); 
                exit();
            }   
       
    }


    /*****************************
        Purpose   : Banner List
        method    : POST [form-data]
        Parameter : {latitude, longitude}
        URL       : http://fanspace.yesitlabs.xyz/Api/get_weeklyTrending
        Response  : JSON
    */

    public function get_weeklyTrending_POST() {
		$today=date('Y-m-d');
		$last_date=date('Y-m-d', strtotime('-7 days'));
		
		$latitude = $this->input->post("latitude");  
		$longitude = $this->input->post("longitude");
		
		if(!empty($this->input->post("latitude")) && !empty($this->input->post('longitude'))) {
			$cond['tbl_restaurant.status'] = "1";
           //$user = $this->apim->getRowsJoin('*,tbl_user.user_id as restaurant_id','tbl_user','tbl_restaurant','tbl_user.user_id = tbl_restaurant.restaurant_id',$cond);

			$query = $this->db->query("select tbl_restaurant.restaurant_id as restaurant_id,tbl_restaurant.title as title,tbl_restaurant_photos.img,tbl_restaurant.street,tbl_restaurant.city,tbl_restaurant.zipcode,tbl_restaurant.lat,tbl_restaurant.log,website,phone,average_star,open_time,close_time,((((acos(sin((".$latitude."*pi()/180)) * sin((`lat`*pi()/180))+cos((".$latitude."*pi()/180))*cos((`lat`*pi()/180)) * cos(((".$longitude."- `log`)*pi()/180))))*180/pi())*60*1.1515*1.609344)) as distance from tbl_restaurant left join tbl_restaurant_photos on tbl_restaurant.restaurant_id = tbl_restaurant_photos.restaurant_id where tbl_restaurant.status=1 GROUP BY tbl_restaurant.restaurant_id HAVING distance <= 20 ORDER BY distance ASC");


if($query->num_rows()>0){
foreach($query->result_array() as $row) 
{
    $restaurant_id1=$row['restaurant_id'];
 
    $query1=$this->db->query("select * from tbl_week_rating where restaurant_id='$restaurant_id1' AND rate_date <= CAST('$today' as DATE) AND rate_date >= CAST('$last_date' as DATE) and going!=''");
    
    foreach($query1->result_array() as $row1) {
        
$query2=$this->db->query("select img from tbl_restaurant_photos where restaurant_id='$restaurant_id1'");
    
    foreach($query2->result_array() as $row2) {

    $row['restaurant_image']=$row2;
    }

    $new[]=$row;
    }
}

}



             $user = $new;
           // $user = $this->apim->getRowsJoin('tbl_restaurant.id as restaurant_id,tbl_restaurant.title as title,img,street,city,zipcode,lat,log,website,phone,average_star,open_time,close_time','tbl_restaurant','tbl_restaurant_photos','tbl_restaurant.restaurant_id = tbl_restaurant_photos.restaurant_id',$cond);
             // print_r($user);
             // die;

          if(!empty($user)){
            //set the response and exit
            $this->response([
                'status' => TRUE,
                'data' => $user,
            ], REST_Controller::HTTP_OK);
        }else{
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No user/restaurant were found.'
            ], REST_Controller::HTTP_OK);
        }
  
}

else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
       
    }

    /*****************************
        Purpose   : Get detail of a country or list of all country 
        method    : GET
        Parameter : {id}
        URL       : http://fanspace.yesitlabs.xyz/api/country
        Response  : JSON
    */ 
    public function country_get($id = 0) {
        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned
        $cond['status'] = 1;
        if(!empty($id)){
            $cond['id'] = $id;
        }
        $country = $this->apim->getRows('id,country_name','tbl_country',$cond);
        //check if the user data exists
        if(!empty($country)){
            //set the response and exit
            $this->response([
                'status' => TRUE,
                'data' => $country,
            ], REST_Controller::HTTP_OK);
        }else{
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No country were found.'
            ], REST_Controller::HTTP_OK);
        }
    }

    /*****************************
        Purpose   : Get list of all Special offer in a Deal 
        method    : GET
        Parameter : {id,lat,long}
        URL       : http://fanspace.yesitlabs.xyz/api/specialOffer
        Response  : JSON
    */ 
    public function specialOffer_post($id = 0,$latitude=0,$longitude=0) {
        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned
        $cond['tbl_deals.status'] = 1;

            @$id = $this->input->post('id');


        if(!empty($id)){
            $cond['tbl_deals.id'] = $this->input->post('id');

              $offer = $this->apim->getRowsJoin('*','tbl_deals','tbl_restaurant','tbl_deals.restaurant_id = tbl_restaurant.restaurant_id',$cond);
        }
        if(empty($id))
        {
           $latitude=$this->input->post('latitude');
             $longitude=$this->input->post('longitude');

            $query=$this->db->query("select *,((((acos(sin((".$latitude."*pi()/180)) * sin((`lat`*pi()/180))+cos((".$latitude."*pi()/180))*cos((`lat`*pi()/180)) * cos(((".$longitude."- `log`)*pi()/180))))*180/pi())*60*1.1515*1.609344)) as `distance` from tbl_restaurant join tbl_deals on tbl_deals.restaurant_id=tbl_restaurant.restaurant_id where tbl_restaurant.status='1' GROUP BY tbl_restaurant.restaurant_id HAVING distance <= 20 ORDER BY `distance` ASC");
            
            //echo $this->db->last_query();
            $offer = $query->result_array();

        }
       // $offer = $this->apim->getRows('deal_title,deal_description,start_date,end_date,t_c,img,deal_name','tbl_deals',$cond);
        
        

        //check if the user data exists
        if(!empty($offer)){
            //set the response and exit
            $this->response([
                'status' => TRUE,
                'data' => $offer,
            ], REST_Controller::HTTP_OK);
        }else{
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No Special Offer were found.'
            ], REST_Controller::HTTP_OK);
        }
    }

    /*****************************
        Purpose   : Deal By ID
        method    : POST [form-data]
        Parameter : { dealid* }
        URL       : http://fanspace.yesitlabs.xyz/Api/get_dealByid
        Response  : JSON
    */

    // public function get_dealByid_POST()
    // {
    //     if(!empty($this->input->post('dealid'))){
    //         if (!empty($this->input->post('dealid'))) {
    //              $data['tbl_deals.id'] = $this->input->post('dealid');
                
    //         }
     
    //     $res = $this->apim->getRowsJoin('*','tbl_deals','tbl_restaurant','tbl_deals.restaurant_id=tbl_restaurant.restaurant_id',$data);


    //         $retunuploadpath = base_url().'assets/images/';
    //         $res[0]['img'] = $retunuploadpath.$res[0]['img'];

    //         if($res){
    //             //set the response and exit
    //             $this->response([
    //                 'status' => true,
    //                 'data' => $res,
    //                 'message' => 'Deal Detail',
    //             ], REST_Controller::HTTP_OK); 
    //             exit();
    //         }
    //         else
    //         {
    //             //set the response and exit
    //             $this->response([
    //                 'status' => true,
    //                 'message' => 'No data found',
    //             ], REST_Controller::HTTP_OK); 
    //             exit();
    //         }   
    //     }
    //     else
    //     {
    //         //set the response and exit
    //         $this->response([
    //             'status' => false,
    //             'message' => 'Send complete information.',
    //         ], REST_Controller::HTTP_OK); 
    //         exit();
    //     }
    // }



    public function get_dealByid_POST()
    {
        if(!empty($this->input->post('dealid'))){
            if (!empty($this->input->post('dealid'))) {
                 $data['tbl_deals.id'] = $this->input->post('dealid');
                
            }
     
        $res = $this->apim->getRowsJoin('*','tbl_deals','tbl_restaurant','tbl_deals.restaurant_id=tbl_restaurant.restaurant_id',$data);

        
       $data1['user_id'] = $res[0]['restaurant_id'];

       $res_event = $this->apim->getRows('event_id','tbl_event',$data1);  

        $event_id = $res_event[0]['event_id'];


            $query1 = $this->db->query("select COUNT(going) from tbl_week_rating  where event_id='$event_id' and going!='' ");

            $query2 = $this->db->query("select COUNT(not_going) from tbl_week_rating  where event_id='$event_id' and not_going!='' ");

            $query3 = $this->db->query("select COUNT(may_be) from tbl_week_rating  where event_id='$event_id' and may_be!='' ");

            $count_going = $query1->result_array();
            $count_not_going = $query2->result_array();
            $count_may_be = $query3->result_array();


            $retunuploadpath = base_url().'assets/images/';
            $res[0]['img'] = $retunuploadpath.$res[0]['img'];

            if($res){
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data' => $res,
                    'going' => $count_going[0]['COUNT(going)'],
                    'not_going' =>$count_not_going[0]['COUNT(not_going)'],
                    'may_be' => $count_may_be[0]['COUNT(may_be)'],
                    'message' => 'Deal Detail',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'No data found',
                ], REST_Controller::HTTP_OK); 
                exit();
            }   
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }





    public function restaurantByid_POST()
    {
        if(!empty($this->input->post('restaurant_id'))){
            if (!empty($this->input->post('restaurant_id'))) {
                 $cond['restaurant_id'] = $this->input->post('restaurant_id');
                 $condi['restaurant_id'] = $this->input->post('restaurant_id');
            }
            

           
            //$res = $this->apim->getRows('*','tbl_user',$data);
            $res = $this->apim->getRows('restaurant_id,title,average_star,description,street,city,state,zipcode,open_time,close_time,lat,log,phone,','tbl_restaurant',$cond);
             $resimg = $this->apim->getRows('img','tbl_restaurant_photos',$condi);
             $resfood = $this->apim->getRowsJoin('food_type','tbl_restaurant_food_type','tbl_food','tbl_restaurant_food_type.food_type_id = tbl_food.food_id',$condi);
              $resbar = $this->apim->getRowsJoin('bar_type','tbl_restaurant_bar_type','tbl_bar','tbl_restaurant_bar_type.bar_type_id = tbl_bar.bar_id',$condi);

               $resmenu =  $this->apim->getRows('menuPdf','tbl_menu',$condi);

               // echo "<pre>";
               // print_r($res);
               // print_r($resimg);
               // print_r($resfood);
               // print_r($resbar);
               // print_r($resmenu);
               // die;
               $retunuploadpath = base_url().'assets/images/';

               for ($i=0; $i <sizeof($resimg) ; $i++) { 
                   
                   $resimg[$i]['img'] = $retunuploadpath.$resimg[$i]['img'];
               }
                
            if($res){
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data' => $res,
                     'photo' => $resimg,
                     'food' => $resfood,
                     'bar' => $resbar,
                      'menu' => $resmenu,
                    'message' => 'Restaurant Detail',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'No data found',
                ], REST_Controller::HTTP_OK); 
                exit();
            }   
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }

    /*****************************
        Purpose   : follow a user
        method    : POST [form-data]
        Parameter : { userId*, dealid* }
        URL       : http://fanspace.yesitlabs.xyz/Api/set_deals
        Response  : JSON
    */

    public function set_deals_POST()
    {
        if(!empty($this->input->post('userId')) && !empty($this->input->post('dealid'))){
            $data['user_id'] = $this->input->post('userId');
            $data['dealid'] = $this->input->post('dealid');

            $exist = $this->apim->getRowNum('tbl_favourite_deal',$data);
            if (!$exist) {
                $data['created'] = date('Y-m-d');
                $insert = $this->apim->insert('tbl_favourite_deal',$data);
                if($insert){
                    //set the response and exit
                    $this->response([
                        'status' => true,
                        'message' => 'Favourite Deal',
                    ], REST_Controller::HTTP_OK); 
                    exit();
                }
                else
                {
                    //set the response and exit
                    $this->response([
                        'status' => false,
                        'message' => 'Something goes wrong',
                    ], REST_Controller::HTTP_OK); 
                    exit();
                }
            }
            else
            {
                //set the response and exit
                    $this->response([
                        'status' => true,
                        'message' => 'Already Favourite Deal',
                    ], REST_Controller::HTTP_OK); 
                    exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }

    /*****************************
        Purpose   : remove my followed user
        method    : POST [form-data]
        Parameter : { userId*, dealid* }
        URL       : http://fanspace.yesitlabs.xyz/Api/remove_deals
        Response  : JSON
    */

    public function remove_deals_POST()
    {
        if(!empty($this->input->post('userId')) && !empty($this->input->post('dealid'))){
            $data['user_id'] = $this->input->post('userId');
            $data['dealid'] = $this->input->post('dealid');
            $delete = $this->apim->delete('tbl_favourite_deal',$data);
            if($delete){
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'Remove Favourite Deal Successfully',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'Some goes wrong, please try again!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }   
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }


    /*****************************
        Purpose   : following users list
        method    : POST [form-data]
        Parameter : { userId/dealid* }
        URL       : http://fanspace.yesitlabs.xyz/Api/get_deals
        Response  : JSON
    */

    public function get_deals_POST()
    {
        if(!empty($this->input->post('userId')) || !empty($this->input->post('dealid'))){
            if (!empty($this->input->post('userId'))) {
                 $data['user_id'] = $this->input->post('userId');
            }
            if (!empty($this->input->post('dealid'))) {
                 $data['dealid'] = $this->input->post('dealid');
            }
           
            //$res = $this->apim->getRowsJoin('*','tbl_deals',$data);

            $res = $this->apim->getRowsJoin('*','tbl_favourite_deal','tbl_deals','tbl_deals.id = tbl_favourite_deal.dealid',$data);

            foreach ($res as $key => $value) {   
                    $res[$key]['favourite_status'] = '1';
                }

            if($res){
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data' => $res,
                    'message' => 'Favourite Deal list',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'message' => 'No data found',
                ], REST_Controller::HTTP_OK); 
                exit();
            }   
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }



       /*****************************
        Purpose   : Banner List
        method    : POST [form-data]
        Parameter : {userid*, lat*, long*}
        URL       : http://fanspace.yesitlabs.xyz/Api/get_deals_by_lat_long
        Response  : JSON
    */

    public function get_deals_by_lat_long_POST()
    {
            // $today=date('Y-m-d');
            // $last_date=date('Y-m-d', strtotime('-7 days'));
            $data['userid'] = $this->input->post("userid");
            $latitude = $this->input->post("lat");  
            $longitude = $this->input->post("long");

             if(!empty($latitude) && !empty($longitude)){
             $cond['tbl_deals.status'] = "1";
             $cond['tbl_restaurant.lat'] = $latitude;
             $cond['tbl_restaurant.log'] = $longitude;
             
$retunuploadpath = base_url().'assets/images/';

if(!empty($data['userid']))
{

$query=$this->db->query("select restaurant_id,title,street,city,lat,log,zipcode,phone,average_star,tbl_states.state_name,((((acos(sin((".$latitude."*pi()/180)) * sin((`lat`*pi()/180))+cos((".$latitude."*pi()/180))*cos((`lat`*pi()/180)) * cos(((".$longitude."- `log`)*pi()/180))))*180/pi())*60*1.1515*1.609344)) as `distance` from tbl_restaurant left join tbl_states on tbl_states.state_id=tbl_restaurant.state where tbl_restaurant.status='1' GROUP BY tbl_restaurant.restaurant_id HAVING distance <= 20 ORDER BY `distance` ASC");
$query1=$query->result_array();


//print_r($query1);
foreach ($query1 as $key => $row) {

    $restaurant_id1=$row['restaurant_id'];
    $query2=$this->db->query("select deal_name,img,start_date,end_date from tbl_deals where restaurant_id='$restaurant_id1'");
   $query3=$query2->result_array();

   // foreach ($query3 as $key => $value) {
   //      $row['image']=$va;
   //  }
   //print_r($query3);
    for ($i=0; $i <sizeof($query3) ; $i++) { 
        
        $information_data[$i]['deal_name'] = $query3[$i]['deal_name'];  
        $information_data[$i]['img'] = $retunuploadpath.$query3[$i]['img'];  
        $information_data[$i]['start_date'] = $query3[$i]['start_date'];  
        $information_data[$i]['end_date'] = $query3[$i]['end_date'];
    }
  
}

 $new[]=$information_data;
}

// echo "<pre>";
// print_r($new);

foreach ($new as $key => $value) {
    
    $information = $value;
}


          if(!empty($information)){
            //set the response and exit
            $this->response([
                'status' => TRUE,
                'data' => $information,
                'message' => 'List of Deal Banner.'

            ], REST_Controller::HTTP_OK);
        }else{
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No Restaurant were found Nearby.'
            ], REST_Controller::HTTP_OK);
        }
      
    }

    else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
       
    }


      /*****************************
        Purpose   : Set Going and Not Going Status
        method    : POST [form-data]
        Parameter : { userId*, restaurant_id*, event_id*, choice*}
        URL       : http://fanspace.yesitlabs.xyz/Api/going_not_going
        Response  : JSON
    */
    public function going_not_going_post()
    {

        $data1['user_id'] = $this->input->post('userId');
        $data1['restaurant_id'] = $this->input->post('restaurant_id');
        $choice = $this->input->post('choice');
        $data1['event_id'] = $this->input->post('event_id');
       
        $data['event_id'] = $this->input->post('event_id');
         $data['user_id'] = $this->input->post('userId');
         $data['restaurant_id'] = $this->input->post('restaurant_id');

        if (!empty($data1['user_id']) && (!empty($data1['restaurant_id']) || !empty($data1['event_id']))) {
            $data1['rate_date'] = date('Y-m-d H:i:s');

            if($choice=="1")
            {
               $data1['going'] = $choice; 
            }
            elseif($choice=="2")
            {
               $data1['may_be'] = $choice; 
            }
            elseif($choice=="3")
            {
               $data1['not_going'] = $choice; 
            }
            else{
                $data1['not_going'] = $choice; 
            }

            $exist = $this->apim->getRowNum('tbl_week_rating',$data);
            if (!$exist) {
            $insert_id = $this->apim->insert('tbl_week_rating',$data1);

            if ($insert_id) {

                if($choice=="1")
            {
               $this->response([
                    'status' => true,
                    'data'  => $data['user_id'],
                    'message' => 'You are Going to Event!!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            elseif($choice=="2")
            {
               $this->response([
                    'status' => true,
                    'data'  => $data['user_id'],
                    'message' => 'You Choose Maybe ',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            elseif($choice=="3")
            {
               $this->response([
                    'status' => true,
                    'data'  => $data['user_id'],
                    'message' => 'You are Not Going to Event!!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }else
            {
               $this->response([
                    'status' => false,
                    'message' => 'Status Not Updated',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
                //set the response and exit
                
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Some goes wrong, please try again!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
             }
            else
            {
                //set the response and exit
                    $this->response([
                        'status' => true,
                        'message' => 'Already Status Updated',
                    ], REST_Controller::HTTP_OK); 
                    exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }



    /*****************************
        Purpose   : Turn on/off Notification
        method    : POST [form-data]
        Parameter : { userId*, notification*}
        URL       : http://fanspace.yesitlabs.xyz/Api/turn_on_off_notification
        Response  : JSON
    */

    public function turn_on_off_notification_POST()
    {

        if(!empty($this->input->post('userId')) && $this->input->post('notification') != '' )
        {
    
            $userid['user_id'] = $this->input->post('userId');
            $notification['notification'] = $this->input->post('notification');
            $response = $this->apim->update('tbl_user', $notification, $userid);

            $notify = intval($notification['notification']);
        //die;   
                if ($notify == 1)
                {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data' => $response,
                    'message' => 'Notifications ON',
                ], REST_Controller::HTTP_OK); 
                exit();
                }
    
            elseif($notify == 0){
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data' => $response,
                    'message' => 'Notifications OFF',
                ], REST_Controller::HTTP_OK); 
                exit();

                }
                else{
                                    //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Wrong Input',
                ], REST_Controller::HTTP_OK); 
                exit();
                }
  
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }

       /*****************************
        Purpose   : Favourite Events Details
        method    : POST [form-data]
        Parameter : { userId*}
        URL       : http://fanspace.yesitlabs.xyz/Api/fav_events
        Response  : JSON
    */

    public function fav_events_POST()
    {

        $data['user_id'] = $this->input->post('userId');

        if(!empty($this->input->post('userId')))
        {
    
            $cond['tbl_favourite_events.fav_event_userid'] = $data['user_id'];

                $response = $this->apim->getRowsJoin('tbl_event.event_id as event_id,tbl_event.event_name as event_name,tbl_event.event_date as start_date,tbl_event.event_time as open_time,tbl_event.event_close_time as close_time,tbl_event.event_id as event_id,tbl_event.event_imsge as image,tbl_event.lat as lat,tbl_event.log as long,tbl_event.event_description as event_details,tbl_event.event_terms as event_terms,tbl_favourite_events.fav_status as fav_status','tbl_favourite_events','tbl_event','tbl_favourite_events.tbl_favourite_eventid = tbl_event.event_id',$cond);
              
              $retunuploadpath = base_url().'assets/images/';

               for ($i=0; $i <sizeof($response) ; $i++) { 
                   
                   $response[$i]['image'] = $retunuploadpath.$response[$i]['image'];
               }


                if ($response)
                {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data' => $response,
                    'message' => 'Favourite Events',
                ], REST_Controller::HTTP_OK); 
                exit();
                }
                else{
                
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'There is No Favourite Event For you',
                ], REST_Controller::HTTP_OK); 
                exit();
                }
  
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }

       /*****************************
        Purpose   : Restaurant List
        method    : POST [form-data]
        Parameter : { userId*}
        URL       : http://fanspace.yesitlabs.xyz/Api/restaurants_nearby
        Response  : JSON
    */

    public function restaurants_nearby_POST()
    {

        $data['user_id'] = $this->input->post('userId');

        if(!empty($this->input->post('userId')))
        {
    
            $lat_log = $this->apim->getRow('lat,lng','tbl_user_address',$data);
    
                $latitude = $lat_log['lat'];
                $longitude = $lat_log['lng'];
                $min_range = '0';
                $min_range = '25';


    $queryy=$this->db->query("select restaurant_id,title,street,city,zipcode,tbl_user.user_avatar,tbl_states.state_name,((((acos(sin((".$latitude."*pi()/180)) * sin((`lat`*pi()/180))+cos((".$latitude."*pi()/180))*cos((`lat`*pi()/180)) * cos(((".$longitude."- `log`)*pi()/180))))*180/pi())*60*1.1515*1.609344)) as `distance` from tbl_restaurant left join tbl_states on tbl_states.state_id=tbl_restaurant.state Left join tbl_user on tbl_user.user_id = tbl_restaurant.restaurant_id where tbl_restaurant.status='1' GROUP BY tbl_restaurant.restaurant_id HAVING distance <= 20 ORDER BY `distance` ASC");
    
    
        //echo $this->db->last_query();

        $response=$queryy->result_array();

            // echo "<pre>";
            // print_r($response);
            // die;
              $retunuploadpath = base_url().'assets/images/';

               for ($i=0; $i <sizeof($response) ; $i++) { 
                   
                   $response[$i]['user_avatar'] = $retunuploadpath.$response[$i]['user_avatar'];
               }


                if ($response)
                {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data' => $response,
                    'message' => 'Favourite Events',
                ], REST_Controller::HTTP_OK); 
                exit();
                }
                else{
                
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'There is No Favourite Event For you',
                ], REST_Controller::HTTP_OK); 
                exit();
                }
  
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }



      /*****************************
        Purpose   : Update Going and Not Going Status
        method    : POST [form-data]
        Parameter : { userId*, restaurant_id*, event_id*, choice*}
        URL       : http://fanspace.yesitlabs.xyz/Api/update_going_status
        Response  : JSON
    */
    public function update_going_status_post()
    {

        $data1['user_id'] = $this->input->post('userId');
        $data1['restaurant_id'] = $this->input->post('restaurant_id');
        $choice = $this->input->post('choice');
        $data1['event_id'] = $this->input->post('event_id');
       
        $data['event_id'] = $this->input->post('event_id');
         $data['user_id'] = $this->input->post('userId');
         $data['restaurant_id'] = $this->input->post('restaurant_id');

        if (!empty($data1['user_id']) && (!empty($data1['restaurant_id']) || !empty($data1['event_id']))) {
            if($choice=="1")
            {
               $data1['going'] = $choice;
               $exist = $this->apim->getRowNum('tbl_week_rating',$data1); 
            if (!$exist) {
            $data1['may_be'] = '';
            $data1['not_going'] = '';
            $data1['rate_date'] = date('Y-m-d');
            $insert_id = $this->apim->update('tbl_week_rating',$data1,$data);

                    if ($insert_id) 
                    {
                       $this->response([
                            'status' => true,
                            'data'  => $data['user_id'],
                            'message' => 'Your are Going to Event!!',
                        ], REST_Controller::HTTP_OK); 
                        exit(); 
                    }
                    else
                    {
                        //set the response and exit
                        $this->response([
                            'status' => false,
                            'message' => 'Some goes wrong, please try again!',
                        ], REST_Controller::HTTP_OK); 
                        exit();
                    }

                }
                else
                {
                    //set the response and exit
                        $this->response([
                            'status' => true,
                            'message' => 'Already Status Updated',
                        ], REST_Controller::HTTP_OK); 
                        exit();
                }
            }
            elseif($choice=="2")
            {
               $data1['may_be'] = $choice;
               print_r($data1);

               $exist = $this->apim->getRowNum('tbl_week_rating',$data1); 
            if (!$exist) {

            $data1['going'] = '';
            $data1['not_going'] = '';
            $data1['rate_date'] = date('Y-m-d');
            $insert_id = $this->apim->update('tbl_week_rating',$data1,$data);
            
                    if ($insert_id) 
                    {
                       $this->response([
                            'status' => true,
                            'data'  => $data['user_id'],
                            'message' => 'Your Status changed to May Be!!',
                        ], REST_Controller::HTTP_OK); 
                        exit(); 
                    }
                    else
                    {
                        //set the response and exit
                        $this->response([
                            'status' => false,
                            'message' => 'Some goes wrong, please try again!',
                        ], REST_Controller::HTTP_OK); 
                        exit();
                    }

                }
                else
                {
                    //set the response and exit
                        $this->response([
                            'status' => true,
                            'message' => 'Already Status Updated',
                        ], REST_Controller::HTTP_OK); 
                        exit();
                }
            }
            elseif($choice=="3")
            {
               $data1['not_going'] = $choice;
               $exist = $this->apim->getRowNum('tbl_week_rating',$data1); 
            if (!$exist) {
            $data1['may_be'] = '';
            $data1['going'] = '';
            $data1['rate_date'] = date('Y-m-d');
            $insert_id = $this->apim->update('tbl_week_rating',$data1,$data);

                    if ($insert_id) 
                    {
                       $this->response([
                            'status' => true,
                            'data'  => $data['user_id'],
                            'message' => 'Your are Not Going to Event!!',
                        ], REST_Controller::HTTP_OK); 
                        exit(); 
                    }
                    else
                    {
                        //set the response and exit
                        $this->response([
                            'status' => false,
                            'message' => 'Some goes wrong, please try again!',
                        ], REST_Controller::HTTP_OK); 
                        exit();
                    }

                }
                else
                {
                    //set the response and exit
                        $this->response([
                            'status' => true,
                            'message' => 'Already Status Updated',
                        ], REST_Controller::HTTP_OK); 
                        exit();
                } 
            }
            else{
                        //set the response and exit
                        $this->response([
                            'status' => false,
                            'message' => 'Some goes wrong, please try again!',
                        ], REST_Controller::HTTP_OK); 
                        exit();
            }


        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }


      /*****************************
        Purpose   : Update Going and Not Going Status
        method    : POST [form-data]
        Parameter : { event_id*}
        URL       : http://fanspace.yesitlabs.xyz/Api/set_fav_event
        Response  : JSON
    */




        public function set_fav_event_post()
    {
        $data1['event_id'] = $this->input->post('event_id');
        
        if (!empty($data1['event_id'])) {
            $tbl_event_data = $this->apim->getRow('event_id,user_id','tbl_event',$data1);

                if(!empty($tbl_event_data))
                {
        $data['fav_event_userid'] = $tbl_event_data['user_id'];
        $data['tbl_favourite_eventid'] = $tbl_event_data['event_id'];
        $data['fav_status'] = 1;
                
                $exist = $this->apim->getRowNum('tbl_favourite_events',$data);

                }
            if (!$exist) {
            $insert_id = $this->apim->insert('tbl_favourite_events',$data);

            if ($insert_id) {
                //set the response and exit
                $this->response([
                    'status' => true,
                    'data'  => $data,
                    'message' => 'Event set to Favourites',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Some goes wrong, please try again!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
             }
            else
            {
                //set the response and exit
                    $this->response([
                        'status' => true,
                        'message' => 'Already Status Updated',
                    ], REST_Controller::HTTP_OK); 
                    exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }



      /*****************************
        Purpose   : Update Going and Not Going Status
        method    : POST [form-data]
        Parameter : {restaurant_id*}
        URL       : http://fanspace.yesitlabs.xyz/Api/get_checkin_restaurant
        Response  : JSON
    */




        public function get_checkin_restaurant_post()
    {
        $data1['tbl_restaurant_crowd.restaurant_id'] = $this->input->post('restaurant_id');
        $data1['tbl_crowd.create_date'] = date('Y-m-d');

        if (!empty($data1['tbl_restaurant_crowd.restaurant_id'])) {
            
         
            $result = $this->apim->getRowJoin('crowd_count,create_date','tbl_restaurant_crowd','tbl_crowd','tbl_restaurant_crowd.crowd_id = tbl_crowd.crowd_id',$data1);

            if ($result) {
          
                $this->response([
                    'status' => true,
                    'data'  => $result,
                    'message' => 'No. of Today`s Check-in',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Sorry No Crown Found for this Restaurant!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }



      /*****************************
        Purpose   : Update Going and Not Going Status
        method    : POST [form-data]
        Parameter : {userId*}
        URL       : http://fanspace.yesitlabs.xyz/Api/get_checkin_user
        Response  : JSON
    */




        public function get_checkin_user_post()
    {
        $data1['tbl_week_rating.user_id'] = $this->input->post('userId');
        $data1['tbl_week_rating.going'] = 1;

        if (!empty($data1['tbl_week_rating.user_id'])) {
            

            $result = $this->apim->getRowsJoin('*','tbl_week_rating','tbl_restaurant','tbl_week_rating.restaurant_id = tbl_restaurant.restaurant_id',$data1);

            if ($result) {
          
                $this->response([
                    'status' => true,
                    'data'  => $result,
                    'message' => ' Check-in Restaurant Lists',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Sorry No Crown Found for this Restaurant!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }


      /*****************************
        Purpose   : Get Restaurants by Search
        method    : POST [form-data]
        Parameter : {search*}
        URL       : http://fanspace.yesitlabs.xyz/Api/get_restaurant_by_search
        Response  : JSON
    */




        public function get_restaurant_by_search_post()
    {
        $search = $this->input->post('search');
        $status = 1;

        $retunuploadpath = base_url().'assets/images/';
        if (!empty($search)) {
            

            $query = $this->db->query("SELECT * FROM tbl_restaurant WHERE title LIKE '%$search%' or  street LIKE '%$search%' or city LIKE '%$search%' AND status = 1");
            $result = $query->result_array();

            //print_r($result);
            for ($i=0; $i <sizeof($result) ; $i++) 
            { 
                $user_id = $result[$i]['user_id'];
            $query1 = $this->db->query("SELECT user_avatar FROM tbl_user WHERE user_id = $user_id  AND status = 1");
            $result1 = $query1->row_array();

                //print_r($result1);
                $result[$i]['img'] = $retunuploadpath.$result1['user_avatar'];

            }
            

            if ($result) {
          
                $this->response([
                    'status' => true,
                    'data'  => $result,
                    'message' => 'Restaurant Lists based on your search',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Sorry No Restaurant Found!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }


      /*****************************
        Purpose   : Get Users by search
        method    : POST [form-data]
        Parameter : {search*}
        URL       : http://fanspace.yesitlabs.xyz/Api/get_user_by_search
        Response  : JSON
    */




        public function get_user_by_search_post()
    {
        $search = $this->input->post('search');

        $retunuploadpath = base_url().'assets/images/';

        if (!empty($search)) {
            
            $query = $this->db->query("SELECT * FROM tbl_user WHERE first_name LIKE '$search%' or  last_name LIKE '$search%' AND status = 1 AND user_role ='User'");

            $result = $query->result_array();
            //print_r($result);
            for ($i=0; $i <sizeof($result) ; $i++) 
            { 
                //print_r($result1);
                if(!empty($result[$i]['user_avatar'])){
                $result[$i]['user_avatar'] = $retunuploadpath.$result[$i]['user_avatar'];
                    }
            }

            if ($result) {
          
                $this->response([
                    'status' => true,
                    'data'  => $result,
                    'message' => 'Users Lists search',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            else
            {
                //set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Sorry No User Name like  this Found!',
                ], REST_Controller::HTTP_OK); 
                exit();
            }
            
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Send complete information.',
            ], REST_Controller::HTTP_OK); 
            exit();
        }
    }


    /*****************************
        Purpose   : Get list of all Coupans
        method    : POST
        Parameter : {id,latitude,longitude}
        URL       : http://fanspace.yesitlabs.xyz/api/coupan_list
        Response  : JSON
    */ 
    public function coupan_list_post() {

        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned

            $data['id'] = 6;
            $data['status'] = 0;

            $path = base_url().'assets/images/';
            $res = $this->apim->getRow('banner_img','tbl_banner',$data);

            $res['banner_img'] = $path.$res['banner_img'];
           
            
          //             for ($i=0; $i <sizeof($res) ; $i++) 
          // { 
              
          //     $match[$i]['banner_img'] = $path.$res[$i]['banner_img'];
            
          // }

        $cond['tbl_coupon.status'] = 1;

            @$coupon_id = $this->input->post('coupon_id');


        if(!empty($coupon_id)){
            $cond['tbl_coupon.coupon_id'] = $this->input->post('coupon_id');

              $offer = $this->apim->getRowsJoin('*','tbl_coupon','tbl_restaurant','tbl_coupon.created_by = tbl_restaurant.restaurant_id',$cond);
        }

        if(empty($coupon_id))
        {
           $latitude=$this->input->post('latitude');
             $longitude=$this->input->post('longitude');

            $query=$this->db->query("select *,((((acos(sin((".$latitude."*pi()/180)) * sin((`lat`*pi()/180))+cos((".$latitude."*pi()/180))*cos((`lat`*pi()/180)) * cos(((".$longitude."- `log`)*pi()/180))))*180/pi())*60*1.1515*1.609344)) as `distance` from tbl_restaurant join tbl_coupon on tbl_coupon.created_by = tbl_restaurant.restaurant_id where tbl_coupon.status='1' GROUP BY tbl_restaurant.restaurant_id HAVING distance <= 20 ORDER BY `distance` ASC");
            
            //echo $this->db->last_query();
            $offer = $query->result_array();

        }
        $retunuploadpath = base_url().'assets/images/';
               for ($i=0; $i <sizeof($offer) ; $i++) { 
                   
                   $offer[$i]['coupon_img'] = $retunuploadpath.$offer[$i]['coupon_img'];
               }


        //check if the user data exists
        if(!empty($offer)){
            //set the response and exit
            $this->response([
                'status' => TRUE,
                'data' => $offer,
                'banner' => $res['banner_img'],
            ], REST_Controller::HTTP_OK);
        }else{
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No Special Offer were found.'
            ], REST_Controller::HTTP_OK);
        }
    }

   /*****************************
        Purpose   : APIs for ALEXA
        method    : GET
        Parameter : {query*}
        URL       : http://fanspace.yesitlabs.xyz/api/alexa/
        Response  : JSON
    */ 

    public function alexa_get($id = 0) 
    {
        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned

  if(!empty($id)){

            $cond['order_number'] = $id;

            $status = $this->apim->getRow('status','alexa_order',$cond);
        
            $message = 'Status Of Your Order <b>'.$cond['order_number'].'</b> is <b>'.$status['status'].'.</b>';
                $this->load->library('email');
                $this->email->set_newline("\r\n");
                $this->email->set_mailtype("html");
                $this->email->from('alexa@yesitlabs.com'); // change it to yours
                $this->email->to('testnazey@yopmail.com');// change it to yours
                // $this->email->subject('Alexa- Status of Order '.$cond['order_number']);
                // $this->email->message($message);
                // $send_status=$this->email->send();


            if(!empty($status)){

        $msg = "Status Of Your Order ".$cond['order_number']." is ".$status['status'];
            //$msg = "Your otp for BCme app is $code ".$random_number;
            $send_status = $this->twilio->sms('+12512548900','+919548974198',$msg);

                $this->email->subject('Alexa- Status of Order '.$cond['order_number']);
                $this->email->message($message);
                $send_status=$this->email->send();

                //set the response and exit
                $this->response([
                    'status' => TRUE,
                    'data' => $status,
                ], REST_Controller::HTTP_OK);
            }else{
                $this->email->subject('Alexa- Status of Order '.$cond['order_number']);
                $this->email->message('Your Order <b>'.$cond['order_number'].' is not in database </b>');
                $send_status=$this->email->send();
                //set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Order Number is not in list'
                ], REST_Controller::HTTP_OK);
            }
        }
        else
        {
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Please send at least one parameter.'
            ], REST_Controller::HTTP_OK);
        }
    }


}

?>