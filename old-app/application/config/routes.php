<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Admin_controller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['admin_dashboard'] ='Admin_controller/admin_dashboard';
$route['admin_profile'] ='Admin_controller/admin_profile';
$route['logout'] ='Admin_controller/logout';
$route['states_settings/(:any)'] ='Admin_controller/states_settings/$1';
$route['states_settings'] ='Admin_controller/states_settings';
$route['social_link'] ='Admin_controller/social_link';
$route['admin_change_password'] ='Admin_controller/admin_change_password';
$route['users'] ='Admin_controller/users';
$route['users/(:any)'] ='Admin_controller/users/$1';
$route['restaurant'] ='Admin_controller/restaurant';
$route['restaurant/(:any)'] ='Admin_controller/restaurant/$1';
$route['add_restaurant'] ='Admin_controller/add_restaurant';
$route['total_food'] ='Admin_controller/total_food';
$route['total_food/(:any)'] ='Admin_controller/total_food/$1';
$route['total_music_type'] ='Admin_controller/total_music_type';
$route['total_music_type/(:any)'] ='Admin_controller/total_music_type/$1';
$route['total_bar_type'] ='Admin_controller/total_bar_type';
$route['total_bar_type/(:any)'] ='Admin_controller/total_bar_type/$1';
$route['total_amenities_type'] ='Admin_controller/total_amenities_type';
$route['total_amenities_type/(:any)'] ='Admin_controller/total_amenities_type/$1';
$route['total_eatery_type'] ='Admin_controller/total_eatery_type';
$route['total_eatery_type/(:any)'] ='Admin_controller/total_eatery_type/$1';
$route['total_crowd_type'] ='Admin_controller/total_crowd_type';
$route['total_crowd_type/(:any)'] ='Admin_controller/total_crowd_type/$1';
$route['total_coupon'] ='Admin_controller/total_coupon';
$route['total_coupon/(:any)'] ='Admin_controller/total_coupon/$1';
$route['member_ship_plan'] ='Admin_controller/member_ship_plan';
$route['admin_forgot_password/(:any)'] ='Admin_controller/admin_forgot_password/$1';
$route['registration'] ='User_controller/registration';
$route['total_events'] ='User_controller/total_events';
$route['total_events/(:any)'] ='User_controller/total_events/$1';
$route['coupon'] ='User_controller/coupon';
$route['coupon/(:any)'] ='User_controller/coupon/$1';
$route['photo_gallery'] ='User_controller/photo_gallery';
$route['profile'] ='User_controller/profile';
$route['change_password'] ='User_controller/change_password';
$route['setting'] ='Admin_controller/setting';
$route['feedback'] ='Admin_controller/feedback';
$route['feedback/(:any)'] ='Admin_controller/feedback/$1';

$route['country_settings'] ='Admin_controller/country_settings';
$route['country_settings/(:any)'] ='Admin_controller/country_settings/$1';

$route['city_settings'] ='Admin_controller/city_settings';
$route['city_settings/(:any)'] ='Admin_controller/city_settings/$1';

$route['test'] ='User_controller/test';

$route['user_feedback'] ='User_controller/user_feedback';
$route['subscription'] ='User_controller/subscription';
$route['user_feedback/(:any)'] ='User_controller/user_feedback/$1';
$route['subscription/(:any)'] ='User_controller/subscription/$1';
$route['Membership'] ='Admin_controller/Membership';
$route['Membership/(:any)'] ='Admin_controller/Membership/$1';

$route['test'] ='Admin_controller/test';
$route['Promote'] ='Admin_controller/Promote';
$route['Promote_Type'] ='Admin_controller/Promote_Type';
$route['transaction'] ='Admin_controller/transaction';
$route['transaction/(:any)'] ='Admin_controller/transaction/$1';
$route['claimed_restaurant'] ='Admin_controller/claimed_restaurant';
$route['subscription_plan'] ='Admin_controller/subscription_plan';

$route['registration_setting'] ='User_controller/registration_setting';
$route['deals'] ='User_controller/deals';
$route['deal_promote'] ='User_controller/deal_promote';
$route['banner_promote'] ='User_controller/banner_promote';
$route['clicks'] ='User_controller/clicks';
$route['views'] ='User_controller/viewdata';
$route['deals_redeemed'] ='User_controller/deals_redeemed';
$route['about_us'] ='User_controller/about_us';
$route['terms'] ='User_controller/t_c';
$route['policy'] ='User_controller/privacy_policy';
$route['contact_us'] ='User_controller/contact_us';