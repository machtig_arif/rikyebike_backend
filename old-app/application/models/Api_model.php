<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class api_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        
        //load database library
        $this->load->database();
    }


    // function slug($slug){
    //     $this->db->select('data');
    //     $this->db->from('config');
    //     $this->db->where(array('slug' => $slug));
    //     $query = $this->db->get();
    //     return $query->row_array();
    // }

    /*
     * Fetch user data
     */
    function getRows($column='*',$tbl,$condition=''){
            $this->db->select($column);
            $this->db->from($tbl);
            if(!empty($condition)){
            $this->db->where($condition);
            }
            $query = $this->db->get();
            return $query->result_array();
    }

    function getRowsSql($sql){
            $query = $this->db->query($sql);
            return $query->result_array();
    }

    function getRow($column='*',$tbl,$condition=''){
        $this->db->select($column);
        $this->db->from($tbl);
        if(!empty($condition)){
        $this->db->where($condition);
        }
        $query = $this->db->get();
        return $query->row_array();
    }

    function getRowSql($sql){
            $query = $this->db->query($sql);
            return $query->row_array();
    }



    function getRowNum($tbl,$condition){
            $this->db->select('*');
            $this->db->from($tbl);
            if(!empty($condition)){
            $this->db->where($condition);
            }
            $query = $this->db->get();
            return $query->num_rows();
    }

    /*
     * Insert user data
     */
    public function insert($tbl,$data = array()) {
        $insert = $this->db->insert($tbl, $data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }


    function getRowsJoin($column='*',$tbl1,$tbl2,$joinCondition,$condition){
        
        $this->db->select($column);
        $this->db->from($tbl1);
        $this->db->join($tbl2,$joinCondition,'left');
        if ($condition) {
            $this->db->where($condition);
        }
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }

    function getRowJoin($column='*',$tbl1,$tbl2,$joinCondition,$condition){
        
        $this->db->select($column);
        $this->db->from($tbl1);
        $this->db->join($tbl2,$joinCondition,'left');
        if ($condition) {
            $this->db->where($condition);
        }
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->row_array();
    }

    /*
     * Delete user data
     */
    public function delete($tbl_name, $data){
        $delete = $this->db->delete($tbl_name,$data);
        return $delete?true:false;
    }

    public function update($tbl, $data, $cond) {
        if (!empty($data)) {
            $update = $this->db->update($tbl, $data, $cond);
            return $update ? true : false;
        } else {
            return false;
        }
    }

    public function getRowJoin_new($cond)
    {
     $this->db->select("*");
     $this->db->from('tbl_user as u');
     $this->db->join('tbl_user_address as b','b.user_id=u.user_id');
     $this->db->where('u.user_id',$cond);
    $query = $this->db->get();
    return $query->row_array();
    }

}
?>