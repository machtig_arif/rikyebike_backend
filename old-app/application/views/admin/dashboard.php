<?php $this->load->view('admin/include/header'); ?>

    <div class="main-container ace-save-state" id="main-container">
      <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
      </script>

      <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <script type="text/javascript">
          try{ace.settings.loadState('sidebar')}catch(e){}
        </script>

       <?php $this->load->view('admin/include/navigation'); ?>

    

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
          <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
      </div>

      <div class="main-content">
        <div class="main-content-inner">
          <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
              <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Home</a>
              </li>
              <li class="active">Dashboard</li>
            </ul><!-- /.breadcrumb -->

          
          </div>

          <div class="page-content">
           

            <div class="page-header">
              <h1>
                Dashboard
                
              </h1>
            </div><!-- /.page-header -->

            <div class="row">
              <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->

            <?php if ( !empty($this->session->flashdata('msgs'))) {
              ?>
                <div class="alert alert-block alert-success">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                  Welcome to
                  <strong class="green">
                    FanSpace ADMINISTRATOR DASHBOARD
                   
                  </strong>
 
                </div>

                <?php
          } ?>

                  <div class="row">
    <div class="col-sm-3 col-md-3 " style="margin-bottom: 10px;">
      <div class="card text-white bg-primary o-hidden h-100 box" >
        <div class="card-body box1">
          <div class="card-body-icon box2"> <i style="font-size: inherit;" class="fa fa-user"></i> </div>
          <div class="mr-5"><?php echo $user_count ?> Total User </div>
        </div>
        <a  class="card-footer text-white clearfix small z-1 mr-5" href="<?php echo site_url('users') ?>"> <span  class="float-left detail">View Details</span> <span class="float-right"> <i class="fa fa-angle-right"></i> </span> </a> </div>
    </div>


    <div class="col-sm-3 col-md-3" style="margin-bottom: 10px; ">
      <div class="card text-white bg-warning o-hidden h-100 box" >
        <div class="card-body box1">
          <div class="card-body-icon box2"> <i class="fa fa-user"></i> </div>
          <div class="mr-5"><?php echo $restaurant_count ?> Restaurant Manager </div>
        </div>
        <a  class="card-footer text-white clearfix small z-1 mr-5" href="<?php echo site_url('restaurant') ?>"> <span class="float-left detail">View Details</span> <span class="float-right"> <i class="fa fa-angle-right"></i> </span> </a> </div>
    </div>
    <div class="col-sm-3 col-md-3" style="margin-bottom: 10px;">
      <div class="card text-white bg-success o-hidden h-100 box" >
        <div class="card-body box1">
          <div class="card-body-icon box2"> <i class="fa fa-file"></i> </div>
          <div class="mr-5"><?php echo $food_count ?>  Food Type Manager </div>
        </div>
        <a class="card-footer text-white clearfix small z-1 mr-5" href="<?php echo site_url('total_food'); ?>"> <span class="float-left detail">View Details</span> <span class="float-right"> <i class="fa fa-angle-right"></i> </span> </a> </div>
    </div>
    <div class="col-sm-3 col-md-3 " style="margin-bottom: 10px;">
      <div class="card text-white bg-danger o-hidden h-100 box" >
        <div class="card-body  box1">
          <div class="card-body-icon box2"> <i class="fa fa-fw fa-list"></i> </div>
          <div class="mr-5"><?php echo $music_genre_count ?>   Eatery type Manager</div>
        </div>
        <a class="card-footer text-white clearfix small z-1 mr-5" href="<?php echo site_url('total_eatery_type'); ?>"> <span class="float-left detail">View Details</span> <span class="float-right"> <i class="fa fa-angle-right"></i> </span> </a> </div>
    </div>

    <!-- <div class="col-sm-3 col-md-3 " style="margin-bottom: 10px;">
      <div class="card text-white bg-danger o-hidden h-100 box" >
        <div class="card-body  box1">
          <div class="card-body-icon box2"> <i class="fa fa-fw fa-list"></i> </div>
          <div class="mr-5"> <?php echo $bar_count ?>  Bar Type Manager</div>
        </div>
        <a class="card-footer text-white clearfix small z-1 mr-5" href="<?php echo site_url('total_bar_type'); ?>"> <span class="float-left detail">View Details</span> <span class="float-right"> <i class="fa fa-angle-right"></i> </span> </a> </div>
    </div> -->

    <div class="col-sm-3 col-md-3 " style="margin-bottom: 10px;">
      <div class="card text-white bg-danger o-hidden h-100 box" >
        <div class="card-body  box1">
          <div class="card-body-icon box2"> <i class="fa fa-fw fa-list"></i> </div>
          <div class="mr-5"> <?php echo $amenities_count ?>  Amenities Manager</div>
        </div>
        <a class="card-footer text-white clearfix small z-1 mr-5" href="<?php echo site_url('total_amenities_type'); ?>"> <span class="float-left detail">View Details</span> <span class="float-right"> <i class="fa fa-angle-right"></i> </span> </a> </div>
    </div>

   <!--  <div class="col-sm-3 col-md-3 " style="margin-bottom: 10px;">
      <div class="card text-white bg-danger o-hidden h-100 box" >
        <div class="card-body  box1">
          <div class="card-body-icon box2"> <i class="fa fa-fw fa-list"></i> </div>
          <div class="mr-5"> <?php echo $music_genre_count ?>  Music Genre Manager</div>
        </div>
        <a class="card-footer text-white clearfix small z-1 mr-5" href="<?php echo site_url('total_music_genre_type'); ?>"> <span class="float-left detail">View Details</span> <span class="float-right"> <i class="fa fa-angle-right"></i> </span> </a> </div>
    </div> -->

    <!-- <div class="col-sm-3 col-md-3 " style="margin-bottom: 10px;">
      <div class="card text-white bg-danger o-hidden h-100 box" >
        <div class="card-body  box1">
          <div class="card-body-icon box2"> <i class="fa fa-fw fa-list"></i> </div>
          <div class="mr-5"> <?php echo $crowd_count ?>  Average Crowd Manager</div>
        </div>
        <a class="card-footer text-white clearfix small z-1 mr-5" href="<?php echo site_url('total_crowd_type'); ?>"> <span class="float-left detail">View Details</span> <span class="float-right"> <i class="fa fa-angle-right"></i> </span> </a> </div>
    </div> -->

    <div class="col-sm-3 col-md-3 " style="margin-bottom: 10px;">
      <div class="card text-white bg-danger o-hidden h-100 box" >
        <div class="card-body  box1">
          <div class="card-body-icon box2"> <i class="fa fa-fw fa-list"></i> </div>
          <div class="mr-5">  <?php echo $coupon_count ?>  Coupon Manager</div>
        </div>
        <a class="card-footer text-white clearfix small z-1 mr-5" href="<?php echo site_url('total_coupon'); ?>"> <span class="float-left detail">View Details</span> <span class="float-right"> <i class="fa fa-angle-right"></i> </span> </a> </div>
    </div>

     <!--  <div class="col-sm-3 col-md-3 " style="margin-bottom: 10px;">
      <div class="card text-white bg-danger o-hidden h-100 box" >
        <div class="card-body  box1">
          <div class="card-body-icon box2"> <i class="fa fa-fw fa-list"></i> </div>
          <div class="mr-5">   Member ship Plan</div>
        </div>
        <a class="card-footer text-white clearfix small z-1 mr-5" href="<?php //echo site_url('report'); ?>"> <span class="float-left detail">View Details</span> <span class="float-right"> <i class="fa fa-angle-right"></i> </span> </a> </div>
    </div> -->

   <!--   <div class="col-sm-3 col-md-3 " style="margin-bottom: 10px;">
      <div class="card text-white bg-danger o-hidden h-100 box" >
        <div class="card-body  box1">
          <div class="card-body-icon box2"> <i class="fa fa-fw fa-list"></i> </div>
          <div class="mr-5"> States Settings</div>
        </div>
        <a class="card-footer text-white clearfix small z-1 mr-5" href="<?php //echo site_url('report'); ?>"> <span class="float-left detail">View Details</span> <span class="float-right"> <i class="fa fa-angle-right"></i> </span> </a> </div>
    </div> -->

    


    

  </div>

      

             


                <!-- PAGE CONTENT ENDS -->
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.page-content -->
        </div>
      </div><!-- /.main-content -->

    <?php $this->load->view('admin/include/footer'); ?> 
