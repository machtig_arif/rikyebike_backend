<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Login Page - Ace Admin</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/ace.min.css" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	</head>

<body>
<div class="wrapper web_bg">

<?php 
if($this->session->flashdata('msg')!='')
{
?>
 <div class="alert alert-block alert-success" style="text-align: center;margin-top: 20px;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $this->session->flashdata('msg'); ?>
 
                </div>
<?php
}
?>
<div class="space50"> </div> 

<div class="container">
<div class="row">
<div class="col-md-6 col-md-offset-3">
<div class="loginpanel panel panel-default border0" style="margin-top: 30px;background: #000;">
<div class="panel-heading font18b"> Reset Password </div>
<div class="panel-body">
<p class="text-center"><img style="margin-top: 15px;" src="<?php echo base_url()?>assets/images/logo.png"></p>
<form  action="" method="post">
<div class="form">

<div class="form-group">
<div class="cols-sm-10">
<div class="input-group">
<span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
<input type="password" class="form-control" name="new_password" id="new_password"  placeholder="new Password"/>
</div>
</div>
</div>

<div class="form-group">
<div class="cols-sm-10">
<div class="input-group">
<span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
<input type="password" class="form-control" name="conform_password" id="conform_password"  placeholder="Confirm Password"/>
</div>
</div>
</div>

<p> <a href="#" role="button"><input type="submit"  class="btn btn-default loginbtn btn-group-justified" id="submit" name="submit" value="Reset Password"> </a> </p>
<div class="pad15"></div>
</div>
</form>
</div>
</div>
</div><!-- col-md-6 -->  
</div><!-- row -->   				
</div>

</div><!-- rightpanel -->

</section>




 <?php $this->load->view('admin/include/footer'); ?> 

</body>
</html>
