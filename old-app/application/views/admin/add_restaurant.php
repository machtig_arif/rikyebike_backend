<?php $this->load->view('admin/include/header'); ?>

		<div class="main-container ace-save-state" id="main-container">
			

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				
 <?php $this->load->view('admin/include/navigation'); ?>

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('admin_dashboard') ?>">Home</a>
							</li>

							
							
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
	

						<div class="page-header">
							
							<h1>
								<i class="fa fa-user" aria-hidden="true"></i>
								Restaurant Manager
							
							</h1>
						</div><!-- /.page-header -->

						<?php if ( !empty($this->session->flashdata('add_state'))) {
							?>

							 <div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $this->session->flashdata('add_state'); ?>
 
                </div>
					
							
						<?php
					} ?>

				<div class="col-md-12">
	<div class="panel panel-primary">
	<div class="panel-heading" style="background-color: #438eb9;">
	<h2><span class="glyphicon glyphicon-user"></span>&nbsp;
		 <?php if (empty($restaurant_info)) {?>
		Add New Restaurant
	<?php  }else{?>
   Update Restaurant
	<?php }?>
	
	</h2>
	</div>
	<div class="panel-body">
				<form class="form-horizontal" role="form" multiple="multiple" enctype="multipart/form-data" id="LphRestaurant" method="post">		 <?php if (empty($restaurant_info)) {	?>
					<div class="form-group">
				<label for="website" class="col-sm-2 control-label"><label for="LphRestaurant_Website">First Name</label> : </label>
				<div class="col-sm-4">					
				<input class="form-control" required name="fname" type="text">	
				<div class="errorMessage" id="LphRestaurant_website_em_" style="display: none; opacity: 1;"></div>	
				</div>
				<label for="phone" class="col-sm-2 control-label"><label for="LphRestaurant_Phone">Last Name</label> : </label>
				<div class="col-sm-4">					
				<input class="form-control" name="lname" type="text" maxlength="100"  required>
				<div class="errorMessage" id="LphRestaurant_phone_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>	

					<div class="form-group">

				<label for="title" class="col-sm-2 control-label"><label for="LphRestaurant_Title">Email</label> : </label>
				<div class="col-sm-4">					
				<input class="form-control" required  name="email" type="text" maxlength="255">	
				<div class="errorMessage" id="LphRestaurant_title_em_" style="display: none; opacity: 1;"></div>
				</div>
				<label for="phone" class="col-sm-2 control-label"><label for="LphRestaurant_Phone">Password</label> : </label>
				<div class="col-sm-4">					
				<input class="form-control" required name="password" type="password" maxlength="100" >
				<div class="errorMessage" id="LphRestaurant_phone_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>
<?php } ?>
					<div class="form-group">

				<label for="title" class="col-sm-2 control-label"><label for="LphRestaurant_Title">Title</label> : </label>
				<div class="col-sm-10">					
				<input required="" class="form-control" <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['title'] ?>"
				<?php } ?> name="title" type="text" maxlength="255" required="">	
				<div class="errorMessage" id="LphRestaurant_title_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>
			
			<div class="form-group">
				<label for="address" class="col-sm-2 control-label"><label for="LphRestaurant_Address">Address</label> : </label>
				<div class="col-sm-10">					
				<textarea class="form-control" name="street" id="street" required=""><?php if (!empty($restaurant_info)) {
				 echo $restaurant_info[0]['street'] ;
				 } ?></textarea>
				<div class="errorMessage" id="LphRestaurant_address_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>
			
			<div class="form-group">
				<label for="city" class="col-sm-2 control-label"><label for="LphRestaurant_City">City</label> : </label>
				<div class="col-sm-3">					
				<input class="form-control" name="city" id="city"  type="text" maxlength="100" <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['city'] ?>"
				<?php } ?> required="">	
				<div class="errorMessage" id="LphRestaurant_city_em_" style="display: none; opacity: 1;"></div>		
				</div>

				<label for="state" class="col-sm-1 control-label"><label for="LphRestaurant_State">State</label> : </label>
				<div class="col-sm-2">					
				<select class="form-control" name="state" id="state" required="">
					<option value="">SELECT STATE</option>
					<?php foreach ($state as  $value) { ?>
<option  <?php if (!empty($restaurant_info)) { echo $restaurant_info[0]['state'] == $value['state_id'] ? 'selected':''; }?>
				  	 value="<?php echo $value['state_id'] ?>" ><?php echo $value['state_name'] ?></option>
						<?php
					} ?>
					</select>
					<input type="hidden" id="state1" value="<?php echo $value['state_name'] ?>">
				<div class="errorMessage" id="LphRestaurant_state_em_" style="display: none; opacity: 1;"></div>
			</div>
				<label for="zipcode" class="col-sm-1 control-label"><label for="LphRestaurant_Zipcode">Zipcode</label> : </label>
				<div class="col-sm-3">					
				<input class="form-control" name="zipcode" id="zipcode" type="text" maxlength="50" <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['zipcode'] ?>"
				<?php } ?>  required="">	
				<div class="errorMessage" id="LphRestaurant_zip_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>

				   <input type="hidden" id="latitude" name="latitude"  <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['lat'] ?>"
				<?php } ?> >
        
      
            <input type="hidden" id="longitude" name="longitude"  <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['log'] ?>"
				<?php } ?>>

			
			<div class="form-group">
				<label for="website" class="col-sm-2 control-label"><label for="LphRestaurant_Website">Website</label> : </label>
				<div class="col-sm-5">					
				<input class="form-control" name="website" type="text" <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['website'] ?>"
				<?php } ?> >	
				<div class="errorMessage" id="LphRestaurant_website_em_" style="display: none; opacity: 1;"></div>	
				</div>
				<label for="phone" class="col-sm-1 control-label"><label for="LphRestaurant_Phone">Phone</label> : </label>
				<div class="col-sm-4">					
				<input class="form-control" name="phone" type="text" maxlength="100"  <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['phone'] ?>"
				<?php } ?> required="">
				<div class="errorMessage" id="LphRestaurant_phone_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>


				<div class="form-group">
				<label for="website" class="col-sm-2 control-label"><label for="LphRestaurant_Website">Open Time</label> : </label>
				<div class="col-sm-4">					
				<input required=""  class="form-control" name="otime" id="otime" type="text" <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['open_time'] ?>"
				<?php } ?> autocomplete="off" >	
				<div class="errorMessage" id="LphRestaurant_website_em_" style="display: none; opacity: 1;"></div>	
				</div>
				<label for="phone" class="col-sm-2 control-label"><label for="LphRestaurant_Phone">Close Time</label> : </label>
				<div class="col-sm-4">					
				<input class="form-control" required name="ctime" id="ctime" type="text" maxlength="100"  <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['close_time'] ?>"
				<?php } ?> autocomplete="off" >
				<div class="errorMessage" id="LphRestaurant_phone_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>
			
			<!-- <div class="form-group">
				<label for="bar_type" class="col-sm-2 control-label">
					<label for="LphRestaurant_bar_Type">Bar Type</label> : </label>
				<div class="col-sm-4">	
				<div class="select-block check_bx"> 				
					<?php foreach ($bar as  $value) {
						?>
					<span id="LphRestBarTypes_bar_type">
						<input  <?php if(!empty($select_restaurant_bar)){ foreach ($select_restaurant_bar as  $result) {
						echo ($result['bar_type_id']==$value['bar_id'] ? 'checked' : '');	
						} }?>  value="<?php echo $value['bar_id'] ?>" type="checkbox" name="bar_type[]"> <label><?php echo $value['bar_type'] ?></label><br>

								</span>	
				<?php		} ?>
			</div>
				<div class="errorMessage" id="LphRestBarTypes_bar_type_em_" ></div>	
				</div>
				<div class="col-sm-2  control-label"><label for="LphRestaurant_Music_Type_:_">Music  Type :</label></div>
				<div class="col-sm-4">	
				<div class="select-block check_bx">				
				<?php foreach ($music as  $value) {
						?>
				<span id="LphRestMusicTypes_music_type">
					<input  value="<?php echo $value['music_id'] ?>" type="checkbox" name="music_type[]" <?php if(!empty($select_restaurant_music)){ foreach ($select_restaurant_music as  $result) {
						echo ($result['music_type_id']==$value['music_id'] ? 'checked' : '');	
						} }?>  > <label><?php echo $value['music_type'] ?></label><br>
					</span>
				<?php }?>	
				</div>
				<div class="errorMessage" id="LphRestMusicTypes_music_type_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div> -->
			
			<div class="form-group">
				<label for="music_genre" class="col-sm-2 control-label"><label for="LphRestaurant_Music_Genre">Eatery Type</label> : </label>
				<div class="col-sm-4">	
				<div class="select-block check_bx">				
				
				<?php foreach ($music_genre as  $value) {
						?>

				<span id="LphRestMusicGenre_music_genre">
					<input  value="<?php echo $value['music_genre_id'] ?>" type="checkbox" name="music_genre[]"  <?php if(!empty($select_restaurant_music_genre)){ foreach ($select_restaurant_music_genre as  $result) {
						echo ($result['music_genre_id']==$value['music_genre_id'] ? 'checked' : '');	
						} }?> > <label> <?php echo $value['music_genre_name'] ?></label><br>
					 </span>	
					  <?php }?>		
				</div>	
				<div class="errorMessage" id="LphRestMusicGenre_music_genre_em_" style="display: none; opacity: 1;"></div>				</div>
				<label for="amenities" class="col-sm-2 control-label"><label for="LphRestaurant_Amenities">Amenities</label> : </label>
				<div class="col-sm-4">	
				<div class="select-block check_bx">				
				
					<?php foreach ($amenities as  $value) {
						?>

				<span id="LphRestAmenities_amenities">
					<input  value="<?php echo $value['amenities_id'] ?>" type="checkbox" name="amenities[]" <?php if(!empty($select_restaurant_amenities)){ foreach ($select_restaurant_amenities as  $result) {
						echo ($result['amenities_id']==$value['amenities_id'] ? 'checked' : '');	
						} }?> > <label><?php echo $value['amenities_name'] ?></label><br>
					</span>		
				<?php }?>
				</div>	
				<div class="errorMessage" id="LphRestAmenities_amenities_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>
			
			<div class="form-group">
				<label for="food_type" class="col-sm-2 control-label"><label for="LphRestaurant_Food_Type">Food  Type</label> : </label>
				<div class="col-sm-4">	
				<div class="select-block check_bx">				
				
				<?php foreach ($food as  $value) {
						?>
				<span id="LphRestFoodTypes_food_type">


				<input  value="<?php echo $value['food_id'] ?>" type="checkbox" name="food_type[]" <?php if(!empty($select_restaurant_food)){ foreach ($select_restaurant_food as  $result) {
						echo ($result['food_type_id']==$value['food_id'] ? 'checked' : '');	
						} }?> > <label><?php echo $value['food_type'] ?></label><br>
				</span>	
				<?php }?>	
					</div>	
				<div class="errorMessage" id="LphRestFoodTypes_food_type_em_" style="display: none; opacity: 1;"></div>				</div>
				<label for="average_crowd" class="col-sm-2 control-label" ><label style="width:100px;" for="LphRestaurant_Average_Crowd_:_">Average  Crowd </label> : </label>
				<div class="col-sm-4">
				<div class="select-block check_bx">				
				
				<?php foreach ($crowd as  $value) {
						?>
				<span id="LphRestAverageCrowd_crowd_size">
				<input  value="<?php echo $value['crowd_id'] ?>" type="checkbox" name="average_crowd[]" <?php if(!empty($select_restaurant_crowd)){ foreach ($select_restaurant_crowd as  $result) {
						echo ($result['crowd_id']==$value['crowd_id'] ? 'checked' : '');	
						} }?> > <label><?php echo $value['crowd_count'] ?></label><br>
				</span>	
			<?php }?>
				</div>	
				<div class="errorMessage" id="LphRestAverageCrowd_crowd_size_em_" style="display: none; opacity: 1;"></div>				</div>
			</div>
			<div class="form-group">
			
				<label for="gallery" class="col-sm-2 control-label"><label for="LphRestaurant_Photo_Gallery">Photo/Video  Gallery</label> : </label>
				<div class="col-sm-4">					
				<input id="files" class="form-control" type="file" multiple="" name="files[]"  <?php if(!empty($restaurant_info)){ }else{
				?> required="" <?php } ?>>
				</div>
			</div>
			<div class="form-group">
				<label for="state" class="col-sm-2 control-label"><label for="LphRestaurant_State">Status</label> : </label>
				<div class="col-sm-4">					
				<select class="form-control" name="status">
					<option value="">Select Status</option>
					<option <?php if(!empty($restaurant_info)){ echo $restaurant_info[0]['status']==1 ? 'selected':'';  } ?> value="1">Active</option>
                     <option <?php if(!empty($restaurant_info)){ echo $restaurant_info[0]['status']==0 ? 'selected':'';  } ?> value="0">Inactive</option>
					
					</select>
				<div class="errorMessage" id="LphRestaurant_state_em_" style="display: none; opacity: 1;"></div>
			</div>
		</div>
				<div class="col-sm-12">&nbsp;</div>
          <!-- Image Section    -->
          <?php if (!empty($select_restaurant_images)) {
          	
         ?>
				 <label for="state"  class="col-sm-2 control-label"><label for="LphRestaurant_State">Gallery Photos</label> : </label>
				
				<div class="col-sm-10" style="border:2px solid #888; padding-bottom:10px;margin-top: 10px;margin-bottom: 10px;">
					<?php foreach ($select_restaurant_images as  $value) {
						?>
						<div class="imgbox" style="display: inline-block;margin-right: 4px;margin-left: 4px;" >
							<img style="height: 100px;width: 100px;margin-top: 10px;" src="assets/images/<?php echo $value['img']; ?>">
							<div id="<?php echo $value['id']; ?>" style="background: #438eb9;text-align: center;padding: 2px;color: #fff;text-transform: capitalize;" class="button_full delete">delete</div >
						</div>

					<?php
				} ?>
								</div>
							<?php  }?>

								 <!-- End Image Section    -->
			<div class="col-sm-offset-3 col-sm-9">
				  <button style="background-color: #438eb9;float: right;height: 50px;width: 40%;color: white;" type="submit" name="submit" class="form-control">Submit</button>
							</div>
			</form>			</div>
			<div class="col-md-2"> </div>
		</div>
	</div>

			

	
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

		
 <?php $this->load->view('admin/include/footer'); ?> 
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

	</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-2.1.4.min.js"></script>


    <script src="<?php echo base_url()?>assets/js/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/moment.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/daterangepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/autosize.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.inputlimiter.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.maskedinput.min.js"></script>
    
    <script src="<?php echo base_url()?>assets/js/ace-elements.min.js"></script>


<script type="text/javascript">
	$(document).ready(function(){
		$('.delete').click(function(){
			if(confirm("Do you want to delete?"))
			{

			var img_id=$(this).attr('id');
			}else{
				  return false;
			}
			//alert(state_id)

			$.post("<?php echo base_url('Admin_controller/img_delete') ?>",{img_id:img_id},function(res){
				// if(res=='success')
				// {
					window.location.reload();
				//}
			})
		})
	})

</script>


<script type="text/javascript">


	function showResult(result) {
    document.getElementById('latitude').value = result.geometry.location.lat();
    document.getElementById('longitude').value = result.geometry.location.lng();
}

function getLatitudeLongitude(callback, address) {
    // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
    address = address || 'Ferrol, Galicia, Spain';
    // Initialize the Geocoder
    geocoder = new google.maps.Geocoder();
    if (geocoder) {
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                callback(results[0]);
            }
        });
    }
}


$(document).ready(function(){
$('#zipcode').blur(function(){
var zipcode = document.getElementById('zipcode').value;
	var street = document.getElementById('street').value;
var city = document.getElementById('city').value;
var state = document.getElementById('state1').value;

    var address =street +" "+ city +" "+state+" "+ zipcode;
  
    getLatitudeLongitude(showResult, address)

})
		
	})
$(document).ready(function(){
$('#street').blur(function(){
var zipcode = document.getElementById('zipcode').value;
	var street = document.getElementById('street').value;
var city = document.getElementById('city').value;
var state = document.getElementById('state1').value;

    var address =street +" "+ city +" "+state+" "+ zipcode;
      
    getLatitudeLongitude(showResult, address)

})
		
	})

$(document).ready(function(){
$('#city').blur(function(){
var zipcode = document.getElementById('zipcode').value;
	var street = document.getElementById('street').value;
var city = document.getElementById('city').value;
var state = document.getElementById('state1').value;

    var address =street +" "+ city +" "+state+" "+ zipcode;
       
    getLatitudeLongitude(showResult, address)

})
		
	})

$(document).ready(function(){
$('#state').blur(function(){
var zipcode = document.getElementById('zipcode').value;
	var street = document.getElementById('street').value;
var city = document.getElementById('city').value;
var state = document.getElementById('state1').value;

    var address =street +" "+ city +" "+state+" "+ zipcode;
      
    getLatitudeLongitude(showResult, address)

})
		
	})
</script>

<script>


$(document).ready(function () {
        $('#otime').datetimepicker({
            format: "HH:mm A",
        });
    });

$(document).ready(function () {
        $('#ctime').datetimepicker({
            format: "HH:mm A",
        });
    });
</script>