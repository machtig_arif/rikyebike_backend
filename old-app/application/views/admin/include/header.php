<!DOCTYPE html>
<?php  
if(empty($this->session->userdata('admin_id')))
{
  echo "<script>window.location.href='".base_url('Admin_controller')."';</script>";
   //redirect(base_url('Admin_controller'));
}
$admin_id=$this->session->userdata('admin_id');
 $data=$this->Admin_modal->admin_profile($admin_id);    
  if($data[0]['user_avatar']=="")
{
  $img='assets/images/user-male-icon.png';
}else{
  $img='assets/images/'.$data[0]['user_avatar'];
}       
?>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Dashboard - FanSpace Admin</title>

    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />

    <!-- page specific plugin styles -->

    <!-- text fonts -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fonts.googleapis.com.css" />

    <!-- ace styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
 <script src="<?php echo base_url()?>assets/js/jquery-2.1.4.min.js"></script>
 <script src="<?php echo base_url()?>assets/js/moment.min.js"></script>
   <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-datepicker3.min.css" />
   <link rel="stylesheet" href="<?php echo base_url()?>assets/css/daterangepicker.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/ace-rtl.min.css" />

    <link rel="shortcut icon" href="https://pubhub.yesitlabs.com/assets/images/logo.png" type="image/icon">
   

   
  </head>

  <body class="no-skin">
    <div id="navbar" class="navbar navbar-default          ace-save-state">
      <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
          <span class="sr-only">Toggle sidebar</span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>
        </button>

        <div class="navbar-header pull-left">
          <a href="<?php echo base_url('admin_dashboard') ?>" class="navbar-brand">
            <small>
              <i class="fa fa-leaf"></i> 
              FanSpace ADMINISTRATOR
            </small>
          </a>
        </div>

        <div class="navbar-buttons navbar-header pull-right" role="navigation">
          <ul class="nav ace-nav">


            <li class="light-blue dropdown-modal">
              <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                <img style="height: 44px; width: 44px;" class="nav-user-photo" src="<?php echo $img ?>" alt="Jason's Photo" />
                <span class="user-info">
                  <?php echo $data[0]['first_name']." ".$data[0]['last_name'] ?>
                </span>

                <i class="ace-icon fa fa-caret-down"></i>
              </a>

              <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                <li>
                  <a href="<?php echo base_url()?>admin_change_password">
                    <i class="ace-icon fa fa-cog"></i>
                    Change Password
                  </a>
                </li>

                <li>
                  <a href="<?php echo base_url()?>admin_profile">
                    <i class="ace-icon fa fa-user"></i>
                    Profile
                  </a>
                </li>

                <li class="divider"></li>

                <li>
                  <a href="<?php echo base_url()?>logout ">
                    <i class="ace-icon fa fa-power-off"></i>
                    Logout
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div><!-- /.navbar-container -->
    </div>