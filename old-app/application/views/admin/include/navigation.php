       <?php  $path = $this->router->fetch_class().'/'.$this->router->fetch_method(); 
      $get_number_promote= $this->Admin_modal->get_number_promote();
 
       ?>

       <style>
.nav ul li a {
  position: relative;
}
.notification-count {
    background: #ff0000;
    color: #fff;
    padding: 0px;
    border-radius: 3%;
    min-width: 16px;
    display: inline-block;
    text-align: center;
    position: absolute;   
    right: 35px;
}
</style>
    <ul class="nav nav-list">
          <li class="<?php echo $path == 'Admin_controller/admin_dashboard' ? 'active nav-active':''; ?>">
            <a href="<?php echo base_url('admin_dashboard') ?>">
              <i class="menu-icon fa fa-tachometer"></i>
              <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
          </li>



          <li class="">
            <a href="#" class="dropdown-toggle">
              <i style="font-size: 22px;" class="fa fa-cog fa-spin fa-3x fa-fw"></i>
              <span class="menu-text">
               Admin Settings
              </span>

              <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu" <?php echo ($path == 'Admin_controller/states_settings' || $path == 'Admin_controller/social_link' || $path == 'Admin_controller/country_settings' || $path == 'Admin_controller/member_ship_plan' || $path == 'Admin_controller/city_settings' || $path == 'Admin_controller/settings' || $path == 'Admin_controller/Promote_Type') ? 'style="display:block;"':''; ?>>

              <li class="<?php echo $path == 'Admin_controller/settings' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('setting') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Settings
                </a>

                <b class="arrow"></b>
              </li>

             <!--   <li class="<?php echo $path == 'Admin_controller/country_settings' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('country_settings') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Country Settings
                </a>

                <b class="arrow"></b>
              </li> -->
    

              <li class="<?php echo $path == 'Admin_controller/states_settings' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('states_settings') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  States Settings
                </a>

                <b class="arrow"></b>
              </li>


                <li class="<?php echo $path == 'Admin_controller/city_settings' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('city_settings') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  City Settings
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php echo $path == 'Admin_controller/member_ship_plan' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('member_ship_plan') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Member ship Plan
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php echo $path == 'Admin_controller/social_link' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('social_link') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Social Icons
                </a>

                <b class="arrow"></b>
              </li>

             <!--  <li class="<?php //echo $path == 'Admin_controller/Promote_Type' ? 'active nav-active':''; ?>">
                <a href="<?php //echo base_url('Promote_Type') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Promote Type
                </a>

                <b class="arrow"></b>
              </li> -->

            </ul>
          </li>

          <li class="">
            <a href="#" class="dropdown-toggle">
              <i class="menu-icon fa fa-list"></i>
              <span class="menu-text"> User Manager </span>        
               <b class="arrow fa fa-angle-down"></b>     
            </a>
            <b class="arrow"></b>  


            <ul class="submenu" <?php echo ($path == 'Admin_controller/users' || $path == 'Admin_controller/' || $path == 'Admin_controller/manage_supervisor_tradesman') ? 'style="display:block;"':''; ?> >
    

              <li class="<?php echo $path == 'Admin_controller/users' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('users') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  View all User
                </a>

                <b class="arrow"></b>
              </li>

                <li class="<?php echo $path == 'Admin_controller/users?status=1' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url() ?>users?status=1">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Active User
                </a>

                <b class="arrow"></b>
              </li>
                <li class="">
                <a href="<?php echo base_url() ?>users?status=0">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Inactive User
                </a>

                <b class="arrow"></b>
              </li>

             

            </ul>         
          </li>

          <li class="">
            <a href="#" class="dropdown-toggle">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text"> Restaurant Manager </span>   
               <b class="arrow fa fa-angle-down"></b>          
            </a>
            <b class="arrow"></b>     

            <ul class="submenu" <?php echo ($path == 'Admin_controller/restaurant' || $path == 'Admin_controller/add_restaurant' || $path == 'Admin_controller/manage_supervisor_tradesman') ? 'style="display:block;"':''; ?> >

               <li class="<?php echo $path == 'Admin_controller/restaurant' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('restaurant')?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                View All Restaurant
                </a>

                <b class="arrow"></b>
              </li>
    

              <li class="<?php echo $path == 'Admin_controller/add_restaurant' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('add_restaurant')?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                 Add Restaurant
                </a>

                <b class="arrow"></b>
              </li>

               <li class="">
                <a href="<?php echo base_url()?>restaurant?status=1">
                  <i class="menu-icon fa fa-caret-right"></i>
                 Active Restaurant
                </a>

                <b class="arrow"></b>
              </li>

               <li class="">
                <a href="<?php echo base_url()?>restaurant?status=0">
                  <i class="menu-icon fa fa-caret-right"></i>
                 Inactive Restaurant
                </a>

                <b class="arrow"></b>
              </li>

                          

            </ul>              
          </li>

          <li class="">
            <a href="" class="dropdown-toggle">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text">  Food Type Manager </span>
               <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu"  <?php echo ($path == 'Admin_controller/total_food') ? 'style="display:block;"':''; ?>>
    

              <li class="<?php echo $path == 'Admin_controller/total_food' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('total_food')?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  View all Food Type
                </a>

                <b class="arrow"></b>
              </li>

               <li class="">
                <a href="<?php echo base_url() ?>total_food?status=1">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Active Food Type
                </a>

                <b class="arrow"></b>
              </li>

                <li class="">
                <a href="<?php echo base_url()?>total_food?status=0">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Inactive Food Type
                </a>

                <b class="arrow"></b>
              </li>

             

            </ul>       
          </li>


 <!--          <li class="">
            <a href="" class="dropdown-toggle">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Music Type Manager </span>
               <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu" <?php echo ($path == 'Admin_controller/total_music_type') ? 'style="display:block;"':''; ?> >
    

              <li class="<?php echo $path == 'Admin_controller/total_music_type' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url()?>total_music_type">
                  <i class="menu-icon fa fa-caret-right"></i>
                  View all Music Type
                </a>

                <b class="arrow"></b>
              </li>
                <li class="">
                <a href="<?php echo base_url()?>total_music_type?status=1">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Active Music Type
                </a>

                <b class="arrow"></b>
              </li>
                <li class="">
                <a href="<?php echo base_url()?>total_music_type?status=0">
                  <i class="menu-icon fa fa-caret-right"></i>
                Inactive Music Type
                </a>

                <b class="arrow"></b>
              </li>

             

            </ul>       
          </li> -->

        

        <!--   <li class="">
            <a href="" class="dropdown-toggle">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text">  Bar Type Manager </span>
               <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu" <?php echo ($path == 'Admin_controller/total_bar_type') ? 'style="display:block;"':''; ?> >
    

              <li class="<?php echo $path == 'Admin_controller/total_bar_type' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url()?>total_bar_type">
                  <i class="menu-icon fa fa-caret-right"></i>
                  View all Bar Type
                </a>

                <b class="arrow"></b>
              </li>

               <li class="">
                <a href="<?php echo base_url()?>total_bar_type?status=1">
                  <i class="menu-icon fa fa-caret-right"></i>
                   Active Music Type
                </a>

                <b class="arrow"></b>
              </li>

               <li class="">
                <a href="<?php echo base_url()?>total_bar_type?status=0">
                  <i class="menu-icon fa fa-caret-right"></i>
                   Inactive Music Type
                </a>

                <b class="arrow"></b>
              </li>

             

            </ul>       
          </li> -->

            <li class="">
            <a href="" class="dropdown-toggle">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Amenities Manager </span>
               <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu" <?php echo ($path == 'Admin_controller/total_amenities_type') ? 'style="display:block;"':''; ?>>
    

              <li class="<?php echo $path == 'Admin_controller/total_amenities_type' ? 'active nav-active':''; ?>">
               
                  <a href="<?php echo base_url()?>total_amenities_type">
                  View all Amenities Type
                </a>

                <b class="arrow"></b>
              </li>

               <li class="">
               
                  <a href="<?php echo base_url()?>total_amenities_type?status=1">
                 Active Amenities Type
                </a>

                <b class="arrow"></b>
              </li>


               <li class="">
              
                  <a href="<?php echo base_url()?>total_amenities_type?status=0">
                 Inactive Amenities Type
                </a>

                <b class="arrow"></b>
              </li>

             

            </ul>       
          </li>

           <li class="">
            <a href="" class="dropdown-toggle">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Eatery Type Manager </span>
               <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu" <?php echo ($path == 'Admin_controller/total_eatery_type') ? 'style="display:block;"':''; ?> >
    

              <li class="<?php echo $path == 'Admin_controller/total_eatery_type' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url()?>total_eatery_type">
                  <i class="menu-icon fa fa-caret-right"></i>
                  View all Eatery Type
                </a>

                <b class="arrow"></b>
              </li>

               <li class="">
                <a href="<?php echo base_url()?>total_eatery_type?status=1">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Active Eatery Type
                </a>

                <b class="arrow"></b>
              </li>

               <li class="">
                <a href="<?php echo base_url()?>total_eatery_type?status=0">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Inactive Eatery Type
                </a>

                <b class="arrow"></b>
              </li>

             

            </ul>       
          </li>

       <!--    <li class="">
            <a href="" class="dropdown-toggle">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Average Crowd Manager </span>
               <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu" <?php echo ($path == 'Admin_controller/total_crowd_type') ? 'style="display:block;"':''; ?> >
    

              <li class="<?php echo $path == 'Admin_controller/total_crowd_type' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url()?>total_crowd_type">
                  <i class="menu-icon fa fa-caret-right"></i>
                   View all Average Crowd
                </a>

                <b class="arrow"></b>
              </li>

                 <li class="">
                <a href="<?php echo base_url()?>total_crowd_type?status=1">
                  <i class="menu-icon fa fa-caret-right"></i>
                   Active Average Crowd
                </a>

                <b class="arrow"></b>
              </li>

                 <li class="">
                <a href="<?php echo base_url()?>total_crowd_type?status=0">
                  <i class="menu-icon fa fa-caret-right"></i>
                   Inactive Average Crowd
                </a>

                <b class="arrow"></b>
              </li>

             

            </ul>       
          </li> -->

           <li class="">
            <a href="" class="dropdown-toggle">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Coupon Manager </span>
               <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

          
            <ul class="submenu" <?php echo ($path == 'Admin_controller/total_coupon') ? 'style="display:block;"':''; ?> >
    

              <li class="<?php echo $path == 'Admin_controller/total_coupon' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url()?>total_coupon">
                  <i class="menu-icon fa fa-caret-right"></i>
                   View all Coupon
                </a>

                <b class="arrow"></b>
              </li>

                 <li class="">
                <a href="<?php echo base_url()?>total_coupon?status=1">
                  <i class="menu-icon fa fa-caret-right"></i>
                   Active Coupon
                </a>

                <b class="arrow"></b>
              </li>

                 <li class="">
                <a href="<?php echo base_url()?>total_coupon?status=0">
                  <i class="menu-icon fa fa-caret-right"></i>
                   Inactive Coupon
                </a>

                <b class="arrow"></b>
              </li>

             

            </ul>       
          </li>

          <li class="<?php echo $path == 'Admin_controller/claimed_restaurant' ? 'active nav-active':''; ?>">
            <a href="<?php echo base_url()?>claimed_restaurant">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Claimed Restaurant </span>
            </a>         
          </li>

          <li class="<?php echo $path == 'Admin_controller/subscription_plan' ? 'active nav-active':''; ?>">
            <a href="<?php echo base_url()?>subscription_plan">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Subscription Plan </span>
            </a>         
          </li>

          <li class="<?php echo $path == 'Admin_controller/Promote' ? 'active nav-active':''; ?>">
            <a href="<?php echo base_url()?>Promote">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Promote </span><span class="notification-count"><?php if($get_number_promote>0){ echo $get_number_promote;} ?></span>
              
            </a>         
           
          </li>

          <li class="<?php echo $path == 'Admin_controller/feedback' ? 'active nav-active':''; ?>">
            <a href="<?php echo base_url()?>feedback">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Reviews & Ratings </span>
              
            </a>         
           
          </li>

           <li class="<?php echo $path == 'Admin_controller/transaction' ? 'active nav-active':''; ?>">
            <a href="<?php echo base_url()?>transaction">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Transactions </span>
              
            </a>         
           
          </li>

        </ul><!-- /.nav-list -->