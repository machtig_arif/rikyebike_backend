<?php $this->load->view('admin/include/header'); ?>
<div class="main-container ace-save-state" id="main-container">
    <div id="sidebar" class="sidebar responsive ace-save-state">
        <?php $this->load->view('admin/include/navigation'); ?>
        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="<?php echo base_url('admin_dashboard') ?>">Home</a>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <?php if ( !empty($this->session->flashdata('add_state'))) {
							?>
                <div class="alert alert-block alert-success" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <i class="ace-icon fa fa-check green"></i>
                    <?php echo $this->session->flashdata('add_state'); ?>
                </div>
                <?php
					} ?>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="simple-table" class="table  table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Business Title</th>
                                            <th class="hidden-480">Email</th>
                                            <th>name</th>
                                            <th>Status</th>
                                            <!-- <th style="width: 7%;">Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
												$count=$star;
												 foreach ($result as $value) {
													$count++
													 ?>
                                        <tr>
                                            <td>
                                                <?php echo $count; ?>
                                            </td>
                                            <td class="hidden-480">
                                                <?php echo $value['title']; ?>
                                            </td>
                                            <td>
												 <?php echo $value['user_email']; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['name']; ?>
                                            </td>
                                  <!--           <td>
                                                <?php $package=$this->Admin_modal->restaurant_package($value['restaurant_id']);
															if ($package['package_id'] == 1) {
																echo 'Silver';
															}
															else if ($package['package_id'] == 2) {
																echo 'Gold';
															}
															else{
																echo 'not selected';
															}
														?>
                                            </td> -->
                                            <td class="hidden-480">
                                                <?php if($value['status']==0){ ?>
                                                <span class="label label-sm label-success">Pending</span>
                                                <?php }elseif($value['status']==1){?>
                                                <span class="label label-sm label-danger arrowed-in">Pending</span>
                                                <?php }?>
                                            </td>
                                         <!--    <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="<?php echo base_url()?>add_restaurant?res=<?php echo $value['restaurant_id'] ?>">
                                                        <button class="btn btn-xs btn-info" id="bt-modal" style="height: 26px;">
                                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                        </button>
                                                    </a>
                                                    <button id="<?php echo $value['restaurant_id'] ?>" class="btn btn-xs btn-danger delete">
                                                        <i class="ace-icon fa fa-trash-o bigger-120 "></i>
                                                    </button>
                                                </div>
                                            </td> -->
                                        </tr>
                                        <?php
											}
											?>
                                    </tbody>
                                </table>
                            </div><!-- /.span -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <?php echo $this->pagination->create_links(); ?>
                                </div>
                            </div>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->
    <?php $this->load->view('admin/include/footer'); ?>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->
</body>

</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.delete').click(function() {
        if (confirm("Do you want to delete?")) {

            var restaurant_id = $(this).attr('id');
        } else {
            return false;
        }

        $.post("<?php echo base_url('Admin_controller/restaurant_delete') ?>", { restaurant_id: restaurant_id }, function(res) {
            // if(res=='success')
            // {
            location.reload();
            //}
        })
    })
})
</script>