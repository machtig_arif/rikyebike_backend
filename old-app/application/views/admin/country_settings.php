<?php $this->load->view('admin/include/header'); ?>

		<div class="main-container ace-save-state" id="main-container">
			

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				
 <?php $this->load->view('admin/include/navigation'); ?>

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('admin_dashboard') ?>">Home</a>
							</li>

							
							
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
	

						<div class="page-header">
							<button style="float: right;margin-bottom: inherit;height: 40px;width: 30%;color: white;  background-color: #438eb9;border-radius: 10px;"   type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1" >Add Country</button>
							<h1>
								<i class="fa fa-user" aria-hidden="true"></i>
								Country Management
							
							</h1>
						</div><!-- /.page-header -->

						<?php if ( !empty($this->session->flashdata('add_country'))) {
							?>

							 <div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $this->session->flashdata('add_country'); ?>
 
                </div>
					
							
						<?php
					} ?>

					<!-- <h2 style="margin-top: 0px;">Import States Csv</h2>
					<div class="col-md-2 clearfix">
						<form action="<?php echo base_url('Admin_controller/import') ?>" enctype="multipart/form-data" method="post">
					<input type="file" name="csv_file" accept=".csv" style="margin-bottom: 10px;">
					<input class="btn-info" type="submit" name="csv_submit" >

				</form>
			</div> -->
<!-- 					<div class="col-md-5 clearfix">
					<p style="margin-left: 50px;color: red;" >Note: Please check Sample CSV File :<a href="<?php echo base_url('Admin_controller/download') ?>"> Click Here</a> </p></div> -->
				<div class="col-md-12 clearfix">
          <form class="form-horizontal" action="" method="get">
            <div  class="form-group pull-right">
              <div class="col-md-12">
                <select style="border-radius: 10px;width: 160%;background-color: #f1ebe0;" class="form-control pull-right" name="status" onchange="this.form.submit()">
                  <option value="1"> Active</option>
                  <option <?php echo $this->input->get('status') == '0' ? 'selected':''; ?> value="0"> Inactive</option>
                </select>
              </div>
            </div>
          </form>
        </div>

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									<div class="col-xs-12">
										<table id="simple-table" class="table  table-bordered table-hover">
											<thead>
												<tr>
																									
													<th>#</th>
													
															<th>Country Name</th>	
																							
													<th class="hidden-480">Status</th>

													<th></th>
												</tr>
											</thead>

											<tbody>
												<?php
												$count=$star;
												 foreach ($result as $value) {
													$count++
													 ?>
												<tr>
													
													<td><?php echo $count; ?></td>
													<td class="hidden-480"><?php echo $value['country_name']; ?></td>
												

													<td class="hidden-480">
														<?php if($value['status']==1){ ?>
															<span class="label label-sm label-success">Active</span>
														<?php }elseif($value['status']==0){?>
														<span class="label label-sm label-danger arrowed-in">Inactiv</span>
													<?php }?>
													</td>

													<td>
														<div class="hidden-sm hidden-xs btn-group">
															

															<button class="btn btn-xs btn-info" id="bt-modal" data-toggle="modal" data-target="#myModal<?php echo $count;?>">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</button>

															<button id="<?php echo $value['id'] ?>" class="btn btn-xs btn-danger delete">
																<i class="ace-icon fa fa-trash-o bigger-120 "></i>
															</button>

															
														</div>


														  <div class="modal fade" id="myModal<?php echo $count; ?>" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content" style="border-radius: 20px;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Update Country</h4>
                              </div>
                               
                              <div class="modal-body">
                                <form method="post">
                                  <label for="uname"><b>Country Name :</b></label>
                                  <input type="text" placeholder="Country Name"  name="country_name" class="form-control" value="<?php echo  $value['country_name'];  ?>" required>
                                  <input type="hidden" placeholder="Event Name" value="<?php echo $value['id'] ?>" name="id" class="form-control" required>

                                 <!--  <label for="psw"><b>State Code :</b></label>
                                  <input type="text" placeholder="State Code" name="state_code" id="start_date1"  class="form-control" value="<?php echo  $value['state_code'];  ?>" required> -->

                                  <label for="psw"><b>State Status :</b></label>
                                  <select class="form-control" name="status" required="">
                                  	<option value="">Select State</option>
                                  	<option <?php echo $value['status']==1 ? 'selected':''  ?> value="1">Active</option>
                                  	<option <?php echo $value['status']==0 ? 'selected':''  ?> value="0">Inactiv</option>
                                  </select>
                                  <br/>
                                  <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                                 
                                </form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->

														
													</td>
												</tr>
												<?php
											}
											?>

											
											</tbody>
										</table>
									</div><!-- /.span -->
								</div><!-- /.row -->

								<div class="row">
              <div class="col-md-12">
      <div class="row"><?php echo $this->pagination->create_links(); ?></div> 
     </div>
    </div>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

		
 <?php $this->load->view('admin/include/footer'); ?> 
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		  <div class="modal fade" id="myModal-1" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content" style="border-radius: 20px;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add New Country</h4>
                              </div>
                              <div class="modal-body">
                                <form method="post">
                                  <label for="uname"><b>Country Name :</b></label>
                                  <input type="text" placeholder="Country Name"  name="country_name" class="form-control" required>
                                 
                                  <!-- <label for="psw"><b>State Code :</b></label>
                                  <input type="text" placeholder="State Code" name="state_code" id="start_date1"  class="form-control" required> -->

                                  <label for="psw"><b>State Status :</b></label>
                                  <select class="form-control" name="status" required="">
                                  	<option value="">Select State</option>
                                  	<option value="1">Active</option>
                                  	<option value="0">Inactive</option>
                                  </select>
                                  <br/>
                                  <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                                 
                                </form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->

	</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.delete').click(function(){
			if(confirm("Do you want to delete?"))
			{

			var country_id=$(this).attr('id');

			}else{
				  return false;
			}

			$.post("<?php echo base_url('Admin_controller/country_delete') ?>",{country_id:country_id},function(res){
				// if(res=='success')
				// {
					 location.reload();
				//}
			})
		})
	})

</script>