<?php $this->load->view('admin/include/header'); ?>
<div class="main-container ace-save-state" id="main-container">
    <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <?php $this->load->view('admin/include/navigation'); ?>
        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="<?php echo base_url('admin_dashboard') ?>">Home</a>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="header smaller lighter blue">Merchant transactions</h3>
                                <form class="form-search" method="get">
                                    <span class="input-icon">
                                        <label> Name : </label> <input type="text" name="names" value="<?php echo $this->input->get('names'); ?>" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                                        <label> From : </label> <input class="date-picker" id="id-date-picker-1" name="from" type="text" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $this->input->get('from'); ?>" />
                                        <label> TO : </label> <input class="date-picker" id="id-date-picker-1" name="to" type="text" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $this->input->get('to'); ?>" />
                                        <!-- <label> Palce : </label> <input type="text"  name="name" value="<?php echo $this->input->get('name'); ?>" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" / > -->
                                        <input type="submit" name="submit" value="Search" class="btn-info" style="height: 33px;width: 91px; margin-left: 15px;">
                                    </span>
                                    </label>
                                    <div class="clearfix">
                                        <div class="pull-right tableTools-container"></div>
                                    </div>
                                    <div class="table-header">
                                        Restaurant transactions
                                    </div>
                                    <!-- div.table-responsive -->
                                    <!-- div.dataTables_borderWrap -->
                                    <div>
                                        <!-- <table id="dynamic-table" class="table table-striped table-bordered table-hover"> -->
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <!-- <th>avatar</th> -->
                                                    <th>Restaurant name</th>
                                                    <!-- <th>Coins</th> -->
                                                    <th>Amount</th>
                                                    <th>package</th>
                                                    <th>Transactions id</th>
                                                    <th>Payment Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($reset as $value) { ?>
                                                <tr class='fade_<?php echo $value['user_id'] ?>'>
                                                    <!-- 	<td><img src="<?php echo AVATAR_IMG_PATH.$reset['avatar_image']; ?>" height = 30px width=30px ></td> -->
                                                    <td>
                                                        <?php echo $value['first_name']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $value['amount']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo ($value['package_id']==1)?'Silver':'Gold'; ?>
                                                    </td>
                                                    <td>
                                                    	test_1236547890
                                                        <?php //echo $value['transfection_id']; ?>
                                                    </td>
                                                    <td>
                                                    	success
                                                        <?php //echo $value['payment_status']; ?>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                        </form>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->		
 <?php $this->load->view('admin/include/footer'); ?> 
    <script type="text/javascript">
    $(document).ready(function() {
        $('#dynamic-table').DataTable();
    });
    </script>
    <script>
    function Search() {
        var x = document.getElementById("nav-search-input").value;
        $.post("<?php echo base_url('value/list') ?>", { name: x }, function(res) {
            console.log(res);
        })
    }

    function DeleteUser(id) {
        $.post("<?php echo base_url('value/delete_user') ?>", { id: id }, function(res) {
            if (res == 1) {
                var $row = $(".fade_" + id);
                $row.fadeOut(1000, function() {
                    $row.remove();
                });
            }
        })

    }

    $('.date-picker').datepicker({
        autoclose: true,
        todayHighlight: true
    })
    </script>