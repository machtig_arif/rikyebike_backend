<?php $this->load->view('user/include/header'); ?>

		<div class="main-container ace-save-state" id="main-container">
		

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
 <?php $this->load->view('user/include/navigation'); ?>

			 	
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('User_controller') ?>">Home</a>
							</li>
							<li class="active">Gallery</li>
						</ul><!-- /.breadcrumb -->

					
					</div>

					<div class="page-content">
						
							<div class="page-header">
							<button style="float: right;margin-bottom: inherit;height: 40px;width: 30%;color: white;  background-color: #438eb9;border-radius: 10px;"   type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1" >Add Gallery Images</button>
							<h1>
								<i class="fa fa-user" aria-hidden="true"></i>
								Photos Management
							
							</h1>
						</div><!-- /.page-header -->

						<?php if ( !empty($this->session->flashdata('msg'))) {
							?>

							 <div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $this->session->flashdata('msg'); ?>
 
                </div>
					
							
						<?php
					} ?>
								

						<div class="page-header">
							<h1>
								Gallery
								
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div>
									<ul class="ace-thumbnails clearfix">
										
									<?php  foreach ($result as  $value) {
										 ?>
										
										<li>
											<a href="<?php echo base_url()?>assets/images/<?php echo $value['img'] ?>" data-rel="colorbox">
												<img width="150" height="150" alt="150x150" src="assets/images//<?php echo $value['img'] ?>">
												<div class="tags">
													<span class="label-holder">
														<span class="label label-info arrowed"></span>
													</span>

													
												</div>
											</a>

											<div class="tools tools-top">
												
												<a href="#">
													<i class="ace-icon fa fa-times red delete" id="<?php echo $value['id'] ?>" ></i>
												</a>
											</div>
										</li>
  
										<?php  }?>

										
										
									</ul>
								</div><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			 					<div class="modal fade" id="myModal-1" role="dialog">
                         		  <div class="modal-dialog">
                            
                            		  <!-- Modal content-->
                             	 <div class="modal-content" style="border-radius: 20px;">
                            	  <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add New Gallery Images</h4>
                             	 </div>
                            	  <div class="modal-body">
                              	  <form method="post" enctype="multipart/form-data">
                                 <!--  <label for="uname"><b>Title :</b></label>
                                  <input type="text" placeholder="Title"  name="title" class="form-control" required> -->
                                  <label for="uname"><b>Gallery Images :</b></label>
                                  <input type="file" placeholder="Short Description" multiple=""  name="files[]" class="form-control" required>
                                 
                                  <br/>
                                  <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                                 
                                </form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->

			 <?php $this->load->view('user/include/footer'); ?> 

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->


		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.delete').click(function(){
			if(confirm("Do you want to delete?"))
			{

			var coupon_id=$(this).attr('id');
			}else{
				  return false;
			}

			$.post("<?php echo base_url('Admin_controller/img_delete') ?>",{img_id:coupon_id},function(res){
				// if(res=='success')
				// {
					 location.reload();
				//}
			})
		})
	})

</script>

		
	</body>
</html>
