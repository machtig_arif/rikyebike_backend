<?php $this->load->view('user/include/header'); ?>
<div class="main-container ace-save-state" id="main-container">
    <div id="sidebar" class="sidebar responsive ace-save-state">
        <?php $this->load->view('user/include/navigation'); ?>
        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="<?php echo base_url('User_controller') ?>">Home</a>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        <i class="fa fa-user" aria-hidden="true"></i>
                        Deals Redeemed
                    </h1>
                </div><!-- /.page-header -->
                <?php if ( !empty($this->session->flashdata('msg'))) {
							?>
                <div class="alert alert-block alert-success" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <i class="ace-icon fa fa-check green"></i>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
                <?php
					} ?>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="simple-table" class="table  table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Deals Image</th>
                                            <th>Deals Name</th>
                                            <th>Today Redeemed</th>
                                            <th>Total Redeemed</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
												$count=$star;
												 foreach ($result as $value) {
													$count++
													 ?>
                                        <tr>
                                            <td>
                                                <?php echo $count; ?>
                                            </td>
                                            <td class="hidden-480"><img style="height: 100px;width: 100px;" src="<?php echo base_url()?>assets/images/<?php echo $value['img'] ?>"> </td>
                                            <td class="hidden-480">
                                                <?php echo $value['deal_name']; ?>
                                            </td>
                                            <td class="hidden-480">
                                                <?php echo "10"; ?>
                                            </td>
                                            <td class="hidden-480">
                                                <?php echo "100"; ?>
                                            </td>
                                        </tr>
                                        <?php
											}
											?>
                                    </tbody>
                                </table>
                            </div><!-- /.span -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <?php echo $this->pagination->create_links(); ?>
                                </div>
                            </div>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->
    <?php $this->load->view('user/include/footer'); ?>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->
<div class="modal fade" id="myModal-1" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 20px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Deal</h4>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data">
                    <label for="uname"><b>Deal Name :</b></label>
                    <input type="text" placeholder="Deal Name" name="deal_name" class="form-control" required>
                    <label for="uname"><b>Deal Title :</b></label>
                    <input type="text" placeholder="Deal Title" name="deal_title" class="form-control" required>
                    <label for="uname"><b>Deal Start Date :</b></label>
                    <input id="coupon_start_date1" name="deal_start_date" autocomplete="off" type="text" placeholder="Deal Start Date" class="form-control event_date" />
                    <label for="uname"><b>Deal End Date :</b></label>
                    <input id="coupon_start_date1" name="deal_end_date" autocomplete="off" type="text" placeholder="Deal End Date" class="form-control event_date" />
                    <label for="uname"><b>Deal Description:</b></label>
                    <textarea placeholder="Deal Description" autocomplete="off" name="deal_description" class="form-control" required=""></textarea>
                    <label for="uname"><b>Deal Terms:</b></label>
                    <textarea placeholder="Deal Terms" autocomplete="off" name="deal_terms" class="form-control" required=""></textarea>
                    <label for="uname"><b>Deal Image :</b></label>
                    <input type="file" placeholder="Cover Charge" name="deal_img" class="form-control" required>
                    <!--  <label for="psw"><b>Coupon Status :</b></label>
                                  <select class="form-control" name="status" required="">
                                  	<option value="">Select Status</option>
                                  	<option value="1">Active</option>
                                  	<option value="0">Inactive</option>
                                  </select> -->
                    <br />
                    <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                </form>
            </div>
            <!--  <div class="modal-footer">
                               
                              </div> -->
        </div>
        <!--model-content-->
    </div>
</div>
<!--model-->
</body>

</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo base_url()?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/moment.min.js"></script>
<script src="<?php echo base_url()?>assets/js/daterangepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/autosize.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.inputlimiter.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url()?>assets/js/ace-elements.min.js"></script>