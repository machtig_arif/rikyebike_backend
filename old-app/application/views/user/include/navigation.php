     <?php  $path = $this->router->fetch_class().'/'.$this->router->fetch_method(); ?>
    <ul class="nav nav-list">
          <li class="<?php echo $path == 'User_controller/index' ? 'active nav-active':''; ?>">
            <a href="<?php echo base_url('User_controller') ?>">
              <i class="menu-icon fa fa-tachometer"></i>
              <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
          </li>



          <li class="">
            <a href="#" class="dropdown-toggle">
              <i style="font-size: 22px;" class="fa fa-cog fa-spin fa-3x fa-fw"></i>
              <span class="menu-text">
               Restaurant Settings
              </span>

              <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu" <?php echo ($path == 'User_controller/photo_gallery' || $path == 'User_controller/profile' || $path == 'User_controller/change_password' || $path == 'User_controller/registration_setting') ? 'style="display:block;"':''; ?>>
    

              <li class="<?php echo $path == 'User_controller/photo_gallery' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('photo_gallery') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Photo Gallery
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php echo $path == 'User_controller/profile' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('profile') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  View Profile
                </a>

                <b class="arrow"></b>
              </li>

               <li class="<?php echo $path == 'User_controller/registration_setting' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('registration_setting') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Registration Banner Setting
                </a>

                <b class="arrow"></b>
              </li>

              <li class="<?php echo $path == 'User_controller/change_password' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('change_password') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Change Password
                </a>

                <b class="arrow"></b>
              </li>

           

            </ul>
          </li>

          <li class="">
            <a href="#" class="dropdown-toggle">
              <i class="menu-icon fa fa-list"></i>
              <span class="menu-text"> Events Manager </span>        
               <b class="arrow fa fa-angle-down"></b>     
            </a>
            <b class="arrow"></b>  


            <ul class="submenu" <?php echo ($path == 'User_controller/total_events' || $path == 'User_controller/' || $path == 'User_controller/manage_supervisor_tradesman') ? 'style="display:block;"':''; ?> >
    

              <li class="<?php echo $path == 'User_controller/total_events' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('total_events') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  View all Events
                </a>

                <b class="arrow"></b>
              </li>

                <li class="<?php echo $path == 'User_controller/total_events?status=1' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url() ?>total_events?status=1">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Active Events
                </a>

                <b class="arrow"></b>
              </li>
                <li class="">
                <a href="<?php echo base_url() ?>total_events?status=0">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Inactive Events
                </a>

                <b class="arrow"></b>
              </li>

             

            </ul>         
          </li>


           <li class="">
            <a href="" class="dropdown-toggle">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Coupon Manager </span>
               <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

          
            <ul class="submenu" <?php echo ($path == 'User_controller/coupon') ? 'style="display:block;"':''; ?> >
    

              <li class="<?php echo $path == 'User_controller/coupon' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url()?>coupon">
                  <i class="menu-icon fa fa-caret-right"></i>
                   View all Coupon
                </a>

                <b class="arrow"></b>
              </li>

                 <li class="">
                <a href="<?php echo base_url()?>coupon?status=1">
                  <i class="menu-icon fa fa-caret-right"></i>
                   Active Coupon
                </a>

                <b class="arrow"></b>
              </li>

                 <li class="">
                <a href="<?php echo base_url()?>coupon?status=0">
                  <i class="menu-icon fa fa-caret-right"></i>
                   Inactive Coupon
                </a>

                <b class="arrow"></b>
              </li>

             

            </ul>       
          </li>



           <li class="">
            <a href="" class="dropdown-toggle">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Deals Manager </span>
               <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

          
            <ul class="submenu" <?php echo ($path == 'User_controller/deals') ? 'style="display:block;"':''; ?> >
    

              <li class="<?php echo $path == 'User_controller/deals' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url()?>deals">
                  <i class="menu-icon fa fa-caret-right"></i>
                   View all Deals
                </a>

                <b class="arrow"></b>
              </li>

                 <li class="">
                <a href="<?php echo base_url()?>deals?status=1">
                  <i class="menu-icon fa fa-caret-right"></i>
                   Active Deals
                </a>

                <b class="arrow"></b>
              </li>

                 <li class="">
                <a href="<?php echo base_url()?>deals?status=0">
                  <i class="menu-icon fa fa-caret-right"></i>
                   Inactive Deals
                </a>

                <b class="arrow"></b>
              </li>

             

            </ul>       
          </li>


                <li class="">
            <a href="" class="dropdown-toggle">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Promote Manager </span>
               <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

          
            <ul class="submenu" <?php echo ($path == 'User_controller/deal_promote' || $path == 'User_controller/banner_promote') ? 'style="display:block;"':''; ?> >
    

              <li class="<?php echo $path == 'User_controller/deal_promote' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url()?>deal_promote">
                  <i class="menu-icon fa fa-caret-right"></i>
                   Deals Promote
                </a>

                <b class="arrow"></b>
              </li>

                 <li class="<?php echo $path == 'User_controller/banner_promote' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url()?>banner_promote">
                  <i class="menu-icon fa fa-caret-right"></i>
                   Banners Promote
                </a>

                <b class="arrow"></b>
              </li>       

            </ul>       
          </li>

          <li class="<?php echo $path == 'User_controller/subscription' ? 'active nav-active':''; ?>">
            <a href="<?php echo base_url('subscription')?>">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Subscription </span>
              
            </a>         
           
          </li>


         <li class="<?php echo $path == 'User_controller/user_feedback' ? 'active nav-active':''; ?>">
            <a href="<?php echo base_url('user_feedback')?>">
              <i class="menu-icon fa fa-list-alt"></i>
              <span class="menu-text"> Feedback </span>
              
            </a>         
           
          </li>


          <li class="">
            <a href="#" class="dropdown-toggle">
              <i class="menu-icon fa fa-list"></i>
              <span class="menu-text"> User Analytics </span>        
               <b class="arrow fa fa-angle-down"></b>     
            </a>
            <b class="arrow"></b>  


            <ul class="submenu" <?php echo ($path == 'User_controller/clicks' || $path == 'User_controller/' || $path == 'User_controller/viewdata' || $path == 'User_controller/deals_redeemed') ? 'style="display:block;"':''; ?> >
    

              <li class="<?php echo $path == 'User_controller/clicks' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url('clicks') ?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  CTA or Clicks
                </a>

                <b class="arrow"></b>
              </li>

                <li class="<?php echo $path == 'User_controller/viewdata' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url() ?>views">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Views
                </a>

                <b class="arrow"></b>
              </li>
                <li class="<?php echo $path == 'User_controller/deals_redeemed' ? 'active nav-active':''; ?>">
                <a href="<?php echo base_url() ?>deals_redeemed">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Deals Redeemed
                </a>

                <b class="arrow"></b>
              </li>

             

            </ul>         
          </li>

        </ul><!-- /.nav-list -->