

		<?php $this->load->view('user/include/header'); ?>

		<div class="main-container ace-save-state" id="main-container">
			

			<div id="sidebar" class="sidebar responsive ace-save-state">
				
		 <?php $this->load->view('user/include/navigation'); ?>

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('User_controller') ?>">Home</a>
							</li>						
						</ul><!-- /.breadcrumb -->					
					</div>
					<div class="page-content">
					<div class="page-header">							
							<h1>
								<i class="fa fa-user" aria-hidden="true"></i>
								Events Manager							
							</h1>
						</div><!-- /.page-header -->

						<?php if ( !empty($this->session->flashdata('add_state'))) {
							?>

				<div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>
                  <i class="ace-icon fa fa-check green"></i>
                <?php echo $this->session->flashdata('add_state'); ?> 
                </div>			
							
						<?php
					} ?>
				<div class="col-md-12">
	<div class="panel panel-primary">
	<div class="panel-heading" style="background-color: #438eb9;">
	<h2><span class="glyphicon glyphicon-user"></span>&nbsp;Add New Event
	
	</h2>
	</div>
	<div class="panel-body">
				<form class="form-horizontal" role="form" multiple="multiple" enctype="multipart/form-data" id="LphRestaurant" method="post">	

					<div class="form-group">

				<label for="title" class="col-sm-2 control-label"><label for="LphRestaurant_Title">Event name</label> : </label>
				<div class="col-sm-10">					
				<input required="" class="form-control" <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['event_name'] ?>"
				<?php } ?> name="event_name" type="text" maxlength="255">	
			
				</div>
			</div>
			
		
			
			<div class="form-group">
				<label for="city" class="col-sm-2 control-label"><label for="LphRestaurant_City">Event Date</label> : </label>
				<div class="col-sm-4">					
	<input class="form-control" required="" name="event_date" id="event_date"  type="text" maxlength="100" <?php if (!empty($restaurant_info)) {?>value="<?php echo $restaurant_info[0]['event_date'] ?>"<?php } ?>   >				
				</div>
				<label for="city" class="col-sm-2 control-label"><label for="LphRestaurant_City">Time</label> : </label>
				<div class="col-sm-4">					
<input class="form-control" name="event_time" id="event_time" required=""  type="text" maxlength="100" <?php if (!empty($restaurant_info)) {?>value="<?php echo $restaurant_info[0]['event_time'] ?>"<?php } ?>   >				
				</div>
			</div>


			<div class="form-group">

				<label for="title" class="col-sm-2 control-label"><label for="LphRestaurant_Title">Cover Charge</label> : </label>
				<div class="col-sm-10">					
				<input class="form-control" required="" <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['event_charge'] ?>"
				<?php } ?> name="event_charge" type="text" maxlength="255">	
			
				</div>
			</div>


			
		
			
			<div class="form-group">
				<label for="bar_type" class="col-sm-2 control-label">
					<label for="LphRestaurant_bar_Type">Average Crowd</label> : </label>
				<div class="col-sm-4">	
				<div class="select-block check_bx"> 				
					<?php foreach ($crowd as  $value) {
						?>
					<span id="LphRestBarTypes_bar_type">
						<input  <?php if(!empty($select_restaurant_bar)){ foreach ($select_restaurant_bar as  $result) {
						echo ($result['bar_type_id']==$value['bar_id'] ? 'checked' : '');	
						} }?>  value="<?php echo $value['crowd_count'] ?>" type="radio" name="average_crowd[]"> <label><?php echo $value['crowd_count'] ?></label><br>

								</span>	
				<?php		} ?>
			</div>
				<div class="errorMessage" id="LphRestBarTypes_bar_type_em_" ></div>	
				</div>
				<div class="col-sm-2  control-label"><label for="LphRestaurant_Music_Type_:_">Music Genre :</label></div>
				<div class="col-sm-4">	
				<div class="select-block check_bx">				
				<?php foreach ($music_genre as  $value) {
						?>
				<span id="LphRestMusicTypes_music_type">
					<input  value="<?php echo $value['music_genre_id'] ?>" type="checkbox" name="music_genre[]" <?php if(!empty($select_restaurant_music)){ foreach ($select_restaurant_music as  $result) {
						echo ($result['music_type_id']==$value['music_id'] ? 'checked' : '');	
						} }?>  > <label><?php echo $value['music_genre_name'] ?></label><br>
					</span>
				<?php }?>	
				</div>
				
				</div>
			</div>

				<div class="col-sm-12">&nbsp;</div>
          <!-- Image Section    -->
          <?php if (!empty($select_restaurant_images)) {
          	
         ?>
				 <label for="state"  class="col-sm-2 control-label"><label for="LphRestaurant_State">Gallery Photos</label> : </label>
				
				<div class="col-sm-10" style="border:2px solid #888; padding-bottom:10px;margin-top: 10px;margin-bottom: 10px;">
					<?php foreach ($select_restaurant_images as  $value) {
						?>
						<div class="imgbox" style="display: inline-block;margin-right: 4px;margin-left: 4px;" >
							<img style="height: 100px;width: 100px;margin-top: 10px;" src="images/<?php echo $value['img']; ?>">
							<div id="<?php echo $value['id']; ?>" style="background: black;text-align: center;padding: 2px;color: #fff;text-transform: capitalize;" class="button_full delete">delete</div >
						</div>

					<?php
				} ?>
								</div>
							<?php  }?>

								 <!-- End Image Section    -->
			<div class="col-sm-offset-3 col-sm-9">
				  <button style="background-color: #438eb9;float: right;height: 50px;width: 40%;color: white;" type="submit" name="submit" class="form-control">Submit</button>
							</div>
			</form>			</div>
			<div class="col-md-2"> </div>
		</div>
	</div>

			

	
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

		
 <?php $this->load->view('user/include/footer'); ?> 
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

	</body>
</html>
<script src="<?php echo base_url()?>assets/js/jquery-2.1.4.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/jquery-ui.custom.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/moment.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/daterangepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/autosize.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.inputlimiter.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.maskedinput.min.js"></script>    
    <script src="<?php echo base_url()?>assets/js/ace-elements.min.js"></script>

    <script>
jQuery(document).ready(function(){ 
  // Date Picker
  jQuery('#event_date').datepicker();
  
  jQuery('#datepicker-inline').datepicker();
  
  jQuery('#datepicker-multiple').datepicker({
    numberOfMonths: 3,
    showButtonPanel: true
  });

});

$(document).ready(function () {
        $('#event_time').datetimepicker({
            format: "HH:mm A",
        });
    });
</script>