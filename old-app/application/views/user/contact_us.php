<?php $this->load->view('user/include/header');?>
<div class="main-container ace-save-state" id="main-container">
    <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <?php $this->load->view('user/include/navigation'); ?>
        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="<?php echo base_url('User_controller') ?>">Home</a>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        <i class="fa fa-user" aria-hidden="true"></i>
                        Contact Us
                    </h1>
                </div><!-- /.page-header -->
                        <?php if ( !empty($this->session->flashdata('msg'))) {
                            ?>

                             <div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $this->session->flashdata('msg'); ?>
 
                </div>
                    
                            
                        <?php
                    } ?>
                <div class="row">
                    <div class="col-xs-6">
                        <!-- PAGE CONTENT BEGINS -->
                        <form class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Name </label>

                                <div class="col-sm-10">
                                    <input type="text" id="form-field-1" required placeholder="Name" name="name" class="col-md-10" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Email </label>

                                <div class="col-sm-10">
                                    <input type="email" id="form-field-1" required placeholder="Email" name="email" class="col-md-10" value="<?php echo $result['0']['user_email']; ?>" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> Message </label>

                                <div class="col-sm-10">
                                    <textarea class="col-md-10" rows="7" cols="50" name="message" id="form-field-1"></textarea>
                                </div>
                            </div>                                   
                            <div class="col-md-offset-3 col-md-9">
                                <input class="btn btn-info" type="Submit" name="Submit">
                            </div>
                                    
                        </form>
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                    <div class="col-xs-6">
                        <h1>Contact Details : </h1>
                        <?php echo $setting['4']['contact_detail'];  ?>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->
    <?php $this->load->view('user/include/footer'); ?>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->
</body>

</html>