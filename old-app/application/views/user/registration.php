<style type="text/css">
	a.refresh i {
    margin-top: 17px;
    font-size: 22px;
    color: #929292;
}
.image{
    float: left;
    margin-right: 8px;
  
}
</style>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />

    <!-- page specific plugin styles -->

    <!-- text fonts -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fonts.googleapis.com.css" />

    <!-- ace styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

   <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/ace-rtl.min.css" />

    <!--[if lte IE 9]>
      <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="<?php echo base_url()?>assets/js/ace-extra.min.js"></script>

		<div class="main-container ace-save-state" id="main-container">
			

			

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('') ?>"> Already Registered</a>
							</li>

							
							
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
	

						

						<?php if ( !empty($this->session->flashdata('msg'))) {
							?>

							 <div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $this->session->flashdata('msg'); ?>
 
                </div>
					
							
						<?php
					} ?>

				<div class="col-md-12">
	<div class="panel panel-primary">
	<div class="panel-heading" style="background-color: #438eb9;">
	<h2><span class="glyphicon glyphicon-user"></span>&nbsp;Add New Restaurant
	
	</h2>
	</div>
	<div class="panel-body">
				<form class="form-horizontal" role="form" multiple="multiple" enctype="multipart/form-data" method="post">

				<div class="form-group">
				<label for="website" class="col-sm-2 control-label"><label for="LphRestaurant_Website">First Name</label> : </label>
				<div class="col-sm-4">					
				<input class="form-control" required name="fname" type="text" <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['website'] ?>"
				<?php } ?> >	
				<div class="errorMessage" id="LphRestaurant_website_em_" style="display: none; opacity: 1;"></div>	
				</div>
				<label for="phone" class="col-sm-1 control-label"><label for="LphRestaurant_Phone">Last Name</label> : </label>
				<div class="col-sm-5">					
				<input class="form-control" name="lname" type="text" maxlength="100"  required <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['phone'] ?>"
				<?php } ?> >
				<div class="errorMessage" id="LphRestaurant_phone_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>	

					<div class="form-group">

				<label for="title" class="col-sm-2 control-label"><label for="LphRestaurant_Title">Email</label> : </label>
				<div class="col-sm-4">					
				<input class="form-control" required <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['title'] ?>"
				<?php } ?> name="email" type="text" maxlength="255">	
				<div class="errorMessage" id="LphRestaurant_title_em_" style="display: none; opacity: 1;"></div>
				</div>
				<label for="phone" class="col-sm-1 control-label"><label for="LphRestaurant_Phone">Password</label> : </label>
				<div class="col-sm-5">					
				<input class="form-control" required name="password" type="password" maxlength="100"  <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['phone'] ?>"
				<?php } ?> >
				<div class="errorMessage" id="LphRestaurant_phone_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>
			<div class="form-group">

				<label for="phone" class="col-sm-2 control-label"><label for="LphRestaurant_Phone">Business Name</label> : </label>
				<div class="col-sm-10">					
				<input class="form-control" required name="business_name" type="text" maxlength="100"  <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['phone'] ?>"
				<?php } ?> >
				<div class="errorMessage" id="LphRestaurant_phone_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>
			
			<div class="form-group">
				<label for="address" class="col-sm-2 control-label"><label for="LphRestaurant_Address">Address</label> : </label>
				<div class="col-sm-10">					
				<textarea required class="form-control" name="street" id="street"><?php if (!empty($restaurant_info)) {
				 echo $restaurant_info[0]['street'] ;
				 } ?></textarea>
				<div class="errorMessage" id="LphRestaurant_address_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>
			
			<div class="form-group">
				<label for="city" class="col-sm-2 control-label"><label for="LphRestaurant_City">City</label> : </label>
				<div class="col-sm-3">					
				<input class="form-control" required name="city" id="city"  type="text" maxlength="100" <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['title'] ?>"
				<?php } ?>   >	
				<div class="errorMessage" id="LphRestaurant_city_em_" style="display: none; opacity: 1;"></div>		
				</div>

				<label for="state" class="col-sm-1 control-label"><label for="LphRestaurant_State">State</label> : </label>
				<div class="col-sm-2">					
				<select class="form-control" required name="state" id="state">
					<option value="">SELECT STATE</option>
					<?php foreach ($state as  $value) { ?>
<option  <?php if (!empty($restaurant_info)) { echo $restaurant_info[0]['state'] == $value['state_id'] ? 'selected':''; }?>
				  	 value="<?php echo $value['state_id'] ?>"><?php echo $value['state_name'] ?></option>
						<?php
					} ?>
					</select>
				<div class="errorMessage" id="LphRestaurant_state_em_" style="display: none; opacity: 1;"></div>
			</div>
				<label for="zipcode" class="col-sm-1 control-label"><label for="LphRestaurant_Zipcode">Zipcode</label> : </label>
				<div class="col-sm-3">					
				<input class="form-control" required name="zipcode" id="zipcode" type="text" maxlength="50" <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['zipcode'] ?>"
				<?php } ?>   >	
				<div class="errorMessage" id="LphRestaurant_zip_em_" style="display: none; opacity: 1;"></div>
				</div>

				    <input type="hidden" id="latitude" name="latitude" readonly />
        
      
            <input type="hidden" id="longitude" name="longitude" readonly />
			</div>
			
			<div class="form-group">
				<label for="website" class="col-sm-2 control-label"><label for="LphRestaurant_Website">Website</label> : </label>
				<div class="col-sm-5">					
				<input class="form-control" name="website" type="text" <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['website'] ?>"
				<?php } ?> >	
				<div class="errorMessage" id="LphRestaurant_website_em_" style="display: none; opacity: 1;"></div>	
				</div>
				<label for="phone" class="col-sm-1 control-label"><label for="LphRestaurant_Phone">Phone</label> : </label>
				<div class="col-sm-4">					
				<input class="form-control" required name="phone" type="text" maxlength="100"  <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['phone'] ?>"
				<?php } ?> >
				<div class="errorMessage" id="LphRestaurant_phone_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>

			<div class="form-group">
				<label for="website" class="col-sm-2 control-label"><label for="LphRestaurant_Website">Open Time</label> : </label>
				<div class="col-sm-4">					
				<input class="form-control" name="otime" id="otime" type="text" <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['open_time'] ?>"
				<?php } ?> >	
				<div class="errorMessage" id="LphRestaurant_website_em_" style="display: none; opacity: 1;"></div>	
				</div>
				<label for="phone" class="col-sm-2 control-label"><label for="LphRestaurant_Phone">Close Time</label> : </label>
				<div class="col-sm-4">					
				<input class="form-control" required name="ctime" id="ctime" type="text" maxlength="100"  <?php if (!empty($restaurant_info)) {
					?>
					value="<?php echo $restaurant_info[0]['close_time'] ?>"
				<?php } ?> >
				<div class="errorMessage" id="LphRestaurant_phone_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>
			
			<!-- <div class="form-group">
				<label for="bar_type" class="col-sm-2 control-label">
					<label for="LphRestaurant_bar_Type">Bar Type</label> : </label>
				<div class="col-sm-4">	
				<div class="select-block check_bx"> 				
					<?php foreach ($bar as  $value) {
						?>
					<span id="LphRestBarTypes_bar_type">
						<input  <?php if(!empty($select_restaurant_bar)){ foreach ($select_restaurant_bar as  $result) {
						echo ($result['bar_type_id']==$value['bar_id'] ? 'checked' : '');	
						} }?>  value="<?php echo $value['bar_id'] ?>" type="checkbox" name="bar_type[]"> <label><?php echo $value['bar_type'] ?></label><br>

								</span>	
				<?php		} ?>
			</div>
				<div class="errorMessage" id="LphRestBarTypes_bar_type_em_" ></div>	
				</div>
				<div class="col-sm-2  control-label"><label for="LphRestaurant_Music_Type_:_">Music  Type :</label></div>
				<div class="col-sm-4">	
				<div class="select-block check_bx">				
				<?php foreach ($music as  $value) {
						?>
				<span id="LphRestMusicTypes_music_type">
					<input  value="<?php echo $value['music_id'] ?>" type="checkbox" name="music_type[]" <?php if(!empty($select_restaurant_music)){ foreach ($select_restaurant_music as  $result) {
						echo ($result['music_type_id']==$value['music_id'] ? 'checked' : '');	
						} }?>  > <label><?php echo $value['music_type'] ?></label><br>
					</span>
				<?php }?>	
				</div>
				<div class="errorMessage" id="LphRestMusicTypes_music_type_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div> -->
			
			<div class="form-group">
				<label for="music_genre" class="col-sm-2 control-label"><label for="LphRestaurant_Music_Genre">Eatery Type </label> : </label>
				<div class="col-sm-4">	
				<div class="select-block check_bx">				
				
				<?php foreach ($music_genre as  $value) {
						?>

				<span id="LphRestMusicGenre_music_genre">
					<input  value="<?php echo $value['music_genre_id'] ?>" type="checkbox" name="Eatery_type[]"  <?php if(!empty($select_restaurant_music_genre)){ foreach ($select_restaurant_music_genre as  $result) {
						echo ($result['music_genre_id']==$value['music_genre_id'] ? 'checked' : '');	
						} }?> > <label> <?php echo $value['music_genre_name'] ?></label><br>
					 </span>	
					  <?php }?>		
				</div>	
				<div class="errorMessage" id="LphRestMusicGenre_music_genre_em_" style="display: none; opacity: 1;"></div>				</div>
				<label for="amenities" class="col-sm-2 control-label"><label for="LphRestaurant_Amenities">Amenities</label> : </label>
				<div class="col-sm-4">	
				<div class="select-block check_bx">				
				
					<?php foreach ($amenities as  $value) {
						?>

				<span id="LphRestAmenities_amenities">
					<input  value="<?php echo $value['amenities_id'] ?>" type="checkbox" name="amenities[]" <?php if(!empty($select_restaurant_amenities)){ foreach ($select_restaurant_amenities as  $result) {
						echo ($result['amenities_id']==$value['amenities_id'] ? 'checked' : '');	
						} }?> > <label><?php echo $value['amenities_name'] ?></label><br>
					</span>		
				<?php }?>
				</div>	
				<div class="errorMessage" id="LphRestAmenities_amenities_em_" style="display: none; opacity: 1;"></div>
				</div>
			</div>
			
			<div class="form-group">
				<label for="food_type" class="col-sm-2 control-label"><label for="LphRestaurant_Food_Type">Food  Type</label> : </label>
				<div class="col-sm-4">	
				<div class="select-block check_bx">				
				
				<?php foreach ($food as  $value) {
						?>
				<span id="LphRestFoodTypes_food_type">


				<input  value="<?php echo $value['food_id'] ?>" type="checkbox" name="food_type[]" <?php if(!empty($select_restaurant_food)){ foreach ($select_restaurant_food as  $result) {
						echo ($result['food_type_id']==$value['food_id'] ? 'checked' : '');	
						} }?> > <label><?php echo $value['food_type'] ?></label><br>
				</span>	
				<?php }?>	
					</div>	
				<div class="errorMessage" id="LphRestFoodTypes_food_type_em_" style="display: none; opacity: 1;"></div>				</div>
				<label for="average_crowd" class="col-sm-2 control-label" ><label style="width:100px;" for="LphRestaurant_Average_Crowd_:_">Average  Crowd </label> : </label>
				<div class="col-sm-4">
				<div class="select-block check_bx">				
				
				<?php foreach ($crowd as  $value) {
						?>
				<span id="LphRestAverageCrowd_crowd_size">
				<input   value="<?php echo $value['crowd_id'] ?>" type="checkbox" name="average_crowd[]" <?php if(!empty($select_restaurant_crowd)){ foreach ($select_restaurant_crowd as  $result) {
						echo ($result['crowd_id']==$value['crowd_id'] ? 'checked' : '');	
						} }?> > <label><?php echo $value['crowd_count'] ?></label><br>
				</span>	
			<?php }?>
				</div>	
				<div class="errorMessage" id="LphRestAverageCrowd_crowd_size_em_" style="display: none; opacity: 1;"></div>				</div>
			</div>
			<div class="form-group">
			
				<label for="gallery" class="col-sm-2 control-label"><label for="LphRestaurant_Photo_Gallery">Photo/Video  Gallery</label> : </label>
				<div class="col-sm-4">					
				<input id="files" required  class="form-control" type="file" multiple="" name="files[]">
				</div>
			
				<label for="state" class="col-sm-2 control-label"><label for="LphRestaurant_State">Subscription Type</label> : </label>
				<div class="col-sm-4">					
				<select class="form-control"  name="subscription_type" required>
					<option value="">Select State</option>
					<option  value="1">	Silver</option>
                     <option value="0">	Gold</option>
					
					</select>
				<div class="errorMessage" id="LphRestaurant_state_em_" style="display: none; opacity: 1;"></div>
			</div>
		</div>
				<div class="col-sm-12">&nbsp;</div>
          <!-- Image Section    -->

          <!-- Capcha -->


          <div class="form-group">
			
				<label for="gallery" class="col-sm-2 control-label"><label for="LphRestaurant_Photo_Gallery">Captcha</label> : </label>
				<div class="col-sm-4">	
					<div   class="g-recaptcha" data-sitekey="6LdVnKoUAAAAAO6ARPGyBtfYYb8LSrWxW6_Ivh6r" required></div>
				<!-- <div class='image'>				
				<?php echo $image; ?></div> <a href='javascript:void(0)' class ='refresh'><i id = 'ref_symbol' class="fa fa-refresh"></i></a> -->
				</div>
			</div>
			
			
          <?php if (!empty($select_restaurant_images)) {
          	
         ?>
				 <label for="state"  class="col-sm-2 control-label"><label for="LphRestaurant_State">Gallery Photos</label> : </label>
				
				<div class="col-sm-10" style="border:2px solid #888; padding-bottom:10px;margin-top: 10px;margin-bottom: 10px;">
					<?php foreach ($select_restaurant_images as  $value) {
						?>
						<div class="imgbox" style="display: inline-block;margin-right: 4px;margin-left: 4px;" >
							<img style="height: 100px;width: 100px;margin-top: 10px;" src="images/<?php echo $value['img']; ?>">
							<div id="<?php echo $value['id']; ?>" style="background: black;text-align: center;padding: 2px;color: #fff;text-transform: capitalize;" class="button_full delete">delete</div >
						</div>

					<?php
				} ?>
								</div>
							<?php  }?>

 

<!-- <script src="https://www.google.com/recaptcha/api.js?render=6Lc8P6kUAAAAALux9HgkMuSOalaGPhIvtPcqFHZi"></script>
  <script>
  grecaptcha.ready(function() {
      grecaptcha.execute('6Lc8P6kUAAAAALux9HgkMuSOalaGPhIvtPcqFHZi', {action: 'homepage'}).then(function(token) {
         ...
      });
  });
  </script> -->

			<div class="col-sm-offset-3 col-sm-9">
				  <button style="background-color: #438eb9;float: right;height: 50px;width: 40%;color: white;" type="submit" name="submit" class="form-control">Submit</button>
							</div>
			</form>			</div>
			<div class="col-md-2"> </div>
		</div>
	</div>

			

	
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			
       
          
   
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=6LdVnKoUAAAAAO6ARPGyBtfYYb8LSrWxW6_Ivh6r&libraries=places&callback=initAutocomplete" async defer></script> -->
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">


	function showResult(result) {
    document.getElementById('latitude').value = result.geometry.location.lat();
    document.getElementById('longitude').value = result.geometry.location.lng();
}

function getLatitudeLongitude(callback, address) {
    // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
    address = address || 'Ferrol, Galicia, Spain';
    // Initialize the Geocoder
    geocoder = new google.maps.Geocoder();
    if (geocoder) {
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                callback(results[0]);
            }
        });
    }
}

var zipcode = document.getElementById('zipcode').value;

$(document).ready(function(){
$('#zipcode').blur(function(){
	var street = document.getElementById('street').value;
var city = document.getElementById('city').value;
var state = document.getElementById('state').value;

    var address =street+city+state+zipcode;
    getLatitudeLongitude(showResult, address)

})
		
	})
</script>
		
 <?php $this->load->view('user/include/footer'); ?> 
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

	</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-2.1.4.min.js"></script>


    <script src="<?php echo base_url()?>assets/js/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/moment.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/daterangepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/autosize.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.inputlimiter.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.maskedinput.min.js"></script>
    
    <script src="<?php echo base_url()?>assets/js/ace-elements.min.js"></script>




<script type="text/javascript">


</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.delete').click(function(){
			if(confirm("Do you want to delete?"))
			{

			var img_id=$(this).attr('id');
			}else{
				  return false;
			}
			//alert(state_id)

			$.post("<?php echo base_url('Admin_controller/img_delete') ?>",{img_id:img_id},function(res){
				// if(res=='success')
				// {
					 location.reload();
				//}
			})
		})
	})

	// Ajax post for refresh captcha image.
// $(document).ready(function() {
// $("a.refresh").click(function() {
// jQuery.ajax({
// type: "POST",
// url: "<?php echo base_url(); ?>" + "User_controller/captcha_refresh",
// success: function(res) {
// if (res)
// {
// jQuery("div.image").html(res);
// }
// }
// });
// });
// });

</script>
<script>


$(document).ready(function () {
        $('#otime').datetimepicker({
            format: "HH:mm A",
        });
    });

$(document).ready(function () {
        $('#ctime').datetimepicker({
            format: "HH:mm A",
        });
    });
</script>