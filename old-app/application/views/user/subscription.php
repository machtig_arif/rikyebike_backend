<?php $this->load->view('user/include/header'); ?>
<div class="main-container ace-save-state" id="main-container">
    <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <?php $this->load->view('user/include/navigation'); ?>
        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="<?php echo base_url('User_controller') ?>">Home</a>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        <i class="fa fa-user" aria-hidden="true"></i>
                        Subscription
                    </h1>
                </div><!-- /.page-header -->
                <?php if ( !empty($this->session->flashdata('msg'))) {
							?>
                <div class="alert alert-block alert-success" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <i class="ace-icon fa fa-check green"></i>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
                <?php
					} ?>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                        	<?php foreach ($result as $value) { ?>       	
                        	<div class="col-xs-6 col-sm-6 pricing-box ">
                            <div class="widget-box widget-color-dark">
											<div class="widget-header">
												<h5 class="widget-title bigger lighter"><?php echo $value['name']; ?></h5>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<?php echo $value['discription']; ?>
													<!-- <ul class="list-unstyled spaced2">

														<li>
															<i class="ace-icon fa fa-check green"></i>
															10 GB Disk Space
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															200 GB Bandwidth
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															100 Email Accounts
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															10 MySQL Databases
														</li>

														<li>
															<i class="ace-icon fa fa-check green"></i>
															$10 Ad Credit
														</li>

														<li>
															<i class="ace-icon fa fa-times red"></i>
															Free Domain
														</li>
													</ul> -->

													<hr />
													<div class="price">
														$<?php echo  $value['amount']; ?>
														<small>/month</small>
													</div>
												</div>

												<div>
													<a href="<?php echo base_url('subscription/'.$value['id']); ?>" onclick="return confirm('Are you sure?')" class="btn btn-block btn-inverse">
														<i class="ace-icon fa fa-shopping-cart bigger-110"></i>
														<span>Buy</span>
													</a>
												</div>
											</div>
										</div>
									</div>

								<?php } ?>
                        </div><!-- /.row -->
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->
    <?php $this->load->view('user/include/footer'); ?>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->
</body>

</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.delete').click(function() {
        if (confirm("Do you want to delete?")) {

            var id = $(this).attr('id');
        } else {
            return false;
        }

        $.post("<?php echo base_url('User_controller/feedback_delete') ?>", { id: id }, function(res) {
            // if(res=='success')
            // {
            location.reload();
            //}
        })
    })
})
</script>

<style type="text/css">
	.widget-header {
    background-color: #438EB9 !important;
}

.widget-color-dark {
    border-color: #e7e7e7;
}

.btn-inverse, .btn-inverse.focus, .btn-inverse:focus {
    background-color: #438EB9!important;
    border-color: #438eb9;
}


.btn-inverse.focus:hover, .btn-inverse:active:hover, .btn-inverse:focus:active, .btn-inverse:focus:hover, .btn-inverse:hover, .open>.btn-inverse.dropdown-toggle, .open>.btn-inverse.dropdown-toggle.focus, .open>.btn-inverse.dropdown-toggle:active, .open>.btn-inverse.dropdown-toggle:focus, .open>.btn-inverse.dropdown-toggle:hover {
      background-color: #438EB9!important;
    border-color: #438eb9;
}
</style>