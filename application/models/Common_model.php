	<?php
defined('BASEPATH') or die ('Not Allow To Access');

class Common_model extends CI_Model
{
	public function getNewCouponCode($length = 6) {
		$token = "";
		$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$codeAlphabet.= "0123456789";
		for($i = 0; $i < $length; $i++) {
			$token .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
		}
		return $token;
	}

	function crypto_rand_secure($min, $max) {
		$range = $max - $min;
		if ($range < 0) return $min; // not so random...
		$log = log($range, 2);
		$bytes = (int) ($log / 8) + 1; // length in bytes
		$bits = (int) $log + 1; // length in bits
		$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
		do {
			$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
			$rnd = $rnd & $filter; // discard irrelevant bits
		} while ($rnd >= $range);
		return $min + $rnd;
	}


	// 27-12-2019//
	public function daysDropdown($key=null){
		$days = [
			"Sunday" => "Sunday",
			"Monday" => "Monday",
			"Tuesday" => "Tuesday",
			"Wednesday" => "Wednesday",
			"Thursday" => "Thursday",
			"Friday" => "Friday",
			"Saturday" => "Saturday"
		];
		if($key){
			$days[$key];
		}
		return $days;
	}

	public function hoursDropdown($key=null){
		$hours = [
			"6 Hours" => "6 Hours",
			"12 Hours" => "12 Hours",
			"24 Hours" => "24 Hours"
		];
		if($key){
			$hours[$key];
		}
		return $hours;
	}

	public function periodDropdown($key=null){
		$period = [
			"Morning" => "Morning",
			"Afternoon" => "Afternoon",
			"Evening" => "Evening",
			"Night" => "Night"
		];
		if($key){
			$period[$key];
		}
		return $period;
	}
}