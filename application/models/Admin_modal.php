	<?php
defined('BASEPATH') or die ('Not Allow To Access');

class Admin_modal extends CI_Model
{
	public function admin_login($email,$password)
	{
	$data=$this->db->query("select * from tbl_user where user_email='$email' and user_password='$password' and status='1' and (user_role='Admin' or user_role='Restaurant')");
			if($data->num_rows() > 0)  
		{  
		return $data->result_array();  
		}  
		else  
		{  
		return false;       
		}  
			}

	public function admin_profile($admin_id)
	{
		$data=$this->db->query("select * from tbl_user where user_id='$admin_id'");
		return $data->result_array();
	}

	public function get_subscription($id)
	{
		$data=$this->db->query("select * from tbl_subscription_plan where id='$id'");
		return $data->result_array();
	}

	public function admin_profile_update($data,$admin_id)
	{
		 $this->db->where("user_id",$admin_id);

$this->db->update("tbl_user",$data);
return true;
	}

	public function add_states($data)
	{
		$this->db->insert('tbl_states',$data);
		return true;
	}

	public function add_country($data)
	{
		$this->db->insert('tbl_country',$data);
		return true;
	}

	public function add_user($data)
	{
		$this->db->insert('tbl_user',$data);
		return true;
	}

	public function add_city($data)
	{
		$this->db->insert('tbl_city',$data);
		return true;
	}

	public function add_subscription_plan($data)
	{
		$this->db->insert('tbl_subscription_plan',$data);
		return true;
	}


	public function select_state($where = '',$limit=null,$offset=NULL) {
      
       $this->db->select('tbl_states.state_id,tbl_states.state_name,tbl_states.status,tbl_states.state_code,tbl_states.country_id,tbl_country.id,tbl_country.country_name');
    $this->db->from('tbl_states');
      $this->db->join('tbl_country', 'tbl_country.id = tbl_states.country_id', 'left'); 
    $this->db->order_by("tbl_states.state_id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("tbl_states.status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();
    
	}

	function states_record_count($where = ''){
    
		$query = $this->db->select('*')
			->from('tbl_states');
			  
	if($where!=""){
			$query->where("status",$where);			
		}
		$query = $query->get();	
		return $query->num_rows();

	}

	public function state_delete($state_id)
	{
		$this->db->query("delete from tbl_states where state_id='$state_id'");
		return true;
	}

	public function city_delete($city_id)
	{
		$this->db->query("delete from tbl_city where id='$city_id'");
		return true;
	}

	public function subscription_plan_delete($id)
	{
		$data['delete_status'] = 2;
		$this->db->where("id",$id);
		$this->db->update("tbl_subscription_plan",$data);
		return true;
	}

	public function update_states($data,$state_id)
	{
		$this->db->where("state_id",$state_id);

				$this->db->update("tbl_states",$data);
				return true;
	}

	public function update_country($data,$state_id)
	{
		$this->db->where("id",$state_id);

				$this->db->update("tbl_country",$data);
				return true;
	}


	public function update_city($data,$state_id)
	{
		$this->db->where("id",$state_id);

				$this->db->update("tbl_city",$data);
				return true;
	}

	public function update_subscription_plan($data,$subscription_id)
	{
		$this->db->where("id",$subscription_id);

				$this->db->update("tbl_subscription_plan",$data);
				return true;
	}

	public function select_social_link()
	{
		$data=$this->db->query("select * from tbl_social");
		return $data->result_array();
	}

	public function update_social_link($data,$social_id)
	{
		$this->db->where('social_id',$social_id);
		$this->db->update("tbl_social",$data);
		return true;
	}


	public function select_admin_password($admin_id,$old_pass)
	{
		$data=$this->db->query("select user_password from tbl_user where user_password='$old_pass' and user_id='$admin_id'");
		return $data->result_array();
	}

	public function change_password($new_pass1,$admin_id)
	{
		$this->db->query("update tbl_user set user_password='$new_pass1' where user_id='$admin_id'");
		return true;
		
	}


	public function user_record_count($where='')
	{

		$query=$this->db->select('*')
		->from('tbl_user')
		->where("user_role='User'");
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}

	public function select_users($where='',$limit=null,$offset=NULL)
	{
		 $this->db->select('*');
    $this->db->from('tbl_user');
    $this->db->where("user_role='User'");
    $this->db->order_by("user_id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	public function update_user_status($status,$user_id)
	{
		$this->db->query("update tbl_user set status='$status' where user_id='$user_id'");
		return true;
	}

	public function user_delete($user_id)
	{
		$this->db->query("delete from tbl_user where user_id='$user_id' and user_role='User'");
		return true;
	}


	public function restaurant_record_count($where='')
	{

		$query=$this->db->select('*')
		->from('tbl_restaurant');
		$this->db->where("claimed='1'");
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}

	public function select_restaurant($where='',$limit=null,$offset=NULL) {
		$this->db->select('*');
		$this->db->from('tbl_restaurant');
		//$this->db->order_by("restaurant_id",'DESC');
		$this->db->where("claimed='1'");			
		$this->db->limit($limit, $offset);
		if($where != ""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();
		
		return $query->result_array();
	}

	public function claimed_restaurant_record_count($where='')
	{

		$query=$this->db->select('*')->from('tbl_claim_list as tcl')
		->join('tbl_restaurant as tr','tcl.r_id = tr.restaurant_id');
		$this->db->where("claimed='0'");
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}

	public function select_claimed_restaurant($where='',$limit=null,$offset=NULL)
	{
		$query=$this->db->select('*')->from('tbl_claim_list as tcl')
		->join('tbl_restaurant as tr','tcl.r_id = tr.restaurant_id');
		$this->db->order_by("restaurant_id",'DESC');
		$this->db->where("claimed='0'");			
		$this->db->limit($limit, $offset);
		if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	public function subscription_plan_record_count($where='')
	{

		$query=$this->db->select('*')->from('tbl_subscription_plan');
		$this->db->where("delete_status='1'");
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}

	public function select_subscription_plan($where='',$limit=null,$offset=NULL)
	{
		$query=$this->db->select('*')->from('tbl_subscription_plan');
		$this->db->order_by("id",'DESC');
		$this->db->where("delete_status='1'");			
		$this->db->limit($limit, $offset);
		if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	///////////////////////////////////

	public function add_restaurant($add_restaurant)
	{
		$this->db->insert('tbl_restaurant',$add_restaurant);
		return true;
	}

	public function add_restaurant_bar($restaurant_id,$bar_type)
	{

		$this->db->query("insert into tbl_restaurant_bar_type set restaurant_id='$restaurant_id',bar_type_id='$bar_type'");
		return true;
	}

	public function add_restaurant_music($restaurant_id,$music_type)
	{
		$this->db->query("insert into tbl_restaurant_music_type set restaurant_id='$restaurant_id',music_type_id='$music_type'");
		return true;
	}

	public function add_restaurant_music_genre($restaurant_id,$music_genre)
	{
		$this->db->query("insert into tbl_restaurant_music_genre set restaurant_id='$restaurant_id',music_genre_id='$music_genre'");
		return true;
	}

	public function add_restaurant_amenities($restaurant_id,$amenities)
	{
		$this->db->query("insert into tbl_restaurant_amenities set restaurant_id='$restaurant_id',amenities_id='$amenities'");
		return true;
	}

	public function add_restaurant_food($restaurant_id,$food_type)
	{
		$this->db->query("insert into tbl_restaurant_food_type set restaurant_id='$restaurant_id',food_type_id='$food_type'");
		return true;
	}

	public function add_restaurant_crowd($restaurant_id,$average_crowd)
	{
		$this->db->query("insert into tbl_restaurant_crowd set restaurant_id='$restaurant_id',crowd_id='$average_crowd'");
		return true;
	}

	public function add_restaurant_image($restaurant_id,$file_name)
	{
		$this->db->query("insert into tbl_restaurant_photos set restaurant_id='$restaurant_id',img='$file_name'");
		return true;
	}

	public function select_restaurant_bar($restaurant_id)
	{
		$data=$this->db->query("select * from tbl_restaurant_bar_type join tbl_bar on tbl_bar.bar_id=tbl_restaurant_bar_type.bar_type_id where tbl_restaurant_bar_type.restaurant_id='$restaurant_id' ");
		return $data->result_array();
	}

	public function select_restaurant_food($restaurant_id)
	{
		$data=$this->db->query("select * from tbl_restaurant_food_type join tbl_food on tbl_food.food_id=tbl_restaurant_food_type.food_type_id where tbl_restaurant_food_type.restaurant_id='$restaurant_id' ");
		return $data->result_array();
	}

	public function select_restaurant_music($restaurant_id)
	{
		$data=$this->db->query("select * from tbl_restaurant_music_type join tbl_music on tbl_music.music_id=tbl_restaurant_music_type.music_type_id where tbl_restaurant_music_type.restaurant_id='$restaurant_id' ");
		return $data->result_array();
	}

	public function select_restaurant_music_genre($restaurant_id)
	{
		$data=$this->db->query("select * from tbl_restaurant_music_genre join tbl_music_genre on tbl_music_genre.music_genre_id=tbl_restaurant_music_genre.music_genre_id where tbl_restaurant_music_genre.restaurant_id='$restaurant_id' ");
		return $data->result_array();
	}

	public function select_restaurant_amenities($restaurant_id)
	{
		$data=$this->db->query("select * from tbl_restaurant_amenities join tbl_amenities on tbl_amenities.amenities_id=tbl_restaurant_amenities.amenities_id where tbl_restaurant_amenities.restaurant_id='$restaurant_id' ");
		return $data->result_array();
	}


	public function restaurant_info($select_restaurant_id)
	{
		$data=$this->db->query("select * from tbl_restaurant where restaurant_id='$select_restaurant_id'");
		return $data->result_array();
	}


	public function select_restaurant_crowd($select_restaurant_id)
	{
		$data=$this->db->query("select * from tbl_restaurant_crowd join tbl_crowd on tbl_crowd.crowd_id=tbl_restaurant_crowd.crowd_id where tbl_restaurant_crowd.restaurant_id='$select_restaurant_id' ");
		return $data->result_array();
	}


	public function select_restaurant_images($select_restaurant_id)
	{
		$data=$this->db->query("select * from tbl_restaurant_photos where restaurant_id='$select_restaurant_id' ");
		return $data->result_array();
	}

	public function img_delete($img_id)
	{
		$this->db->query("delete from tbl_restaurant_photos where id='$img_id'");
		return true;
	}

	public function delete_restaurant_bar($restaurant_id)
	{
		$this->db->query("delete from tbl_restaurant_bar_type where restaurant_id='$restaurant_id'");
	}

	public function delete_restaurant_music($restaurant_id)
	{
		$this->db->query("delete from tbl_restaurant_music_type where restaurant_id='$restaurant_id'");
	}
	public function delete_restaurant_music_genre($restaurant_id)
	{
		$this->db->query("delete from tbl_restaurant_music_genre where restaurant_id='$restaurant_id'");
	}

	public function delete_restaurant_amenities($restaurant_id)
	{
		$this->db->query("delete from tbl_restaurant_amenities where restaurant_id='$restaurant_id'");
	}

	public function delete_restaurant_food($restaurant_id)
	{
		$this->db->query("delete from tbl_restaurant_food_type where restaurant_id='$restaurant_id'");
	}
	public function delete_restaurant_crowd($restaurant_id)
	{
		$this->db->query("delete from tbl_restaurant_crowd where restaurant_id='$restaurant_id'");
	}

	public function update_restaurant($restaurant_id,$add_restaurant)
	{
		$this->db->where('restaurant_id',$restaurant_id);
		$this->db->update('tbl_restaurant',$add_restaurant);

		return true;
	}

	public function restaurant_delete($img_id)
	{
		$this->db->query("delete from tbl_restaurant where 	restaurant_id='$img_id' ");
		return true;
	}


	public function email_validation($email)
	{
		$data=$this->db->query("select user_email from tbl_user where user_email='$email'");
		return $data->num_rows();

	}




	/////////////////////////////////////

	public function food_record_count($where='')
	{

		$query=$this->db->select('*')
		->from('tbl_food');
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}

	public function select_food($where='',$limit=null,$offset=NULL)
	{
		 $this->db->select('*');
    $this->db->from('tbl_food');
    $this->db->order_by("food_id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	public function add_food($data)
	{
		$this->db->insert('tbl_food',$data);
		return truel;
	}

	public function update_food($data,$state_id)
	{
		$this->db->where("food_id",$state_id);

				$this->db->update("tbl_food",$data);
				return true;
	}

	public function food_delete($food_id)
	{
		$this->db->query("delete from tbl_food where food_id='$food_id'");
		return true;
	}

	public function music_record_count($where='')
	{

		$query=$this->db->select('*')
		->from('tbl_music');
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}

	public function select_music($where='',$limit=null,$offset=NULL)
	{
		 $this->db->select('*');
    $this->db->from('tbl_music');
    $this->db->order_by("music_id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	public function add_music($data)
	{
		$this->db->insert('tbl_music',$data);
		return truel;
	}

	public function update_music($data,$music_id)
	{
		$this->db->where("music_id",$music_id);

				$this->db->update("tbl_music",$data);
				return true;
	}

	public function music_delete($music_id)
	{
		$this->db->query("delete from tbl_music where music_id='$music_id'");
		return true;
	}


		public function bar_record_count($where='')
	{

		$query=$this->db->select('*')
		->from('tbl_bar');
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}

	public function select_bar($where='',$limit=null,$offset=NULL)
	{
		 $this->db->select('*');
    $this->db->from('tbl_bar');
    $this->db->order_by("bar_id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	public function add_bar($data)
	{
		$this->db->insert('tbl_bar',$data);
		return truel;
	}

	public function update_bar($data,$bar_id)
	{
		$this->db->where("bar_id",$bar_id);

				$this->db->update("tbl_bar",$data);
				return true;
	}

	public function bar_delete($bar_id)
	{
		$this->db->query("delete from tbl_bar where bar_id='$bar_id'");
		return true;
	}


	
	


			public function amenities_record_count($where='')
	{

		$query=$this->db->select('*')
		->from('tbl_amenities');
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}

	public function select_amenities($where='',$limit=null,$offset=NULL)
	{
		 $this->db->select('*');
    $this->db->from('tbl_amenities');
    $this->db->order_by("amenities_id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	public function add_amenities($data)
	{
		$this->db->insert('tbl_amenities',$data);
		return truel;
	}

	public function update_amenities($data,$amenities_id)
	{
		$this->db->where("amenities_id",$amenities_id);

				$this->db->update("tbl_amenities",$data);
				return true;
	}

	public function amenities_delete($amenities_id)
	{
		$this->db->query("delete from tbl_amenities where amenities_id='$amenities_id'");
		return true;
	}


// New


			public function music_genre_record_count($where='')
	{

		$query=$this->db->select('*')
		->from('tbl_music_genre');
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}

	public function select_music_genre($where='',$limit=null,$offset=NULL)
	{
		 $this->db->select('*');
    $this->db->from('tbl_music_genre');
    $this->db->order_by("music_genre_id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	public function add_music_genre($data)
	{
		$this->db->insert('tbl_music_genre',$data);
		return truel;
	}

	public function update_music_genre($data,$music_genre_id)
	{
		$this->db->where("music_genre_id",$music_genre_id);

				$this->db->update("tbl_music_genre",$data);
				return true;
	}

	public function music_genre_delete($music_genre_id)
	{
		$this->db->query("delete from tbl_music_genre where music_genre_id='$music_genre_id'");
		return true;
	}



				public function crowd_record_count($where='')
	{

		$query=$this->db->select('*')
		->from('tbl_crowd');
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}

	public function select_crowd($where='',$limit=null,$offset=NULL)
	{
		 $this->db->select('*');
    $this->db->from('tbl_crowd');
    $this->db->order_by("crowd_id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	public function add_crowd($data)
	{
		$this->db->insert('tbl_crowd',$data);
		return truel;
	}

	public function update_crowd($data,$crowd_id)
	{
		$this->db->where("crowd_id",$crowd_id);

				$this->db->update("tbl_crowd",$data);
				return true;
	}

	public function crowd_delete($crowd_id)
	{
		$this->db->query("delete from tbl_crowd where crowd_id='$crowd_id'");
		return true;
	}




	public function coupon_record_count($where='')
	{

		$query=$this->db->select('*')
		->from('tbl_coupon');
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}

	public function select_coupon($where='',$limit=null,$offset=NULL)
	{
		 $this->db->select('*');
    $this->db->from('tbl_coupon');
    $this->db->order_by("tbl_coupon.coupon_id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("tbl_coupon.status='$where'");			
		}
		$this->db->join('tbl_user', 'tbl_coupon.created_by = tbl_user.user_id');
		$this->db->join('tbl_restaurant', 'tbl_user.user_id = tbl_restaurant.user_id');
		$query = $this->db->get();

		return $query->result_array();

	}

	public function add_coupon($data) {
		$this->db->insert('tbl_coupon', $data);
		return true;
	}
	public function update_coupon($data,$coupon_id) {
		$this->db->where("coupon_id", $coupon_id);
		$this->db->update("tbl_coupon", $data);
		return true;
	}

	public function add_deal($data)
	{
		$this->db->insert('tbl_deals',$data);
		return truel;
	}

	public function add_match($data)
	{
		/*$query = $this->db->get_where('tbl_sport_event', array(//making selection
            'event_name' => $data['event_name']
        ));*/
        $event_name = $data['event_name'];
        $query=$this->db->query("select * from tbl_sport_event where event_name = '$event_name'");
		$record = $query->result_array();
        if (count($record) > 0) {
            return  $record[0]['match_id'];
        }
        else {
	       
	        $this->db->insert('tbl_sport_event', $data);
			return $this->db->insert_id();
		}
	}

	public function add_team($data)
	{
		$this->db->insert('tbl_sport_team',$data);
		return true;
	}
	

	public function update_deal($data,$deal_id)
	{
		$this->db->where("id",$deal_id);
		$this->db->update("tbl_deals",$data);
		return true;
	}

	public function coupon_delete($coupon_id)
	{
		$this->db->query("delete from tbl_coupon where coupon_id='$coupon_id'");
		return true;
	}

	public function member_ship_plan()
	{
		$data=$this->db->query("select * from tbl_membership");
		return $data->result_array();
	}

	public function update_member_ship_plan($data,$membership_id)
	{
		$this->db->where('membership_id',$membership_id);
		$this->db->update('tbl_membership',$data);
		return true;
	}


	function admin_forgot_password($admin_id,$new_pass)
	{
		$this->db->query("update tbl_user set user_password='$new_pass' where user_email='$admin_id'");
		return true;
	}


	//////////////////////////////////////11/12/2018////////////////////////////

	public function average_crowd()
	{
		$data=$this->db->query("select * from tbl_crowd where status='1'");
		return $data->result_array();
	}

	public function music_genre()
	{
		$data=$this->db->query("select * from tbl_music_genre where status='1'");
		return $data->result_array();
	}

	public function add_events($add_event)
	{
		$this->db->insert('tbl_event',$add_event);
		return true;
	}

	public function event_record_count($where = [], $user_id = '') {
		$query = $this->db->select('*')
		->from('tbl_event')
		->where("user_id = '$user_id'");
		if($where) {
			$query->where($where);
		}
		/*if($where != "") {
			$query->where('status', $where);
		}*/
		$query = $query->get();
		return $query->num_rows();
	}

	public function select_event($where = [], $limit = null, $offset = NULL, $restaurant_id = NULL) {
		$this->db->select('tbl_event.event_id,tbl_event.user_id,tbl_event.event_description,tbl_event.event_terms,tbl_event.event_name,tbl_event.event_close_time,tbl_event.event_imsge,tbl_event.event_date,tbl_event.event_time,tbl_event.cover_charge,tbl_event.average_crowd,tbl_event.music_genre,tbl_event.status,tbl_music_genre.music_genre_id,tbl_music_genre.music_genre_name,tbl_event.address,tbl_event.lat,tbl_event.log,tbl_event.venue,tbl_event.phone');
		$this->db->from('tbl_event');
		$this->db->join('tbl_music_genre', 'tbl_music_genre.music_genre_id = tbl_event.music_genre', 'left'); 
		$this->db->where("user_id='$restaurant_id'") ;
		$this->db->order_by("tbl_event.event_id",'DESC');
		$this->db->limit($limit, $offset);
		
		//$this->db->where($where);
        if($where){
			foreach ($where as $key => $condition) {
				if ($condition != '') {
					$this->db->where("$key = '$condition'");	
				}
			}
		}
		$query = $this->db->get();

		return $query->result_array();
	}


	public function update_event($update_event,$event_id)
	{
		 $this->db->where("event_id",$event_id);

		$this->db->update("tbl_event",$update_event);
		return true;
	}

	public function event_delete($state_id)
	{
		$this->db->query("delete from tbl_event where event_id='$state_id'");
		return true;
	}

	public function deal_delete($deals_id)
	{
		$this->db->query("delete from tbl_deals where id='$deals_id'");
		return true;
	}



	public function coupon_record_counts($where='',$user_d="")
	{

		$query=$this->db->select('*')
		->from('tbl_coupon')
		->where("created_by='$user_d'") ;
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}

	public function select_coupons($where='',$limit=null,$offset=NULL,$restaurant_id1=NULL)
	{
		
		 $this->db->select('*');
    $this->db->from('tbl_coupon');
    $this->db->where("created_by='$restaurant_id1'") ;
    $this->db->order_by("coupon_id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	public function restaurant_all_info($restaurant_id)
	{
		$data=$this->db->query("select * from tbl_user join tbl_restaurant on tbl_restaurant.user_id=tbl_user.user_id where tbl_restaurant.restaurant_id='$restaurant_id'");
		return $data->result_array();
	}

	public function restaurant_data($user_id)
	{
		$data=$this->db->query("select restaurant_id from tbl_restaurant where user_id='$user_id' ");
		return $data->result_array();
	}


	public function update_user($use_data,$user_id)
	{
		$this->db->where('user_id',$user_id);
		$this->db->update('tbl_user',$use_data);
		return true;
	}

	public function setting()
	{
		$data=$this->db->query("select * from tbl_banner");
		return $data->result_array();
	}

	public function registration_setting($user_id)
	{
		$data=$this->db->query("select * from tbl_restaurant_banner where user_id='$user_id'");
		return $data->result_array();
	}

	public function update_banner($banner_id,$file_name)
	{
		$this->db->query("update tbl_banner set banner_img='$file_name' where id='$banner_id'");
		return true;
	}
	public function update_t_c($t_c_id,$data2)
	{
		$this->db->where('id',$t_c_id);
		$this->db->update('tbl_banner',$data2);
	}
	public function update_t_c_r($t_c_id,$data2,$title)
	{
		$this->db->where('user_id',$t_c_id);
		$this->db->where('title',$title);
		$this->db->update('tbl_restaurant_banner',$data2);
		return true;
	}

	public function update_banner_r($t_c_id,$data2,$title)
	{
		$this->db->where('user_id',$t_c_id);
		$this->db->where('title',$title);
		$this->db->update('tbl_restaurant_banner',$data2);
		return true;
	}

	public function update_about($t_c_id,$data2)
	{
		$this->db->where('id',$t_c_id);
		$this->db->update('tbl_banner',$data2);
	}
	public function update_about_r($t_c_id,$data2,$title)
	{
		$this->db->where('user_id',$t_c_id);
		$this->db->where('title',$title);
		$this->db->update('tbl_restaurant_banner',$data2);
		return true;
	}

	public function update_p_p($t_c_id,$data2)
	{
		$this->db->where('id',$t_c_id);
		$this->db->update('tbl_banner',$data2);
		echo $this->db->last_query();
	}


	public function update_p_p_r($t_c_id,$data2,$title)
	{
		$this->db->where('user_id',$t_c_id);
		$this->db->where('title',$title);
		$this->db->update('tbl_restaurant_banner',$data2);
		return true;
	}

	public function feedback_record_count($value='')
	{
		$query = $this->db->select('*')
			->from('tbl_feedback');
		$query = $query->get();	
		return $query->num_rows();
	}

	public function select_feedback($where = '',$limit=null,$offset=NULL) {
		$this->db->select('*');
		$this->db->from('tbl_feedback');
		$this->db->join('tbl_user', 'tbl_user.user_id = tbl_feedback.user_id', 'left'); 
		$this->db->order_by("id",'DESC');
		$this->db->limit($limit, $offset);
		if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	public function feedback_delete($id)
	{
		$this->db->query("delete from tbl_feedback where id='$id'");
		return true;
	}



	function country_record_count($where = ''){
    
		$query = $this->db->select('*')
			->from('tbl_country');
			  
	if($where!=""){
			$query->where("status",$where);			
		}
		$query = $query->get();	
		return $query->num_rows();

	}


	public function select_country($where = '',$limit=null,$offset=NULL) {
      
       $this->db->select('*');
    $this->db->from('tbl_country');
    $this->db->order_by("id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();
    
	}


	public function select_coupon_list()
	{
		$data=$this->db->query("select * from tbl_states where status='1'");
		return $data->result_array();
	}

	public function list_states($id)
	{
		$data=$this->db->query("select * from tbl_states where country_id='$id'");
		return $data->result_array();
	}


	function city_record_count($where = ''){
    
		$query = $this->db->select('*')
			->from('tbl_city');
			  
	if($where!=""){
			$query->where("status",$where);			
		}
		$query = $query->get();	
		return $query->num_rows();

	}

		public function select_city($where = '',$limit=null,$offset=NULL) {
      
       $this->db->select('tbl_city.id,tbl_city.city_name,tbl_city.status,tbl_country.country_name,tbl_country.id as country_id,tbl_states.state_id,tbl_states.state_name');
    $this->db->from('tbl_city');
      $this->db->join('tbl_country', 'tbl_country.id = tbl_city.country_name', 'left'); 
      $this->db->join('tbl_states', 'tbl_states.state_id = tbl_city.state_name', 'left'); 
    $this->db->order_by("tbl_city.id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("tbl_city.status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();
    
	}

	public function select_restaurant_id($user_id)
	{
		$data=$this->db->query("select restaurant_id from tbl_restaurant where user_id='$user_id'");
		return $data->result_array();
	}


	public function country_delete($country_id)
	{
		$this->db->query("delete from tbl_country where id='$country_id'");
		return true;
	}
    public function restaurant_details($restaurant_id)
    {
    	$data=$this->db->query("select * from tbl_restaurant left join tbl_states on tbl_states.state_id=tbl_restaurant.state where tbl_restaurant.restaurant_id='$restaurant_id'");
    	return $data->result_array();
    }

    public function restaurant_coupon_delete($restaurant_id)
    {
    	$this->db->query("delete from tbl_coupon where created_by='$restaurant_id'");
		return true;

    }

    public function restaurant_event_delete($restaurant_id)
    {
    	$this->db->query("delete from tbl_event where user_id='$restaurant_id'");
		return true;

    }

     public function restaurant_rating_delete($restaurant_id)
    {
    	$this->db->query("delete from tbl_week_rating where user_id='$restaurant_id'");
		return true;

    }

    public function select_event_going($id,$user_id)
    {
    	$query=$this->db->select('*')
		->from('tbl_week_rating')
		->where("restaurant_id='$user_id' and event_id='$id' and going!=''");
		
		$query=$query->get();
		return $query->num_rows();

    }

     public function select_event_may_be($id,$user_id)
    {
    	$query=$this->db->select('*')
		->from('tbl_week_rating')
		->where("restaurant_id='$user_id' and event_id='$id' and may_be!=''");
		
		$query=$query->get();
		return $query->num_rows();

    }

     public function select_event_not_going($id,$user_id)
    {
    	$query=$this->db->select('*')
		->from('tbl_week_rating')
		->where("restaurant_id='$user_id' and event_id='$id' and not_going!=''");
		
		$query=$query->get();
		return $query->num_rows();

    }


    public function user_feedback_count($where='',$user_id)
	{

		$query=$this->db->select('*')
		->from('tbl_restaurant_star')
		->where("restaurant_id='$user_id'") ;
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}


	public function select_user_feedback($where='',$limit=null,$offset=NULL,$restaurant_id1=NULL)
	{
		
		 $this->db->select('*');
    $this->db->from('tbl_restaurant_star');
      $this->db->join('tbl_user', 'tbl_user.user_id= tbl_restaurant_star.user_id', 'left'); 
      $this->db->where("tbl_restaurant_star.restaurant_id='$restaurant_id1'");		
    $this->db->order_by("tbl_restaurant_star.id",'DESC');
        $this->db->limit($limit, $offset);
  //       if($where!=""){
		// 	$this->db->where("tbl_states.status='$where'");			
		// }
		$query = $this->db->get();

		return $query->result_array();

	}


	public function feedback_deletes($id)
	{
		$this->db->query("delete from tbl_restaurant_star where id='$id'");
		return true;
	}

   public function set_promote_type($value)
   {
   	$this->db->insert('tbl_promote',$value);
   	return true;
   }

   public function update_promote_type($value,$Promote_id)
   {
   	$this->db->where('id',$Promote_id);
   	$this->db->update('tbl_promote',$value);
   	return true;
   }

   function promote_type_record_count($where = ''){
    
		$query = $this->db->select('*')
			->from('tbl_promote');
			  
	if($where!=""){
			$query->where("status",$where);			
		}
		$query = $query->get();	
		return $query->num_rows();

	}

	public function promote_type_record($where='',$limit=null,$offset=NULL)
	{
		 $this->db->select('*');
    $this->db->from('tbl_promote');    
    $this->db->order_by("id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	public function promote_delete($id)
	{
		$this->db->query("delete from tbl_promote where id='$id'");
		return true;
	}

	public function get_t_c($user_id,$title){
		$this->db->select('*');
		$this->db->from('tbl_restaurant_banner');
		$this->db->where('user_id',$user_id);
		$this->db->where('title',$title);
		$data=$this->db->get();
		return $data->num_rows();
	}

	public function isert_data($data2)
	{
		$this->db->insert('tbl_restaurant_banner',$data2);
		return true;
	}

	public function deal_record_counts($where='',$user_d="")
	{

		$query=$this->db->select('*')
		->from('tbl_deals')
		->where("restaurant_id='$user_d'") ;
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}


public function select_deal($where='',$limit=null,$offset=NULL,$restaurant_id1=NULL)
	{
		
		 $this->db->select('*');
    $this->db->from('tbl_deals');
    $this->db->where("restaurant_id='$restaurant_id1'") ;
    $this->db->order_by("id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	public function deal_record_promote_counts($where='',$user_d="")
	{

		$query=$this->db->select('*')
		->from('tbl_deals')
		->where("restaurant_id='$user_d'");
		$query->where('status','1');
		if($where!="")
		{
			$query->where('status',$where);
		}
		$query=$query->get();
		return $query->num_rows();

	}

	public function select_deal_promote($where='',$limit=null,$offset=NULL,$restaurant_id1=NULL)
	{
		$this->db->select('tbl_deals.id,tbl_deals.restaurant_id,tbl_deals.deal_title,tbl_deals.deal_description,tbl_deals.start_date,tbl_deals.end_date,tbl_deals.t_c,tbl_deals.img,tbl_deals.deal_name,tbl_promote.status,tbl_promote.start_date as deal_start_date,tbl_promote.end_date as deal_end_date ');
    $this->db->from('tbl_deals');
    $this->db->join('tbl_promote','tbl_deals.id=tbl_promote.deal_id','left');
    $this->db->where("tbl_deals.restaurant_id='$restaurant_id1'");
    $this->db->where("tbl_deals.status='1'");
    $this->db->order_by("tbl_deals.id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("tbl_deals.status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	

	 public function get_number_promote($value='')
	{
		$this->db->select('*');
		$this->db->from('tbl_promote');
		$this->db->where('read_status','1');
		$query=$this->db->get();
		return $query->num_rows();
	}

	public function promote_requerst_record_count($value='')
	{
		$this->db->select('*');
		$this->db->from('tbl_promote');
		//$this->db->join('tbl_user','tbl_user.user_id=tbl_promote.restaurant_id','left');
		$this->db->order_by('tbl_promote.id','DESC');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function promote_requerst_record($where='',$limit=null,$offset=NULL,$restaurant_id1=NULL)
	{
		
		 $this->db->select('tbl_user.first_name,tbl_user.last_name,tbl_user.user_email,tbl_user.user_email,tbl_promote.deal_id,tbl_promote.banner_id,tbl_promote.restaurant_id,tbl_promote.start_date,tbl_promote.end_date,tbl_promote.status,tbl_promote.id');
   $this->db->from('tbl_promote');
   $this->db->join('tbl_user','tbl_user.user_id=tbl_promote.restaurant_id','left');
    $this->db->order_by("tbl_promote.id",'DESC');
        $this->db->limit($limit, $offset);
        if($where!=""){
			$this->db->where("tbl_promote.status='$where'");			
		}
		$query = $this->db->get();

		return $query->result_array();

	}

	public function change_promote_status($Promote_id,$status)
	{
		$this->db->where('id',$Promote_id);
		$this->db->update('tbl_promote',$status);
		return true;
	}

	public function update_read_count($arrayName1){
		$this->db->update('tbl_promote',$arrayName1);
		return true;
	}

	public function banner_record_promote_counts($Searchkey='',$user_id)
	{
		
		$this->db->select('*');
		$this->db->from('tbl_promote_banner');	
		$this->db->join('tbl_promote','tbl_promote_banner.id=tbl_promote.banner_id','left');		
		$this->db->where("tbl_promote_banner.restaurant_id",$user_id);
		//$this->db->where("tbl_promote.banner_id !='' ");
		//$this->db->where($whereArray);
		$this->db->order_by('tbl_promote_banner.id','DESC');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function select_banner_promote($where = '', $limit = NULL, $offset = NULL, $user_id = NULL) {

		$this->db->select('*');
		$this->db->from('tbl_promote_banner');
		//$this->db->join('tbl_promote','tbl_promote_banner.id=tbl_promote.banner_id','left');
		$this->db->where("tbl_promote_banner.restaurant_id", $user_id);
		//$this->db->where("tbl_promote.banner_id !='' ");
		//$this->db->order_by("tbl_promote_banner.id",'DESC');
		$this->db->limit($limit, $offset);
		if($where != ""){
		$this->db->where("tbl_promote_banner.status='$where'");
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function add_subscription($data='')
	{
		return $insert = $this->db->insert('tbl_subscriber',$data);
	
	}

	public function restaurant_package($data)
	{
		$dataa = $this->db->select("package_id")->from('tbl_subscriber')->where("restaurant_id",$data)->order_by('id', 'desc')->get()->row_array();
		return $dataa;
	}

	public function get_transaction_count($startdate,$enddate,$Searchkey)
    {
        $this->db->select('*');
        $this->db->from('tbl_subscriber');
        $this->db->join('tbl_user','tbl_user.user_id=tbl_subscriber.restaurant_id','left');

        if (!empty($Searchkey)) {
            $this->db->like('tbl_user.first_name',$Searchkey);
        }

        if (!empty($startdate)) {
            $this->db->where("DATE_FORMAT(tbl_subscriber.created,'%Y-%m-%d')>='$startdate'");
        }
        if (!empty($enddate)) {

           $this->db->where("DATE_FORMAT(tbl_subscriber.created,'%Y-%m-%d')<='$enddate'");
        }

        $this->db->order_by('tbl_subscriber.id','DESC');
        $data=$this->db->get();
        return $data->num_rows();
    }

    public function get_transaction($limit=null,$offset=NULL,$startdate,$enddate,$Searchkey)
    {
        $this->db->select('*');
        $this->db->from('tbl_subscriber');
        $this->db->join('tbl_user','tbl_user.user_id=tbl_subscriber.restaurant_id','left');

        if (!empty($Searchkey)) {
            $this->db->like('tbl_user.first_name',$Searchkey);
        }

        if (!empty($startdate)) {
            $this->db->where("DATE_FORMAT(tbl_subscriber.created,'%Y-%m-%d')>='$startdate'");
        }
        if (!empty($enddate)) {

           $this->db->where("DATE_FORMAT(tbl_subscriber.created,'%Y-%m-%d')<='$enddate'");
        }

        $this->db->order_by('tbl_subscriber.id','DESC');
        $data=$this->db->get();
        return $data->result_array();
    }

    public function contact_us($value)
	{
		$this->db->insert('tbl_contact_us',$value);
		return true;
	}

	// Start 24-Dec-2019 //
	// Events Detail //

	public function showEvent($restaurant_id = NULL) {
		$this->db->select('tbl_event.event_id,tbl_event.event_name');
		$this->db->from('tbl_event');
		$this->db->where("user_id='$restaurant_id'") ;
		$data = $this->db->get();
		return $data->result_array();
	}

	public function add_event_promote($data) {
		$this->db->insert('tbl_promote',$data);
		return true;
	}

	public function select_promote_event($limit=null, $offset=NULL, $restaurant_id1=NULL) {
		$this->db->select('tbl_event.event_id,tbl_event.event_imsge,tbl_event.event_name,tbl_event.event_date,tbl_event.event_time,tbl_event.event_close_time,tbl_promote.start_date,tbl_promote.start_time,tbl_promote.end_date,tbl_promote.end_time,tbl_promote.status');
		$this->db->from('tbl_event');
	    $this->db->join('tbl_promote','tbl_event.event_id = tbl_promote.event_id','right');
	    $this->db->where("tbl_event.user_id='$restaurant_id1'");
	    $this->db->where("tbl_event.status='1'");
	    $this->db->order_by("tbl_event.event_id",'ASC');
        $this->db->limit($limit, $offset);
		$data = $this->db->get();
		return $data->result_array();
	}

	// Deals Deatails //

	public function showDeal($restaurant_id = NULL) {
		$this->db->select('tbl_deals.id,tbl_deals.deal_title');
		$this->db->from('tbl_deals');
		$this->db->where("restaurant_id='$restaurant_id'") ;
		$data = $this->db->get();
		//print_r($data);
		return $data->result_array();
	}

	public function add_deal_promote($data){
		$this->db->insert('tbl_promote',$data);
		return true;
	}

	public function select_promote_deal($limit=null, $offset=NULL, $restaurant_id1=NULL) {
		$this->db->select('tbl_deals.id,tbl_deals.img,tbl_deals.deal_title,tbl_deals.start_date,tbl_deals.end_date,tbl_deals.deal_start_time,tbl_deals.deal_end_time,tbl_promote.start_date,tbl_promote.start_time,tbl_promote.end_date,tbl_promote.end_time,tbl_promote.status');
		$this->db->from('tbl_deals');
	    $this->db->join('tbl_promote','tbl_deals.id = tbl_promote.deal_id','right');
	    $this->db->where("tbl_deals.restaurant_id='$restaurant_id1'");
	    $this->db->where("tbl_deals.status='1'");
	    $this->db->order_by("tbl_deals.id",'ASC');
        $this->db->limit($limit, $offset);
		$data = $this->db->get();
		return $data->result_array();
	}

	// Banner Details

	public function add_banner($data){
		$this->db->insert('tbl_promote_banner',$data);
		return true;
	}

	public function showBanner($restaurant_id = NULL) {
		$this->db->select('ban.id,ban.title');
		$this->db->from('tbl_promote_banner as ban');
		$this->db->where("restaurant_id = '$restaurant_id'") ;
		$data = $this->db->get();
		return $data->result_array();
	}

	public function update_banner_manager($data,$id)
	{
		$this->db->where("id",$id);
		$this->db->update("tbl_promote_banner",$data);
		return true;
	}	

	public function delete_banner_manager($id)
	{
		$this->db->query("delete from tbl_promote_banner where id ='$id'");
		return true;
	}


	// Promote Banner //
	public function add_banner_promote($data)
	{
		$this->db->insert('tbl_promote',$data);
		return true;
	}

	public function select_promote_banner($limit=null, $offset=NULL, $restaurant_id1=NULL) {
		$this->db->select('ban.id,ban.banner_img,ban.title,tbl_promote.start_date,tbl_promote.start_time,tbl_promote.end_date,tbl_promote.end_time,tbl_promote.status');
		$this->db->from('tbl_promote_banner as ban');
	    $this->db->join('tbl_promote','ban.id = tbl_promote.banner_id','right');
	    $this->db->where("ban.restaurant_id='$restaurant_id1'");
	    $this->db->where("ban.status='1'");
	    $this->db->order_by("ban.id",'ASC');
        $this->db->limit($limit, $offset);
		$data = $this->db->get();
		return $data->result_array();
	}

	// Subscription Manager -> Price Slot //

	public function addPriceSlot($data){
		$this->db->insert('tbl_price_slot',$data);
		return true;
	}

	public function select_price_slot($limit=null, $offset=NULL) {
		$this->db->select('*');
		$this->db->from('tbl_price_slot as price');
	    $this->db->order_by("price.price_id",'ASC');
        $this->db->limit($limit, $offset);
		$data = $this->db->get();
		return $data->result_array();
	}

	public function delete_price_slot($price_id)
	{
		$this->db->query("delete from tbl_price_slot where price_id ='$price_id'");
		return true;
	}

	public function update_price_slot($data,$price_id)
	{
		$this->db->where("price_id",$price_id);
		$this->db->update("tbl_price_slot",$data);
		return true;
	}

	// Fanspace Special Offer //

	public function showRestaurants() {
		$this->db->select('res.id,res.title');
		$this->db->from('tbl_restaurant as res');
		$data = $this->db->get();
		return $data->result_array();
	}

	public function addFanspaceSpecial($data){
		$this->db->insert('tbl_fanspace_special',$data);
		return true;
	}

	public function selectFanspaceSpecial($limit=null, $offset=NULL){
		$this->db->select('tbl_fanspace_special.special_id,tbl_fanspace_special.restuarant_id,tbl_fanspace_special.status,res.id,res.title');
		$this->db->from('tbl_fanspace_special');
	    $this->db->join('tbl_restaurant as res','tbl_fanspace_special.restuarant_id = res.id','left');
        $this->db->limit($limit, $offset);
		$data = $this->db->get();
		return $data->result_array();
	}

	public function deleteFanspaceSpecial($special_id)
	{	
		$this->db->query("delete from tbl_fanspace_special where special_id ='$special_id'");
		//echo $this->db->last_query();die;
		return true;
	}

	public function updateFanspaceSpecial($data,$special_id)
	{
		$this->db->where("special_id",$special_id);
		$this->db->update("tbl_fanspace_special",$data);
		return true;
	}

	// Searching by Restuarants //

	public function searchRestaurants($search_data = NULL){
		$this->db->select('*');
		$this->db->from('tbl_restaurant');
		if($search_data != ''){
			$this->db->like('tbl_restaurant.title', $search_data);
		}
		//echo $this->db->last
		$data = $this->db->get();
        return $data->result_array();
	}

	// End 02-01-2020 //
}