<?php $this->load->view('user/include/header'); ?>

		<div class="main-container ace-save-state" id="main-container">
		

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
 <?php $this->load->view('user/include/navigation'); ?>

			 	
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('User_controller') ?>">Home</a>
							</li>
							<li class="active">Gallery</li>
						</ul><!-- /.breadcrumb -->

					
					</div>

					<div class="page-content">
						
							<div class="page-header">
							<button style="float: right;margin-bottom: inherit;height: 40px;width: 30%;color: white;  background-color: #438eb9;border-radius: 10px;"   type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1" >Add Gallery Images</button>
							<h1>
								<i class="fa fa-user" aria-hidden="true"></i>
								Photos Management
							
							</h1>
						</div><!-- /.page-header -->

						<?php if ( !empty($this->session->flashdata('msg'))) {
							?>

							 <div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $this->session->flashdata('msg'); ?>
 
                </div>
					
							
						<?php
					} ?>
								

						<div class="page-header">
							<h1>
								Gallery
								
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div>
									<ul class="ace-thumbnails clearfix">
										
									<?php  foreach ($result as  $value) {
										 ?>
										
										<li>
											<a class="open_image" href="<?php echo base_url()?>assets/images/<?php echo $value['img'] ?>">
												<img width="150" height="150" alt="150x150" src="assets/images/<?php echo $value['img'] ?>">
												<div class="tags">
													<span class="label-holder">
														<span class="label label-info arrowed"></span>
													</span>

													
												</div>
											</a>

											<div class="tools tools-top">
												
												<a href="#">
													<i class="ace-icon fa fa-times red delete" id="<?php echo $value['id'] ?>" ></i>
												</a>
											</div>
										</li>
  
										<?php  }?>

										
										
									</ul>
								</div><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			 					

			 <?php $this->load->view('user/include/footer'); ?> 

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!--Gallery Popup-->
<link rel="stylesheet" href="<?php echo base_url('assets/')?>magnific-popup/magnific-popup.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?php echo base_url('assets/')?>magnific-popup/jquery.magnific-popup.js"></script>
<!--Gallery Popup-->
<script type="text/javascript">
$(document).ready(function(){
	$('.delete').click(function(){
		if(confirm("Do you want to delete?"))
		{

		var coupon_id=$(this).attr('id');
		}else{
			  return false;
		}

		$.post("<?php echo base_url('Admin_controller/img_delete') ?>",{img_id:coupon_id},function(res){
			// if(res=='success')
			// {
				 location.reload();
			//}
		})
	})
});
<!--Gallery Popup-->
$('.ace-thumbnails').magnificPopup({
	delegate: 'a.open_image',
	type: 'image',
	tLoading: 'Loading image #%curr%...',
	mainClass: 'mfp-img-mobile',
	gallery: {
		enabled: true,
		navigateByImgClick: true,
		preload: [0,1] // Will preload 0 - before current, and 1 after the current image
	},
	image: {
		tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		titleSrc: function(item) {
			//return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
		}
	}
});
<!--Gallery Popup-->
</script>

		
	</body>
</html>
