	<?php $this->load->view('user/include/header'); ?>

		<div class="main-container ace-save-state" id="main-container">
			

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				
 <?php $this->load->view('user/include/navigation'); ?>

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('User_controller') ?>">Home</a>
							</li>

							
							
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
	

						<div class="page-header">
							
							<h1>
								<i class="fa fa-user" aria-hidden="true"></i>
								Feedback
							
							</h1>
						</div><!-- /.page-header -->

						<?php if ( !empty($this->session->flashdata('msg'))) {
							?>

							 <div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $this->session->flashdata('msg'); ?>
 
                </div>
					
							
						<?php
					} ?>

				

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									<div class="col-xs-12">
										<table id="simple-table" class="table  table-bordered table-hover">
											<thead>
												<tr>
																									
													<th>#</th>													     
															
															<th>User Name</th>	
															<th>Message</th>						
															
													

													<th>Action</th>
												</tr>
											</thead>

											<tbody>
												<?php
												$count=$star;
												 foreach ($result as $value) {
												 //print_r($value);
													$count++
													 ?>
												<tr>
													
													<td><?php echo $count; ?></td>
													
													<td class="hidden-480"><?php echo $value['first_name']." ".$value['last_name']; ?></td>
													<td class="hidden-480"><?php echo $value['msg']; ?></td>
													
												

													<td>
														<div class="hidden-sm hidden-xs btn-group">					

			

															<button id="<?php echo $value['id'] ?>" class="btn btn-xs btn-danger delete">
																<i class="ace-icon fa fa-trash-o bigger-120 "></i>
															</button>

															
														</div>


														  <div class="modal fade" id="myModal<?php echo $count; ?>" role="dialog">
                            <div class="modal-dialog">
                            
             
                            

														
													</td>
												</tr>
												<?php
											}
											?>

											
											</tbody>
										</table>
									</div><!-- /.span -->
								</div><!-- /.row -->

								<div class="row">
              <div class="col-md-12">
      <div class="row"><?php echo $this->pagination->create_links(); ?></div> 
     </div>
    </div>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

		
 <?php $this->load->view('user/include/footer'); ?> 
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		

	</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.delete').click(function(){
			if(confirm("Do you want to delete?"))
			{

			var id=$(this).attr('id');
			}else{
				  return false;
			}

			$.post("<?php echo base_url('User_controller/feedback_delete') ?>",{id:id},function(res){
				// if(res=='success')
				// {
					 location.reload();
				//}
			})
		})
	})

</script>

