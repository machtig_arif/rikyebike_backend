<?php $this->load->view('user/include/header'); ?>
	<div class="main-container ace-save-state" id="main-container">
		<div id="sidebar" class="sidebar responsive ace-save-state">		
 			<?php $this->load->view('user/include/navigation'); ?>
			<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
				<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
			</div>
		</div>
		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="<?php echo base_url('User_controller') ?>">Home</a>
						</li>
					</ul>
				</div>
				<div class="page-content">
					<div class="page-header">
						<button style="float: right;margin-bottom: inherit;height: 40px;width: 30%;color: white;  background-color: #438eb9;border-radius: 10px;"   type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1" >Add Banner
						</button>
						<h1>
							<i class="fa fa-user" aria-hidden="true"></i>
							Banner Management
						</h1>
					</div>
					<?php 
						if ( !empty($this->session->flashdata('msg'))) {
					?>
						<div class="alert alert-block alert-success" style="text-align: center;">
	                  		<button type="button" class="close" data-dismiss="alert">
	                    		<i class="ace-icon fa fa-times"></i>
	                 		</button>
	                  		<i class="ace-icon fa fa-check green"></i>
	                		<?php echo $this->session->flashdata('msg'); ?>
	                	</div>	
					<?php
						}
					?>
					<div class="col-md-12 clearfix">
                        <form class="form-horizontal" action="" method="get">
                            <div  class="form-group pull-right">
                                <div class="col-md-12">
                                    <select style="border-radius: 10px;width: 160%;background-color: #f1ebe0;" class="form-control pull-right" name="status" onchange="this.form.submit()">
                                        <option value="1"> Active</option>
                                        <option <?php echo $this->input->get('status') == '0' ? 'selected':''; ?> value="0"> Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
								<div class="col-xs-12">
									<table id="simple-table" class="table  table-bordered table-hover">
										<thead>
											<tr>
												<th>#</th>
										        <th>Banner Image</th>
												<th>Banner Title</th>
												<th>Description</th>		
												<th class="hidden-480">Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$count=$start;
											 	foreach ($result as $value) {
													$count++
											?>
											<tr>
												<td>
													<?php echo $count; ?>
												</td>
												<td class="hidden-480">
													<img style="height: 100px;width: 100px;" src="<?php echo base_url()?>assets/images/<?php echo $value['banner_img'] ?>">
												</td>
												<td class="hidden-480">
													<?php echo $value['title']; ?>
												</td>
												<td class="hidden-480">
													<?php  echo $value['description']; ?>
												</td>
												<td class="hidden-480">
														<?php if($value['status']==1){ ?>
															<span class="label label-sm label-success">Active</span>
														<?php }elseif($value['status']==0){?>
														<span class="label label-sm label-danger arrowed-in">Inactive</span>
													<?php }?>
												</td>
	                                            <td>
														<div class="hidden-sm hidden-xs btn-group">
															

															<button class="btn btn-xs btn-info" id="bt-modal" data-toggle="modal" data-target="#myModal<?php echo $count;?>">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</button>

															<button id="<?php echo $value['id'] ?>" class="btn btn-xs btn-danger delete">
																<i class="ace-icon fa fa-trash-o bigger-120 "></i>
															</button>

															
														</div>


														  <div class="modal fade" id="myModal<?php echo $count; ?>" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content" style="border-radius: 20px;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Update Banner</h4>
                              </div>
                               
                              <div class="modal-body">
                                <form method="post" enctype="multipart/form-data">
                                	<input type="hidden" name="id" value="<?php echo $value['id'] ?>">
              			<label><b>Banner Title :</b></label>
              			<input type="text" name="banner_title" value="<?php echo $value['title']; ?>" class="form-control" required>
             			<label><b>Banner Description:</b></label>
               			<textarea autocomplete="off" name="banner_description" class="form-control" required ><?php echo $value['description']; ?></textarea>
             			<label><b>Banner Image :</b></label>
              			<input type="file" name="banner_img" class="form-control" required>
              			<label><b>Subscription Status :</b></label>
	                    <select class="form-control" name="status" required>
                          	<option value="">Select State</option>
                          	<option <?php echo $value['status']==1 ? 'selected':''  ?> value="1">Active</option>
                          	<option <?php echo $value['status']==0 ? 'selected':''  ?> value="0">Inactive</option>
                        </select>
              			<br/>
              			<button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
            		</form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->

														
													</td>
											</tr>
											<?php
												}
											?>
										</tbody>
									</table>
								</div><!-- /.span -->
							</div><!-- /.row -->
							<div class="row">
				            	<div class="col-md-12">
					      			<div class="row"><?php echo $this->pagination->create_links(); ?>
					      			</div> 
					     		</div>
				    		</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			<?php 
				$this->load->view('user/include/footer');
			?> 
		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
			<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
		</a>
	</div>
	<div class="modal fade" id="myModal-1" role="dialog">
        <div class="modal-dialog">
        	<div class="modal-content" style="border-radius: 20px;">
          		<div class="modal-header">
            		<button type="button" class="close" data-dismiss="modal">&times;
            		</button>
            		<h4 class="modal-title">Add New Banner</h4>
          		</div>
          		<div class="modal-body">
           			<form method="post" enctype="multipart/form-data">
              			<label><b>Banner Title :</b></label>
              			<input type="text" placeholder="Banner Title"  name="banner_title" class="form-control" required>
             			<label><b>Banner Description:</b></label>
               			<textarea placeholder="Banner Description" autocomplete="off"  name="banner_description" class="form-control" required ></textarea>
             			<label><b>Banner Image :</b></label>
              			<input type="file" placeholder="Banner Promote Image"  name="banner_img" class="form-control" required>
              			<label><b>Banner Status :</b></label>
	                    <select class="form-control" name="status" required>
	                        <option value="">Select Banner Status</option>
	                        <option value="1">Active</option>
	                        <option value="0">Inactive</option>
	                    </select>
              			<br/>
              			<button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
            		</form>
          		</div>
          	</div>
        </div>
    </div>
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo base_url()?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/moment.min.js"></script>
<script src="<?php echo base_url()?>assets/js/daterangepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/autosize.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.inputlimiter.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url()?>assets/js/ace-elements.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.delete').click(function() {
            if (confirm("Do you want to delete?")) {
                var id = $(this).attr('id');
                //alert(price_id);
            } else {
                return false;
            }
            $.post("<?php echo base_url('User_controller/banner_delete') ?>", { id: id }, function(res) {
                // if(res=='success')
                // {
                location.reload();
                //}
            })
        })
    })
</script>
<script>
$( function() {
	$( ".event_date" ).datepicker({
			autoclose: true,
	todayHighlight: true,
	startDate: '-0m'
	});
});
$(document).ready(function () {
        $('.coupon_start_time').datetimepicker({
            format: "HH:mm A",
        });
    });
</script>