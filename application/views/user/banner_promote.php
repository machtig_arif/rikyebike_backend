<?php $this->load->view('user/include/header'); ?>
	<div class="main-container ace-save-state" id="main-container">
		<div id="sidebar" class="sidebar responsive ace-save-state">
			<?php $this->load->view('user/include/navigation'); ?>
			<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
				<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
			</div>
		</div>
		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="<?php echo base_url('User_controller') ?>">Home</a>
						</li>
					</ul><!-- /.breadcrumb -->
				</div>
				<div class="page-content">
      		<div class="page-header">
						<button style="float: right;margin-bottom: inherit;height: 40px;width: 30%;color: white;  background-color: #438eb9;border-radius: 10px;"   type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1" >Add Promotion
            </button>
						<h1>
							<i class="fa fa-user" aria-hidden="true"></i>
							Banner Promotion
						</h1>
					</div><!-- /.page-header -->
					<?php if (!empty($this->session->flashdata('msg'))) { ?>
					  <div class="alert alert-block alert-success" style="text-align: center;">
              <button type="button" class="close" data-dismiss="alert">
              <i class="ace-icon fa fa-times"></i>
              </button>
              <i class="ace-icon fa fa-check green"></i>
						  <?php echo $this->session->flashdata('msg'); ?>
					  </div>
					<?php } ?>
					<div class="row">
						<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<div class="row">
								<div class="col-xs-12">
									<table id="simple-table" class="table  table-bordered table-hover">
										<thead>
											<tr>																									
                        <th>#</th>
                        <th>Banner Image</th>	
                        <!-- <th>Deal Name</th>	 -->
                        <th>Banner Title</th>
                        <th>Promotion Start Date</th>
                        <th>Promotion Start Time</th>
                        <th>Promotion End Date</th>
                        <th>Promotion End Time</th>
												<th class="hidden-480">Status</th>
											</tr>
										</thead>
										<tbody>
											<?php
											  $count=$star;
											  foreach ($promote_banner as $banner) {
                          //print_r($value);die;
												  $count++
											?>
											<tr>
												<td>
                          <?php echo $count; ?>
                        </td>
												<td class="hidden-480">
                          <img style="height: 100px;width: 100px;" src="<?php echo base_url()?>assets/images/<?php echo $banner['banner_img'] ?>">
                        </td>
                        <td class="hidden-480">
                          <?php echo $banner['title']; ?>
                        </td>
												<td class="hidden-480">
                          <?php echo $banner['start_date']; ?>
                        </td>
                        <td class="hidden-480">
                          <?php  echo $banner['start_time']; ?>
                        </td>
                        <td class="hidden-480">
                          <?php echo $banner['end_date']; ?>
                        </td>
												<td class="hidden-480">
                          <?php echo $banner['end_time']; ?>
                        </td>
                        <td class="hidden-480">
                          <?php if($banner['status']==0){ ?>
                            <span class="label label-sm label-danger arrowed-in">Inactive</span>
                          <?php }elseif($banner['status']==1){?>
                          <span class="label label-sm label-success arrowed-in">Accept</span>
                          <?php }elseif($banner['status']==2){?>
                          <span class="label label-sm label-danger">Reject</span>
                          <?php }?>
                        </td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div><!-- /.span -->
							</div>
              <!-- /.row -->
							<div class="row">
                <div class="col-md-12">
                	<div class="row"><?php echo $this->pagination->create_links(); ?>
                  </div> 
                </div>
              </div>
							<!-- PAGE CONTENT ENDS -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.page-content -->
			</div>
		</div><!-- /.main-content -->
    <?php $this->load->view('user/include/footer'); ?> 
		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
			<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
		</a>
	</div><!-- /.main-container -->
  <div class="modal fade" id="myModal-1" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" style="border-radius: 20px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Promotion</h4>
        </div>
        <div class="modal-body">
          <form method="post" enctype="multipart/form-data">
          	<label for="uname"><b>Banner Name :</b></label>
            <select name="id" class="form-control" required>
              <option>Select Banner</option>
              <?php
                  foreach ($banner_dd as $val) {
              ?>
              <option value="<?php echo $val['id']; ?>">
                <?php echo $val['title']; ?>
              </option>
            <?php }?> 
            </select>
          
            <label for="uname"><b>Promotion Start Date :</b></label>
            <input id="coupon_start_date1" name="promotion_start_date" autocomplete="off" type="text" placeholder="Promotion Start Date"  class="form-control event_date" />
            <label for="uname"><b>Promotion Start Time :</b></label>
            <input id="event_time" name="promotion_start_time" autocomplete="off" type="text" placeholder="Promotion Start Time"  class="form-control event_time" />
            <label for="uname"><b>Promotion End Date :</b></label>
            <input id="coupon_start_date1" name="promotion_end_date" autocomplete="off" type="text" placeholder="Promotion End Date"  class="form-control event_date" />
            <label for="uname"><b>Promotion End Time :</b></label>
            <input id="event_time1" name="promotion_end_time" autocomplete="off" type="text" placeholder="Promotion End Time"  class="form-control event_time" />
            <br/>
            <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
          </form>
        </div>
        <!--  <div class="modal-footer"> -->
      </div><!--model-content-->
    </div>
  </div><!--model-->
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo base_url()?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/moment.min.js"></script>
<script src="<?php echo base_url()?>assets/js/daterangepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/autosize.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.inputlimiter.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url()?>assets/js/ace-elements.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.delete').click(function(){
			if(confirm("Do you want to delete?")){
			  var coupon_id=$(this).attr('id');
			} else {
				return false;
			}
			$.post("<?php echo base_url('Admin_controller/coupon_delete') ?>",{coupon_id:coupon_id},function(res){
				// if(res=='success')
				// {
					 location.reload();
				//}
			})
		})
	})
</script>
<script>
  $( function() {
  	$( ".event_date" ).datepicker({
			autoclose: true,
		  todayHighlight: true,
		  startDate: '-0m'
  	});
  });
  $(document).ready(function () {
  	$('.coupon_start_time').datetimepicker({
  		format: "HH:mm A",
  	});
  });
</script>
<script>
  $(document).ready(function () {
    $('#event_time').datetimepicker({
      format: "HH:mm A",
    });
  });

  $(document).ready(function () {
    $('#event_time1').datetimepicker({
      format: "HH:mm A",
    });
  });
</script>