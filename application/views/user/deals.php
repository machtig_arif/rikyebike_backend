<?php
$memUrl = "http://site.api.espn.com/apis/site/v2/sports/football/nfl/scoreboard";
$sessionMem = curl_init();
curl_setopt($sessionMem, CURLOPT_URL, $memUrl);
curl_setopt($sessionMem, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($sessionMem, CURLOPT_HTTPGET, 1);
curl_setopt($sessionMem, CURLOPT_HEADER, false);
curl_setopt($sessionMem, CURLOPT_HTTPHEADER, array('Accept: application/json'));
curl_setopt($sessionMem, CURLOPT_RETURNTRANSFER, true);
curl_setopt($sessionMem, CURLOPT_SSL_VERIFYPEER, false);

$responseMem = curl_exec($sessionMem);
$jsonMem = json_decode($responseMem);
//print_r($jsonMem);
//print_r( $jsonMem->events);
$sports_events = $jsonMem->events;

?>

	<?php $this->load->view('user/include/header'); ?>

		<div class="main-container ace-save-state" id="main-container">
			

			<div id="sidebar" class="sidebar responsive ace-save-state">
				
 <?php $this->load->view('user/include/navigation'); ?>

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('User_controller') ?>">Home</a>
							</li>

							
							
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
	

						<div class="page-header">
							<button style="float: right;margin-bottom: inherit;height: 40px;width: 30%;color: white;  background-color: #438eb9;border-radius: 10px;"   type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1" >Add Deals</button>
							<h1>
								<i class="fa fa-user" aria-hidden="true"></i>
								Deals Management
							
							</h1>
						</div><!-- /.page-header -->

						<?php if ( !empty($this->session->flashdata('msg'))) {
							?>

							 <div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $this->session->flashdata('msg'); ?>
 
                </div>
					
							
						<?php
					} ?>

				<div class="col-md-12 clearfix">
          <form class="form-horizontal" action="" method="get">
            <div  class="form-group pull-right">
              <div class="col-md-12">
                <select style="border-radius: 10px;width: 160%;background-color: #f1ebe0;" class="form-control pull-right" name="status" onchange="this.form.submit()">
                  <option value="1"> Active</option>
                  <option <?php echo $this->input->get('status') == '0' ? 'selected':''; ?> value="0"> Inactive</option>
                </select>
              </div>
            </div>
          </form>
        </div>

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									<div class="col-xs-12">
										<table id="simple-table" class="table  table-bordered table-hover">
											<thead>
												<tr>
																									
													<th>#</th>
													        <th>Deals Image</th>	
													        <th>Deals Name</th>	
															<th>Deals Title</th>	
															<th>Start Date</th>
															<th>Start Time</th>	
															<th>End Date</th>	
															<th>End Time</th>
															<th>Deals Terms</th>	
															<th>Description</th>	
																						
													<th class="hidden-480">Status</th>

													<th>Action</th>
												</tr>
											</thead>

											<tbody>
												<?php
												$count=$star;
												 foreach ($result as $value) {
													$count++
													 ?>
												<tr>
													
													<td><?php echo $count; ?></td>
													<td class="hidden-480"><img style="height: 100px;width: 100px;" src="<?php echo base_url()?>assets/images/<?php echo $value['img'] ?>"> </td>
													<td class="hidden-480"><?php echo $value['deal_name']; ?></td>
													<td class="hidden-480"><?php echo $value['deal_title']; ?>  </td>
													<td class="hidden-480"><?php echo $value['start_date']; ?>  </td>
													<td class="hidden-480"><?php echo $value['deal_start_time']; ?>  </td>
													<td class="hidden-480"><?php echo $value['end_date']; ?></td>
													<td class="hidden-480"><?php echo $value['deal_end_time']; ?></td>
													<td class="hidden-480"><?php echo $value['t_c']; ?></td>
													<td class="hidden-480"><?php echo $value['deal_description']; ?></td>
													
													<td class="hidden-480">
														<?php if($value['status']==1){ ?>
															<span class="label label-sm label-success">Active</span>
														<?php }elseif($value['status']==0){?>
														<span class="label label-sm label-danger arrowed-in">Inactive</span>
													<?php }?>
													</td>

													<td>
														<div class="hidden-sm hidden-xs btn-group">
															

															<button class="btn btn-xs btn-info" id="bt-modal" data-toggle="modal" data-target="#myModal<?php echo $count;?>">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</button>

															<button id="<?php echo $value['id'] ?>" class="btn btn-xs btn-danger delete">
																<i class="ace-icon fa fa-trash-o bigger-120 "></i>
															</button>

															
														</div>


														  <div class="modal fade" id="myModal<?php echo $count; ?>" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content" style="border-radius: 20px;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Update Deal</h4>
                              </div>
                               
                              <div class="modal-body">
                                <form method="post" enctype="multipart/form-data">
                                	<input type="hidden" placeholder="Deal Name" value="<?php echo $value['id'] ?>"  name="deal_id" class="form-control">
                                	<label ><b>Deal Name :</b></label>
                                  <input type="text" placeholder="Deal Name" value="<?php echo $value['deal_name'] ?>"  name="deal_name" class="form-control" required>
                                  <label ><b>Deal Title :</b></label>
                                  <input type="text" placeholder="Deal Title" value="<?php echo $value['deal_title'] ?>"  name="deal_title" class="form-control" required>
                                 
                                   <label ><b>Deal Start Date :</b></label>
                                <input id="coupon_start_date1" value="<?php echo $value['start_date'] ?>" name="deal_start_date" autocomplete="off" type="text" placeholder="Deal Start Date"  class="form-control event_date" />

                                <label ><b>Deal Start Time :</b></label>
                                <input value="<?php echo $value['deal_start_time'] ?>" name="deal_start_time" id = "deal_start_time" autocomplete="off" type="text" placeholder="Deal Start Time"  class="form-control event_time" />

                                <label ><b>Deal End Date :</b></label>
                                <input id="coupon_start_date1" name="deal_end_date" value="<?php echo $value['end_date'] ?>"  autocomplete="off" type="text" placeholder="Deal End Date"  class="form-control event_date" />

                                <label ><b>Deal End Time :</b></label>
                                <input name="deal_end_time" id = "deal_end_time" value="<?php echo $value['deal_end_time'] ?>"  autocomplete="off" type="text" placeholder="Deal End Time"  class="form-control event_time" />

                                 <label ><b>Deal Description:</b></label>
                                   <textarea placeholder="Deal Description" autocomplete="off"   name="deal_description" class="form-control" required ><?php echo $value['deal_description'] ?></textarea>
                                   

                                   <label ><b>Deal Terms:</b></label>
                                   <textarea placeholder="Deal Terms"  autocomplete="off"  name="deal_terms" class="form-control" required ><?php echo $value['t_c'] ?></textarea>
                               

                                 <label ><b>Deal Image :</b></label>
                                  <input type="file" placeholder="Cover Charge"  name="deal_img" class="form-control" >

                                  <label for="psw"><b>Coupon Status :</b></label>
                                   <select class="form-control" name="status" required>
                                  	<option value="">Select State</option>
                                  	<option <?php echo $value['status']==1 ? 'selected':''  ?> value="1">Active</option>
                                  	<option <?php echo $value['status']==0 ? 'selected':''  ?> value="0">Inactive</option>
                                  </select>
                                  <br/>
                                  <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                                 
                                </form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->

														
													</td>
												</tr>
												<?php
											}
											?>

											
											</tbody>
										</table>
									</div><!-- /.span -->
								</div><!-- /.row -->

								<div class="row">
              <div class="col-md-12">
      <div class="row"><?php echo $this->pagination->create_links(); ?></div> 
     </div>
    </div>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

		
 <?php $this->load->view('user/include/footer'); ?> 
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		  <div class="modal fade" id="myModal-1" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content" style="border-radius: 20px;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add New Deal</h4>
                              </div>
                              <div class="modal-body">
                                <form method="post" enctype="multipart/form-data">
                                	<label><b>Deal Name :</b></label>
                                  <input type="text" placeholder="Deal Name"  name="deal_name" class="form-control" required>
                                  <label><b>Deal Title :</b></label>
                                  <input type="text" placeholder="Deal Title"  name="deal_title" class="form-control" required>
                                 
                                 <label for="match"><b>Select Match :</b></label>
                                 <select name="match" id="match" class="form-control" required>
                                	<option value=''>--Select Match--</option>
                                 <?php
								 foreach ($sports_events as $sports_event) {
									$competitions = $sports_event->competitions;
									$competitors = $competitions[0]->competitors;
									$team_names = [];
									foreach ($competitors as $competitor) {
										$team = $competitor->team;
										$team_names[] = $team->name;
									}
									$team_names = implode(', ', $team_names);
									echo "<option data-teams='".$team_names."' value='".$sports_event->name."'>".$sports_event->name."</option>";
								 }
								 ?>
                                 </select>
                                 <label for="teams"><b>Teams :</b></label>
                                <input id="teams" name="teams" autocomplete="off" type="text" placeholder="Teams"  class="form-control" />
                                
                                 <label><b>Deal Start Date :</b></label>
                                <input id="coupon_start_date1" name="deal_start_date" autocomplete="off" type="text" placeholder="Deal Start Date"  class="form-control event_date" />

                                <label><b>Deal Start Time :</b></label>
                                <input name="deal_start_time" id = "deal_start_time" autocomplete="off" type="text" placeholder="Deal Start Time"  class="form-control event_time" />

                                <label><b>Deal End Date :</b></label>
                                <input id="coupon_start_date1" name="deal_end_date" autocomplete="off" type="text" placeholder="Deal End Date"  class="form-control event_date" />

                                <label><b>Deal End Time :</b></label>
                                <input name="deal_end_time" id = "deal_end_time" autocomplete="off" type="text" placeholder="Deal End Time"  class="form-control event_time" />

                                 <label><b>Deal Description:</b></label>
                                   <textarea placeholder="Deal Description" autocomplete="off"  name="deal_description" class="form-control" required ></textarea>
                                   

                                   <label><b>Deal Terms:</b></label>
                                   <textarea placeholder="Deal Terms" autocomplete="off"  name="deal_terms" class="form-control" required ></textarea>
                               

                                 <label><b>Deal Image :</b></label>
                                  <input type="file" placeholder="Cover Charge"  name="deal_img" class="form-control" required>

                                 <!--  <label for="psw"><b>Coupon Status :</b></label>
                                  <select class="form-control" name="status" required="">
                                  	<option value="">Select Status</option>
                                  	<option value="1">Active</option>
                                  	<option value="0">Inactive</option>
                                  </select> -->
                                  <br/>
                                  <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                                 
                                </form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->

	</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-2.1.4.min.js"></script>

<script src="<?php echo base_url()?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/moment.min.js"></script>
<script src="<?php echo base_url()?>assets/js/daterangepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/autosize.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.inputlimiter.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.maskedinput.min.js"></script>

<script src="<?php echo base_url()?>assets/js/ace-elements.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.delete').click(function(){
		if(confirm("Do you want to delete?"))
		{

		var deal_id=$(this).attr('id');
		
		}else{
			  return false;
		}

		$.post("<?php echo base_url('User_controller/deal_delete') ?>",{deal_id:deal_id},function(res){
			// if(res=='success')
			// {
				 location.reload();
			//}
		});
	});
});
</script>
<script>
		// $( function() {
	 //   			$( ".event_date" ).datepicker({
	 //   					autoclose: true,
		// 			todayHighlight: true,
		// 			minDate: 0,
	 //   			});
	 //  		});

$( function() {
	$( ".event_date" ).datepicker({
			autoclose: true,
		todayHighlight: true,
		startDate: '-0m'
	});
});
		// $(".event_date").datepicker().datepicker("setDate", new Date());
// jQuery(document).ready(function(){ 
//   // Date Picker
//   jQuery('.event_date').datepicker();
  
//   jQuery('#datepicker-inline').datepicker();
  
//   jQuery('#datepicker-multiple').datepicker({
    
//     autoclose: true,
// 					todayHighlight: true,
// 					minDate: 0,
//   });

// });

// $(document).ready(function () {
//         $('.coupon_start_time').datetimepicker({
//             format: "HH:mm A",
//         });
//     });
</script>
<script>
$(document).ready(function () {
	$('.event_time').datetimepicker({
		format: "HH:mm A",
	});
	$(document).on('change', '#match', function() {
		$("#teams").val( $(this).find(':selected').attr('data-teams') );
	});
});
</script>