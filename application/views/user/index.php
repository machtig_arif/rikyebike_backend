
<?php $this->load->view('user/include/header'); ?>
<style>
  .card-body-icon.box2 i {
    color: #053956;
    font-size: 34px;
}


 .icon {
    width: 20px;
    height: 20px;
    color: #BAB8B8;
    position: absolute;
    right: 53px;
    top: 22px;
    z-index: 1;

}
.count {
    position: relative;
    margin: 0 0 0 10px;
    z-index: 5;
    padding: 0;
    font-size: 30px;
}

i.fa.fa-user {
    margin: 0;
    font-size: 50px;
    line-height: 0;
    vertical-align: bottom;
    padding: 0;
}
</style>
    <div class="main-container ace-save-state" id="main-container">
      <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
      </script>

      <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <script type="text/javascript">
          try{ace.settings.loadState('sidebar')}catch(e){}
        </script>

       <?php $this->load->view('user/include/navigation'); ?>

    

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
          <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
      </div>

      <div class="main-content">
        <div class="main-content-inner">
          <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
              <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Home</a>
              </li>
              <li class="active">Dashboard</li>
            </ul><!-- /.breadcrumb -->

          
          </div>

          <div class="page-content">
           

            <div class="page-header">
              <h1>
                Dashboard
                
              </h1>
            </div><!-- /.page-header -->
               <!-- <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding-bottom: 40px;">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-user"></i></div>
                  <div class="count">150<?php //echo count($userData); ?></div>
                  <h3>Going</h3>
                 
                </div>
              </div>

                 <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-user"></i></div>
                  <div class="count">0<?php //echo count($userData); ?></div>
                  <h3>May Be</h3>
                 
                </div>
              </div>

                 <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-user"></i></div>
                  <div class="count">0<?php //echo count($userData); ?></div>
                  <h3>Not Interested</h3>
                 
                </div>
              </div> -->

            <div class="row">
              <div class="col-xs-8" style="border:1px solid #eae9e9;">
                <h4 class="text-center">Restaurant Details </h4>
                Rating :
                    <?php
                    $starNumber=$restaurant_details[0]['average_star'];
for($x=1;$x<=$starNumber;$x++) {
echo "<i class='fa fa-star' style='color: #eca505; font-size: 18px;' aria-hidden='true'></i>";
}
if (strpos($starNumber,'.')) {
echo "<i class='fa fa-star-half-full' style='color: #eca505; font-size: 18px;'></i>";
$x++;
}
while ($x<=5) {
echo "<i class='fa fa-star-o' style='font-size: 18px;'></i>";
$x++;
}
?>
                <p style="float: right;">Package Type: <b><?php if($package['package_id'] == 1) echo 'Silver'; if($package['package_id'] == 2) echo "Gold"; ?></b></p>
                  <div class="row" style="border:1px solid #eae9e9; padding: 10px 0px;">
       

                    <div class="col-sm-7" style="margin-bottom: 10px;">
                        <div class="row repeatsec">
                          <div class="col-md-4">
                              BAR TYPE :
                          </div>
                          <div class="col-md-8">
                          <?php $restaurant_bar=$this->Admin_modal->select_restaurant_bar($restaurant_details[0]['restaurant_id']);
                          foreach ($restaurant_bar as  $value1) {
                            
                           ?>
                          <?php $myArray[] = $value1['bar_type']; ?> 
                        <?php }
                        echo implode($myArray,' , ' );
                         ?>
                      </div>
                        </div>
                        <div class="row repeatsec">
                          <div class="col-md-4">
                              Music Type :
                          </div>
                          <div class="col-md-8">
                            
                          <?php $restaurant_bar=$this->Admin_modal->select_restaurant_music($restaurant_details[0]['restaurant_id']);
                          foreach ($restaurant_bar as  $value3) {
                            
                           ?>
                          <?php $myArray2[] = $value3['music_type']; ?> 
                        <?php }
                        echo implode($myArray2,' , ' ); ?>
                  
                           </div>
                        </div>
                        <div class="row repeatsec">
                          <div class="col-md-4">
                              Music Genre :
                          </div>
                          <div class="col-md-8"> 
                          
                           
                          <?php $restaurant_bar=$this->Admin_modal->select_restaurant_music_genre($restaurant_details[0]['restaurant_id']);
                          foreach ($restaurant_bar as  $value4) {
                            
                           ?>
                          <?php $myArray3[] = $value4['music_genre_name']; ?> 
                        <?php }
                        echo implode($myArray3,' , ' ); ?>
                     
                           </div>
                        </div>
                        <div class="row repeatsec">
                          <div class="col-md-4">
                              Amenities :
                          </div>
                          <div class="col-md-8">
                        
                          <?php $restaurant_bar=$this->Admin_modal->select_restaurant_amenities($restaurant_details[0]['restaurant_id']);
                          foreach ($restaurant_bar as  $value5) {
                            
                           ?>
                          <?php $myArray4[] = $value5['amenities_name']; ?> 
                        <?php }
                        echo implode($myArray4,' , ' ); ?>
                
                            </div>
                        </div>
                        <div class="row repeatsec">
                          <div class="col-md-4">
                              Food Type :
                          </div>
                          <div class="col-md-8">
                        
                          <?php $restaurant_bar=$this->Admin_modal->select_restaurant_food($restaurant_details[0]['restaurant_id']);
                          foreach ($restaurant_bar as  $value2) {
                            
                           ?>
                          <?php $myArray1[] = $value2['food_type']; ?> 
                        <?php }
                        echo implode($myArray1,' , ' );
                         ?>
                   
                            </div>
                        </div>
                        <div class="row repeatsec">
                          <div class="col-md-4">
                             Average Crowd  :
                          </div>
                          <div class="col-md-8">
                         
                          <?php $restaurant_bar=$this->Admin_modal->select_restaurant_crowd($restaurant_details[0]['restaurant_id']);
                          foreach ($restaurant_bar as  $value2) {
                            
                           ?>
                          <?php $myArray10[] = $value2['crowd_count']; ?> 
                        <?php }
                        echo implode($myArray10,' , ' );
                         ?>
                         </div>
                        </div>
                       
                      
                    </div>
                    <div class="col-sm-5" style="margin-bottom: 10px;">
                        <div class="row repeatsec">
                          <div class="col-md-5">
                              STREET :
                          </div>
                          <div class="col-md-7"><?php print_r($restaurant_details[0]['street']) ?>  </div>
                        </div>

                         <div class="row repeatsec">
                          <div class="col-md-5">
                              City :
                          </div>
                          <div class="col-md-7"> <?php print_r($restaurant_details[0]['city']) ?> </div>
                        </div>

                         <div class="row repeatsec">
                          <div class="col-md-5">
                              State :
                          </div>
                          <div class="col-md-7"> <?php print_r($restaurant_details[0]['state_name']) ?> </div>
                        </div>
                         <div class="row repeatsec">
                          <div class="col-md-5">
                              Zipcode :
                          </div>
                          <div class="col-md-7"> <?php print_r($restaurant_details[0]['zipcode']) ?> </div>
                        </div>
                         <div class="row repeatsec">
                          <div class="col-md-5">
                              Phone :
                          </div>
                          <div class="col-md-7"> <?php print_r($restaurant_details[0]['phone']) ?> </div>
                        </div>
                         <div class="row repeatsec">
                          <div class="col-md-5">
                              Website :
                          </div>
                          <div class="col-md-7"> <?php print_r($restaurant_details[0]['website']) ?> </div>
                        </div>
                       
                    </div>

                  </div>
              </div> 
               <div class="col-xs-2">
					<h4 class="text-center"> Events </h4>
					<div class="" style="margin-bottom: 10px;">
      <div class="card text-white bg-danger o-hidden h-100 box" >
        <div class="card-body  box1">
          <div class="card-body-icon box2"> <i class="fa fa-fw fa-list"></i> </div>
          <div class="mr-5"> <?php echo $event_count ?>  Events Manager</div>
        </div>
        <a class="card-footer text-white clearfix small z-1 mr-5" href="<?php echo site_url('total_events'); ?>"> <span class="float-left detail">View Details</span> <span class="float-right"> <i class="fa fa-angle-right"></i> </span> </a> </div>
    </div>
           		</div>
               <div class="col-xs-2">
               		<h4 class="text-center"> Coupons </h4>
               		<div class="" style="margin-bottom: 10px;">
      <div class="card text-white bg-danger o-hidden h-100 box" >
        <div class="card-body  box1">
          <div class="card-body-icon box2"> <i class="fa fa-fw fa-list"></i> </div>
          <div class="mr-5"> <?php print_r($coupon_count) ?>  Coupons Manager</div>
        </div>
        <a class="card-footer text-white clearfix small z-1 mr-5" href="<?php echo site_url('coupon'); ?>"> <span class="float-left detail">View Details</span> <span class="float-right"> <i class="fa fa-angle-right"></i> </span> </a> </div>
    </div>
               </div>
            </div><!-- /.row -->
          </div><!-- /.page-content -->
        </div>
      </div><!-- /.main-content -->

    <?php $this->load->view('user/include/footer'); ?> 