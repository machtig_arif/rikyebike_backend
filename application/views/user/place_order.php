<?php $this->load->view('user/include/header'); ?>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script> 
<div class="main-container ace-save-state" id="main-container">
    <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <?php $this->load->view('user/include/navigation'); ?>
        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="<?php echo base_url('User_controller') ?>">Home</a>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        <i class="fa fa-user" aria-hidden="true"></i>
                        Subscription
                    </h1>
                </div><!-- /.page-header -->
                <?php if ( !empty($this->session->flashdata('msg'))) {?>
                <div class="alert alert-block alert-success" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <i class="ace-icon fa fa-check green"></i>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <?php foreach ($result as $value) { ?>          
                            <div class="col-xs-6 col-sm-6 pricing-box ">
                            <div class="widget-box widget-color-dark">
                                <div class="widget-header">
                                    <h5 class="widget-title bigger lighter"><?php echo $value['name']; ?></h5>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <?php echo $value['discription']; ?>
                                        <!-- <ul class="list-unstyled spaced2">
                                            <li>
                                                <i class="ace-icon fa fa-check green"></i>
                                                10 GB Disk Space
                                            </li>
                                            <li>
                                                <i class="ace-icon fa fa-check green"></i>
                                                200 GB Bandwidth
                                            </li>
                                            <li>
                                                <i class="ace-icon fa fa-check green"></i>
                                                100 Email Accounts
                                            </li>
                                            <li>
                                                <i class="ace-icon fa fa-check green"></i>
                                                10 MySQL Databases
                                            </li>
                                            <li>
                                                <i class="ace-icon fa fa-check green"></i>
                                                $10 Ad Credit
                                            </li>
                                            <li>
                                                <i class="ace-icon fa fa-times red"></i>
                                                Free Domain
                                            </li>
                                        </ul> -->
                                        <hr />
                                        <div class="price">
                                            $<?php echo  $value['amount']; ?>
                                            <small>/month</small>
                                        </div>
                                    </div>

                                    <div>
                                        <!--<a href="<?php //echo base_url('subscription/'.$value['id']); ?>" onclick="return confirm('Are you sure?')" class="btn btn-block btn-inverse">
                                            <i class="ace-icon fa fa-shopping-cart bigger-110"></i>
                                            <span>Buy</span>
                                        </a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="col-xs-6 col-sm-6 pricing-box ">
                            <form class="enter-card" id="paymentFrm" action="<?php echo base_url('User_controller/check') ?>" method="post">
                                <label ><b>Name On Card :</b></label>
                                <input type="text" value="Ankita" name="cardholder" placeholder="Name On Card" class="form-control" />
                                <label ><b>Credit Card Number :</b></label>
                                <input type="text" value="4242424242424242" id="card_num" name="cardnum" placeholder="Credit card number" class="form-control" />
                                <label ><b>Exp Month :</b></label>
                                <input type="text" value="12" id="card-expiry-month" name="expmonth" placeholder="Expiry Month" class="form-control" />
                                <label ><b>Exp year :</b></label>
                                <input type="text" value="2020" id="card-expiry-year" name="expyear" placeholder="Expiry Year" class="form-control" />
                                <label ><b> CVV :</b></label>
                                <input type="text" value="123" id="card-cvc" name="cvv" placeholder="CVV" class="form-control" />
                                <label ><b> Email :</b></label>
                                <input type="text" value="yesitlabs.ankita@gmail.com" id="email" name="email" placeholder="Email" class="form-control" />
                                <input type="hidden" id="package_id" name="package_id" value="<?php echo $value['id']; ?>" class="form-control" />
                                <input type="hidden" id="points" name="points" value="<?php echo $value['points']; ?>" class="form-control" />
                                <input type="hidden" id="package_name" name="package_name" value="<?php echo $value['name']; ?>" class="form-control" />
                                <input type="hidden" id="package_discription" name="package_discription" value="<?php echo $value['discription']; ?>" class="form-control" />
                                <input type="hidden" id="amount" name="amount" value="<?php echo $value['amount']; ?>" class="form-control" />
                                <button id="payBtn" type="button" name="payBtn" class="form-control" style="background-color: #438eb9;color: white;margin-bottom: 25px;" >Submit</button>
                            </form>
                        </div>
                        </div><!-- /.row -->
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->
    <?php $this->load->view('user/include/footer'); ?>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
/*$(document).ready(function() {
    $('.delete').click(function() {
        if (confirm("Do you want to delete?")) {

            var id = $(this).attr('id');
        } else {
            return false;
        }

        $.post("<?php //echo base_url('User_controller/feedback_delete') ?>", { id: id }, function(res) {
            location.reload();
        })
    })
});*/
//set your publishable key
Stripe.setPublishableKey('pk_test_PX7Ey5VeLiVXtp5x7krB6alR00S3CKmGT3');
//  Stripe.setPublishableKey('pk_live_nxQI6OVkFFzTXtd6L2wcq7Kl');
//callback to handle the response from stripe
function stripeResponseHandler(status, response) {
    if (response.error) {
        //enable the submit button
        $('#payBtn').removeAttr("disabled");
        //display the errors on the form
        // $('#payment-errors').attr('hidden', 'false');
//$('#payment-errors').addClass('alert alert-danger');
//$("#payment-errors").html(response.error.message);
    } else {
        var form$ = $("#paymentFrm");
        //get token id
        var token = response['id'];
        //insert the token into the form
        form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
        //submit form to the server
        //$("form#paymentFrm").submit();
        document.getElementById("paymentFrm").submit(); 
        //$(this).closest("form").submit();
    }
}
$(document).ready(function () {
    //on form submit
    $(document).on('click', "#payBtn", function (event) {
        //disable the submit button to prevent repeated clicks
        $(this).attr("disabled", "disabled");

        //create single-use token to charge the user
        Stripe.createToken({
            number: $('#card_num').val(),
            cvc: $('#card-cvc').val(),
            exp_month: $('#card-expiry-month').val(),
            exp_year: $('#card-expiry-year').val()
        }, stripeResponseHandler);

        //submit from callback
        return false;
    });
});

</script>
<style type="text/css">
    .widget-header {
    background-color: #438EB9 !important;
}
.widget-color-dark {
    border-color: #e7e7e7;
}
.btn-inverse, .btn-inverse.focus, .btn-inverse:focus {
    background-color: #438EB9!important;
    border-color: #438eb9;
}
.btn-inverse.focus:hover, .btn-inverse:active:hover, .btn-inverse:focus:active, .btn-inverse:focus:hover, .btn-inverse:hover, .open>.btn-inverse.dropdown-toggle, .open>.btn-inverse.dropdown-toggle.focus, .open>.btn-inverse.dropdown-toggle:active, .open>.btn-inverse.dropdown-toggle:focus, .open>.btn-inverse.dropdown-toggle:hover {
      background-color: #438EB9!important;
    border-color: #438eb9;
}
</style>