<?php
  if(!empty($this->session->flashdata('banner_update')))
  {
  $msg=$this->session->flashdata('banner_update');
  }
   if(!empty($this->session->flashdata('t_c_update')))
  {
  $msg=$this->session->flashdata('t_c_update');
  }
   if(!empty($this->session->flashdata('about_update')))
  {
  $msg=$this->session->flashdata('about_update');
  }

   $this->load->view('user/include/header'); ?>

		<div class="main-container ace-save-state" id="main-container">
			

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				
 <?php $this->load->view('user/include/navigation'); ?>

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('admin_dashboard') ?>">Home</a>
							</li>						
						</ul>						
					</div>

					<div class="page-content">
	

						<div class="page-header">
							
							<h1>
								<i class="fa fa-user" aria-hidden="true"></i>
								Registration Setting Management
							
							</h1>
						</div><!-- /.page-header -->

						<?php if ( !empty($msg)) {
							?>

							 <div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $msg; ?>
 
                </div>					
							
						<?php
					} ?>

				<!-- <div class="col-md-12 clearfix">
          <form class="form-horizontal" action="" method="get">
            <div  class="form-group pull-right">
              <div class="col-md-12">
                <select style="border-radius: 10px;width: 160%;background-color: #f1ebe0;" class="form-control pull-right" name="status" onchange="this.form.submit()">
                  <option value="1"> Active</option>
                  <option <?php echo $this->input->get('status') == '0' ? 'selected':''; ?> value="0"> Inactive</option>
                </select>
              </div>
            </div>
          </form>
        </div> -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									<div class="col-xs-12">
										<table id="simple-table" class="table  table-bordered table-hover">
											<thead>
												<tr>
																									
													<th>#</th>
													
															<th>#</th>	
																						
												

													<th>Action</th>
												</tr>
											</thead>

											<tbody>
												
												<tr>
													
													<tH>BANNER</tH>
													<?php foreach ($result  as $value) {
														if ($value['title']=='banner') {
															# code...
														
													?>
													<td class="hidden-480"><img style="height: 20%; width: 20%;" src="assets/images/<?php echo $value['data']; ?>"></td>
													<?php
													}
												} 
												?>
													
													

													<td>
														<div class="hidden-sm hidden-xs btn-group">
															

															<button class="btn btn-xs btn-info" id="bt-modal" data-toggle="modal" data-target="#myModal-1">

																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</button>

														</div>														  
														
													</td>
												</tr>

												<tr>
													
													<tH>TERMS AND CONDITION </tH>
													<?php foreach ($result  as $value) {
														if ($value['title']=='t_c') {
															# code...
														
													?>
													<td class="hidden-480"><?php echo $value['data']; ?></td>
													<?php
													}
												} 
												?>
													
												

													<td>
														<div class="hidden-sm hidden-xs btn-group">
															

															<button class="btn btn-xs btn-info" id="bt-modal" data-toggle="modal" data-target="#myModal-2">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</button>

														</div>														  
														
													</td>
												</tr>

												<tr>
													
													<tH>ABOUT US</tH>
													<?php foreach ($result  as $value) {
														if ($value['title']=='about') {
															# code...
														
													?>
													<td class="hidden-480"><?php echo $value['data']; ?></td>
													<?php
													}
												} 
												?>

													<td>
														<div class="hidden-sm hidden-xs btn-group">
															

															<button class="btn btn-xs btn-info" id="bt-modal" data-toggle="modal" data-target="#myModal-3">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</button>

														</div>														  
														
													</td>
												</tr>
												

												<tr>
													
													<tH>PRIVACY AND POLICY</tH>
													<?php foreach ($result  as $value) {
														if ($value['title']=='p_p') {
															# code...
														
													?>
													<td class="hidden-480"><?php echo $value['data']; ?></td>
													<?php
													}
												} 
												?>
													
												

													<td>
														<div class="hidden-sm hidden-xs btn-group">
															

															<button class="btn btn-xs btn-info" id="bt-modal" data-toggle="modal" data-target="#myModal-4">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</button>

														</div>														  
														
													</td>
												</tr>
											

											
											</tbody>
										</table>
									</div><!-- /.span -->
								</div><!-- /.row -->
							
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			 <div class="modal fade" id="myModal-1" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content" style="border-radius: 20px;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Update Banner</h4>
                              </div>
                              <div class="modal-body">
                                <form method="post" enctype="multipart/form-data">
                                  <label for="uname"><b>Select Banner Image :</b></label>
                                  <input type="file" name="banner" class="form-control" required>
                                   <input type="hidden"   name="banner_id" value="<?php echo $result[0]['id'] ?>" >
                                 <!--  <label for="psw"><b>State Status :</b></label>
                                  <select class="form-control" name="status" required="">
                                  	<option value="">Select Status</option>
                                  	<option value="1">Active</option>
                                  	<option value="0">Inactive</option>
                                  </select> -->
                                  	
                                  <br/>
                                  <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="banner_submit" class="form-control">Submit</button>
                                 
                                </form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->

			  <div class="modal fade" id="myModal-2" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content" style="border-radius: 20px;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Update Terms And Condition</h4>
                              </div>
                              <div class="modal-body">
                                <form method="post">
                                  <label for="uname"><b>Terms And Condition :</b></label>
                                  <?php foreach ($result  as $value) {
														if ($value['title']=='t_c') {
															# code...
														
													?>
													<div class="wysiwyg-editor" name="t_c" id="editor1"><?php echo $value['data'] ?></div>
													<?php
													}
												} 
												?>
                               
                                  	
                                  	
                                  <br/>
                                  <a  href="javascript:void(0)" style="background-color: #438eb9;color: white;margin-bottom: 25px; text-align: center;" type="submit" name="t_c_submit" id="t_c_submit" class="form-control">Submit</a>
                                 
                                </form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->

                             <div class="modal fade" id="myModal-3" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content" style="border-radius: 20px;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Update About Us</h4>
                              </div>
                              <div class="modal-body">
                                <form method="post">
                                  <label for="uname"><b>About Us :</b></label>
                                  <?php foreach ($result  as $value1) {
														if ($value1['title']=='about') {
															# code...
														
													?>
													<div class="wysiwyg-editor" name="about" id="editor2"><?php echo $value1['data'] ?></div>
													<?php
													}
												} 
												?>
                               
                                  	
                                  	 
                                  <br/>
                                 <a  href="javascript:void(0)" style="background-color: #438eb9;color: white;margin-bottom: 25px; text-align: center;" type="submit" name="about_submit" id="about_submit" class="form-control">Submit</a>
                                 
                                </form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->


                              <div class="modal fade" id="myModal-4" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content" style="border-radius: 20px;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">UPDATE PRIVACY AND POLICY</h4>
                              </div>
                              <div class="modal-body">
                                <form method="post">
                                  <label for="uname"><b>PRIVACY AND POLICY :</b></label>
                                  <?php foreach ($result  as $value) {
														if ($value['title']=='p_p') {
															# code...
														
													?>
													<div class="wysiwyg-editor" name="p_p" id="editor3"><?php echo $value['data'] ?></div>
													<?php
													}
												} 
												?>

                                  	
                                  	 <input type="hidden"  name="p_p_id" id="p_p_id" value="<?php echo $result[3]['id'] ?>" >
                                  <br/>
                                 <a  href="javascript:void(0)" style="background-color: #438eb9;color: white;margin-bottom: 25px; text-align: center;" type="submit" name="p_p_submit" id="p_p_submit" class="form-control">Submit</a>
                                 
                                </form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->

		
 <?php $this->load->view('user/include/footer'); ?> 
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div>
	</body>
</html>		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#t_c_submit').click(function(){
					// var t_c_id=document.getElementById("t_c_id").value;
					
					var data=$('#editor1').html();
						$.post("<?php echo base_url('User_controller/t_c_admin') ?>",{data:data},function(res){
				
					 location.reload();
				
			})
				})
			})
		</script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#about_submit').click(function(){
					//var about_id=document.getElementById("about_id").value;
					
					var data=$('#editor2').html();
						$.post("<?php echo base_url('User_controller/about_admin') ?>",{data:data},function(res){
				
					 location.reload();
				
			})
				})
			})
		</script>

		<script type="text/javascript">
			$(document).ready(function(){
				$('#p_p_submit').click(function(){
					var about_id=document.getElementById("p_p_id").value;
					
					var data=$('#editor3').html();
						$.post("<?php echo base_url('User_controller/p_p_admin') ?>",{data:data,about_id:about_id},function(res){
				
					 location.reload();
				
			})
				})
			})
		</script>
		
		<script src="assets/js/jquery-ui.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/markdown.min.js"></script>
		<script src="assets/js/bootstrap-markdown.min.js"></script>
		<script src="assets/js/jquery.hotkeys.index.min.js"></script>
		<script src="assets/js/bootstrap-wysiwyg.min.js"></script>
		<script src="assets/js/bootbox.js"></script>

		<!-- ace scripts -->
		


		<script src="assets/js/bootstrap-wysiwyg.min.js"></script>		
		<script src="assets/js/ace-elements.min.js"></script>
		

<script type="text/javascript">


		$('#editor1').ace_wysiwyg({
		toolbar:
		[
			'font',
			null,
			'fontSize',
			null,
			{name:'bold', className:'btn-info'},
			{name:'italic', className:'btn-info'},
			{name:'strikethrough', className:'btn-info'},
			{name:'underline', className:'btn-info'},
			null,
			{name:'insertunorderedlist', className:'btn-success'},
			{name:'insertorderedlist', className:'btn-success'},
			{name:'outdent', className:'btn-purple'},
			{name:'indent', className:'btn-purple'},
			null,
			{name:'justifyleft', className:'btn-primary'},
			{name:'justifycenter', className:'btn-primary'},
			{name:'justifyright', className:'btn-primary'},
			{name:'justifyfull', className:'btn-inverse'},
			null,		
			{name:'undo', className:'btn-grey'},
			{name:'redo', className:'btn-grey'}
		],
		
	}).prev().addClass('wysiwyg-style2');

		$('[data-toggle="buttons"] .btn').on('click', function(e){
		var target = $(this).find('input[type=radio]');
		var which = parseInt(target.val());
		var toolbar = $('#editor1').prev().get(0);
		if(which >= 1 && which <= 4) {
			toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g , '');
			if(which == 1) $(toolbar).addClass('wysiwyg-style1');
			else if(which == 2) $(toolbar).addClass('wysiwyg-style2');
			if(which == 4) {
				$(toolbar).find('.btn-group > .btn').addClass('btn-white btn-round');
			} else $(toolbar).find('.btn-group > .btn-white').removeClass('btn-white btn-round');
		}
	});


			$('#editor2').ace_wysiwyg({
		toolbar:
		[
			'font',
			null,
			'fontSize',
			null,
			{name:'bold', className:'btn-info'},
			{name:'italic', className:'btn-info'},
			{name:'strikethrough', className:'btn-info'},
			{name:'underline', className:'btn-info'},
			null,
			{name:'insertunorderedlist', className:'btn-success'},
			{name:'insertorderedlist', className:'btn-success'},
			{name:'outdent', className:'btn-purple'},
			{name:'indent', className:'btn-purple'},
			null,
			{name:'justifyleft', className:'btn-primary'},
			{name:'justifycenter', className:'btn-primary'},
			{name:'justifyright', className:'btn-primary'},
			{name:'justifyfull', className:'btn-inverse'},
			null,		
			{name:'undo', className:'btn-grey'},
			{name:'redo', className:'btn-grey'}
		],
		
	}).prev().addClass('wysiwyg-style2');

		$('[data-toggle="buttons"] .btn').on('click', function(e){
		var target = $(this).find('input[type=radio]');
		var which = parseInt(target.val());
		var toolbar = $('#editor2').prev().get(0);
		if(which >= 1 && which <= 4) {
			toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g , '');
			if(which == 1) $(toolbar).addClass('wysiwyg-style1');
			else if(which == 2) $(toolbar).addClass('wysiwyg-style2');
			if(which == 4) {
				$(toolbar).find('.btn-group > .btn').addClass('btn-white btn-round');
			} else $(toolbar).find('.btn-group > .btn-white').removeClass('btn-white btn-round');
		}
	});


		$('#editor3').ace_wysiwyg({
		toolbar:
		[
			'font',
			null,
			'fontSize',
			null,
			{name:'bold', className:'btn-info'},
			{name:'italic', className:'btn-info'},
			{name:'strikethrough', className:'btn-info'},
			{name:'underline', className:'btn-info'},
			null,
			{name:'insertunorderedlist', className:'btn-success'},
			{name:'insertorderedlist', className:'btn-success'},
			{name:'outdent', className:'btn-purple'},
			{name:'indent', className:'btn-purple'},
			null,
			{name:'justifyleft', className:'btn-primary'},
			{name:'justifycenter', className:'btn-primary'},
			{name:'justifyright', className:'btn-primary'},
			{name:'justifyfull', className:'btn-inverse'},
			null,		
			{name:'undo', className:'btn-grey'},
			{name:'redo', className:'btn-grey'}
		],
		
	}).prev().addClass('wysiwyg-style2');

		$('[data-toggle="buttons"] .btn').on('click', function(e){
		var target = $(this).find('input[type=radio]');
		var which = parseInt(target.val());
		var toolbar = $('#editor3').prev().get(0);
		if(which >= 1 && which <= 4) {
			toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g , '');
			if(which == 1) $(toolbar).addClass('wysiwyg-style1');
			else if(which == 2) $(toolbar).addClass('wysiwyg-style2');
			if(which == 4) {
				$(toolbar).find('.btn-group > .btn').addClass('btn-white btn-round');
			} else $(toolbar).find('.btn-group > .btn-white').removeClass('btn-white btn-round');
		}
	});


</script>