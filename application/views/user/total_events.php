<?php $this->load->view('user/include/header'); ?>
		<div class="main-container ace-save-state" id="main-container">
			<div id="sidebar" class="sidebar responsive ace-save-state">
 				<?php $this->load->view('user/include/navigation'); ?>
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('User_controller') ?>">Home</a>
							</li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<div class="page-header">
							<button style="float: right;margin-bottom: inherit;height: 40px;width: 30%;color: white;  background-color: #438eb9;border-radius: 10px;"   type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1" >Add Event</button>
							<h1>
								<i class="fa fa-user" aria-hidden="true"></i>
								Event Management
							</h1>
						</div><!-- /.page-header -->

						<?php if ( !empty($this->session->flashdata('msgs'))) {?>

						<div class="alert alert-block alert-success" style="text-align: center;">
                            <button type="button" class="close" data-dismiss="alert">
                            	<i class="ace-icon fa fa-times"></i>
                            </button>
                            <i class="ace-icon fa fa-check green"></i>

                			<?php echo $this->session->flashdata('msgs'); ?>
 
                		</div>
						<?php }?>

                        <div class="col-md-12 clearfix">
                            <form class="form-horizontal" action="" method="get">
                                <div  class="form-group">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-3">
                                        <input style="border-radius: 10px;width: 100%;background-color: #f1ebe0;" type="text" class="form-control pull-right event_date_filter" name="event_date" value="<?php echo $this->input->get('event_date'); ?>" autocomplete="off" />
                                    </div>
                                    <div class="col-md-3">
                                        <select style="border-radius: 10px;width: 100%;background-color: #f1ebe0;" class="form-control pull-right" name="status" onchange="this.form.submit()">
                                            <option value="1"> Active</option>
                                            <option <?php echo $this->input->get('status') == '0' ? 'selected':''; ?> value="0"> Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									<div class="col-xs-12">
										<table id="simple-table" class="table  table-bordered table-hover">
											<thead>
												<tr>
																									
													<th>#</th>
													<th>Event Image</th>	
															<th>Event name</th>	
															<th>Event Date</th>	
															<th>Day </th>	
															<th>Average Crowd</th>	
															<th>Start Time</th>
															<th>End Time</th>	
															<!-- <th>Music Genre</th>	 -->
															<th>Cover Charge</th>	
															<th>Event Address</th>	
															<th>Going</th>	
															<th>May Be</th>	
															<th>Not Going</th>	
																						
													<th class="hidden-480">Status</th>

													<th>Action</th>
												</tr>
											</thead>

											<tbody>
												<?php
												$count=$star;
												 foreach ($result as $value) {
													$count++
													 ?>
												<tr>
													
													<td><?php echo $count; ?></td>
													<td class="hidden-480"><img style="height: 100px;width: 100px;" src="<?php echo base_url()?>assets/images/<?php echo $value['event_imsge'] ?>"> </td>
													<td class="hidden-480"><?php echo $value['event_name']; ?></td>
													<td class="hidden-480"><?php echo $value['event_date']; ?></td>
													<td class="hidden-480"><?php
													$timestamp = strtotime($value['event_date']);

															$day = date('D', $timestamp);
														 
													 echo $day; ?></td>
													<td class="hidden-480"><?php echo $value['average_crowd']; ?></td>
													<td class="hidden-480"><?php echo $value['event_time']; ?></td>
													<td class="hidden-480"><?php echo $value['event_close_time']; ?></td>
													<!-- <td class="hidden-480"><?php echo $value['music_genre_name']; ?></td> -->
													<td class="hidden-480"><?php echo $value['cover_charge']; ?></td>
													<td class="hidden-480"><?php echo $value['address']; ?></td>

														<td>
													<?php $going_count=$this->Admin_modal->select_event_going($value['event_id'],$value['user_id']);
													print_r($going_count);
													
												 ?>
											</td>

											<td>
													<?php $not_count=$this->Admin_modal->select_event_may_be($value['event_id'],$value['user_id']);
													print_r($not_count);
													
												 ?>
											</td>

											<td>
													<?php $may_count=$this->Admin_modal->select_event_not_going($value['event_id'],$value['user_id']);
													print_r($may_count);
													
												 ?>
											</td>
													
													
													<td class="hidden-480">
														<?php if($value['status']==1){ ?>
															<span class="label label-sm label-success">Active</span>
														<?php }elseif($value['status']==0){?>
														<span class="label label-sm label-danger arrowed-in">Inactive</span>
													<?php }?>
													</td>

													<td>
														<div class="hidden-sm hidden-xs btn-group">
															

															<button class="btn btn-xs btn-info" id="bt-modal" data-toggle="modal" data-target="#myModal<?php echo $count;?>">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</button>

															<button id="<?php echo $value['event_id'] ?>" class="btn btn-xs btn-danger delete">
																<i class="ace-icon fa fa-trash-o bigger-120 "></i>
															</button>

															
														</div>


														  <div class="modal fade" id="myModal<?php echo $count; ?>" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content" style="border-radius: 20px;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Update Event</h4>
                              </div>
                              <div class="modal-body">
                                <form method="post" enctype="multipart/form-data">
                                  <label for="uname"><b>Event name :</b></label>
                                  <input type="text" placeholder="Event name"  name="event_name" class="form-control" value="<?php echo $value['event_name'] ?>" required>
 
                                  <input type="hidden" placeholder="Event name"  name="event_id" class="form-control" value="<?php echo $value['event_id'] ?>" required>


                                  <label for="uname"><b>Event Date :</b></label>
                                <input id="event_date1" name="event_date" type="text" placeholder="Event Date" value="<?php  echo $newDate = date("m/d/Y", strtotime($value['event_date'])); ?>" class="form-control event_date" />
															
                                  <label for="uname"><b>Average Crowd :</b></label>
                                  <select  name="average_crowd" class="form-control" required>
                                  	<option value="">Select</option>
                                  	<?php foreach ($average_crowd as $value2) { ?>
                                  		
                                  	<option  <?php if (!empty($value)) { echo $value['average_crowd'] == $value2['crowd_count'] ? 'selected':''; }?> value="<?php echo $value2['crowd_count'] ?>"><?php echo $value2['crowd_count'] ?></option>
                                 <?php 	} ?>

                                  </select>
                                
                                  <label for="uname"><b>Event Start Time :</b></label>
                                  <input type="text" placeholder="Time" id="event_time1"  name="event_time" value="<?php echo $value['event_time']; ?>" class="form-control event_time" required>

                                    <label for="uname"><b>Event End Time :</b></label>
                                  <input type="text" placeholder="Time" id="event_time1"  name="event_e_time" value="<?php echo $value['event_close_time']; ?>" class="form-control event_time1" required>

                                  <!--  <label for="uname"><b>Music Genre :</b></label>
                                    <select  name="music_genre" class="form-control" required>
                                  	<option value="">Select</option> -->
                                  	<?php //foreach ($music_genre as $value1) { ?>
                                  		
                                  <!-- 	<option  <?php //if (!empty($value)) { echo $value['music_genre'] == $value1['music_genre_id'] ? 'selected':''; }?> value="<?php //echo $value1['music_genre_id'] ?>"><?php// echo $value1['music_genre_name'] ?></option> -->
                                 <?php 	//} ?>

                                <!--   </select> -->
                                
                                  <label for="uname"><b>Cover Charge :</b></label>
                                  <input type="text" placeholder="Cover Charge"  name="cover_charge" class="form-control" value="<?php echo $value['cover_charge'] ?>" required>

                                    <label for="uname"><b>Venue :</b></label>
                                  <input type="text" placeholder="Venue"  name="venue" class="form-control" value="<?php echo $value['venue'] ?>" required>

                                   <label for="uname"><b>Phone No :</b></label>
                                  <input type="text" placeholder="Phone No"  name="phone" class="form-control" value="<?php echo $value['phone'] ?>" required>


                                   <label for="uname"><b>Event Address :</b></label>
                                    <textarea name="address" id="address<?php echo $count; ?>" placeholder="Address" class="form-control address" required><?php echo $value['address'] ?></textarea>


                                      <input type="hidden" id="latitudeaddress<?php echo $count; ?>" name="latitude" value="<?php echo $value['lat']; ?>"  />
        
      
            <input type="hidden" id="longitudeaddress<?php echo $count; ?>" name="longitude" value="<?php echo $value['log']; ?>" />




                                   <label for="uname"><b>Terms :</b></label>
                                    <textarea name="terms"  placeholder="Terms" class="form-control" required><?php echo $value['event_terms'] ?></textarea>

                                     <label for="uname"><b>Description :</b></label>
                                    <textarea name="description" placeholder="Description" class="form-control" required><?php echo $value['event_description'] ?></textarea>
                              
                                    <label for="uname"><b>Event Image :</b></label>
                                  <input type="file" placeholder="Cover Charge"  name="event_imsge" class="form-control" >

                                   <label for="psw"><b>State Status :</b></label>
                                  <select class="form-control" name="status" required>
                                  	<option value="">Select State</option>
                                  	<option <?php echo $value['status']==1 ? 'selected':''  ?> value="1">Active</option>
                                  	<option <?php echo $value['status']==0 ? 'selected':''  ?> value="0">Inactive</option>
                                  </select>
                                  <br/>

                                  <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                                 
                                </form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->

														
													</td>
												</tr>
												<?php
											}
											?>

											
											</tbody>
										</table>
									</div><!-- /.span -->
								</div><!-- /.row -->

								<div class="row">
              <div class="col-md-12">
      <div class="row"><?php echo $this->pagination->create_links(); ?></div> 
     </div>
    </div>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

		
 <?php $this->load->view('user/include/footer'); ?> 
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		  <div class="modal fade" id="myModal-1" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content" style="border-radius: 20px;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add New Event</h4>
                              </div>
                              <div class="modal-body">
                                <form method="post" enctype="multipart/form-data">
                                  <label for="uname"><b>Event name :</b></label>
                                  <input type="text" placeholder="Event name"  name="event_name" class="form-control" required>
                                  <label for="uname"><b>Event Date :</b></label>
                                <input id="event_date" autocomplete="off" name="event_date" type="text" placeholder="Event Date" class="form-control" />
															
                                  <label for="uname"><b>Average Crowd :</b></label>
                                  <select  name="average_crowd" class="form-control" required>
                                  	<option value="">Select</option>
                                  	<?php foreach ($average_crowd as $value) { ?>
                                  		
                                  	<option value="<?php echo $value['crowd_count'] ?>"><?php echo $value['crowd_count'] ?></option>
                                 <?php 	} ?>

                                  </select>
                                
                                  <label for="uname"><b>Event Start Time :</b></label>
                                  <input type="text" autocomplete="off" placeholder="Time" id="event_time"  name="event_time" class="form-control" required>

                                   <label for="uname"><b>Event End Time :</b></label>
                                  <input type="text" placeholder="Time" id="event_time1"  name="event_e_time"  class="form-control event_time1" required>

                                  <!--  <label for="uname"><b>Music Genre :</b></label>
                                    <select  name="music_genre" class="form-control" required>
                                  	<option value="">Select</option> -->
                                  	<?php f//oreach ($music_genre as $value) { ?>
                                  		
                                  	<!-- <option value="<?php echo $value['music_genre_id'] ?>"><?php echo $value['music_genre_name'] ?></option> -->
                                 <?php //		} ?>

                                  </select>
                                
                                  <label for="uname"><b>Cover Charge :</b></label>
                                  <input type="text" placeholder="Cover Charge"  name="cover_charge" class="form-control" required>


                                     <label for="uname"><b>Venue :</b></label>
                                  <input type="text" placeholder="Venue"  name="venue" class="form-control" required>

                                   <label for="uname"><b>Phone No :</b></label>
                                  <input type="text" placeholder="Phone No"  name="phone" class="form-control" required>

                                     <label for="uname"><b>Event Address :</b></label>
                                    <textarea name="address" id="address1" placeholder="Address" class="form-control address1" required></textarea>


                                      <input type="hidden" id="latitude1" name="latitude" readonly />
        
      
            <input type="hidden" id="longitude1" name="longitude" readonly />

                                    <label for="uname"><b>Terms :</b></label>
                                    <textarea name="terms"  placeholder="Terms" class="form-control" required></textarea>

                                     <label for="uname"><b>Description :</b></label>
                                    <textarea name="description" placeholder="Description" class="form-control" required></textarea>

                                  <label for="uname"><b>Event Image :</b></label>
                                  <input type="file" placeholder="Cover Charge"  name="event_imsge" class="form-control" required>

                                  <br/>
                                  <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                                 
                                </form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->

	</body>
</html>


<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOUqP6zn50u60EeB8ZgB3NplB3F3SB3Aw&libraries=places&callback=initAutocomplete" async defer></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">


	function showResult(result) {
    document.getElementById('latitude1').value = result.geometry.location.lat();
    document.getElementById('longitude1').value = result.geometry.location.lng();
}

function getLatitudeLongitude(callback, address) {
    // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
    address = address || 'Ferrol, Galicia, Spain';
    // Initialize the Geocoder
    geocoder = new google.maps.Geocoder();
    if (geocoder) {
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                callback(results[0]);
            }
        });
    }
}



$(document).ready(function(){
$('.address1').blur(function(){		
	var address = document.getElementById('address1').value;

    getLatitudeLongitude(showResult, address)

})
		
	})


	function showResult1(result,state_id) {

    document.getElementById('latitude'+state_id).value = result.geometry.location.lat();
    document.getElementById('longitude'+state_id).value = result.geometry.location.lng();
}

function getLatitudeLongitude1(callback, address,state_id) {
    // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
    address = address || 'Ferrol, Galicia, Spain';
    // Initialize the Geocoder
    geocoder = new google.maps.Geocoder();
    if (geocoder) {
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                callback(results[0],state_id);
            }
        });
    }
}



$(document).ready(function(){
$('.address').blur(function(){
			var state_id=$(this).attr('id');
	var address = document.getElementById(state_id).value;

    getLatitudeLongitude1(showResult1, address,state_id)

})
		
	})


</script>
<script src="<?php echo base_url()?>assets/js/jquery-2.1.4.min.js"></script>


    <script src="<?php echo base_url()?>assets/js/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/moment.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/daterangepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/autosize.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.inputlimiter.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.maskedinput.min.js"></script>
    
    <script src="<?php echo base_url()?>assets/js/ace-elements.min.js"></script>


<script type="text/javascript">
	$(document).ready(function(){
		$('.delete').click(function(){
			if(confirm("Do you want to delete?"))
			{

			var state_id=$(this).attr('id');
			}else{
				  return false;
			}

			$.post("<?php echo base_url('User_controller/event_delete') ?>",{event_id:state_id},function(res){
				// if(res=='success')
				// {
					 location.reload();
				//}
			})
		})
	})

</script>

<script>
$( function() {
	$( "#event_date" ).datepicker({
			autoclose: true,
		todayHighlight: true,
		startDate: '-0m'
	});
});

$(document).ready(function () {
	$('#event_time').datetimepicker({
		format: "HH:mm A",
	});
});

$(document).ready(function () {
	$('#event_time1').datetimepicker({
		format: "HH:mm A",
	});
});
</script>
<script>
$( function() {
	$( ".event_date" ).datepicker({
		autoclose: true,
		todayHighlight: true,
		startDate: '-0m'
	});
	$(".event_date_filter").datepicker({
		/*onSelect: function(dateText) {
			//$(this).closest("form").submit();
			//$(this).change();
		},*/
		autoclose: true,
		todayHighlight: true,
		//startDate: '-0m'
	});
	$(".event_date_filter").on("change", function(){
		$(this).closest("form").submit();
	});
});
$(document).ready(function () {
	$('.event_time').datetimepicker({
		format: "HH:mm A",
	});
});
$(document).ready(function () {
	$('.event_time1').datetimepicker({
		format: "HH:mm A",
	});
});
</script>
