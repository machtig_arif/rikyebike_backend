<?php
if($result[0]['user_avatar']=="")
{
	 $img='assets/images/user-male-icon.png';
}else{
	$img='assets/images/'.$result[0]['user_avatar'];
}
 $this->load->view('admin/include/header'); ?>

		<div class="main-container ace-save-state" id="main-container">
			

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				
  <?php $this->load->view('admin/include/navigation'); ?>

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('admin_dashboard') ?>">Home</a>
							</li>

							
							<li class="active">Admin Profile</li>
						</ul><!-- /.breadcrumb -->

					
					</div>

					<div class="page-content">
						

						<div class="page-header">
							<h1>
								Change Password
								
							</h1>
						</div><!-- /.page-header -->

						<?php if ( !empty($this->session->flashdata('msg'))) {
							?>

							 <div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $this->session->flashdata('msg'); ?>
 
                </div>
					
							
						<?php
					} ?>

					<?php if ( !empty($this->session->flashdata('error'))) {
							?>

							 <div class="alert alert-block alert-danger" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-times danger"></i>

                <?php echo $this->session->flashdata('error'); ?>
 
                </div>
					
							
						<?php
					} ?>


						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="clearfix">
									
								</div>

								<div class="hr dotted"></div>

								<div>
									

									<div id="user-profile-1" class="user-profile row">
										<div class="col-xs-12 col-sm-3 center">
											<div>
												<span class="profile-picture">
						<img id="output"  class="editable img-responsive" alt="Alex's Avatar" src="<?php echo $img ?>" />
												</span>

												<div class="space-4"></div>
												<div class="col-md-12">
						
						</div>

													
												<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">


													<div class="inline position-relative">
														<a href="#" class="user-title-label" >
															<i class="ace-icon fa fa-circle light-green"></i>
															&nbsp;
															<span class="white"><?php echo $result[0]['first_name']." ".$result[0]['last_name'] ?></span>
														</a>

												
													</div>
												</div>
											</div>

											<div class="space-6"></div>


										


											<div class="hr hr16 dotted"></div>
										</div>

										<div class="col-xs-12 col-sm-9">
											

											<form method="post">

											<div class="profile-user-info profile-user-info-striped">
											
												<div class="profile-info-row">
													<div class="profile-info-name"> Old Password </div>

													<div class="profile-info-value">
														<input type="Password" name="old_pass" class="form-control">
														<i class="ui-icon ui-icon-pencil"></i>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> New Password </div>

													<div class="profile-info-value">
														<input type="Password" name="new_pass" class="form-control"  autocomplete="off" >
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Confirm Password </div>

													<div class="profile-info-value">
                                                <input type="Password" name="con_pass" class="form-control" >
													</div>
												</div>

										<div class="clearfix form-actions">
													<div style="margin-left: auto;" class="col-md-offset-3 col-md-9">
														<button class="btn btn-info" type="submit" name="submit">
															<i class="ace-icon fa fa-check bigger-110"></i>
															Save
														</button>

													</div>
												</div>
										
											<div class="hr hr2 hr-double"></div>
</form>
										
										</div>
									</div>
								</div>


	
						</div><!-- /.row -->
					
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

		 <?php $this->load->view('admin/include/footer'); ?> 

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->




	</body>
</html>
