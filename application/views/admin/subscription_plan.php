<?php $this->load->view('admin/include/header'); ?>
<div class="main-container ace-save-state" id="main-container">
    <div id="sidebar" class="sidebar responsive ace-save-state">
        <?php $this->load->view('admin/include/navigation'); ?>
        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="<?php echo base_url('admin_dashboard') ?>">Home</a>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="page-header">
                    <button style="float: right;margin-bottom: inherit;height: 40px;width: 30%;color: white;  background-color: #438eb9;border-radius: 10px;" type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1">Add Subscription Plan</button>
                    <h1>
                        <i class="fa fa-user" aria-hidden="true"></i>
                        Subscription Plan
                    </h1>
                </div><!-- /.page-header -->
                <?php 
				    if ( !empty($this->session->flashdata('add_city'))) {
				?>
                <div class="alert alert-block alert-success" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <i class="ace-icon fa fa-check green"></i>
                    <?php echo $this->session->flashdata('add_city'); ?>
                </div>
                <?php } ?>
                <!--<h2 style="margin-top: 0px;">Import States Csv</h2>
					<div class="col-md-2 clearfix">
						<form action="<?php //echo base_url('Admin_controller/import') ?>" enctype="multipart/form-data" method="post">
					       <input type="file" name="csv_file" accept=".csv" style="margin-bottom: 10px;">
					       <input class="btn-info" type="submit" name="csv_submit" >
				        </form>
                    </div>
					<div class="col-md-5 clearfix">
					<p style="margin-left: 50px;color: red;" >Note: Please check Sample CSV File :<a href="<?php echo base_url('Admin_controller/download') ?>"> Click Here</a> </p></div> -->
                <div class="col-md-12 clearfix">
                    <form class="form-horizontal" action="" method="get">
                        <div class="form-group pull-right">
                            <div class="col-md-12">
                                <select style="border-radius: 10px;width: 160%;background-color: #f1ebe0;" class="form-control pull-right" name="status" onchange="this.form.submit()">
                                    <option value="1"> Active</option>
                                    <option <?php echo $this->input->get('status') == '0' ? 'selected':''; ?> value="0"> Inactive</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="simple-table" class="table  table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <!--  <th>Country Name</th>	 -->
                                            <th>Name</th>
                                            <th>Discription</th>
                                            <th>Amount</th>
                                            <th>Points</th>
                                            <th class="hidden-480">Status</th>
                                            <th>Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
											$count=$star;
											foreach ($result as $value) {
												//print_r($value);
												$count++
										?>
                                        <tr>
                                            <td>
                                                <?php echo $count; ?>
                                            </td>
                                            <!-- <td class="hidden-480"><?php echo $value['country_name']; ?></td> -->
                                            <td class="hidden-480">
                                                <?php echo $value['name']; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['discription']; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['amount']; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['points']; ?>
                                            </td>
                                            <td class="hidden-480">
                                                <?php if($value['status']==1){ ?>
                                                <span class="label label-sm label-success">Active</span>
                                                <?php }elseif($value['status']==0){?>
                                                <span class="label label-sm label-danger arrowed-in">Inactive</span>
                                                <?php }?>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <button class="btn btn-xs btn-info" id="bt-modal" data-toggle="modal" data-target="#myModal<?php echo $count;?>">
                                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                    </button>
                                                    <button id="<?php echo $value['id'] ?>" class="btn btn-xs btn-danger delete">
                                                        <i class="ace-icon fa fa-trash-o bigger-120 "></i>
                                                    </button>
                                                </div>
                                                <div class="modal fade" id="myModal<?php echo $count; ?>" role="dialog">
                                                    <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content" style="border-radius: 20px;">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Update Subscription Plan</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form method="post">
                                                                    <input type="hidden" name="id" value="<?php echo $value['id'] ?>">
                                                                    <label for="uname"><b>Name :</b></label>
                                                                    <input type="text" placeholder="Subscription Name" value="<?php echo $value['name'] ?>" name="name" class="form-control" required>
                                                                    <label for="uname"><b>Description :</b></label>
                                                                    <!-- <input type="text" placeholder="Description" value="<?php echo $value['discription'] ?>" name="discription" class="form-control" required> -->
                                                                    <textarea class="form-control" id="form-field-8" name="discription" placeholder="Description" spellcheck="false"><?php echo $value['discription'] ?></textarea>
                                                                    <label for="amount"><b>Amount :</b></label>
                                                                    <input type="text" placeholder="Amount" value="<?php echo $value['amount'] ?>" name="amount" class="form-control" required>
                                                                    <label for="points"><b>Points :</b></label>
                                                                    <input type="text" placeholder="Points" value="<?php echo $value['points'] ?>" name="points" class="form-control" required>
                                                                    <label for="status"><b>Status :</b></label>
                                                                    <select class="form-control" name="status" required>
                                                                        <option value="">Select Status</option>
                                                                        <option <?php echo $value['status']==1 ? 'selected' : '' ?> value="1">Active</option>
                                                                        <option <?php echo $value['status']==0 ? 'selected' :'' ?> value="0">Inactive</option>
                                                                    </select>
                                                                    <br />
                                                                    <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <!--model-content-->
                                                    </div>
                                                </div>
                                                <!--model-->
                                            </td>
                                        </tr>
                                        <?php
											}
										?>
                                    </tbody>
                                </table>
                            </div><!-- /.span -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <?php echo $this->pagination->create_links(); ?>
                                </div>
                            </div>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->
    <?php $this->load->view('admin/include/footer'); ?>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->
<div class="modal fade" id="myModal-1" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 20px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Subscription Plan</h4>
            </div>
            <div class="modal-body">
                <form method="post">
                    <label><b>Name :</b></label>
                    <input type="text" placeholder="Subscription Name" name="name" class="form-control" required>
                    <label><b>Description :</b></label>
                    <textarea class="form-control" id="form-field-8" name="discription" placeholder="Description" spellcheck="false"></textarea>
                    <label><b>Amount :</b></label>
                    <input type="text" placeholder="Amount" name="amount" class="form-control" required>
                    <label><b>Points :</b></label>
                    <input type="text" placeholder="Points" name="points" class="form-control" required>
                    <label><b>Subscription Status :</b></label>
                    <select class="form-control" name="status" required>
                        <option value="">Select Status</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                    <br />
                    <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                </form>
            </div>
        </div>
        <!--model-content-->
    </div>
</div>
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.delete').click(function() {
            if (confirm("Do you want to delete?")) {
                var city_id = $(this).attr('id');
            } else {
                return false;
            }
            $.post("<?php echo base_url('Admin_controller/subscription_plan_delete') ?>", { city_id: city_id }, function(res) {
                // if(res=='success')
                // {
                location.reload();
                //}
            })
        })
    })
</script>