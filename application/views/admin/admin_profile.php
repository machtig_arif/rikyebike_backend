<?php

if($result[0]['user_avatar']=="")
{
	 $img='assets/images/user-male-icon.png';
}else{
	$img='assets/images/'.$result[0]['user_avatar'];
}

 $this->load->view('admin/include/header'); ?>

		<div class="main-container ace-save-state" id="main-container">
			

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				
  <?php $this->load->view('admin/include/navigation'); ?>

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('admin_dashboard') ?>">Home</a>
							</li>

							
							<li class="active">Admin Profile</li>
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
						

						<div class="page-header">
							<h1>
								Admin Profile Page
								
							</h1>
						</div><!-- /.page-header -->

						<?php if ( !empty($this->session->flashdata('profile_update'))) {
							?>

							 <div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $this->session->flashdata('profile_update'); ?>
 
                </div>
					
							
						<?php
					} ?>


						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="clearfix">
									
								</div>

								<div class="hr dotted"></div>

								<div>
									<form method="post" enctype="multipart/form-data">

									<div id="user-profile-1" class="user-profile row">
										<div class="col-xs-12 col-sm-3 center">
											<div>
												<span class="profile-picture">
						<img id="output"  class="editable img-responsive" alt="Alex's Avatar" src="<?php echo $img ?>" />
												</span>

												<div class="space-4"></div>
												<div class="col-md-12">
						<div class="form-group input-group">
						    <input type="file" class="form-control" onchange="loadFile(event)" name="photo" >
							
						</div>
						</div>

													
												<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">


													<div class="inline position-relative">
														<a href="#" class="user-title-label" >
															<i class="ace-icon fa fa-circle light-green"></i>
															&nbsp;
															<span class="white"><?php echo $result[0]['first_name']." ".$result[0]['last_name'] ?></span>
														</a>

												
													</div>
												</div>
											</div>

											<div class="space-6"></div>


										


											<div class="hr hr16 dotted"></div>
										</div>

										<div class="col-xs-12 col-sm-9">
											

											

											<div class="profile-user-info profile-user-info-striped">
												<div class="profile-info-row">
													<div class="profile-info-name"> Name </div>

													<div class="profile-info-value">
														
														<input type="text" name="username"  class="form-control" value="<?php echo $result[0]['first_name']." ".$result[0]['last_name'] ?>" >
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Email </div>

													<div class="profile-info-value">
														<input type="email" name="email" class="form-control" value="<?php echo $result[0]['user_email'] ?>" id="username">
														<i class="ui-icon ui-icon-pencil"></i>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Date of birth </div>

													<div class="profile-info-value">
														<input type="text" name="dob" class="form-control" value="<?php echo $result[0]['dob'] ?>" id="dob" autocomplete="off" >
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Joined </div>

													<div class="profile-info-value">
                                                <input type="text" readonly="" class="form-control" value="<?php echo date('d-m-Y',strtotime($result[0]['user_reg_date'])) ?>">
													</div>
												</div>

										<div class="clearfix form-actions">
													<div style="margin-left: auto;" class="col-md-offset-3 col-md-9">
														<button class="btn btn-info" type="submit" name="submit">
															<i class="ace-icon fa fa-check bigger-110"></i>
															Save
														</button>

													</div>
												</div>
										
											<div class="hr hr2 hr-double"></div>

										
										</div>
									</div>
								</div>


	
						</div><!-- /.row -->
					</form>
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

		
		 <?php $this->load->view('admin/include/footer'); ?> 
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

	
		<script src="assets/js/bootstrap-datepicker.min.js"></script>
		
	
		
		 <script>
		 	$('input[id$=dob]').datepicker({
    dateFormat: 'dd-mm-yy'
});
 
  </script>
		

		<script>
  var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };
</script>

	</body>
</html>
