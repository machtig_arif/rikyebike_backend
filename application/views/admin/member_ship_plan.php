<?php $this->load->view('admin/include/header'); ?>

		<div class="main-container ace-save-state" id="main-container">
			

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				
 <?php $this->load->view('admin/include/navigation'); ?>

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('admin_dashboard') ?>">Home</a>
							</li>

							
							
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
	

						<div class="page-header">
							
							<h1>
								<i class="fa fa-user" aria-hidden="true"></i>
								Membership Plan
							
							</h1>
						</div><!-- /.page-header -->

						<?php if ( !empty($this->session->flashdata('msg'))) {
							?>

							 <div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $this->session->flashdata('msg'); ?>
 
                </div>
					
							
						<?php
					} ?>

		<!-- 		<div class="col-md-12 clearfix">
          <form class="form-horizontal" action="" method="get">
            <div  class="form-group pull-right">
              <div class="col-md-12">
                <select style="border-radius: 10px;width: 160%;background-color: #f1ebe0;" class="form-control pull-right" name="status" onchange="this.form.submit()">
                  <option value="1"> Active</option>
                  <option <?php echo $this->input->get('status') == '0' ? 'selected':''; ?> value="0"> Inactive</option>
                </select>
              </div>
            </div>
          </form>
        </div> -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									<div class="col-xs-12">
										<table id="simple-table" class="table  table-bordered table-hover">
											<thead>
												<tr>
																									
													<th>#</th>
													
															<th>Membership Title</th>	
															<th>Amount</th>	
																						
													<th class="hidden-480">Status</th>

													<th>Created</th>
													<th>Action</th>
												</tr>
											</thead>

											<tbody>
												<?php
												
												 foreach ($result as $value) {
													@$count++
													 ?>
												<tr>
													
													<td><?php echo $count; ?></td>
													<td class="hidden-480"><?php echo $value['title']; ?></td>
													<td class="hidden-480"><?php echo $value['amount']; ?></td>
													
													<td class="hidden-480">
														<?php if($value['status']==1){ ?>
															<span class="label label-sm label-success">Active</span>
														<?php }elseif($value['status']==0){?>
														<span class="label label-sm label-danger arrowed-in">Inactive</span>
													<?php }?>
													</td>
													<td><?php echo $value['create_date'] ?></td>

													<td>
														<div class="hidden-sm hidden-xs btn-group">
															

															<button class="btn btn-xs btn-info" id="bt-modal" data-toggle="modal" data-target="#myModal<?php echo $count;?>">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</button>

															

															
														</div>


														  <div class="modal fade" id="myModal<?php echo $count; ?>" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content" style="border-radius: 20px;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Update Membership</h4>
                              </div>
                               
                              <div class="modal-body">
                                <form method="post">
                                  <label for="uname"><b>Title :</b></label>
                                  <input type="text" placeholder="Title"  name="title" class="form-control" value="<?php echo  $value['title'];  ?>" required>
                                   <label for="uname"><b>Amount :</b></label>
                                  <input type="text" placeholder="Amount"  name="amount" class="form-control" value="<?php echo  $value['amount'];  ?>" required>
                                  <input type="hidden" placeholder="Event Name" value="<?php echo $value['membership_id'] ?>" name="membership_id" class="form-control" required>

                                  <label for="uname"><b>Description :</b></label>
                                  <textarea name="description" class="form-control" rows="7" ><?php echo $value['description'] ?></textarea>
                                 
                                  <label for="psw"><b>Membership Status :</b></label>
                                  <select class="form-control" name="status" required="">
                                  	<option value="">Select Status</option>
                                  	<option <?php echo $value['status']==1 ? 'selected':''  ?> value="1">Active</option>
                                  	<option <?php echo $value['status']==0 ? 'selected':''  ?> value="0">Inactive</option>
                                  </select>
                                  <br/>
                                  <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                                 
                                </form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->

														
													</td>
												</tr>
												<?php
											}
											?>

											
											</tbody>
										</table>
									</div><!-- /.span -->
								</div><!-- /.row -->

								<div class="row">
              <div class="col-md-12">
      <div class="row"><?php echo $this->pagination->create_links(); ?></div> 
     </div>
    </div>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

		
 <?php $this->load->view('admin/include/footer'); ?> 
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->


	</body>
</html>
<script type="text/javascript">
	
</script>