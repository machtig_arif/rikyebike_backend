<?php
// $response = Unirest\Request::get("https://sports-scores.p.rapidapi.com/get_all_sports",
//   array(
//     "X-RapidAPI-Host" => "sports-scores.p.rapidapi.com",
//     "X-RapidAPI-Key" => "176a640343mshb7f1a5d64bd2cd4p184a6cjsne7672e243aec"
//   )
// );


// print_r($response);
?>

<style type="text/css">
	@font-face {
 font-family: "UglyQua";
 font-style: normal;
 src: url(UglyQua/UglyQua.eot); /*if IE */
 src: local("UglyQua"), url("UglyQua/UglyQua.ttf") format("truetype"); /* non-IE */
}

body,h1,h2,h3,p,quote,small,form,input,ul,li,ol,label{
 margin:0px;
 padding:0px;
}


body{
 color:#888888;
 font-size:13px;
 background: #eeeeee;
 font-family: "UglyQua";
}

hr{
 margin-top:10px;
 margin-bottom:10px;
 color:#eeeeee;
}
div.main{
 width:400px;
 margin: auto;
 padding: 10px;
}
h2{
 color:#888888;
 font-size:33px;
 padding:10px;
 text-shadow:2px 1px 6px #333;
}
div.news{
 width:400px;
 height:auto;
 height:415px;
 overflow: hidden;
 overflow-y: auto;
}
ul li{
 padding:8px;
 font-size:18px;
}
a.readmore {
    color: #999999;
    font-size: 60%;
    padding-left: 10px;
    text-decoration: underline;
}
div.button{
 text-align: center;
 padding: 10px;
}
a.button {
 display: inline-block;
 outline: none;
 cursor: pointer;
 text-align: center;
 text-decoration: none;
 font: 14px/100% Arial, Helvetica, sans-serif;
 padding: .5em 2em .55em;
 text-shadow: 0 1px 1px rgba(0,0,0,.3);
 -webkit-border-radius: .5em; 
 -moz-border-radius: .5em;
 border-radius: .5em;
 -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
 -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
 box-shadow: 0 1px 2px rgba(0,0,0,.2);
}
.white {
  background-attachment:initial;
  background-clip:initial;
  background-color:initial;
  background-image:-webkit-gradient(linear, 0 0%, 0 100%, from(#FFFFFF), to(#EDEDED));
  background-origin:initial;
  background-position:initial initial;
  background-repeat:initial initial;
  border-bottom-color:#B7B7B7;
  border-bottom-style:solid;
  border-bottom-width:1px;
  border-left-color:#B7B7B7;
  border-left-style:solid;
  border-left-width:1px;
  border-right-color:#B7B7B7;
  border-right-style:solid;
  border-right-width:1px;
  border-top-color:#B7B7B7;
  border-top-style:solid;
  border-top-width:1px;
  color:#606060;
}
.bigrounded {
  border-bottom-left-radius:2em 2em;
  border-bottom-right-radius:2em 2em;
  border-top-left-radius:2em 2em;
  border-top-right-radius:2em 2em;
}
</style>

<html>
<style>

</style>
<head>
    <title>ESPN Sports API | WebSpeaks.in</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <!--  <link rel="stylesheet" type="text/css" href="style.css" /> -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
</head>
<body>
    <div class="main">
        <h2>Latest in Sports World</h2>
        <hr />
        <div class="news">
            <div style="text-align:center" id="result"></div>
            <div>
                <ul id="list"></ul>
            </div>
        </div>
        <hr />
        <div class="button"><a class="button white bigrounded" id="load_more">Load More</a></div>
    </div>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function(){
        var news_limit = 5;
        var news_offset = 0;

        $('a#load_more').click(function(){
            load_more_news();
        });

        function load_more_news(){
            news_offset += news_limit;
            $.ajax({ 
                url: "http://api.espn.com/v1/sports/news/headlines", 
                data: { 
                    // enter your developer api key here 
                    apikey: "176a640343mshb7f1a5d64bd2cd4p184a6cjsne7672e243aec",
                    // the type of data you're expecting back from the api 
                    _accept: "application/json",
                    // number of results to be shown
                    limit: news_limit,
                    offset: news_offset
                }, 
                dataType: "jsonp", 
                beforeSend: function(){
                    $('#result').html('Loading...');
                },
                success: function(data) { 
                    $('#result').html('');
                    // create an unordered list of headlines 
                    var ul = $('ul#list'); 
                    $.each(data.headlines, function() { 
                     var htm = this.headline;
                     htm += '<a class="readmore" href="'+this.links.web.href+'" target="_blank">Read more</a>';
                        var li = $('<li/>').html(htm); 
                        ul.append(li) 
                    }); 
                }, 
                error: function() { 
                     // handle the error 
                } 
            });
        }
        load_more_news();
    });
</script>