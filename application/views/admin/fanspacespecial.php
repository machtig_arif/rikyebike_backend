<?php $this->load->view('admin/include/header'); ?>
  <div class="main-container ace-save-state" id="main-container">
    <div id="sidebar" class="sidebar responsive ace-save-state">
    <?php $this->load->view('admin/include/navigation'); ?>
    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
      <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
  </div>
  <div class="main-content">
    <div class="main-content-inner">
      <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
          <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="<?php echo base_url('admin_dashboard') ?>">Home</a>
          </li>
        </ul>
      </div>
      <div class="page-content">
        <div class="page-header">
          <button style="float: right;margin-bottom: inherit;height: 40px;width: 30%;color: white;  background-color: #438eb9;border-radius: 10px;"   type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1" >Add Fanspace Special</button>
          <h1>
            <i class="fa fa-user" aria-hidden="true"></i>
            Fanspace Special Management
          </h1>
        </div>
        <?php if ( !empty($this->session->flashdata('msg'))) {
          ?>
        <div class="alert alert-block alert-success" style="text-align: center;">
          <button type="button" class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
          </button>
          <i class="ace-icon fa fa-check green"></i>
          <?php echo $this->session->flashdata('msg'); ?>
        </div>
        <?php
          } ?>
        <div class="col-md-12 clearfix">
          <form class="form-horizontal" action="" method="get">
              <div  class="form-group pull-right">
                  <div class="col-md-12">
                      <select style="border-radius: 10px;width: 160%;background-color: #f1ebe0;" class="form-control pull-right" name="status" onchange="this.form.submit()">
                          <option value="1"> Active</option>
                          <option <?php echo $this->input->get('status') == '0' ? 'selected':''; ?> value="0"> Inactive</option>
                      </select>
                  </div>
              </div>
          </form>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-12">
                <table id="simple-table" class="table  table-bordered table-hover">
                  <thead>
                    <tr>                 
                      <th>#</th>
                      <th>Restaurant Name</th> 
                      <th class="hidden-480">Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $count = $star;
                      foreach ($fan_result as $value_fan) {
                        $count++
                    ?>
                    <tr>
                      <td><?php echo $count; ?></td>
                      <td class="hidden-480"><?php echo $value_fan['title']; ?></td>
                      <td class="hidden-480">
                        <?php if($value_fan['status']==1){ ?>
                          <span class="label label-sm label-success">Active</span>
                        <?php }elseif($value_fan['status']==0){?>
                        <span class="label label-sm label-danger arrowed-in">Inactive</span>
                        <?php }?>
                      </td>
                      <td>
                        <div class="hidden-sm hidden-xs btn-group">
                            <button class="btn btn-xs btn-info" id="bt-modal" data-toggle="modal" data-target="#myModal<?php echo $count;?>">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </button>
                            <button id="<?php echo $value_fan['special_id'] ?>" class="btn btn-xs btn-danger delete">
                              <i class="ace-icon fa fa-trash-o bigger-120 "></i>
                            </button>
                        </div>
                        <div class="modal fade" id="myModal<?php echo $count; ?>" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content" style="border-radius: 20px;">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Update Fanspace Special</h4>
                                    </div>
                                     <div class="modal-body">
                                        <form method="post">
                                          <input type="hidden" name="special_id" value="<?php echo $value_fan['special_id'] ?>">
                                            <label><b>Restaurants Name :</b></label>
                                            <select class="form-control" name="restaurant" required>
                                    <option> Select Restaurant </option>
                                    <?php 
                                        foreach ($special_dd as $val_dd) {
            $selected = ($val_dd['title'] == $value_fan['title'] ? 'selected' : '');
            echo '<option value="'.$val_dd['id'].'" '.$selected.'>'.$val_dd['title'].'</option>';
                    
                        }
                        ?>
                                  </select>
                                            <label for="status"><b>Status :</b></label>
                                              <select class="form-control" name="status" required="">
                                    <option value="">Select Status</option>
                                    <option <?php echo $value_fan['status']==1 ? 'selected':''  ?> value="1">Active</option>
                                    <option <?php echo $value_fan['status']==0 ? 'selected':''  ?> value="0">Inactive</option>
                                  </select>
                                              <br />
                                              <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                                        </form>
                                    </div>
                                </div>
                                <!--model-content-->
                            </div>
                        </div>
                        <!--model-->
                      </td>
                    </tr>
                      <?php
                      }
                      ?>
                      </tbody>
                    </table>
                  </div><!-- /.span -->
                </div><!-- /.row -->

                <div class="row">
              <div class="col-md-12">
      <div class="row"><?php echo $this->pagination->create_links(); ?></div> 
     </div>
    </div>

                <!-- PAGE CONTENT ENDS -->
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.page-content -->
        </div>
      </div><!-- /.main-content -->

    
 <?php $this->load->view('admin/include/footer'); ?> 
      <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
      </a>
    </div><!-- /.main-container -->

      <div class="modal fade" id="myModal-1" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content" style="border-radius: 20px;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Fanspace Special</h4>
                              </div>
                              <div class="modal-body">
                                <form method="post">
                                  <label for="uname"><b>Restaurant :</b></label>
                                  <select class="form-control" name="restaurant" required>
                                    <option> Select Restaurant </option>
                                    <?php 
                                        foreach ($special_dd as $val_dd) {
                                    ?>
                                    <option value="<?php echo $val_dd['id']; ?>"> 
                                        <?php echo $val_dd['title']; ?>
                                    </option>
                                    <?php
                                        }
                                    ?> 
                                  </select>
                                  <label for="psw"><b>Select Status :</b></label>
                                  <select class="form-control" name="status" required="">
                                    <option value="">Select Status</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                  </select>
                                  <br/>
                                  <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Add to Special</button>
                                 
                                </form>
                              </div>
                             <!--  <div class="modal-footer">
                               
                              </div> -->
                              </div><!--model-content-->
                              
                            </div>
                            </div><!--model-->

  </body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.delete').click(function() {
            if (confirm("Do you want to delete?")) {
                var special_id = $(this).attr('id');
                //alert(special_id);
            } else {
                return false;
            }
            $.post("<?php echo base_url('Admin_controller/fanspace_special_delete') ?>", { special_id: special_id }, function(res) {
                // if(res=='success')
                // {
                location.reload();
                //}
            })
        })
    })
</script>