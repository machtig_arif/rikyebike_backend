<?php $this->load->view('admin/include/header'); ?>
<div class="main-container ace-save-state" id="main-container">
    <div id="sidebar" class="sidebar responsive ace-save-state">
        <?php $this->load->view('admin/include/navigation'); ?>
        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="<?php echo base_url('admin_dashboard') ?>">Home</a>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="page-header">
                    <button style="float: right;margin-bottom: inherit;height: 40px;width: 30%;color: white;  background-color: #438eb9;border-radius: 10px;" type="button" id="bt-modal" data-toggle="modal" data-target="#myModal-1">Add Price Slots</button>
                    <h1>
                        <i class="fa fa-user" aria-hidden="true"></i>
                        Price Slots
                    </h1>
                </div><!-- /.page-header -->
                <?php 
				    if ( !empty($this->session->flashdata('add_city'))) {
				?>
                <div class="alert alert-block alert-success" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <i class="ace-icon fa fa-check green"></i>
                    <?php echo $this->session->flashdata('add_city'); ?>
                </div>
                <?php } ?>
                <!--<h2 style="margin-top: 0px;">Import States Csv</h2>
					<div class="col-md-2 clearfix">
						<form action="<?php //echo base_url('Admin_controller/import') ?>" enctype="multipart/form-data" method="post">
					       <input type="file" name="csv_file" accept=".csv" style="margin-bottom: 10px;">
					       <input class="btn-info" type="submit" name="csv_submit" >
				        </form>
                    </div>
					<div class="col-md-5 clearfix">
					<p style="margin-left: 50px;color: red;" >Note: Please check Sample CSV File :<a href="<?php echo base_url('Admin_controller/download') ?>"> Click Here</a> </p></div> -->
                <!-- <div class="col-md-12 clearfix">
                    <form class="form-horizontal" action="" method="get">
                        <div class="form-group pull-right">
                            <div class="col-md-12">
                                <select style="border-radius: 10px;width: 160%;background-color: #f1ebe0;" class="form-control pull-right" name="status" onchange="this.form.submit()">
                                    <option value="1"> Active</option>
                                    <option <?php //echo $this->input->get('status') == '0' ? 'selected':''; ?> value="0"> Inactive</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div> -->
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="simple-table" class="table  table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th> Hours </th>
                                            <th> Time Period </th>
                                            <th> Day </th>
                                            <th> Promotion Type </th>
                                            <th> Points </th>
                                            <th> Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
											$count=$star;
											foreach ($price_slot as $value) {
												$count++
										?>
                                        <tr>
                                            <td>
                                                <?php echo $count; ?>
                                            </td>
                                            <td class="hidden-480">
                                                <?php echo $value['hours']; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['day_time']; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['day']; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['promotion_place']; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['points']; ?>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <button class="btn btn-xs btn-info" id="bt-modal" data-toggle="modal" data-target="#myModal<?php echo $count;?>">
                                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                    </button>
                                                    <button id="<?php echo $value['price_id'] ?>" class="btn btn-xs btn-danger delete">
                                                        <i class="ace-icon fa fa-trash-o bigger-120 "></i>
                                                    </button>
                                                </div>
                                                <div class="modal fade" id="myModal<?php echo $count; ?>" role="dialog">
                                                    <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content" style="border-radius: 20px;">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Update Price Slot</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form method="post">
                                                                    <input type="hidden" name="price_id" value="<?php echo $value['price_id'] ?>">
                                                                    <label><b>Hours :</b></label>
                                                                    <select class="form-control" name="hours" required>
                                                                        <option> Select Hours </option>
                                                                        <?php 
                                                                            foreach ($hours_dd as $val_dd) {
                                                                                $selected = ($val_dd == $value['hours'] ? 'selected' : '');
                                                                        echo '<option value="'.$val_dd.'" '.$selected.'>'.$val_dd.'</option>';
                    
                        }
                        ?>
                                                                    </select>
                                                                    <label><b>Time Period :</b></label>
                                                                    <select class="form-control" name="day_time" required>
                        <option value=""> Select Time Period </option>
                        <?php 
                        //echo $value['day_time'].'====================';die;
                        foreach ($period_dd as $val_dd) {
            $selected = ($val_dd == $value['day_time'] ? 'selected' : '');
            echo '<option value="'.$val_dd.'" '.$selected.'>'.$val_dd.'</option>';
                    
                        }
                        ?>
                    </select>
                                                                    <label><b>Day :</b></label>
                                                                    <select class="form-control" name="day" required>
                        <option> Select Days </option>
                        <?php 
                            foreach ($days_dd as $val_dd) {
                                $selected = ($val_dd == $value['day'] ? 'selected' : '');
                        echo '<option value="'.$val_dd.'" '.$selected.'>'.$val_dd.'</option>';
                    
                        }
                        ?>
                    </select>
                                                                    
                                                                    <label><b>Promotion Type :</b></label>
                                                                    <select class="form-control" name="promotion_place" required>
                        <option> Select Promotion Type </option>
                        <option value="Event" <?php echo ($value['promotion_place'] == 'Event' ? 'selected' : ''); ?>> Event </option>
                        <option value="Deal" <?php echo ($value['promotion_place'] == 'Deal' ? 'selected' : ''); ?>> Deal </option>
                        <option value="Banner" <?php echo ($value['promotion_place'] == 'Banner' ? 'selected' : ''); ?>> Banner </option>
                    </select>
                                                                    <label><b>Points :</b></label>
                                                                    <input type="text" value="<?php echo $value['points'] ?>" name="points" class="form-control" required>
                                                                    <br />
                                                                    <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <!--model-content-->
                                                    </div>
                                                </div>
                                                <!--model-->
                                            </td>
                                        </tr>
                                        <?php
											}
										?>
                                    </tbody>
                                </table>
                            </div><!-- /.span -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <?php echo $this->pagination->create_links(); ?>
                                </div>
                            </div>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->
    <?php $this->load->view('admin/include/footer'); ?>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->
<div class="modal fade" id="myModal-1" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 20px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Price Slots</h4>
            </div>
            <div class="modal-body">
                <form method="post">
                    <label><b> Hours Slot :</b></label>
                    <select class="form-control" name="hours" required>
                        <option> Select Hours </option>
                        <?php 
                            foreach ($hours_dd as $val_dd) {
                        ?>
                        <option value="<?php echo $val_dd; ?>"> 
                            <?php echo $val_dd; ?>
                        </option>
                        <?php
                            }
                        ?>
                    </select>
                    <label><b> Time Period :</b></label>
                    <select class="form-control" name="day_time" required>
                        <option value=""> Select Time Period </option>
                        <?php 
                            foreach ($period_dd as $val_dd) {
                        ?>
                        <option value="<?php echo $val_dd; ?>"> 
                            <?php echo $val_dd; ?>
                        </option>
                        <?php
                            }
                        ?>
                    </select>
                    <label><b> Days :</b></label>
                    <select class="form-control" name="day" required>
                        <option> Select Days </option>
                        <?php 
                            foreach ($days_dd as $val_dd) {
                        ?>
                        <option value="<?php echo $val_dd; ?>"> 
                            <?php echo $val_dd; ?>
                        </option>
                        <?php
                            }
                        ?>
                    </select>
                    <label><b> Promotion Type :</b></label>
                    <select class="form-control" name="promotion_place" required>
                        <option> Select Promotion Type </option>
                        <option value="Event"> Event </option>
                        <option value="Deal"> Deal </option>
                        <option value="Banner"> Banner </option>
                    </select>
                    <label><b> Points :</b></label>
                    <input type="text" name="points" placeholder="Points" class="form-control" required>
                    <br />
                    <button style="background-color: #438eb9;color: white;margin-bottom: 25px; " type="submit" name="submit" class="form-control">Submit</button>
                </form>
            </div>
        </div>
        <!--model-content-->
    </div>
</div>
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.delete').click(function() {
            if (confirm("Do you want to delete?")) {
                var price_id = $(this).attr('id');
                //alert(price_id);
            } else {
                return false;
            }
            $.post("<?php echo base_url('Admin_controller/price_slot_delete') ?>", { price_id: price_id }, function(res) {
                // if(res=='success')
                // {
                location.reload();
                //}
            })
        })
    })
</script>