<?php $this->load->view('admin/include/header'); ?>

		<div class="main-container ace-save-state" id="main-container">
			

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				
 <?php $this->load->view('admin/include/navigation'); ?>

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url('admin_dashboard') ?>">Home</a>
							</li>

							
							
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
	

						<div class="page-header">
							<a href="<?php echo base_url('add_restaurant') ?>"> <button style="float: right;margin-bottom: inherit;height: 40px;width: 30%;color: white;  background-color: #438eb9;border-radius: 10px;"   type="button">Add New Restaurant</button></a>
							<h1>
								<i class="fa fa-user" aria-hidden="true"></i>
								Restaurant Manager
							
							</h1>
						</div><!-- /.page-header -->

						<?php if ( !empty($this->session->flashdata('add_state'))) {
							?>

							 <div class="alert alert-block alert-success" style="text-align: center;">
                  <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                  </button>

                  <i class="ace-icon fa fa-check green"></i>

                <?php echo $this->session->flashdata('add_state'); ?>
 
                </div>
					
							
						<?php
					} ?>
					<h2 style="margin-top: 0px;">Import Restaurants CSV</h2>
					<div class="col-md-2 clearfix">
						<form action="<?php echo base_url('Admin_controller/import_restaurants') ?>" enctype="multipart/form-data"method="post">

							<input type="file" name="csv_file" accept=".csv" style="margin-bottom: 10px;">
							<button class="btn-info" type="submit" name="csv_submit">
								Import Restaurants
							</button>
						</form>
					</div>
					<div class="col-md-5 clearfix">
					<p style="margin-left: 50px;color: red;" >Note: Please check Sample CSV File :<a href="<?php echo base_url('images/csv_format/user.csv') ?>"> Click Here</a> </p></div>
				<div class="col-md-12 clearfix">
					<div class="col-md-9">
						<input placeholder="Search Restuarants" type="text" name="search_data" id="search_data" onkeyup="search_data()" />
					</div>
		          <form class="form-horizontal" action="" method="get">
		            <div  class="form-group pull-right">
		              <div class="col-md-9">
		                <select style="border-radius: 10px;width: 160%;background-color: #f1ebe0;" class="form-control pull-right" name="status" onchange="this.form.submit()">
		                  <option value="1"> Active</option>
		                  <option <?php echo $this->input->get('status') == '0' ? 'selected':''; ?> value="0"> Inactive</option>
		                </select>
		              </div>
		            </div>
		          </form>
		        </div>

						<div class="row">
							<div class="col-xs-12 " id="search_data_response">
								<!-- PAGE CONTENT BEGINS -->
								
								<div class="row">
									<div class="col-xs-12">
										<table id="simple-table" class="table  table-bordered table-hover">
											<thead>
												<tr>
																									
													<th>#</th>
													
															<th>Business Title</th>	
															<!-- <th>Bar Type</th> -->									
													<th class="hidden-480">Foods Type</th>

												<!-- 	<th>Music-Type</th> -->
													<!-- <th>Music Genre</th> -->
													<th>Amenities</th>
													<th>Package</th>
													<th>Status</th>
													<th style="width: 7%;">Action</th>
												</tr>
											</thead>

											<tbody>
												<?php
												$count=$star;
												 foreach ($result as $value) {
													$count++
													 ?>
													 
												<tr>
													
													<td><?php echo $count; ?></td>
													<td class="hidden-480"><?php echo $value['title']; ?></td>
													<!-- <td>
													<?php $restaurant_bar=$this->Admin_modal->select_restaurant_bar($value['restaurant_id']);
													foreach ($restaurant_bar as  $value1) {
														echo $value1['bar_type'];

													
														
													 ?>,
													<?php //$myArray[] = $value1['bar_type']; ?> 
												<?php }
												//echo implode($myArray,' , ' );
												 ?>
											</td> -->
													<td>
													<?php $restaurant_bar=$this->Admin_modal->select_restaurant_food($value['restaurant_id']);
													foreach ($restaurant_bar as  $value2) {
														echo $value2['food_type'];
													 ?>,
													<?php //$myArray1[] = $value2['food_type']; ?> 
												<?php }
												//echo implode($myArray1,' , ' );
												 ?>
											</td>


												<!-- 	<td>
													<?php $restaurant_bar=$this->Admin_modal->select_restaurant_music($value['restaurant_id']);
													foreach ($restaurant_bar as  $value3) {
														echo $value3['music_type'];
													 ?>,
													<?php //$myArray2[] = $value3['music_type']; ?> 
												<?php }
												//echo implode($myArray2,' , ' ); ?>
											</td> -->

														<!-- <td>
													<?php $restaurant_bar=$this->Admin_modal->select_restaurant_music_genre($value['restaurant_id']);
													foreach ($restaurant_bar as  $value4) {
														echo $value4['music_genre_name'];
													 ?>,
													<?php //$myArray3[] = $value4['music_genre_name']; ?> 
												<?php }
												//echo implode($myArray3,' , ' ); ?>
											</td> -->

													<td>
													<?php $restaurant_bar=$this->Admin_modal->select_restaurant_amenities($value['restaurant_id']);
													foreach ($restaurant_bar as  $value5) {
														echo $value5['amenities_name'];
													 ?>,
													<?php // $myArray4[] = $value5['amenities_name']; ?> 
												<?php }
												//echo implode($myArray4,' , ' ); ?>
											</td>


											<td>
													<?php $package=$this->Admin_modal->restaurant_package($value['restaurant_id']);
															if ($package['package_id'] == 1) {
																echo 'Silver';
															}
															else if ($package['package_id'] == 2) {
																echo 'Gold';
															}
															else{
																echo 'not selected';
															}
														?>
											</td>

													<td class="hidden-480">
														<?php if($value['status']==1){ ?>
															<span class="label label-sm label-success">Active</span>
														<?php }elseif($value['status']==0){?>
														<span class="label label-sm label-danger arrowed-in">Inactive</span>
													<?php }?>
													</td>

													<td>
														<div class="hidden-sm hidden-xs btn-group">

															<a href="<?php echo base_url()?>add_restaurant?res=<?php echo $value['restaurant_id'] ?>">
																
															<button class="btn btn-xs btn-info" id="bt-modal" style="height: 26px;">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</button>
														</a>
															


															<button id="<?php echo $value['restaurant_id'] ?>" class="btn btn-xs btn-danger delete">
																<i class="ace-icon fa fa-trash-o bigger-120 "></i>
															</button>

															
														</div>
														
													</td>
												</tr>

												<?php
											}
											?>

											
											</tbody>
										</table>
									</div><!-- /.span -->
								</div><!-- /.row -->

								<div class="row">
              <div class="col-md-12">
      <div class="row"><?php echo $this->pagination->create_links(); ?></div> 
     </div>
    </div>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

		
 <?php $this->load->view('admin/include/footer'); ?> 
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

	</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.delete').click(function(){
			if(confirm("Do you want to delete?"))
			{

			var restaurant_id=$(this).attr('id');
			}else{
				  return false;
			}

			$.post("<?php echo base_url('Admin_controller/restaurant_delete') ?>",{restaurant_id:restaurant_id},function(res){
				// if(res=='success')
				// {
					 location.reload();
				//}
			})
		})
	})

</script>
<script>
	function search_data() {
		var search_data = $("#search_data").val();
		if(search_data.length>=3 || search_data.length === 0  ){
			$.post("<?php echo base_url(); ?>Admin_controller/search_restaurant",
				{search_data:search_data},

				function(data){
					//alert(data);
					$('#search_data_response').html(data);
			})
		}
	}
</script>