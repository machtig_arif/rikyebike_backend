<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CI_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		  $this->load->model('Admin_modal');
      $this->load->library('csvimport');
		
	}

	public function index()
	{

    
		 $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('Password', 'Password', 'required');
        
         if ($this->form_validation->run() == FALSE)
                {
                  // redirect(base_url());
                }else{
                      if($data=$this->Admin_modal->admin_login($this->input->post('username'),md5($this->input->post('Password'))))
                      {
                      	  $admin_id=$data[0]['user_id'];
                          $role=$data[0]['user_role'];
                          if($role=='Admin')
                          {
                      	$this->session->set_userdata('admin_id',$admin_id);
                        echo "<script>window.location.href='".base_url('admin_dashboard')."';</script>";
                      	  //redirect('admin_dashboard');
                        }elseif($role=='Restaurant') {
                         $this->session->set_userdata('user_id',$admin_id);
                          echo "<script>window.location.href='".base_url('User_controller')."';</script>";

                          //redirect('User_controller');
                        }
                      }else
                      {                      	
                      $this->session->set_flashdata('incorrct','Email or Password is invalid');                     
                      }
                   }
 $this->load->view('admin/admin_login');
	}

	
	public function admin_dashboard()
	{

    $data['user_count']=$this->Admin_modal->user_record_count();
    // $data['restaurant_count']=$this->Admin_modal->restaurant_record_count();
    //  $data['food_count']=$this->Admin_modal->food_record_count();
    //    $data['music_count']=$this->Admin_modal->music_record_count();
    //      $data['bar_count']=$this->Admin_modal->bar_record_count();
    //      $data['amenities_count']=$this->Admin_modal->amenities_record_count();
    //       $data['music_genre_count']=$this->Admin_modal->music_genre_record_count();
    //       $data['crowd_count']=$this->Admin_modal->crowd_record_count();
    //        $data['coupon_count']=$this->Admin_modal->coupon_record_count();


            //print_r($data);
		$this->load->view('admin/dashboard',$data);
	}

    public function admin_profile()
    {
    	$admin_id=$this->session->userdata('admin_id');
    	$submit=$this->input->post('submit');
    	if(isset($submit))
    	{
        $date=date_create($this->input->post('dob'));
      $date1=date_format($date,"Y-m-d");
    	
              $tmp_name=$_FILES['photo']['tmp_name'];
              $file_name=$_FILES['photo']['name'];       
              if(!empty($file_name))
              {
             if(move_uploaded_file($tmp_name, 'assets/images/'.$file_name))
             {
    		$data = array('first_name' =>$this->input->post('username') ,'user_email' =>$this->input->post('email') ,'dob' => $date1 ,'user_avatar' => $file_name);    		
             }
         }else{
         	$data = array('first_name' =>$this->input->post('username') ,'user_email' =>$this->input->post('email') ,'dob' =>$date1);
         }
          $this->session->set_flashdata('profile_update','Update successfully'); 
         $this->Admin_modal->admin_profile_update($data,$admin_id);

    	}
    	$admin_id=$this->session->userdata('admin_id');
        $data['result']=$this->Admin_modal->admin_profile($admin_id);        
    	$this->load->view('admin/admin_profile',$data);
    }


    public function email_validation()
    {
      $email=$this->input->post('email');
      $data=$this->Admin_modal->email_validation($email);
     if ($data>0) {   

// $config = array(
// 'protocol' => 'smtp',
// 'smtp_host' => 'mail.yesitlabs.com',
// 'smtp_port' => 25,
// 'smtp_user' => 'no-reply@yesitlabs.com',
// 'smtp_pass' => 'Dev@123456',
// 'mailtype'  => 'html', 
// 'charset'   => 'iso-8859-1'
// );     

$this->session->set_userdata('email',$email);
$code=(rand(10,10000));

$this->session->set_userdata('code',$code);
$message = "<div>Hi,
<p>Please click the following link which will take you back to the site to set up your new password:</p>
</div>";
$message.="<a href='".base_url()."admin_forgot_password/$code'>Click Here</a>
<br/><br/><p>If your email client does not allow you to click the link, please copy and paste the link into your web browser address bar
and press enter.</p> 
<br/> <p>To enable you to enjoy all the benefits of your new online account, please make sure you add our email address to your
safe senders list.</p> 
<br/><p>Kind regards</p>
<p>PUBHUB Team</p>
<br/><br/>
<p>This is an automated email generated by us. Please do not respond directly to this email. If you have a query and would
like to contact us, please visit <a href='http://www.livepubhub.com'> website link </a> and click on the 'Contact us' link</p>
";

$this->load->library('email');
$this->email->set_newline("\r\n");
$this->email->from('no-reply@yesitlabs.xyz'); // change it to yours
$this->email->to($email);//msz  where and in which email to send?
$this->email->subject('Pub-Hub');
$this->email->message($message);
   $this->email->set_mailtype("html");
$this->email->send();
      echo '1';
     }else{
      echo '0';
     }
    }

    public function logout()
    {
      session_destroy();
      //redirect(base_url('Admin_controller'));
      echo "<script>window.location.href='".base_url('Admin_controller')."';</script>";

    }

    public function states_settings($offset=0)
    {
     $state_id=$this->input->post('state_id');     
      $submit=$this->input->post('submit');
      if (empty($state_id)) {
       
      if (isset($submit)) {
        $data = array('country_id' =>$this->input->post('country_name'),'state_name' =>$this->input->post('state_name') ,'status' =>$this->input->post('status') , );
        if($this->Admin_modal->add_states($data))
        {
           $this->session->set_flashdata('add_state','State successfully added'); 
           echo "<script>window.location.href='".base_url('states_settings')."';</script>";
           //redirect(base_url('states_settings'));
        }
      }
    }else{
     $data = array('country_id' =>$this->input->post('country_name'),'state_name' =>$this->input->post('state_name') ,'status' =>$this->input->post('status') , );
        if($this->Admin_modal->update_states($data,$state_id))
        {
           $this->session->set_flashdata('add_state','State successfully updated'); 
            echo "<script>window.location.href='".base_url('states_settings')."';</script>";
           //redirect(base_url('states_settings'));
        }
    }

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->states_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."states_settings";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_state($Searchkey,$config['per_page'],$this->uri->segment(2));
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;
//print_r($data['result']);die;
   $data['country_list'] = $this->Admin_modal->select_coupon_list();
   
      $this->load->view('admin/states_settings',$data);
    }

    public function state_delete()
    {
      $state_id=$this->input->post('state_id');
      if($this->Admin_modal->state_delete($state_id))
      {
        echo 'success';
        $this->session->set_flashdata('add_state','State deleted successfully'); 
      }
    }


    public function city_delete()
    {
      $city_id=$this->input->post('city_id');
      if($this->Admin_modal->city_delete($city_id))
      {
        echo 'success';
        $this->session->set_flashdata('add_city','City deleted successfully'); 
      }
    }

            ///////////////// IMPORT CSV   //////////////

     function import()
      { 
       
        $file_data = $this->csvimport->parse_file($_FILES["csv_file"]["tmp_name"]);
        foreach($file_data as $row)
        {

         $data= array(
          'state_name' =>$row['State Name'],
                'status'  =>1,
                'state_code'   =>$row['State Code'],
               
         );
  $this->Admin_modal->add_states($data);
        }
         $this->session->set_flashdata('add_state','State Import successfully'); 
          echo "<script>window.location.href='".base_url('states_settings')."';</script>";
 //redirect('states_settings');
 }


    function download($filename = NULL) { 
    $path = 'images/csv_format/Book1.csv';
    if(file_exists($path) && is_readable($path)) {
        $size = filesize($path);
        header('Content-Type: application/octet-stream');
        header('Content-Length: '.$size);
        header('Content-Disposition: attachment; filename=Book1.csv');
        header('Content-Transfer-Encoding: binary');
        $file =  fopen($path, 'rb'); 
        fpassthru($file);
    }
  }   




                  //////////////////////////


  //////////////////  ADMIN FORGET PASSWORD  /////////////////////

      public function admin_forgot_password($value='')
      {  
         $code=$this->session->userdata('code');
          $admin_id=$this->session->userdata('email');
        
         $new_pass=$this->input->post('new_password');
         $con_pass=$this->input->post('conform_password');
          $submit=$this->input->post('submit');

          if(isset($submit))
          {
         if($code==$value)
         {
          if($new_pass==$con_pass)
          {

            $this->Admin_modal->admin_forgot_password($admin_id,md5($new_pass));

             $this->session->set_flashdata('incorrct',"Password change successfully please login");
              echo "<script>window.location.href='".base_url('')."';</script>";
            // redirect(base_url(''));
          }else{
            $this->session->set_flashdata('msg',"New Password and confirm password are not matched");
          }

         }else
          {
          $this->session->set_flashdata('msg',"Your Are Not Valid Person");
          }
        }
         $this->load->view('admin/admin_forgot_password');
      }




                   ///////////////////////

    public function social_link()
    {
      $social_id=$this->input->post('social_id');
      if(!empty($social_id))
      {
        $data = array('social_name' =>$this->input->post('social_name') ,'social_link' =>$this->input->post('social_link') ,'status' =>$this->input->post('status') , );
       if($this->Admin_modal->update_social_link($data,$social_id))
       {
          $this->session->set_flashdata('msg','Social link successfully updated');
           echo "<script>window.location.href='".base_url('social_link')."';</script>";
         // redirect(base_url('social_link')); 
       }
      }
      $data['result']=$this->Admin_modal->select_social_link();
      $this->load->view('admin/social_link',$data);
    }


    public function admin_change_password()
    {
        $admin_id=$this->session->userdata('admin_id');
      $submit=$this->input->post('submit');
      if (isset($submit)){
      $old_pass=md5($this->input->post('old_pass'));
      
        $new_pass=$this->input->post('new_pass');
        $con_pass=$this->input->post('con_pass');

        $data=$this->Admin_modal->select_admin_password($admin_id,$old_pass);
       if (!empty($data)){
           if($new_pass== $con_pass)
           {
               $new_pass1=md5($new_pass);
               if($this->Admin_modal->change_password($new_pass1,$admin_id))
               {
                $this->session->set_flashdata('msg','Password changed successfully');
               }
           }else{
            $this->session->set_flashdata('msg','New password and confirm password should be same');
           }
       }else
       {
         $this->session->set_flashdata('msg','Old Password is Incorrect');
       }
      }
        $data['result']=$this->Admin_modal->admin_profile($admin_id);    
      $this->load->view('admin/change_password',$data);
    }


    public function users($offset=0)
    {

      $user_id=$this->input->post('user_id');     
      $submit=$this->input->post('submit');
      if (!empty($user_id)) {       
      if (isset($submit)) {
       $status=$this->input->post('status');
        if($this->Admin_modal->update_user_status($status,$user_id))
        {
           $this->session->set_flashdata('msg','User status updated successfully'); 
            echo "<script>window.location.href='".base_url('users')."';</script>";
           //redirect(base_url('users'));
        }
      }
    }
   

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->user_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."users";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_users($Searchkey,$config['per_page'],$this->uri->segment(2));
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;

      $this->load->view('admin/users',$data);

    }

     public function user_delete()
    {
      $user_id=$this->input->post('user_id');
      if($this->Admin_modal->user_delete($user_id))
      {
        echo 'success';
        $this->session->set_flashdata('msg','User deleted successfully'); 
      }
    }


    public function restaurant()
    {
            $user_id=$this->input->post('user_id');     
      $submit=$this->input->post('submit');
    

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->restaurant_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."users";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_restaurant($Searchkey,$config['per_page'],$this->uri->segment(2));

 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;

      $this->load->view('admin/restaurant',$data);
    }



/////////////////////////////////////////////////////////////////////////////////////////////////


    public function add_restaurant()
    {
      $submit=$this->input->post('submit');
      if(!empty($this->input->get('res')))
      {
      $restaurant_id=$this->input->get('res');
    }
      if (isset($submit)) {
        $date=date('Y-m-d');
        $bar_type= $this->input->post('bar_type');
        $music_type= $this->input->post('music_type');
        $music_genre= $this->input->post('music_genre');
        $amenities= $this->input->post('amenities');
         $food_type= $this->input->post('food_type');
          $average_crowd= $this->input->post('average_crowd');
            $file_count=count($_FILES['files']['name']);
     if (empty($restaurant_id)) {
             $use_data = array('first_name' => $this->input->post('fname'),'last_name' => $this->input->post('lname'),'user_email' => $this->input->post('email'),'user_password' => md5($this->input->post('password')),'user_role' => 'Restaurant','user_reg_date' =>$date ,'remember_token' => $session_id,'status'=>'1', );
        if($this->Admin_modal->add_user($use_data))
        {
           $restaurant_id = $this->db->insert_id();
        }
      
        $add_restaurant = array('title' => $this->input->post('title'),'street' => $this->input->post('street'),'city' => $this->input->post('city'),'state' => $this->input->post('state'),'zipcode' => $this->input->post('zipcode'),'status' => $this->input->post('status'), 'website' => $this->input->post('website'),'phone' => $this->input->post('phone'),'create_date' =>$date,'lat' => $this->input->post('latitude'),'log' => $this->input->post('longitude'),'user_id'=>$restaurant_id,'restaurant_id'=>$restaurant_id,'open_time' => $this->input->post('otime'),'close_time' => $this->input->post('ctime'), );

           $this->Admin_modal->add_restaurant($add_restaurant);
     
     }else{

       $add_restaurant = array('title' => $this->input->post('title'),'street' => $this->input->post('street'),'city' => $this->input->post('city'),'state' => $this->input->post('state'),'zipcode' => $this->input->post('zipcode'),'status' => $this->input->post('status'), 'website' => $this->input->post('website'),'phone' => $this->input->post('phone'),'create_date' =>$date,'lat' => $this->input->post('latitude'),'log' => $this->input->post('longitude'),'user_id'=>$restaurant_id,'restaurant_id'=>$restaurant_id,'open_time' => $this->input->post('otime'),'close_time' => $this->input->post('ctime'), );

       $this->Admin_modal->update_restaurant($restaurant_id,$add_restaurant);

      $this->Admin_modal->delete_restaurant_bar($restaurant_id);
      $this->Admin_modal->delete_restaurant_music($restaurant_id);
      $this->Admin_modal->delete_restaurant_music_genre($restaurant_id);
      $this->Admin_modal->delete_restaurant_amenities($restaurant_id);
      $this->Admin_modal->delete_restaurant_food($restaurant_id);
      $this->Admin_modal->delete_restaurant_crowd($restaurant_id);

     }


 
       for($i=0;$i<count($bar_type);$i++)
       {
      
        $this->Admin_modal->add_restaurant_bar($restaurant_id,$bar_type[$i]);

        }

        for($i=0;$i<count($music_type);$i++)
       {
      
        $this->Admin_modal->add_restaurant_music($restaurant_id,$music_type[$i]);

        }

        for($i=0;$i<count($music_genre);$i++)
       {
      
        $this->Admin_modal->add_restaurant_music_genre($restaurant_id,$music_genre[$i]);

        }

        for($i=0;$i<count($amenities);$i++)
       {
      
        $this->Admin_modal->add_restaurant_amenities($restaurant_id,$amenities[$i]);

        }

        for($i=0;$i<count($food_type);$i++)
       {
      
        $this->Admin_modal->add_restaurant_food($restaurant_id,$food_type[$i]);

        }
        for($i=0;$i<count($average_crowd);$i++)
       {
      
        $this->Admin_modal->add_restaurant_crowd($restaurant_id,$average_crowd[$i]);

        }

       for($i=0;$i<$file_count;$i++)
       {
         
        $file_name=$_FILES['files']['name'][$i];
        $file_tmp_name=$_FILES['files']['tmp_name'][$i];
        if(move_uploaded_file($file_tmp_name, 'assets/images/'.$file_name))
        {
          $this->Admin_modal->add_restaurant_image($restaurant_id,$file_name);
          
        }
        
       }
      
    

      $this->session->set_flashdata('msg','Restaurant successfully added'); 
        echo "<script>window.location.href='".base_url('restaurant')."';</script>";
           //redirect(base_url('restaurant'));

      }
      
       $data['bar']=$this->Admin_modal->select_bar();
      $data['state']=$this->Admin_modal->select_state();
       $data['music']=$this->Admin_modal->select_music();
        $data['music_genre']=$this->Admin_modal->select_music_genre();
        $data['amenities']=$this->Admin_modal->select_amenities();
         $data['food']=$this->Admin_modal->select_food();
          $data['crowd']=$this->Admin_modal->select_crowd();
          if(!empty($this->input->get('res')))
      {

           $data['restaurant_info']=$this->Admin_modal->restaurant_info($restaurant_id);
           $data['select_restaurant_bar']=$this->Admin_modal->select_restaurant_bar($restaurant_id);
           $data['select_restaurant_food']=$this->Admin_modal->select_restaurant_food($restaurant_id);
           $data['select_restaurant_music']=$this->Admin_modal->select_restaurant_music($restaurant_id);
           $data['select_restaurant_music_genre']=$this->Admin_modal->select_restaurant_music_genre($restaurant_id);
           $data['select_restaurant_amenities']=$this->Admin_modal->select_restaurant_amenities($restaurant_id);
            $data['select_restaurant_crowd']=$this->Admin_modal->select_restaurant_crowd($restaurant_id);
              $data['select_restaurant_images']=$this->Admin_modal->select_restaurant_images($restaurant_id);
            }
          
      $this->load->view('admin/add_restaurant',$data);
    }


    public function img_delete()
    {
      $img_id=$this->input->post('img_id');
      if($this->Admin_modal->img_delete($img_id))
      {
        echo 'success';
      }
    }

    public function restaurant_delete($value='')
    {
       $img_id=$this->input->post('restaurant_id');
      if($this->Admin_modal->restaurant_delete($img_id))
      {
        $this->Admin_modal->restaurant_coupon_delete($img_id);
        $this->Admin_modal->restaurant_event_delete($img_id);
         $this->Admin_modal->restaurant_rating_delete($img_id);
         $this->session->set_flashdata('add_state','Restaurant deleted successfully'); 
        echo 'success';

      }
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public function total_food($offset=0)
     {

      $food_id=$this->input->post('food_id');     
      $submit=$this->input->post('submit');
      if (empty($food_id)) {
       
      if (isset($submit)) {
        $date=date("Y-m-d");
        $data = array('food_type' =>$this->input->post('food_type') ,'status' =>$this->input->post('status'),'create_date'=>$date );
        if($this->Admin_modal->add_food($data))
        {
           $this->session->set_flashdata('msg','Food type successfully added'); 
           echo "<script>window.location.href='".base_url('total_food')."';</script>";
          // redirect(base_url('total_food'));
        }
      }
    }else{
     $data = array('food_type' =>$this->input->post('food_type') ,'status' =>$this->input->post('status'),'create_date'=>$date);
        if($this->Admin_modal->update_food($data,$food_id))
        {
           $this->session->set_flashdata('msg','Food type successfully updated'); 
            echo "<script>window.location.href='".base_url('total_food')."';</script>";

          // redirect(base_url('total_food'));
        }
    }

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->food_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."total_food";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_food($Searchkey,$config['per_page'],$this->uri->segment(2));
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;
      $this->load->view('admin/total_food',$data);
    }



     public function food_delete()
    {
      $food_id=$this->input->post('food_id');
      if($this->Admin_modal->food_delete($food_id))
      {
        echo "success";
        $this->session->set_flashdata('msg','Food deleted successfully'); 
      }
    }



        public function total_music_type($offset=0)
     {

      $music_id=$this->input->post('music_id');     
      $submit=$this->input->post('submit');
      if (empty($music_id)) {
       
      if (isset($submit)) {
        $date=date("Y-m-d");
        $data = array('music_type' =>$this->input->post('music_type') ,'status' =>$this->input->post('status'),'create_date'=>$date );
        if($this->Admin_modal->add_music($data))
        {
           $this->session->set_flashdata('msg','Music type successfully added'); 
            echo "<script>window.location.href='".base_url('total_music_type')."';</script>";
          // redirect(base_url('total_music_type'));
        }
      }
    }else{
     $data = array('music_type' =>$this->input->post('music_type') ,'status' =>$this->input->post('status'),'create_date'=>$date);
        if($this->Admin_modal->update_music($data,$music_id))
        {
           $this->session->set_flashdata('msg','Music type successfully updated'); 
             echo "<script>window.location.href='".base_url('total_music_type')."';</script>";
          // redirect(base_url('total_music_type'));

        }
    }

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->music_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."total_music_type";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_music($Searchkey,$config['per_page'],$this->uri->segment(2));
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;
      $this->load->view('admin/total_music_type',$data);
    }



     public function music_delete()
    {
      $music_id=$this->input->post('music_id');
      if($this->Admin_modal->music_delete($music_id))
      {
        echo 'success';
        $this->session->set_flashdata('msg','Music type deleted successfully'); 
      }
    }



        public function total_bar_type($offset=0)
     {

      $bar_id=$this->input->post('bar_id');     
      $submit=$this->input->post('submit');
      if (empty($bar_id)) {
       
      if (isset($submit)) {
        $date=date("Y-m-d");
        $data = array('bar_type' =>$this->input->post('bar_type') ,'status' =>$this->input->post('status'),'create_date'=>$date );
        if($this->Admin_modal->add_bar($data))
        {
           $this->session->set_flashdata('msg','Bar type successfully added');
             echo "<script>window.location.href='".base_url('total_bar_type')."';</script>"; 
          // redirect(base_url('total_bar_type'));
        }
      }
    }else{
     $data = array('bar_type' =>$this->input->post('bar_type') ,'status' =>$this->input->post('status'),'create_date'=>$date);
        if($this->Admin_modal->update_bar($data,$bar_id))
        {
           $this->session->set_flashdata('msg','Bar type successfully updated'); 
            echo "<script>window.location.href='".base_url('total_bar_type')."';</script>"; 
           //redirect(base_url('total_bar_type'));
        }
    }

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->bar_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."total_bar_type";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_bar($Searchkey,$config['per_page'],$this->uri->segment(2));
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;
      $this->load->view('admin/total_bar_type',$data);
    }


    
     public function bar_delete()
    {
      $bar_id=$this->input->post('bar_id');
      if($this->Admin_modal->bar_delete($bar_id))
      {
        echo 'success';
        $this->session->set_flashdata('msg','Bar type deleted successfully'); 
      }
    }


         public function total_amenities_type($offset=0)
     {

      $amenities_id=$this->input->post('amenities_id');     
      $submit=$this->input->post('submit');
      if (empty($amenities_id)) {
       
      if (isset($submit)) {
        $date=date("Y-m-d");
        $data = array('amenities_name' =>$this->input->post('amenities_name') ,'status' =>$this->input->post('status'),'create_date'=>$date );
        if($this->Admin_modal->add_amenities($data))
        {
           $this->session->set_flashdata('msg','Amenities type successfully added'); 
            echo "<script>window.location.href='".base_url('total_amenities_type')."';</script>"; 
         //  redirect(base_url('total_amenities_type'));
        }
      }
    }else{
     $data = array('amenities_name' =>$this->input->post('amenities_name') ,'status' =>$this->input->post('status'),'create_date'=>$date);
        if($this->Admin_modal->update_amenities($data,$amenities_id))
        {
           $this->session->set_flashdata('msg','Amenities type successfully updated'); 
            echo "<script>window.location.href='".base_url('total_amenities_type')."';</script>"; 

           //redirect(base_url('total_amenities_type'));
        }
    }

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->amenities_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."total_amenities_type";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_amenities($Searchkey,$config['per_page'],$this->uri->segment(2));
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;
      $this->load->view('admin/total_amenities_type',$data);
    }


    
     public function amenities_delete()
    {
      $amenities_id=$this->input->post('amenities_id');
      if($this->Admin_modal->amenities_delete($amenities_id))
      {
        echo 'success';
        $this->session->set_flashdata('msg','Amenities type deleted successfully'); 
      }
    }



    public function total_eatery_type($offset=0)
     {

      $music_genre_id=$this->input->post('music_genre_id');     
      $submit=$this->input->post('submit');
      if (empty($music_genre_id)){
       
      if (isset($submit)) {
        $date=date("Y-m-d");
        $data = array('music_genre_name' =>$this->input->post('music_genre_name') ,'status' =>$this->input->post('status'),'create_date'=>$date );
        if($this->Admin_modal->add_music_genre($data))
        {
           $this->session->set_flashdata('msg','Music Genre type successfully added'); 
            echo "<script>window.location.href='".base_url('total_eatery_type')."';</script>"; 
          // redirect(base_url('total_eatery_type'));
        }
      }
    }else{
     $data = array('music_genre_name' =>$this->input->post('music_genre_name') ,'status' =>$this->input->post('status'),'create_date'=>$date);
        if($this->Admin_modal->update_music_genre($data,$music_genre_id))
        {
           $this->session->set_flashdata('msg','Music Genre type successfully updated'); 
            echo "<script>window.location.href='".base_url('total_eatery_type')."';</script>";
          // redirect(base_url('total_eatery_type'));
        }
    }

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->music_genre_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."total_eatery_type";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_music_genre($Searchkey,$config['per_page'],$this->uri->segment(2));
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;
      $this->load->view('admin/total_music_genre_type',$data);
    }


    
     public function music_genre_delete()
    {
      $music_genre_id=$this->input->post('music_genre_id');
      if($this->Admin_modal->music_genre_delete($music_genre_id))
      {
        echo 'success';
        $this->session->set_flashdata('msg','Music Genre type deleted successfully'); 
      }
    }


    public function total_crowd_type($offset=0)
     {

      $crowd_id=$this->input->post('crowd_id');     
      $submit=$this->input->post('submit');
      if (empty($crowd_id)){
       
      if (isset($submit)) {
        $date=date("Y-m-d");
        $data = array('crowd_count' =>$this->input->post('crowd_count') ,'status' =>$this->input->post('status'),'create_date'=>$date );
        if($this->Admin_modal->add_crowd($data))
        {
           $this->session->set_flashdata('msg','Crowd type successfully added'); 
            echo "<script>window.location.href='".base_url('total_crowd_type')."';</script>";
          // redirect(base_url('total_crowd_type'));
        }
      }
    }else{
     $data = array('crowd_count' =>$this->input->post('crowd_count') ,'status' =>$this->input->post('status'),'create_date'=>$date);
        if($this->Admin_modal->update_crowd($data,$crowd_id))
        {
           $this->session->set_flashdata('msg','Crowd type successfully updated'); 
            echo "<script>window.location.href='".base_url('total_crowd_type')."';</script>";
           //redirect(base_url('total_crowd_type'));
        }
    }

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->crowd_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."total_crowd_type";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_crowd($Searchkey,$config['per_page'],$this->uri->segment(2));
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;
      $this->load->view('admin/total_crowd_type',$data);
    }


    
     public function crowd_delete()
    {
      $crowd_id=$this->input->post('crowd_id');
      if($this->Admin_modal->crowd_delete($crowd_id))
      {
        echo 'success';
        $this->session->set_flashdata('msg','Crowd type deleted successfully'); 
      }
    }


        public function total_coupon($offset=0)
     {

      $coupon_id=$this->input->post('coupon_id');     
      $submit=$this->input->post('submit');
      if (empty($coupon_id)){
       
      if (isset($submit)) {
        $date=date("Y-m-d");
         $tmp_name=$_FILES['coupon_img']['tmp_name'];
              $file_name=$_FILES['coupon_img']['name'];  
        $data = array('coupon_title' =>$this->input->post('coupon_title') ,'coupon_description' =>$this->input->post('coupon_description') ,'coupon_terms' =>$this->input->post('coupon_terms') ,'status' =>$this->input->post('status'),'create_date'=>$date,'coupon_terms' =>$this->input->post('coupon_terms') ,'start_date' =>$this->input->post('coupon_start_date') ,'end_date' =>$this->input->post('coupon_end_date') ,'start_time' =>$this->input->post('coupon_start_time') ,'end_time' =>$this->input->post('coupon_end_time') ,'coupon_img' =>$file_name , );
         move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
        if($this->Admin_modal->add_coupon($data))
        {
           $this->session->set_flashdata('msg','Coupon successfully added'); 
            echo "<script>window.location.href='".base_url('total_coupon')."';</script>";
          // redirect(base_url('total_coupon'));
        }
      }
    }else{
          $tmp_name=$_FILES['coupon_img']['tmp_name'];
              $file_name=$_FILES['coupon_img']['name'];    
              if (empty($file_name)) {               
     $data = array('coupon_title' =>$this->input->post('coupon_title') ,'coupon_description' =>$this->input->post('coupon_description') ,'status' =>$this->input->post('status'),'coupon_terms' =>$this->input->post('coupon_terms') ,'create_date'=>$date ,'start_date' =>$this->input->post('coupon_start_date') ,'end_date' =>$this->input->post('coupon_end_date') ,'start_time' =>$this->input->post('coupon_start_time') ,'end_time' =>$this->input->post('coupon_end_time') ,);
              }else{
                 $data = array('coupon_title' =>$this->input->post('coupon_title') ,'coupon_description' =>$this->input->post('coupon_description') ,'status' =>$this->input->post('status'),'coupon_terms' =>$this->input->post('coupon_terms') ,'create_date'=>$date,'start_date' =>$this->input->post('coupon_start_date') ,'end_date' =>$this->input->post('coupon_end_date') ,'start_time' =>$this->input->post('coupon_start_time') ,'end_time' =>$this->input->post('coupon_end_time') ,'coupon_img' =>$file_name ,);
                  move_uploaded_file($tmp_name, 'assets/images/'.$file_name);
              }
        if($this->Admin_modal->update_coupon($data,$coupon_id))
        {
           $this->session->set_flashdata('msg','Coupon successfully updated'); 
            echo "<script>window.location.href='".base_url('total_coupon')."';</script>";
          // redirect(base_url('total_coupon'));
        }
    }

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->coupon_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."total_coupon";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_coupon($Searchkey,$config['per_page'],$this->uri->segment(2));
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;
      $this->load->view('admin/total_coupon',$data);
    }


    
     public function coupon_delete()
    {
      $coupon_id=$this->input->post('coupon_id');
      if($this->Admin_modal->coupon_delete($coupon_id))
      {
        echo 'success';
        $this->session->set_flashdata('msg','Coupon deleted successfully'); 
      }
    }

  public function member_ship_plan()
    {
      $membership_id=$this->input->post('membership_id');
       
      if(!empty($membership_id))
      {
        $data = array('title' =>$this->input->post('title') ,'amount' =>$this->input->post('amount') ,'description' =>$this->input->post('description') ,'status' =>$this->input->post('status') , );
       if($this->Admin_modal->update_member_ship_plan($data,$membership_id))
       {
          $this->session->set_flashdata('msg','Membership successfully updated');
          echo "<script>window.location.href='".base_url('member_ship_plan')."';</script>";
          //redirect(base_url('member_ship_plan')); 
       }
      }
      $data['result']=$this->Admin_modal->member_ship_plan();
      $this->load->view('admin/member_ship_plan',$data);
    }
  
  public function setting()
  {
    $banner_submit=$this->input->post('banner_submit');
    $t_c_submit=$this->input->post('t_c_submit');
    $about_submit=$this->input->post('about_submit');
    if (isset($banner_submit)) {
    $banner_id=$this->input->post('banner_id');
     $file_name=$_FILES['banner']['name'];
        $file_tmp_name=$_FILES['banner']['tmp_name'];
        if(move_uploaded_file($file_tmp_name, 'assets/images/'.$file_name))
        {
          $this->Admin_modal->update_banner($banner_id,$file_name);
           $this->session->set_flashdata('banner_update','Banner successfully updated');
          
        }
    }
    $data['result']=$this->Admin_modal->setting();   

     $this->load->view('admin/setting',$data);
  }
   
   public function t_c_admin()
   {
         $t_c_id=$this->input->post('t_c_id');
          $data2 = array(
           't_c' => $this->input->post('data')
          );
         
          if( $this->Admin_modal->update_t_c($t_c_id,$data2))
          {
           $this->session->set_flashdata('t_c_update','Terms And Condition successfully updated');
          }       
        
    
   }

   public function about_admin()
   {
         $about_id=$this->input->post('about_id');
          $data2 = array(
           'about' => $this->input->post('data')
          );
         
          if( $this->Admin_modal->update_about($about_id,$data2))
          {
           $this->session->set_flashdata('about_update','Terms And Condition successfully updated');
          }       
        
    
   }

   public function p_p_admin()
   {
         $about_id=$this->input->post('about_id');
          $data2 = array(
           'p_p' => $this->input->post('data')
          );
         print_r($data2);
          if( $this->Admin_modal->update_p_p($about_id,$data2))
          {
           $this->session->set_flashdata('about_update','PRIVACY And POLICY successfully updated');
          }       
        
    
   }

   public function feedback($value='')
   {
         $config['total_rows'] = $this->Admin_modal->feedback_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."feedback";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_feedback($Searchkey,$config['per_page'],$this->uri->segment(2));
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;
   //print_r($data['result']);die;

    $this->load->view('admin/feedback',$data);
   }

   public function feedback_delete()
   {
     $id=$this->input->post('amenities_id');
     if($this->Admin_modal->feedback_delete($id))
     {
      echo "success";
     }
   }



    public function country_settings($offset=0)
    {
     $state_id=$this->input->post('id');     
      $submit=$this->input->post('submit');
      if (empty($state_id)) {
       
      if (isset($submit)) {
        $data = array('country_name' =>$this->input->post('country_name') ,'status' =>$this->input->post('status') , );
        if($this->Admin_modal->add_country($data))
        {
           $this->session->set_flashdata('add_state','Country successfully added'); 
           echo "<script>window.location.href='".base_url('country_settings')."';</script>";
         //  redirect(base_url('country_settings'));
        }
      }
    }else{
     $data = array('country_name' =>$this->input->post('country_name') ,'status' =>$this->input->post('status') , );
        if($this->Admin_modal->update_country($data,$state_id))
        {
           $this->session->set_flashdata('add_state','Country successfully updated'); 
            echo "<script>window.location.href='".base_url('country_settings')."';</script>";
          // redirect(base_url('country_settings'));
        }
    }

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->country_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."country_settings";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_country($Searchkey,$config['per_page'],$this->uri->segment(2));
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;
   
      $this->load->view('admin/country_settings',$data);
    }



        public function city_settings($offset=0)
    {
     $city_id=$this->input->post('id');     
      $submit=$this->input->post('submit');
      if (empty($city_id)) {
       
      if (isset($submit)) {
        $data = array('state_name' =>$this->input->post('state_name') ,'city_name' =>$this->input->post('city_name') ,'status' =>$this->input->post('status') , );
        if($this->Admin_modal->add_city($data))
        {
           $this->session->set_flashdata('add_city','City successfully added'); 
            echo "<script>window.location.href='".base_url('city_settings')."';</script>";
         //  redirect(base_url('city_settings'));
        }
      }
    }else{
     $data = array('state_name' =>$this->input->post('state_name') ,'city_name' =>$this->input->post('city_name') ,'status' =>$this->input->post('status') , );
        if($this->Admin_modal->update_city($data,$city_id))
        {
           $this->session->set_flashdata('add_city','City successfully updated'); 
            echo "<script>window.location.href='".base_url('city_settings')."';</script>";
          // redirect(base_url('city_settings'));
        }
    }

      $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->city_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."city_settings";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->select_city($Searchkey,$config['per_page'],$this->uri->segment(2));
//echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;

  // print_r($data['result']);die;
      $data['country_list'] = $this->Admin_modal->select_coupon_list();
   
      $this->load->view('admin/city_settings',$data);
    }

 

     public function country_delete()
    {
      $country_id=$this->input->post('country_id');
      if($this->Admin_modal->country_delete($country_id))
      {
        echo 'success';
        $this->session->set_flashdata('add_country','Country deleted successfully'); 
      }
    }


    public function test(){
      $this->load->view('admin/test');
    }

     public function Promote($value='')
    {     

   $arrayName1 = array('read_status' => '0');
   $this->Admin_modal->update_read_count($arrayName1);

  $Searchkey=$this->input->get('status');
     
  $config['total_rows'] = $this->Admin_modal->promote_requerst_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."Promote";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->promote_requerst_record($Searchkey,$config['per_page'],$this->uri->segment(2));
// print_r($query);
//  echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;


      $this->load->view('admin/promote',$data);
    }

    public function Promote_Type($value='')
    {

      $submit=$this->input->post('submit');
      if (isset($submit)) {
        $arrayName = array('promote_name' =>$this->input->post('promote_name') , 'term_condition' =>$this->input->post('term_condition'), 'status' =>$this->input->post('status'));
        $Promote_id=$this->input->post('Promote_id');
        if (!empty($Promote_id)) {
         if($this->Admin_modal->update_promote_type($arrayName,$Promote_id))
        {
          $this->session->set_flashdata('add_Promote','Promote Type Update Successfully'); 
        }
        }else{
        if($this->Admin_modal->set_promote_type($arrayName))
        {
          $this->session->set_flashdata('add_Promote','Promote Type Added Successfully'); 
        }
        }        
      }


       $Searchkey=$this->input->get('status');
     
    $config['total_rows'] = $this->Admin_modal->promote_type_record_count($Searchkey);
    //echo $this->db->last_query(); die;
          
  $config['base_url'] = base_url()."Promote_Type";
  $config['per_page'] = 10;
  $config['uri_segment'] = '2';

 $config['full_tag_open'] = '<ul class="pagination">';

    $config['full_tag_close'] = '</ul>';

    $config['first_link'] = false;

    $config['last_link'] = false;

    $config['first_tag_open'] = '<li>';

    $config['first_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';

    $config['prev_tag_open'] = '<li class="prev">';

    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';

    $config['next_tag_open'] = '<li>';

    $config['next_tag_close'] = '</li>';

    $config['last_tag_open'] = '<li>';

    $config['last_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active"><a href="#">';

    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li>';

    $config['num_tag_close'] = '</li>';


  $this->pagination->initialize($config);
   

  $query = $this->Admin_modal->promote_type_record($Searchkey,$config['per_page'],$this->uri->segment(2));
// echo $this->db->last_query(); die;
 $data['star'] =$this->uri->segment(2) <= 1 ? 0 : $this->uri->segment(2);;
  $data['result'] = null;
  
 
   $data['result'] =  $query;

  // print_r($data['result']);die;

      $this->load->view('admin/Promote_Type',$data);
    }

    public function promote_delete()
    {
      $id=$this->input->post('id');
      if($this->Admin_modal->promote_delete($id))
      {
        echo 'success';        
        $this->session->set_flashdata('add_Promote','Promote Type deleted Successfully'); 
      }
    }

    public function change_promote_status()
    {
      $Promote_id=$this->input->post('Promote_id');
      $status=$this->input->post('status');
      $arrayName = array('status' => $status, );
      if($this->Admin_modal->change_promote_status($Promote_id,$arrayName))
      {
        echo "true";
      }else{
        echo "false";
      }
    }




}